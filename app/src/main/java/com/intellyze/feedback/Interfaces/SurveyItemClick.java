package com.intellyze.feedback.Interfaces;

/**
 * Created by INTELLYZE-202 on 01-12-2017.
 */

public interface SurveyItemClick {
    void onNavigateSurveyResult(String surveyId, int completeId);

}
