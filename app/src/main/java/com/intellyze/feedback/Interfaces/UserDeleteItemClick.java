package com.intellyze.feedback.Interfaces;

/**
 * Created by INTELLYZE-202 on 07-12-2017.
 */

public interface UserDeleteItemClick {
    void onUserDeleteClicked(int position);
}
