package com.intellyze.feedback.Interfaces;

/**
 * Created by intellyelabs on 08/03/18.
 */

public interface SmsListener {
    public void messageReceived(String messageText);
}