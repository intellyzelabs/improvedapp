package com.intellyze.feedback.Interfaces;

/**
 * Created by INTELLYZE-202 on 19-12-2017.
 */

public interface SurveyQuestionUserItemClick {
    void onNavigateSurveyUserResult(int id);
}
