package com.intellyze.feedback.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


import com.intellyze.feedback.Api.Login.HttpLogin;
import com.intellyze.feedback.Api.Login.LoginRequestModel;
import com.intellyze.feedback.Api.Login.OnHttpRequestForLogin;
import com.intellyze.feedback.Api.Login.Result;
import com.intellyze.feedback.R;
import com.intellyze.feedback.Utils.UserPref;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import java.util.List;

/**
 * Created by INTELLYZE-202 on 29-11-2017.
 */

public class LoginActivity extends BaseActivity implements OnHttpRequestForLogin {
    @BindView(R.id.edtEmail)
    EditText edtEmail;
    @BindView(R.id.edtPassword)
    EditText edtPassword;
    ProgressDialog progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        Intent intent=getIntent();

        if(intent.hasExtra("emailid")) {
            String emailid = intent.getExtras().get("emailid").toString();
            edtEmail.setText(emailid);
        }
        progressBar= new ProgressDialog(this);
        progressBar.setCancelable(true);//you can cancel it by pressing back button
        progressBar.setMessage(" Please wait..");
        progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);


    }

    @OnClick(R.id.tvForgotPassword)
    public void forgotpassword(TextView tvForgotPassword) {

        Intent intent = new Intent(LoginActivity.this, FrogetPassword.class);
        startActivity(intent);
//        Toast.makeText(this, "Forgot password", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.tvSignUp)
    public void signup(TextView button) {
        Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.tvLogIn)
    void login(Button button) {

//Login();

if(!edtEmail.getText().toString().trim().equals(""))
{
    if (isValidEmail(edtEmail.getText().toString().trim()) || (edtEmail.getText().toString().trim().length() >= 10 && isNumeric(edtEmail.getText().toString().trim())) ) {

       if(edtPassword.getText().toString().trim().length()>=6)
       {
           LoginRequestModel loginRequestModel = new LoginRequestModel();
           loginRequestModel.setUserName(edtEmail.getText().toString().trim());
           loginRequestModel.setPassword(edtPassword.getText().toString().trim());

           progressBar.show();
           HttpLogin httpLogin = new HttpLogin(this);
           httpLogin.Loginn(loginRequestModel);

       }
       else{
           edtPassword.setError(getResources().getString(R.string.invalide_Entry_passworkd));
       }


    } else {
        edtEmail.setError(getResources().getString(R.string.invalide_Entry));
    }

}else
{
    edtEmail.setError(getResources().getString(R.string.invalide_Entry));

}


    }

    private void Login() {


        if (!edtPassword.getText().toString().trim().equals("") && !edtEmail.getText().toString().trim().equals("")) {
            if (isValidEmail(edtEmail.getText().toString().trim()) || (edtEmail.getText().toString().trim().length() >= 10 && isNumeric(edtEmail.getText().toString().trim())) || edtPassword.getText().toString().trim().length() >= 10) {
                LoginRequestModel loginRequestModel = new LoginRequestModel();
                loginRequestModel.setUserName(edtEmail.getText().toString().trim());
                loginRequestModel.setPassword(edtPassword.getText().toString().trim());
                progressBar.show();
                HttpLogin httpLogin = new HttpLogin(this);
                httpLogin.Loginn(loginRequestModel);
            } else {
                edtEmail.setError(getResources().getString(R.string.invalide_Entry));
            }
        } else {
            if (edtPassword.getText().toString().trim().equals("")) {
                edtPassword.setError(getResources().getString(R.string.cant_be_blank));
            }
            if (edtEmail.getText().toString().trim().equals("")) {
                edtEmail.setError(getResources().getString(R.string.cant_be_blank));
            }
        }
    }

    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @Override
    public void onHttpLoginSuccess(String Message, List<Result> result) {
        progressBar.cancel();


        UserPref userPref = new UserPref(getApplicationContext());
        userPref.saveUserName(edtEmail.getText().toString().trim());
        userPref.saveNameOfUser(result.get(0).getBusinessName());
        userPref.saveGst(result.get(0).getBusinessGST());
        userPref.saveUserImage(result.get(0).getProfilepic());
        userPref.setBusinessId(result.get(0).getBusinessId());
        userPref.saveRole(result.get(0).getURole());
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onHttpLoginFailed(String Message) {
        progressBar.cancel();
        SnackbarManager.show(
                Snackbar.with(this)
                        .text(Message));

    }
}
