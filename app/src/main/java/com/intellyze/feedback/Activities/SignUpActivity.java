package com.intellyze.feedback.Activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.intellyze.feedback.Api.SignUp.OnHttpRespoceForSignUp;
import com.intellyze.feedback.Api.SignUp.SignUpRequestModel;
import com.intellyze.feedback.Api.SignUp.HttpSignUp;
import com.intellyze.feedback.R;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import java.io.FileNotFoundException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.intellyze.feedback.Activities.LoginActivity.isValidEmail;

/**
 * Created by INTELLYZE-202 on 29-11-2017.
 */

public class SignUpActivity extends BaseActivity implements OnHttpRespoceForSignUp {
    @BindView(R.id.edtBusinessName)
    EditText edtBusinessName;
    @BindView(R.id.edtAddress)
    EditText edtAddress;
    @BindView(R.id.edtMobileNo)
    EditText edtMobileNo;
    @BindView(R.id.edtLandline)
    EditText edtLandline;
    @BindView(R.id.edtCCN)
    EditText edtCCN;
    @BindView(R.id.edtEmail)
    EditText edtEmail;
    @BindView(R.id.edtWebsite)
    EditText edtWebsite;
    @BindView(R.id.edtCategory)
    EditText edtCategory;
    @BindView(R.id.edtGST)
    EditText edtGST;
    @BindView(R.id.edtPassword)
    EditText edtPassword;
    @BindView(R.id.edtRePassword)
    EditText edtRePassword;

    @OnClick(R.id.back)
    public void signup(ImageView button) {
        Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
//        intent.putExtra("emailid",edtEmail.getText().toString());
        startActivity(intent);
    }

    CircleImageView imgProfile;
    private static final int REQUEST_WRITE_PERMISSION = 786;
    int select_photo = 1;
    Uri selectedImage = Uri.parse("");
    String encodedImage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        setProgressBar();

        ButterKnife.bind(this);
        imgProfile = (CircleImageView) findViewById(R.id.imgProfile);
        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermission();
            }
        });
//        setTestData();
    }

    private void setTestData() {


       edtBusinessName.setText("New Shop");
       edtAddress.setText("address of the shop");
       edtMobileNo.setText("9988776655");
       edtLandline.setText("9988776655");
       edtCCN.setText("9988776655");
       edtEmail.setText("e@gmail.com");
       edtWebsite.setText("www.website.com");
       edtCategory.setText("Category");
       edtGST.setText("GST123");
       edtPassword.setText("1234567");
       edtRePassword.setText("1234567");
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        } else {
            openFilePicker();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openFilePicker();
        }
    }

    private void openFilePicker() {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, select_photo);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == select_photo && resultCode == RESULT_OK && null != data) {
            selectedImage = data.getData();
            final InputStream imageStream;
            try {
                imageStream = getContentResolver().openInputStream(selectedImage);
                Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                encodedImage = encodeImage(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            imgProfile.setImageURI(selectedImage);
        }
    }

    @OnClick(R.id.imgNext)
    public void next(ImageView imageView) {



        if(validate()) {

            SignUpRequestModel signUpRequestModel = new SignUpRequestModel();
            signUpRequestModel.setBusinessName(edtBusinessName.getText().toString().trim());
            signUpRequestModel.setBusinessAddress(edtAddress.getText().toString().trim());
            signUpRequestModel.setBusinessMobileNo(edtMobileNo.getText().toString().trim());
            signUpRequestModel.setBusinessLandLine(edtLandline.getText().toString().trim());
            signUpRequestModel.setBusinessCustomerCareNo(edtCCN.getText().toString().trim());
            signUpRequestModel.setBusinessWebsite(edtWebsite.getText().toString().trim());
            signUpRequestModel.setBusinessCategory(edtCategory.getText().toString().trim());
            signUpRequestModel.setBusinessEmail(edtEmail.getText().toString().trim());
            signUpRequestModel.setUPassword(edtPassword.getText().toString().trim());
            signUpRequestModel.setBusiness_GST(edtGST.getText().toString().trim());
            signUpRequestModel.setBusinessLogoURL(encodedImage);
            showProgress("Sign up");
            HttpSignUp httpSignUp = new HttpSignUp(this);
            httpSignUp.signUpp(signUpRequestModel);
        }

    }

    private boolean validate() {
        boolean fake=false;

        if(edtBusinessName.getText().toString().equals(""))
        {
            edtBusinessName.setError("Enter Business Name");
            edtBusinessName.setFocusable(true);
            fake=false;
            return false;
        }

        if(edtAddress.getText().toString().equals(""))
        {
            edtAddress.setError("Enter address ");
            edtAddress.setFocusable(true);
            fake=false;
            return false;
        }












 if(edtMobileNo.getText().toString().equals(""))
        {
            edtMobileNo.setError("Enter mobile number ");
            edtMobileNo.setFocusable(true);
            fake=false;
            return false;
        }else if (edtMobileNo.getText().toString().length()>10)
 {
     edtMobileNo.setError("Enter a 10 digit Phone number");
     edtMobileNo.setFocusable(true);
     fake=false;
     return false;

 }

if(edtMobileNo.getText().toString().equals(""))
        {
            edtMobileNo.setError("Enter mobile number ");
            edtMobileNo.setFocusable(true);
            fake=false;
            return false;
        }









        if(!isValidEmail(edtEmail.getText().toString().trim()))
{
    edtEmail.setError("Enter valid email");
    edtEmail.setFocusable(true);
    fake=false;
    return false;

}

        if(edtWebsite.getText().toString().equals(""))
        {
            edtWebsite.setError("Enter Website ");
            edtWebsite.setFocusable(true);
            fake=false;
            return false;
        }
        if(edtCategory.getText().toString().equals(""))
        {
            edtCategory.setError("Enter Category ");
            edtCategory.setFocusable(true);
            fake=false;
            return false;
        }


        else if(edtPassword.getText().toString().trim().length()>=6)
        {
//         fake=true;
            if(edtPassword.getText().toString().equals(edtRePassword.getText().toString()))
            {
                fake=true;
            }
            else
            {
                edtRePassword.setError("Password is not matching");
                edtRePassword.setFocusable(true);
                fake=false;
                return false;
            }
        }
        else
{
    edtPassword.setError("Password is too short,Min 6 letter");
    edtPassword.setFocusable(true);
    fake=false;
    return false;
}




        return fake;

    }

    @Override
    public void onHttpSignUpSuccess(String Message) {
        cancelProgress();
        SnackbarManager.show(
                Snackbar.with(this)
                        .text(Message));
        Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
        intent.putExtra("emailid",edtEmail.getText().toString());
        startActivity(intent);
        finish();
//        Toast.makeText(getApplicationContext(), ""+Message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onHttpSignUpFailed(String Message) {
        cancelProgress();
        SnackbarManager.show(
                Snackbar.with(this)
                        .text(Message));


//        Toast.makeText(getApplicationContext(), ""+Message, Toast.LENGTH_SHORT).show();
    }

    public void onBackClick(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
