package com.intellyze.feedback.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.intellyze.feedback.Api.AddAnswerMultipleSurveys.AddMultplieAnswerSurveyRequestModel;
import com.intellyze.feedback.Api.AddAnswerMultipleSurveys.Answer;
import com.intellyze.feedback.Api.AddAnswerMultipleSurveys.AnswersList;
import com.intellyze.feedback.Api.AddAnswerMultipleSurveys.HttpRequestForAddMultipleAnswerSurvey;
import com.intellyze.feedback.Api.AddAnswerMultipleSurveys.OnHttpRequestForAddMultipleAnswerSurvey;
import com.intellyze.feedback.Api.AddMultpileSurveys.AddMultipleSurveyRequestModel;
import com.intellyze.feedback.Api.AddMultpileSurveys.DeleteSurveryParameters;
import com.intellyze.feedback.Api.AddMultpileSurveys.HttpRequestForAddMultipleSurvey;
import com.intellyze.feedback.Api.AddMultpileSurveys.HttpRequestForSurveryDelete;
import com.intellyze.feedback.Api.AddMultpileSurveys.OnHttpRequestAddMultipleSurvey;
import com.intellyze.feedback.Api.AddMultpileSurveys.OnHttpSurveryDeleteResponce;
import com.intellyze.feedback.Api.AddMultpileSurveys.Survey;
import com.intellyze.feedback.Api.FeedbackSurveyQuestions.FeedbackQuestionRequestModel;
import com.intellyze.feedback.Api.FeedbackSurveyQuestions.HttpRequestSurveyQuestion;
import com.intellyze.feedback.Api.FeedbackSurveyQuestions.OnHttpRequestSurveyQuestion;
import com.intellyze.feedback.Api.FeedbackSurveyQuestions.SurveyDatum;
import com.intellyze.feedback.Bus.BusFactory;
import com.intellyze.feedback.Bus.Events.NavigateBack;
import com.intellyze.feedback.Bus.Events.NavigateBackEvent;
import com.intellyze.feedback.Bus.Events.NavigateEditStartQuestionFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateStartQuestionFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateStartSurveyUserFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateSurveyFirstFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateSurveyResultFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateSurveyUserDetailsFillFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateSurveyUserDetailsPageFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateToContinueFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateToDeleteSurveyFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateToEditQuestionEvent;
import com.intellyze.feedback.Bus.Events.NavigateToEditSurveyFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateToEditSurveyFragmentEventWithId;
import com.intellyze.feedback.Bus.Events.NavigateToEditSurveyFragmentSecondEvent;
import com.intellyze.feedback.Bus.Events.NavigateToMainScreenEvent;
import com.intellyze.feedback.Bus.Events.NavigateToSurveyEditQuestionEvent;
import com.intellyze.feedback.Bus.Events.NavigateToSurveyLastPagFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateToSurveySurveyFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateViewResponseFragmentEvent;
import com.intellyze.feedback.Bus.Events.RefreshEvent;
import com.intellyze.feedback.Bus.Events.RefreshEventContinueFragment;
import com.intellyze.feedback.Bus.Events.RefreshListFeedback;
import com.intellyze.feedback.Bus.Events.RefreshQuestionEvent;
import com.intellyze.feedback.Bus.Events.SyncDataWithServer;
import com.intellyze.feedback.Bus.Events.SyncSurveyAfterSubmit;
import com.intellyze.feedback.Custom.TextViewMedium;
import com.intellyze.feedback.Custom.TextViewSemiBold;
import com.intellyze.feedback.Fragments.AboutFragment;
import com.intellyze.feedback.Fragments.AddUserAccount;
import com.intellyze.feedback.Fragments.ContinueSurveyFragment;
import com.intellyze.feedback.Fragments.CreateEditSurveyFirstFragment;
import com.intellyze.feedback.Fragments.CreateSurveyFirstFragment;
import com.intellyze.feedback.Fragments.EditQuestionFragment;
import com.intellyze.feedback.Fragments.EditStartQuestionsFragment;
import com.intellyze.feedback.Fragments.EditSurveyFragement;
import com.intellyze.feedback.Fragments.EditSurveyQuestionFragment;
import com.intellyze.feedback.Fragments.HomeFragment;
import com.intellyze.feedback.Fragments.StartQuestionsFragment;
import com.intellyze.feedback.Fragments.StartSurveyUserFragment;
import com.intellyze.feedback.Fragments.SurveyLastPagFragment;
import com.intellyze.feedback.Fragments.SurveyResultFragment;
import com.intellyze.feedback.Fragments.SurveyUserDetailsFillPageFragment;
import com.intellyze.feedback.Fragments.UserSurveyQuestionsFragment;
import com.intellyze.feedback.Fragments.ViewResponseFragment;
import com.intellyze.feedback.Models.SurveyDetailsModel;
import com.intellyze.feedback.R;
import com.intellyze.feedback.Realm.AddAnswerMaster;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.Column;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.FeedQuestUserSide;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.Linear;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.MultipleChoice;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.MultipleChoiceGrid;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.QuestionMore;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.Row;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.SurveyQuestion;
import com.intellyze.feedback.Realm.QuestionMoreDetails;
import com.intellyze.feedback.Realm.SurveyDetails;
import com.intellyze.feedback.Realm.SurveyQuestionsDetails;
import com.intellyze.feedback.Realm.columnDetails;
import com.intellyze.feedback.Realm.linearquestionDetalis;
import com.intellyze.feedback.Realm.matrixquestionDetails;
import com.intellyze.feedback.Realm.multiplechoicesingleDetails;
import com.intellyze.feedback.Realm.rowDetails;
import com.intellyze.feedback.Utils.UserPref;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, AddUserAccount.OnFragmentInteractionListenerAdduser, ViewResponseFragment.OnListFragmentInteractionListener, OnHttpSurveryDeleteResponce, OnHttpRequestForAddMultipleAnswerSurvey, OnHttpRequestAddMultipleSurvey, OnHttpRequestSurveyQuestion {

    UserPref userPref;
    NavigationView navigationView;
    TextViewMedium tv_BusinessName, tv_BusinessGst;
    TextViewSemiBold tv_BusinessAddress;
    private Realm mRealm;
    private CircleImageView imgProfile;
    private SurveyDetails surveyDetailsdelete;
    //sync answer
    private AddMultplieAnswerSurveyRequestModel addMultplieAnswerSurveyRequestModel;
    private List<Answer> answerListObj;
    private Answer answerModel;
    private RealmResults<AddAnswerMaster> addAnswerMaster;
    //Sync survey
    private AddMultipleSurveyRequestModel addMultipleSurveyRequestModel;
    private RealmResults<SurveyDetails> dataOfflineToOnline;
    private Survey mSurvey;
    private List<Survey> mSurveysList;
    private RealmResults<SurveyQuestionsDetails> surveyQuestionsDetails;
    //Save data
    private List<SurveyDetailsModel> surveyDetailsModels;
    private RealmList<MultipleChoiceGrid> multipleChoiceGrids;
    private SurveyDetails surveySingleDetails;
    private RealmList<Column> columns;
    private MultipleChoiceGrid multipleChoiceGrid;
    private FeedQuestUserSide sdf;
    private RealmList<MultipleChoice> multipleChoices;
    private RealmList<Linear> linears;
    private RealmList<SurveyQuestion> surveyQuestions;
    private RealmList<Row> rows;
    private RealmResults<SurveyDetails> suralldele;
    private int mState=0;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setProgressBar();
        mRealm = Realm.getDefaultInstance();
        userPref = new UserPref(getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (savedInstanceState == null) {
            addFragment(HomeFragment.newInstance(), false);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerLayout = navigationView.getHeaderView(0);

        tv_BusinessName = headerLayout.findViewById(R.id.tv_BusinessName);
        imgProfile = headerLayout.findViewById(R.id.imgProfile);
        tv_BusinessAddress = headerLayout.findViewById(R.id.tv_BusinessAddress);
        tv_BusinessGst = headerLayout.findViewById(R.id.tv_BusinessGst);
        tv_BusinessName.setText(userPref.getNameOfUser());


        String role = "";

        if (userPref.getRole().equals("S_Admin")) {
            role = "Admin";
        } else {
            role = "User";
        }
        tv_BusinessAddress.setText(role);
        tv_BusinessGst.setText(userPref.getGst());

        hideItem();

    }


    @Subscribe
    public void NavigateSurveyUserDetailsFillFragment(NavigateSurveyUserDetailsFillFragmentEvent event) {
        addFragment(SurveyUserDetailsFillPageFragment.newInstance(event.getId()), true);
    }

    @Subscribe
    public void NavigateSurveyUserDetailsPageFragmentEvent(NavigateSurveyUserDetailsPageFragmentEvent event) {
        addFragment(UserSurveyQuestionsFragment.newInstance(event.getId()), true);
    }

    @Subscribe
    public void NavigateStartSurveyUserFragment(NavigateStartSurveyUserFragmentEvent event) {
        addFragment(StartSurveyUserFragment.newInstance(event.getId()), true);
    }

    @Subscribe
    public void NavigateToMainScreenEvent(NavigateToMainScreenEvent event) {
        Fragment fr = getSupportFragmentManager().findFragmentById(R.id.container);
        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
            if (!fr.getClass().getSimpleName().equals("HomeFragment")) {
                getSupportFragmentManager().popBackStack();
            }
        }
        BusFactory.getBus().post(new RefreshListFeedback());
    }

    @Subscribe
    public void NavigateToSurveyLastPagFragmentEvent(NavigateToSurveyLastPagFragmentEvent event) {

        SyncWholeData();

        addFragment(SurveyLastPagFragment.newInstance(event.getmId()), true);
    }

    @Subscribe
    public void NavigateViewResponseFragment(NavigateViewResponseFragmentEvent event) {
        addFragment(ViewResponseFragment.newInstance(event.getmId(), event.getTotResponse(), event.getStatusSurvey(), event.getLastEntry(), event.getOpernorclose(),event.getSurveyname()), true);
    }

    @Subscribe
    public void NavigateSurveyResultFragment(NavigateSurveyResultFragmentEvent event) {
        if (event.getCompleteId() == 1) {
            userPref.saveSurveyId(event.getSurvey());
            addFragment(SurveyResultFragment.newInstance(event.getSurvey()), true);
        } else {

            userPref.saveSurveyId(event.getSurvey());
            addFragment(ContinueSurveyFragment.newInstance(), true);
        }

    }

    @Subscribe
    public void NavigateToContinueFragmentEvent(NavigateToContinueFragmentEvent event) {
        getSupportFragmentManager().popBackStack();
        BusFactory.getBus().post(new RefreshEvent());

        addFragment(ContinueSurveyFragment.newInstance(), true);
//
//
//        BusFactory.getBus().post(new NavigateToContinueFragmentEvent());

    }


    @Subscribe
    public void NavigateToEditSurveyFragmentEvent(NavigateToEditSurveyFragmentEvent event) {
//            getSupportFragmentManager().popBackStack();
        BusFactory.getBus().post(new RefreshEvent());

        userPref.saveSurveyId(event.getSurvey());
//            addFragment(EditSurveyFragement.newInstance(), true);
        addFragment(CreateEditSurveyFirstFragment.newInstance(), true);


    }

    @Subscribe
    public void NavigateToSurveySurveyFragmentEvent(NavigateToSurveySurveyFragmentEvent event) {
//            getSupportFragmentManager().popBackStack();

//        userPref.saveSurveyId(event.getSurvey());
//            addFragment(EditSurveyFragement.newInstance(), true);
       Intent intent=new Intent(this,ReportActivity.class);
       intent.putExtra("MID",event.getSurvey());
       startActivity(intent);


    }


    @Subscribe
    public void NavigateToEditSurveyFragments(NavigateToEditSurveyFragmentEventWithId event) {
        getSupportFragmentManager().popBackStack();
        BusFactory.getBus().post(new RefreshEvent());

        userPref.saveSurveyId(event.getSurvey());
//            addFragment(EditSurveyFragement.newInstance(), true);
        addFragment(EditSurveyFragement.newInstance(), true);


    }

    @Subscribe
    public void NavigateToEditSurveyFragmentSecondEvent(NavigateToEditSurveyFragmentSecondEvent event) {
        getSupportFragmentManager().popBackStack();
        getSupportFragmentManager().popBackStack();


        replaceFragment(EditSurveyFragement.newInstance(), true);


    }


    @Subscribe
    public void NavigateDeleteSurveyFragmentEvent(NavigateToDeleteSurveyFragmentEvent event) {

if (isConnectedToNet(this)) {
    AlertAndDeleteSurvey(event.getSurvey());
}
else
{
showAlert("No internet connection");

}

    }


    private void AlertAndDeleteSurvey(final String surveyId) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Improved");
        builder.setMessage("Are you sure want to delete the survey");
        builder.setIcon(getResources().getDrawable(R.drawable.ic_logo_improved));
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                showProgress("Deleting");

                DeleteSurveryitem(surveyId);
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {


                dialog.cancel();


            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();


    }

    private void DeleteSurveryitem(String surveyId) {

        showProgress("Deleting survery");
        DeleteSurveryParameters deleteSurveryParameters = new DeleteSurveryParameters();
        deleteSurveryParameters.setBusinessId(userPref.getBusinessId());
        deleteSurveryParameters.setSurveyId(surveyId);
        HttpRequestForSurveryDelete httpRequestForSurveryDelete = new HttpRequestForSurveryDelete(this);
        httpRequestForSurveryDelete.deleteUser(deleteSurveryParameters);

    }

    @Subscribe
    public void NavigateSurveyFirstFragment(NavigateSurveyFirstFragmentEvent event) {
        addFragment(CreateSurveyFirstFragment.newInstance(), true);
    }

    @Subscribe
    public void SyncData(SyncDataWithServer event) {


        SyncWholeData();
    }


    @Subscribe
    public void NavigateStartQuestionFragment(NavigateStartQuestionFragmentEvent event) {
        addFragment(StartQuestionsFragment.newInstance(), true);
    }

    @Subscribe
    public void NavigateEditStartQuestionFragment(NavigateEditStartQuestionFragmentEvent event) {
        addFragment(EditStartQuestionsFragment.newInstance(), true);
    }

    @Subscribe
    public void NavigateToEditQuestion(NavigateToEditQuestionEvent event) {
        int id = event.getId();
        addFragment(EditQuestionFragment.newInstance(id), true);
    }

    @Subscribe
    public void NavigateToSurveyEditQuestionEvent(NavigateToSurveyEditQuestionEvent event) {
        int id = event.getId();
        addFragment(EditSurveyQuestionFragment.newInstance(id), true);
    }


    @Subscribe
    public void NavigateBackEvent(NavigateBackEvent event) {
        getSupportFragmentManager().popBackStack();
        BusFactory.getBus().post(new RefreshQuestionEvent());
    }
    @Subscribe
    public void SyncSurveyAfterSubmit(SyncSurveyAfterSubmit event) {

        Log.e( "SyncSurveyAfterSubmit: ","SyncSurveyAfterSubmit" );
    if (isConnectedToNet(this)) {

        SyncWholeData();



    }else
    {
        showAlert("No internet connection");
    }
    }

    @Subscribe
    public void NavigateBack(NavigateBack event) {
        getSupportFragmentManager().popBackStack();
    }

    @Subscribe
    public void NavigateBghackEvent(RefreshEventContinueFragment event) {
        getSupportFragmentManager().popBackStack();
        BusFactory.getBus().post(new RefreshEvent());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        if (mState == 1) //1 is true, 0 is false
        {
            //hide only option 2
            menu.findItem(R.id.refresh).setVisible(false);
        }
        else{
            menu.findItem(R.id.refresh).setVisible(true);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.refresh) {
   SyncWholeData();

        }




        return super.onOptionsItemSelected(item);
    }

    private void hideItem() {

        Menu nav_Menu = navigationView.getMenu();

        if (!userPref.getRole().equals("S_Admin"))
            nav_Menu.findItem(R.id.users).setVisible(false);
        nav_Menu.findItem(R.id.businessdetails).setVisible(false);

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.businessdetails) {
            // Handle the camera action
        } else if (id == R.id.users) {
            mState=1;
            invalidateOptionsMenu();

            getSupportFragmentManager().popBackStack();
            BusFactory.getBus().post(new RefreshEvent());
            addFragment(AddUserAccount.newInstance("", ""), true);
        } else if (id == R.id.nav_home) {
            mState=0;
            invalidateOptionsMenu();
            getSupportFragmentManager().popBackStack();
            getSupportFragmentManager().popBackStack();
            getSupportFragmentManager().popBackStack();
            BusFactory.getBus().post(new RefreshEvent());
            addFragment(HomeFragment.newInstance(), true);
        } else if (id == R.id.about) {
            mState=1;
            invalidateOptionsMenu();
            getSupportFragmentManager().popBackStack();
            BusFactory.getBus().post(new RefreshEvent());
            addFragment(AboutFragment.newInstance("", ""), true);

        } else if (id == R.id.nav_logout) {


            userPref.Clear();

            if (!mRealm.isClosed()) {

                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.deleteAll();
                    }
                });
            } else {
                mRealm = Realm.getDefaultInstance();
                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.deleteAll();
                    }
                });
//    Toast.makeText(this, "Item Null", Toast.LENGTH_SHORT).show();
            }

            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    @Override
    public void onListFragmentInteraction(String answerId, String surveyId) {
        Intent intent = new Intent(this, SurveyDetailsPerCustomer.class);
        intent.putExtra("surveyid", surveyId);
        intent.putExtra("answerid", answerId);
        startActivity(intent);
    }

    @Override
    public void onHttpSurveryDeleteSuccess(String Message, DeleteSurveryParameters surveyid) {

        cancelProgress();
        showProgress("Deleting from local");
        deleteSurveyInLocal(surveyid);
//        BusFactory.getBus().post(new RefreshListFeedback());

    }
    private void deleteSurveyInLocal() {
        mRealm = Realm.getDefaultInstance();

        suralldele = mRealm.where(SurveyDetails.class).findAll();


        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {


                    if (suralldele != null) {
                        suralldele.deleteAllFromRealm();
                    }

                }
            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }




    }

    private void deleteSurveyInLocal(DeleteSurveryParameters surveyid) {
        mRealm = Realm.getDefaultInstance();

        surveyDetailsdelete = mRealm.where(SurveyDetails.class).equalTo("SurveyId", surveyid.getSurveyId()
        ).findFirst();


        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {


                    if (surveyDetailsdelete != null) {
                        surveyDetailsdelete.deleteFromRealm();
                    }
                    cancelProgress();
                }
            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }


        getSupportFragmentManager().popBackStack();
        BusFactory.getBus().post(new RefreshListFeedback());
//        getDataFromServer();

    }

    @Override
    public void onHttpSurveryDeleteFailed(String message) {
        Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show();
        cancelProgress();

    }





    public void SyncWholeData() {
        if (isConnectedToNet(this)) {

            sendAnswerToServer();


        }else
        {
            showAlert("No internet connection");
        }


    }

    private void sendAnswerToServer() {

        showProgress("Sending answer to server");
        mRealm = Realm.getDefaultInstance();
        addMultplieAnswerSurveyRequestModel = new AddMultplieAnswerSurveyRequestModel();
        answerListObj = new ArrayList<Answer>();
        addAnswerMaster = mRealm.where(AddAnswerMaster.class).findAll();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    for (int kl = 0; kl < addAnswerMaster.size(); kl++) {
                        answerModel = new Answer();
                        answerModel.setSurveyId(Long.valueOf(addAnswerMaster.get(kl).getmSurveyId()));
                        answerModel.setAge(addAnswerMaster.get(kl).getAge());
                        answerModel.setProfession(addAnswerMaster.get(kl).getProfession());
                        answerModel.setMobileNo(addAnswerMaster.get(kl).getMobileNo());
                        answerModel.setGender(addAnswerMaster.get(kl).getGender());
                        answerModel.setEmail(addAnswerMaster.get(kl).getEmail());
                        answerModel.setAnswerTime(addAnswerMaster.get(kl).getAnswerTime());
                        answerModel.setAnswerTime(addAnswerMaster.get(kl).getAnswerTime());
                        answerModel.setAnswerDate(addAnswerMaster.get(kl).getAnswerDate());
                        answerModel.setFullName(addAnswerMaster.get(kl).getFullName());
                        List<AnswersList> answersLists = new ArrayList<com.intellyze.feedback.Api.AddAnswerMultipleSurveys.AnswersList>();
                        for (int i = 0; i < addAnswerMaster.get(kl).getAnswersList().size(); i++) {
                            com.intellyze.feedback.Api.AddAnswerMultipleSurveys.AnswersList answersList = new com.intellyze.feedback.Api.AddAnswerMultipleSurveys.AnswersList();
                            answersList.setAnswrValue(addAnswerMaster.get(kl).getAnswersList().get(i).getAnswrValue());
                            answersList.setAnswrValueId(String.valueOf(addAnswerMaster.get(kl).getAnswersList().get(i).getAnswrValueId()));
                            answersList.setAnswrValueRowId(String.valueOf(addAnswerMaster.get(kl).getAnswersList().get(i).getAnswrValueRowId()));
                            answersList.setQuestionId(String.valueOf(addAnswerMaster.get(kl).getAnswersList().get(i).getQuestionId()));
                            answersLists.add(answersList);
                        }
                        answerModel.setAnswersList(answersLists);
                        answerListObj.add(answerModel);
                        addMultplieAnswerSurveyRequestModel.setAnswers(answerListObj);
                    }
                }

            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }
        Gson gson = new GsonBuilder().create();
        String payloadStr = gson.toJson(addMultplieAnswerSurveyRequestModel);
        System.out.println(payloadStr + "dsAnswer");
        Log.e( "sendAnswerToServer: ",""+payloadStr );
        if (!payloadStr.equals("{}")) {

            HttpRequestForAddMultipleAnswerSurvey httpRequestForAddMultipleAnswerSurvey = new HttpRequestForAddMultipleAnswerSurvey(this);
            httpRequestForAddMultipleAnswerSurvey.AddMultipleAnswerSurveyList(addMultplieAnswerSurveyRequestModel);
        }
        else{

            cancelProgress();
            synchOffLineOnline();
        }

    }

    @Override
    public void onHttpAddMultipleAnswerSurveySuccess(String Message) {
        cancelProgress();
        deleteAnswerInLocal();
        synchOffLineOnline();
        Toast.makeText(this, "" + Message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onHttpAddMultipleAnswerSurveyFailed(String message) {
        cancelProgress();
        synchOffLineOnline();
    }

    private void deleteAnswerInLocal() {
        showProgress("Deleting local answers");
        mRealm = Realm.getDefaultInstance();
        addAnswerMaster = mRealm.where(AddAnswerMaster.class).findAll();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (addAnswerMaster.size() != 0) {
                        addAnswerMaster.deleteAllFromRealm();

                    }
                    cancelProgress();
                }
            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }

    }

    private void synchOffLineOnline() {
        showProgress("Syncing offline online");
        addMultipleSurveyRequestModel = new AddMultipleSurveyRequestModel();
        mSurveysList = new ArrayList<Survey>();
        mSurvey = new Survey();
        mRealm = Realm.getDefaultInstance();
        dataOfflineToOnline = mRealm.where(SurveyDetails.class)
                .equalTo("SurveyComplete", 1).equalTo("SurveySync", 0).findAll();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {


                    for (int j = 0; j < dataOfflineToOnline.size(); j++) {

                        if(dataOfflineToOnline.get(j).getEditflag().equals("true"))
                        {
                            mSurvey.setSurveyId(Long.valueOf(dataOfflineToOnline.get(j).getSourveyId()));
                        }
                        else
                        {
                            mSurvey.setSurveyId(Long.valueOf(0));
                        }

                        mSurvey.setSurveyName(dataOfflineToOnline.get(j).getSurveyName());
                        mSurvey.setSurveyDate(dataOfflineToOnline.get(j).getSurveyDate());
                        String encoded = "";
                        if (!dataOfflineToOnline.get(j).getSurveyImage().equals("")) {

                            Log.e( "dataOfflineToOnline.get(j).getSurveyImage(): ",""+dataOfflineToOnline.get(j).getSurveyImage() );

                            Bitmap bb = loadImageFromStorage(dataOfflineToOnline.get(j).getSurveyImage());
                            encoded = encodeImage(bb);
                        }
                        mSurvey.setSurveyImage(encoded);
                        String  edited=dataOfflineToOnline.get(j).getEditflag();
                        mSurvey.setBusiness_Id(userPref.getBusinessId());
                        mSurvey.setSurveyDesc(dataOfflineToOnline.get(j).getSurveyDescription());
                        mSurvey.setSurveyStatus(dataOfflineToOnline.get(j).getSurveyStatus());
                        mSurvey.setSurveyTime(dataOfflineToOnline.get(j).getSurveyTime());
                        List<com.intellyze.feedback.Api.AddMultpileSurveys.SurveyQuestion> surveyQuestions = new ArrayList<com.intellyze.feedback.Api.AddMultpileSurveys.SurveyQuestion>();
                        for (int i = 0; i < dataOfflineToOnline.get(j).getSurveyQuestionsDetails().size(); i++) {
                            com.intellyze.feedback.Api.AddMultpileSurveys.SurveyQuestion surveyQuestion = new com.intellyze.feedback.Api.AddMultpileSurveys.SurveyQuestion();
                            surveyQuestion.setIsOther(Long.valueOf(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getIsOther()));

                            String  added=dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getAdded_flag();

                            if(edited.equals("true"))
                            {


//                                surveyQuestion.setQuestionId(Long.valueOf(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestion_Id()));

                                if(added.equals("true"))
                                {
                                    surveyQuestion.setQuestionId(Long.valueOf(0));
                                }
                                else if(added.equals("false"))
                                {
                                    surveyQuestion.setQuestionId(Long.valueOf(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestion_Id()));
                                }

                            }
                            else
                            {

                                if(added.equals("true"))
                                {
                                    surveyQuestion.setQuestionId(Long.valueOf(0));
                                }
                                else if(added.equals("false"))
                                {
                                    surveyQuestion.setQuestionId(Long.valueOf(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestion_Id()));
                                }
//                                surveyQuestion.setQuestionId(Long.valueOf(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestion_Id()));


                            }



                            if (!dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionImage().equals("")) {

                                Bitmap bb = loadImageFromStorage(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionImage());
                                encoded = encodeImage(bb);
                            } else {
                                encoded = "";
                            }
                            surveyQuestion.setQuestionImage(encoded);
                            surveyQuestion.setQuestionName(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionName());
                            surveyQuestion.setQuestionNumber(Long.valueOf(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionNumber()));
                            surveyQuestion.setQuestionRequired(Long.valueOf(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionRequired()));
                            surveyQuestion.setQuestionType(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionType());
                            List<com.intellyze.feedback.Api.AddMultpileSurveys.QuestionMore> questionMores = new ArrayList<com.intellyze.feedback.Api.AddMultpileSurveys.QuestionMore>();
                            com.intellyze.feedback.Api.AddMultpileSurveys.QuestionMore questionMore = new com.intellyze.feedback.Api.AddMultpileSurveys.QuestionMore();
                            List<com.intellyze.feedback.Api.AddMultpileSurveys.Multiplechoicesingle> multiplechoicesingles = new ArrayList<com.intellyze.feedback.Api.AddMultpileSurveys.Multiplechoicesingle>();
                            if (dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().size() != 0) {
                                for (int p = 0; p < dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMultiplechoicesingleDetails().size(); p++) {
                                    com.intellyze.feedback.Api.AddMultpileSurveys.Multiplechoicesingle multiplechoicesingle = new com.intellyze.feedback.Api.AddMultpileSurveys.Multiplechoicesingle();
                                    multiplechoicesingle.setQuestionsOptionsId(Long.valueOf(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMultiplechoicesingleDetails().get(p).getQuestions_options_Id()));
                                    multiplechoicesingle.setTextChoice(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMultiplechoicesingleDetails().get(p).getTextChoice());
                                    multiplechoicesingles.add(multiplechoicesingle);
                                }
                                questionMore.setMultiplechoicesingle(multiplechoicesingles);
                                List<com.intellyze.feedback.Api.AddMultpileSurveys.Linearquestion> linearquestions = new ArrayList<com.intellyze.feedback.Api.AddMultpileSurveys.Linearquestion>();
                                for (int p = 0; p < dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getLinearquestionDetalis().size(); p++) {
                                    com.intellyze.feedback.Api.AddMultpileSurveys.Linearquestion linearquestion = new com.intellyze.feedback.Api.AddMultpileSurveys.Linearquestion();
                                    linearquestion.setEndText(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getLinearquestionDetalis().get(p).getEndText());
                                    linearquestion.setEndValue(Long.valueOf(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getLinearquestionDetalis().get(p).getEndValue()));
                                    linearquestion.setStartValue(Long.valueOf(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getLinearquestionDetalis().get(p).getStartValue()));
                                    linearquestion.setStartText(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getLinearquestionDetalis().get(p).getStartText());
                                    linearquestion.setQlinearId(Long.valueOf(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getLinearquestionDetalis().get(p).getQlinear_Id()));
                                    linearquestions.add(linearquestion);
                                }
                                questionMore.setLinearquestion(linearquestions);
                                List<com.intellyze.feedback.Api.AddMultpileSurveys.Matrixquestion> matrixquestions = new ArrayList<com.intellyze.feedback.Api.AddMultpileSurveys.Matrixquestion>();
                                if (dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMatrixquestionDetails().size() > 0) {
                                    com.intellyze.feedback.Api.AddMultpileSurveys.Matrixquestion matrixquestion = new com.intellyze.feedback.Api.AddMultpileSurveys.Matrixquestion();
                                    List<com.intellyze.feedback.Api.AddMultpileSurveys.Row> rows = new ArrayList<com.intellyze.feedback.Api.AddMultpileSurveys.Row>();
                                    for (int m = 0; m < dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMatrixquestionDetails().get(0).getRowDetails().size(); m++) {
                                        com.intellyze.feedback.Api.AddMultpileSurveys.Row row = new com.intellyze.feedback.Api.AddMultpileSurveys.Row();
                                        row.setQmultipleGridRowId(Long.valueOf(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMatrixquestionDetails().get(0).getRowDetails().get(m).getQmultiple_grid_row_Id()));
                                        row.setRowName(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMatrixquestionDetails().get(0).getRowDetails().get(m).getRow_name());
                                        rows.add(row);
                                    }
                                    matrixquestion.setRow(rows);
                                    List<com.intellyze.feedback.Api.AddMultpileSurveys.Column> columns = new ArrayList<com.intellyze.feedback.Api.AddMultpileSurveys.Column>();
                                    for (int m = 0; m < dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMatrixquestionDetails().get(0).getColumnDetails().size(); m++) {
                                        com.intellyze.feedback.Api.AddMultpileSurveys.Column column = new com.intellyze.feedback.Api.AddMultpileSurveys.Column();
                                        column.setQmultipleGridColId(Long.valueOf(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMatrixquestionDetails().get(0).getColumnDetails().get(m).getQmultiple_grid_col_Id()));
                                        column.setColumnName(dataOfflineToOnline.get(j).getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMatrixquestionDetails().get(0).getColumnDetails().get(m).getColumn_name());
                                        columns.add(column);
                                    }
                                    matrixquestion.setColumn(columns);
                                    matrixquestions.add(matrixquestion);
                                }
                                questionMore.setMatrixquestion(matrixquestions);
                                questionMores.add(questionMore);
                            }
                            surveyQuestion.setQuestionMore(questionMores);
                            surveyQuestions.add(surveyQuestion);
                        }
                        mSurvey.setSurveyQuestions(surveyQuestions);
                        mSurveysList.add(mSurvey);
                        addMultipleSurveyRequestModel.setSurveys(mSurveysList);
                    }
                }
            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }
        Gson gson = new GsonBuilder().create();
        String payloadStr = gson.toJson(addMultipleSurveyRequestModel);
//        Log.e( "synchOffLine to ",""+payloadStr );

        System.out.println( "Pay load final synck wiith unsync data"+payloadStr);


        if (!payloadStr.equals("{}")) {
            HttpRequestForAddMultipleSurvey httpRequestForAddMultipleSurvey = new HttpRequestForAddMultipleSurvey(this);
            httpRequestForAddMultipleSurvey.AddMultipleSurveyList(addMultipleSurveyRequestModel);


        }
        else {
            cancelProgress();
            getDataFromServer();



        }



    }

    @Override
    public void onHttpAddMultipleSurveySuccess(String Message) {
        callDeleteLocalSurvey();
        cancelProgress();
        getDataFromServer();

        Toast.makeText(this, ""+Message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onHttpAddMultipleSurveyFailed(String message) {
cancelProgress();   getDataFromServer();
    }

    private void callDeleteLocalSurvey() {
        mRealm = Realm.getDefaultInstance();
        dataOfflineToOnline = mRealm.where(SurveyDetails.class)
                .equalTo("SurveyComplete", 1).equalTo("SurveySync", 0).findAll();
        surveyQuestionsDetails=mRealm.where(SurveyQuestionsDetails.class).findAll();

        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (dataOfflineToOnline.size() != 0) {
                        dataOfflineToOnline.deleteAllFromRealm();

                    }  if (surveyQuestionsDetails.size() != 0) {
                        surveyQuestionsDetails.deleteAllFromRealm();

                    }
                }
            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }
//        Log.e( "getDataFromServer: ", "491");
//        getDataFromServer();
    }


    private Bitmap loadImageFromStorage(String path) {
        byte[] decodedString = Base64.decode(path, Base64.NO_WRAP);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }




    private void getDataFromServer() {

        showProgress("Getting data form server");
        deleteSurveyInLocal();

        Log.e( "getDataFromServer: ","getDataFromServer" );

        FeedbackQuestionRequestModel feedbackQuestionRequestModel = new FeedbackQuestionRequestModel();
        feedbackQuestionRequestModel.setQuestionId("0");
        feedbackQuestionRequestModel.setSurveyId("0");
        feedbackQuestionRequestModel.setBusiness_Id(userPref.getBusinessId());
        HttpRequestSurveyQuestion httpRequestSurveyQuestion = new HttpRequestSurveyQuestion(this);
        httpRequestSurveyQuestion.SurveyQuest(feedbackQuestionRequestModel);
    }


    @Override
    public void onHttpSurveyQuestionSuccess(String Message, List<SurveyDatum> result) {
        Gson gson = new GsonBuilder().create();
        String payloadStr = gson.toJson(result);
        Log.e( "Loaded data"," "+payloadStr );


        getFeedbackQuestionListAdminSide(result);
        getFeedbackQuestionListUserSide(result);



        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                // Do something after 5s = 5000ms
                BusFactory.getBus().post(new RefreshListFeedback());
                cancelProgress();
            }
        }, 1000);

    }

    @Override
    public void onHttpSurveyQuestionFailed(String message) {

        cancelProgress();
    }


    private void getFeedbackQuestionListAdminSide(final List<SurveyDatum> result) {


        surveyDetailsModels = new ArrayList<SurveyDetailsModel>();
        try {
            for (int ij = 0; ij < result.size(); ij++) {
                mRealm = Realm.getDefaultInstance();

                surveySingleDetails = mRealm.where(SurveyDetails.class).equalTo("SurveyId", result.get(ij).getSurveyId()).equalTo("SurveySync", 1).findFirst();

                final int finalIj = ij;
                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        if (surveySingleDetails != null) {
//                          set response count here
                            surveySingleDetails.setCount(Integer.parseInt(result.get(finalIj).getCount()));
                            surveySingleDetails.setLastEntryDate(result.get(finalIj).getLastEntryDate());
                            surveySingleDetails.setLastEntryTime(result.get(finalIj).getLastEntryTime());
                            mRealm.copyToRealm(surveySingleDetails);
                        } else {
                            //add new servey here
                            SurveyDetails surveyDetails = new SurveyDetails();

                            surveyDetails.setSurveyImage(result.get(finalIj).getSurvey_image());
                            surveyDetails.setSurveyDate(result.get(finalIj).getSurveyDate());
                            surveyDetails.setSurveyComplete(1);

                            surveyDetails.setSurveySync(1);
                            surveyDetails.setSurveyDescription(result.get(finalIj).getSurveyDescr());
                            surveyDetails.setLastEntryTime(result.get(finalIj).getLastEntryTime());
                            surveyDetails.setLastEntryDate(result.get(finalIj).getLastEntryDate());
                            surveyDetails.setCount(Integer.parseInt(result.get(finalIj).getCount()));
                            surveyDetails.setSurveyId(result.get(finalIj).getSurveyId());
                            surveyDetails.setEditflag("false");


                            surveyDetails.setSurveyName(result.get(finalIj).getSurveyTitle());
                            surveyDetails.setSurveyStatus(result.get(finalIj).getSStatus());
                            surveyDetails.setSurveyTime(result.get(finalIj).getSurveyTime());

                            List<com.intellyze.feedback.Api.FeedbackSurveyQuestions.SurveyQuestion> surveyQuestions=result.get(finalIj).getSurveyQuestions();

                            int surveyq_count=surveyQuestions.size();


                            RealmList<SurveyQuestionsDetails> surveyQuestionsDetails = new RealmList<>();

                            for(int i=0;i<surveyq_count;i++)
                            {

                                SurveyQuestionsDetails surveyQuestionsDetails1=new SurveyQuestionsDetails();

                                surveyQuestionsDetails1.setQuestionName(surveyQuestions.get(i).getQuestionTitle());
                                surveyQuestionsDetails1.setAdded_flag("false");
                                surveyQuestionsDetails1.setQuestion_Id(Integer.parseInt(surveyQuestions.get(i).getQuestionId()));

                                surveyQuestionsDetails1.setQuestionRequired(Integer.parseInt(surveyQuestions.get(i).getIsRequired()));
                                surveyQuestionsDetails1.setQuestionNumber(Integer.parseInt(surveyQuestions.get(i).getQuestionNumber()));
                                surveyQuestionsDetails1.setQuestionType(surveyQuestions.get(i).getQuestionType());
                                surveyQuestionsDetails1.setQuestionImage(surveyQuestions.get(i).getQuestionImage());
                                surveyQuestionsDetails1.setIsOther(Integer.parseInt(surveyQuestions.get(i).getIsOther()));




                                com.intellyze.feedback.Api.FeedbackSurveyQuestions.QuestionMore questionMore=surveyQuestions.get(i).getQuestionMore();




                                List<com.intellyze.feedback.Api.FeedbackSurveyQuestions.MultipleChoice> mMultipleChoice=questionMore.getMultipleChoice();

                                List<com.intellyze.feedback.Api.FeedbackSurveyQuestions.MultipleChoiceGrid> mMultipleChoiceGrid=questionMore.getmMultipleChoiceGrid();
                                RealmList<multiplechoicesingleDetails> multiplechoicesingleDetails_list=new RealmList<>();;

                                RealmList<matrixquestionDetails> matrixquestionDetails_list=new RealmList<>();;


                                List<com.intellyze.feedback.Api.FeedbackSurveyQuestions.Linear> linear=questionMore.getLinear();




                                RealmList<linearquestionDetalis> linearquestionDetalis_list=new RealmList<>();
                                RealmList<QuestionMoreDetails> list_question_more=new RealmList<>();


                                QuestionMoreDetails questionMoreDetails=new QuestionMoreDetails();

                                for (int j=0;j<linear.size();j++)
                                {




                                    linearquestionDetalis linearquestionDetalis=new linearquestionDetalis();
                                    linearquestionDetalis.setEndText(linear.get(j).getEndText());
                                    linearquestionDetalis.setEndValue(Integer.parseInt(linear.get(j).getEndValue()));
                                    linearquestionDetalis.setQlinear_Id(Integer.parseInt(linear.get(j).getQlinearId()));
                                    linearquestionDetalis.setStartText(linear.get(j).getStartText());
                                    linearquestionDetalis.setStartValue(Integer.parseInt(linear.get(j).getStartValue()));
                                    linearquestionDetalis_list.add(linearquestionDetalis);


                                }



                                for (int p = 0; p < mMultipleChoice.size(); p++) {
                                    multiplechoicesingleDetails multiplechoicesingleDetails=new multiplechoicesingleDetails();

                                    multiplechoicesingleDetails.setQuestions_options_Id(Integer.parseInt(mMultipleChoice.get(p).getQuestionsOptionsId()));
                                    multiplechoicesingleDetails.setTextChoice(mMultipleChoice.get(p).getOptionTitle());
                                    multiplechoicesingleDetails_list.add(multiplechoicesingleDetails);



                                }




                                for (int j=0;j<mMultipleChoiceGrid.size();j++)
                                {

                                    matrixquestionDetails matrixquestionDetails=new matrixquestionDetails();

                                    RealmList<rowDetails> rowDetails=new RealmList<>();
                                    RealmList<columnDetails> columnDetails=new RealmList<>();

                                    List<com.intellyze.feedback.Api.FeedbackSurveyQuestions.Column> columns=mMultipleChoiceGrid.get(j).getColumn();
                                    List<com.intellyze.feedback.Api.FeedbackSurveyQuestions.Row> row=mMultipleChoiceGrid.get(j).getRow();


                                    for (int q=0;q<row.size();q++){

                                        rowDetails rowDetails1=new rowDetails();

                                        rowDetails1.setQmultiple_grid_row_Id(Integer.parseInt(row.get(q).getQuestionId()));
                                        rowDetails1.setRow_name(String.valueOf(row.get(q).getRowTitle()));
                                        rowDetails.add(rowDetails1);

                                    }
                                    matrixquestionDetails.setRowDetails(rowDetails);
                                    for (int w=0;w<columns.size();w++){

                                        columnDetails columnDetails1=new columnDetails();

                                        columnDetails1.setQmultiple_grid_col_Id(Integer.parseInt(columns.get(w).getQmultipleGridColId()));
                                        columnDetails1.setColumn_name(String.valueOf(columns.get(w).getColTitle()));
                                        columnDetails.add(columnDetails1);



                                    }

                                    matrixquestionDetails.setColumnDetails(columnDetails);









                                    matrixquestionDetails.setColumnDetails(columnDetails);
                                    matrixquestionDetails.setRowDetails(rowDetails);
                                    matrixquestionDetails_list.add(matrixquestionDetails);

                                }



                                questionMoreDetails.setLinearquestionDetalis(linearquestionDetalis_list);
                                questionMoreDetails.setMatrixquestionDetails(matrixquestionDetails_list);
                                questionMoreDetails.setMultiplechoicesingleDetails(multiplechoicesingleDetails_list);
                                list_question_more.add(questionMoreDetails);


                                surveyQuestionsDetails1.setQuestionMoreDetails(list_question_more);






                                surveyQuestionsDetails.add(surveyQuestionsDetails1);




                            }


                            surveyDetails.setSurveyQuestionsDetails(surveyQuestionsDetails);


                            Gson gson = new Gson();

                            String jsonInString = gson.toJson(surveyDetails);
                            mRealm.copyToRealm(surveyDetails);
                        }
                    }
                });

            }
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }
    }
    /* Method to FETCH User Side survey details list from server to local database */
    private void getFeedbackQuestionListUserSide(final List<SurveyDatum> result) {
        try {
            mRealm = Realm.getDefaultInstance();
            for (int i = 0; i < result.size(); i++) {
                sdf = mRealm.where(FeedQuestUserSide.class).equalTo("mSurveyId", result.get(i).getSurveyId()).findFirst();
                final int finalI = i;
                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        if (sdf != null) {
                            String gh = result.get(finalI).getCount();
                            sdf.setmSurveyCount(result.get(finalI).getCount());

                            mRealm.copyToRealm(sdf);
                        } else {
                            FeedQuestUserSide feedQuestUserSide = new FeedQuestUserSide();
                            feedQuestUserSide.setSStatus(result.get(finalI).getSStatus());
                            feedQuestUserSide.setmSurveyImage(result.get(finalI).getSurvey_image());
                            feedQuestUserSide.setmSurveyCount(result.get(finalI).getCount());
                            feedQuestUserSide.setSurveyDate(result.get(finalI).getSurveyDate());
                            feedQuestUserSide.setSurveyTime(result.get(finalI).getSurveyTime());
                            feedQuestUserSide.setSurveyDescr(result.get(finalI).getSurveyDescr());
                            feedQuestUserSide.setSurveyId(result.get(finalI).getSurveyId());
                            feedQuestUserSide.setSurveyTitle(result.get(finalI).getSurveyTitle());
                            surveyQuestions = new RealmList<SurveyQuestion>();
                            for (int k = 0; k < result.get(finalI).getSurveyQuestions().size(); k++) {
                                SurveyQuestion question = new SurveyQuestion();
                                question.setIsRequired(result.get(finalI).getSurveyQuestions().get(k).getIsRequired());
                                question.setQuestionId(result.get(finalI).getSurveyQuestions().get(k).getQuestionId());
//                                question.setIsOther(Integer.parseInt(result.get(finalI).getSurveyQuestions().get(k).getIsOther()));
                                String s=result.get(finalI).getSurveyQuestions().get(k).getIsOther();
                                if(!s.equals(null))
                                {
                                    question.setIsOther(Integer.parseInt(s));
                                }
                                else
                                {
                                    question.setIsOther(0);
                                }

                                question.setQuestionImage(result.get(finalI).getSurveyQuestions().get(k).getQuestionImage());
                                question.setQuestionNumber(result.get(finalI).getSurveyQuestions().get(k).getQuestionNumber());
                                question.setQuestionTitle(result.get(finalI).getSurveyQuestions().get(k).getQuestionTitle());
                                question.setQuestionType(result.get(finalI).getSurveyQuestions().get(k).getQuestionType());
                                question.setSurveyId(result.get(finalI).getSurveyQuestions().get(k).getSurveyId());
                                QuestionMore questionMore = new QuestionMore();
                                multipleChoices = new RealmList<MultipleChoice>();
                                for (int p = 0; p < result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getMultipleChoice().size(); p++) {
                                    MultipleChoice questionMultiple = new MultipleChoice();
                                    questionMultiple.setOptionTitle(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getMultipleChoice().get(p).getOptionTitle());
                                    questionMultiple.setQuestionId(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getMultipleChoice().get(p).getQuestionId());
                                    questionMultiple.setQuestionsOptionsId(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getMultipleChoice().get(p).getQuestionsOptionsId());

                                    multipleChoices.add(questionMultiple);
                                }
                                questionMore.setMultipleChoice(multipleChoices);
                                linears = new RealmList<Linear>();
                                for (int p = 0; p < result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getLinear().size(); p++) {
                                    Linear linear = new Linear();
                                    linear.setEndText(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getLinear().get(p).getEndText());
                                    linear.setEndValue(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getLinear().get(p).getEndValue());
                                    linear.setQlinearId(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getLinear().get(p).getQlinearId());
                                    linear.setQuestionId(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getLinear().get(p).getQuestionId());
                                    linear.setStartText(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getLinear().get(p).getStartText());
                                    linear.setStartValue(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getLinear().get(p).getStartValue());
                                    linears.add(linear);
                                }
                                questionMore.setLinear(linears);
                                multipleChoiceGrid = new MultipleChoiceGrid();
                                int ghh = result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getmMultipleChoiceGrid().size();
                                multipleChoiceGrids = new RealmList<MultipleChoiceGrid>();
                                if (ghh != 0) {
                                    rows = new RealmList<Row>();
                                    for (int p = 0; p < result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getmMultipleChoiceGrid().get(0).getRow().size(); p++) {
                                        Row row = new Row();
                                        row.setRowTitle(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getmMultipleChoiceGrid().get(0).getRow().get(p).getRowTitle());
                                        row.setQuestionId(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getmMultipleChoiceGrid().get(0).getRow().get(p).getQuestionId());
                                        row.setQmultipleGridRowId(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getmMultipleChoiceGrid().get(0).getRow().get(p).getQmultipleGridRowId());
                                        rows.add(row);
                                    }
                                    multipleChoiceGrid.setRow(rows);
                                    columns = new RealmList<Column>();
                                    for (int p = 0; p < result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getmMultipleChoiceGrid().get(0).getColumn().size(); p++) {
                                        Column column = new Column();
                                        column.setColTitle(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getmMultipleChoiceGrid().get(0).getColumn().get(p).getColTitle());
                                        column.setQuestionId(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getmMultipleChoiceGrid().get(0).getColumn().get(p).getQuestionId());
                                        column.setQmultipleGridColId(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getmMultipleChoiceGrid().get(0).getColumn().get(p).getQmultipleGridColId());
                                        columns.add(column);
                                    }
                                    multipleChoiceGrid.setColumn(columns);
                                    multipleChoiceGrids.add(multipleChoiceGrid);
                                }
                                questionMore.setmMultipleChoiceGrid(multipleChoiceGrids);
                                question.setQuestionMore(questionMore);
                                surveyQuestions.add(question);
                            }
                            feedQuestUserSide.setSurveyQuestions(surveyQuestions);
                            mRealm.copyToRealm(feedQuestUserSide);
                        }
                    }
                });
            }
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }

    }





}
