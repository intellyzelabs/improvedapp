package com.intellyze.feedback.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.intellyze.feedback.R;

public class ReportActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        Intent intent=getIntent();
        String surveyid=intent.getStringExtra("MID");
        Log.e( "surveyid: ",""+surveyid );
    }
}
