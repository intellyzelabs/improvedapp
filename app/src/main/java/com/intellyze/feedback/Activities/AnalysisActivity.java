package com.intellyze.feedback.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.google.gson.Gson;
import com.intellyze.feedback.Api.AnalysisModel.AnalysisDataModel;
import com.intellyze.feedback.Api.AnalysisModel.Answer;
import com.intellyze.feedback.Api.AnalysisModel.AnswerGrid;
import com.intellyze.feedback.Api.AnalysisModel.AnswersList;
import com.intellyze.feedback.Api.AnalysisModel.ColumnsResult;
import com.intellyze.feedback.Api.AnalysisModel.HttpRequestForListAnalysis;
import com.intellyze.feedback.Api.AnalysisModel.OnHttpRequestAnalysis;
import com.intellyze.feedback.Api.AnalysisModel.QuestionMore;
import com.intellyze.feedback.Api.AnalysisModel.SurveyData;
import com.intellyze.feedback.Custom.TextViewBold;
import com.intellyze.feedback.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AnalysisActivity extends BaseActivity implements OnHttpRequestAnalysis, OnChartValueSelectedListener {
LinearLayout shortanswer_layout,shortanswer_linear,paragraph_layout,paragraph_linear,dropdown_linear,multiplechoice_layout,multiplechoice_linear,dropdown_layout,linear_linear,linear_layout,mulgrid_linear,mulgrid_layout;
    TextViewBold shortanswer,paragraphswer,multiplechoice_text,drop_down_text,linear_text,mulgrid_text;
    private PieChart mChart;
    List<String> mult_answearray;
    List<String> mult_countarray;
    private ArrayList<String> drop_answearray;
    private ArrayList<String> drop_countarray;


    private ArrayList<String> linear_answearray;
    private ArrayList<String> linear_countarray;

    private PieChart dropChart;
    private CombinedChart linear;


    private ArrayList<String> choice_grid_answearray;
    private ArrayList<String> choice_grid_countarray;
    private ArrayList<String> column_name_array;
    private ArrayList<String> answer_count_array;
    private BarChart mulgridlinear;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_analysis);


        this.setFinishOnTouchOutside(false);

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8)
        {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //your codes here

        }
        Intent i = getIntent();

        String uid = i.getStringExtra("UID");
setProgressBar();
        initilize();
        String surveyid="117";

showProgress("Loading");
getData(uid);


    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    private void initilize() {
        shortanswer_layout=findViewById(R.id.shortanswer_layout);
        shortanswer_linear=findViewById(R.id.shortanswer_linear);
        shortanswer=findViewById(R.id.shortanswer);

        paragraph_layout=findViewById(R.id.paragraph_layout);
        paragraph_linear=findViewById(R.id.paragraph_linear);
        paragraphswer=findViewById(R.id.paragraphswer);


        multiplechoice_layout=findViewById(R.id.multiplechoice_layout);
        multiplechoice_text=findViewById(R.id.multiplechoice_text);
        multiplechoice_linear=findViewById(R.id.multiplechoice_linear);


        drop_down_text=findViewById(R.id.drop_down_text);
        dropdown_layout=findViewById(R.id.dropdown_layout);
        dropdown_linear=findViewById(R.id.dropdown_linear);


        linear_text=findViewById(R.id.linear_text);
        linear_layout=findViewById(R.id.linear_layout);
        linear_linear=findViewById(R.id.linear_linear);


        mulgrid_text=findViewById(R.id.mulgrid_text);
        mulgrid_layout=findViewById(R.id.mulgrid_layout);
        mulgrid_linear=findViewById(R.id.mulgrid_linear);

        mChart = (PieChart) findViewById(R.id.chart1);
        dropChart = (PieChart) findViewById(R.id.chartdrop);

        linear = (CombinedChart) findViewById(R.id.linear);
        mulgridlinear = (BarChart) findViewById(R.id.mulgridlinear);


        setUpChart();



       setDropchart();
       setLinearChart();
       setMultiChartNew();


    }

    private void setMultiChartNew() {

        mulgridlinear.setOnChartValueSelectedListener(this);

        mulgridlinear.setDrawBarShadow(false);
        mulgridlinear.setDrawValueAboveBar(true);

        mulgridlinear.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        mulgridlinear.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        mulgridlinear.setPinchZoom(false);

        mulgridlinear.setDrawGridBackground(false);
    }

    private void setLinearChart() {


        linear.getDescription().setEnabled(false);
        linear.setBackgroundColor(Color.WHITE);
        linear.setDrawGridBackground(false);
        linear.setDrawBarShadow(false);
        linear.setHighlightFullBarEnabled(false);

        // draw bars behind lines
        linear.setDrawOrder(new CombinedChart.DrawOrder[]{
                CombinedChart.DrawOrder.BAR, CombinedChart.DrawOrder.BUBBLE, CombinedChart.DrawOrder.CANDLE, CombinedChart.DrawOrder.LINE, CombinedChart.DrawOrder.SCATTER
        });




    }

    private void setMultchart() {

        mChart.setUsePercentValues(true);
        mChart.getDescription().setEnabled(false);
        mChart.setExtraOffsets(5, 10, 5, 5);

        mChart.setDragDecelerationFrictionCoef(0.95f);


        mChart.setCenterText("Test item");

        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(Color.WHITE);

        mChart.setTransparentCircleColor(Color.WHITE);
        mChart.setTransparentCircleAlpha(110);

        mChart.setHoleRadius(58f);
        mChart.setTransparentCircleRadius(61f);

        mChart.setDrawCenterText(true);

        mChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChart.setRotationEnabled(false);
        mChart.setHighlightPerTapEnabled(true);



        // add a selection listener
        mChart.setOnChartValueSelectedListener(this);
        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(true);
        l.setXEntrySpace(0f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);
        l.setWordWrapEnabled(true);

        // entry label styling
        mChart.setEntryLabelColor(Color.WHITE);

        mChart.setEntryLabelTextSize(12f);

        // undo all highlights
        mChart.highlightValues(null);


        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
    }
    private void setDropchart() {



        dropChart.setUsePercentValues(true);
        dropChart.getDescription().setEnabled(false);
        dropChart.setExtraOffsets(5, 10, 5, 5);

        dropChart.setDragDecelerationFrictionCoef(0.95f);

        dropChart.setCenterText(generateCenterSpannableText());

        dropChart.setDrawHoleEnabled(true);
        dropChart.setHoleColor(Color.WHITE);

        dropChart.setTransparentCircleColor(Color.WHITE);
        dropChart.setTransparentCircleAlpha(110);

        dropChart.setHoleRadius(58f);
        dropChart.setTransparentCircleRadius(61f);

        dropChart.setDrawCenterText(true);

        dropChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        dropChart.setRotationEnabled(true);
        dropChart.setHighlightPerTapEnabled(true);

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
        dropChart.setOnChartValueSelectedListener(this);


        // mChart.spin(2000, 0, 360);



        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);

        // entry label styling
        dropChart.setEntryLabelColor(Color.WHITE);
        dropChart.setEntryLabelTextSize(12f);










    }




    protected String[] mParties = new String[] {
            "Party A", "Party B", "Party C", "Party D", "Party E", "Party F", "Party G", "Party H",
            "Party I", "Party J", "Party K", "Party L", "Party M", "Party N", "Party O", "Party P",
            "Party Q", "Party R", "Party S", "Party T", "Party U", "Party V", "Party W", "Party X",
            "Party Y", "Party Z"
    };
    protected String[] mPartiesNew = new String[] {
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private void setData(List<String> mult_countarray, List<String> mult_answearray) {
        int count=mult_answearray.size();



        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();



        for (int i = 0; i < count ; i++) {
            int count_int= Integer.parseInt(mult_countarray.get(i));

                String name_string = mult_answearray.get(i);

                entries.add(new PieEntry((float) count_int,
                        name_string,
                        getResources().getDrawable(R.drawable.ic_add_white)));

        }




        PieDataSet dataSet = new PieDataSet(entries, " Results");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);

        mChart.setData(data);


        setUpChart();


        mChart.invalidate();

    }

    private void setUpChart() {

        mChart.setUsePercentValues(true);
        mChart.getDescription().setEnabled(false);
        mChart.setExtraOffsets(5, 10, 5, 5);

        mChart.setDragDecelerationFrictionCoef(0.95f);

        mChart.setCenterText(generateCenterSpannableText());

        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(Color.WHITE);

        mChart.setTransparentCircleColor(Color.WHITE);
        mChart.setTransparentCircleAlpha(110);

        mChart.setHoleRadius(58f);
        mChart.setTransparentCircleRadius(61f);

        mChart.setDrawCenterText(true);

        mChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChart.setRotationEnabled(true);
        mChart.setHighlightPerTapEnabled(true);

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
        mChart.setOnChartValueSelectedListener(this);


        // mChart.spin(2000, 0, 360);



        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setFormToTextSpace(10f);
        l.setYEntrySpace(10f);
        l.setYOffset(10f);

        // entry label styling
        mChart.setEntryLabelColor(Color.WHITE);
        mChart.setEntryLabelTextSize(12f);
    }


    private SpannableString generateCenterSpannableText(String ss) {

        SpannableString s = new SpannableString(ss);
        s.setSpan(new RelativeSizeSpan(1.7f), 0, 14, 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 14, s.length() - 15, 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 14, s.length() - 15, 0);
        s.setSpan(new RelativeSizeSpan(.8f), 14, s.length() - 15, 0);
        s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 14, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 14, s.length(), 0);
        return s;
    }

    private void getData(String surveyid) {

        AnalysisDataModel analysisDataModel=new AnalysisDataModel();
        analysisDataModel.setSurveyId(surveyid);
        HttpRequestForListAnalysis httpRequestForListAnalysis=new HttpRequestForListAnalysis(this);
        httpRequestForListAnalysis.getList(analysisDataModel);
    }

    private SpannableString generateCenterSpannableText() {

        SpannableString s = new SpannableString("MPAndroidChart\ndeveloped by Philipp Jahoda");
        s.setSpan(new RelativeSizeSpan(1.7f), 0, 14, 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 14, s.length() - 15, 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 14, s.length() - 15, 0);
        s.setSpan(new RelativeSizeSpan(.8f), 14, s.length() - 15, 0);
        s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 14, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 14, s.length(), 0);
        return s;
    }

    @Override
    public void onHttpAnalysisSuccess(String Message, final SurveyData surveyData) {



        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setData(surveyData);
                //Your code to run in GUI thread here
            }//public void run() {
        });
    }

    private void setData(SurveyData surveyData) {



        List<QuestionMore> questionMores=surveyData.getQuestionMore();

        for (int i=0;i<questionMores.size();i++) {
            String Question_Title = questionMores.get(i).getQuestionTitle();


            String Question_type = questionMores.get(i).getQuestionType();


if(Question_type.equals("Short answer"))
{
    this.shortanswer.setText(i+"."+Question_Title);
    List<AnswersList> answersList = questionMores.get(i).getAnswersList();
    for (int s = 0; s < answersList.size(); s++) {
String answer=answersList.get(s).getAnswrValue();
        TextView tv = new TextView(this);
        tv.setText(answer);
        tv.setPadding(10, 10, 10, 10);
        shortanswer_linear.setPadding(10,10,10,10);

        this.shortanswer_linear.addView(tv);
    }




}
if(Question_type.equals("Paragraph"))
{

    this.paragraphswer.setText(i+"."+Question_Title);
    List<AnswersList> answersList = questionMores.get(i).getAnswersList();
    for (int s = 0; s < answersList.size(); s++) {
        String answer=answersList.get(s).getAnswrValue();
        TextView tv = new TextView(this);
        tv.setText(answer);
        tv.setPadding(10, 10, 10, 10);
        shortanswer_linear.setPadding(10,10,10,10);
        this.paragraph_linear.addView(tv);
    }

}

if(Question_type.equals("Multiple choice"))
{
    mult_answearray=new ArrayList<>();
    mult_countarray=new ArrayList<>();
    this.multiplechoice_text.setText(i+"."+Question_Title);
    List<Answer> answersList = questionMores.get(i).getAnswer();
    for (int s = 0; s < answersList.size(); s++) {
        String answer=answersList.get(s).getOptionName();
        String count=answersList.get(s).getOptionCount();
        mult_countarray.add(count);
        mult_answearray.add(answer);
    }
    setData(mult_countarray,mult_answearray);


}

if(Question_type.equals("Checkboxes"))
{

}

if(Question_type.equals("Multiple choice grid"))
{

    ArrayList<BarEntry> yVals1 ;
 BarDataSet set1 ;
    List<IBarDataSet> sets ;
    choice_grid_answearray=new ArrayList<>();
    choice_grid_countarray=new ArrayList<>();

    this.mulgrid_text.setText(i+"."+Question_Title);
    List<AnswerGrid> answersList = questionMores.get(i).getAnswerGrid();

    sets=new ArrayList<>();
    for (int s = 0; s < answersList.size(); s++) {
        String answer=answersList.get(s).getRowTitle();
        int  count=answersList.get(s).getColumnsResult().size();
        column_name_array=new ArrayList<>();
        answer_count_array=new ArrayList<>();
        yVals1=new ArrayList<>();

        for (int j = 0; j < count; j++) {
            int answertcount= Integer.parseInt(answersList.get(s).getColumnsResult().get(j).getAnswerCount());
            String column=answersList.get(s).getColumnsResult().get(j).getColumnName();
            yVals1.add(new BarEntry(i, (float) answertcount));
            set1 = new BarDataSet(yVals1,column );
            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            set1.setColor(color);
            sets.add(set1);



//            column_name_array.add(answersList.get(s).getColumnsResult().get(j).getColumnName());
//            answer_count_array.add(answersList.get(s).getColumnsResult().get(j).getAnswerCount());


        }

        BarData data = new BarData(sets);



        mulgridlinear.setData(data);





    }
//    setBarGroupData(choice_grid_countarray,choice_grid_answearray);

}


if(Question_type.equals("Dropdown"))
{
    drop_answearray=new ArrayList<>();
    drop_countarray=new ArrayList<>();
    this.drop_down_text.setText(i+"."+Question_Title);
    List<Answer> answersList = questionMores.get(i).getAnswer();
    for (int s = 0; s < answersList.size(); s++) {
        String answer=answersList.get(s).getOptionName();
        String count=answersList.get(s).getOptionCount();
        drop_countarray.add(count);
        drop_answearray.add(answer);
    }
    setDataDropDown(drop_countarray,drop_answearray);


}
if(Question_type.equals("Linear scale"))
{
    linear_answearray=new ArrayList<>();
    linear_countarray=new ArrayList<>();
    this.linear_text.setText(i+"."+Question_Title);
    List<Answer> answersList = questionMores.get(i).getAnswer();

    for (int s = 0; s < answersList.size(); s++) {
        String answer=answersList.get(s).getOptionName();
        String count=answersList.get(s).getOptionCount();
        linear_countarray.add(count);
        linear_answearray.add(answer);
    }
    CombinedData data = new CombinedData();

    data.setData(setLinearData(linear_countarray,linear_answearray));


    linear.setData(data);


}

        }
        cancelProgress();
        }

        List<IBarDataSet> iBarDataSets;
    private BarData setLinearData(ArrayList<String> linear_countarray, ArrayList<String> linear_answearray) {


        int count=linear_answearray.size();
        float start = 0;

        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();
        iBarDataSets=new ArrayList<>();

        for (int i = (int) start; i < count ; i++) {

            int val = Integer.parseInt(linear_countarray.get(i));

            if (Math.random() * 100 < 25) {
                yVals1.add(new BarEntry(i, val, getResources().getDrawable(R.drawable.ic_add_white)));
            } else {
                yVals1.add(new BarEntry(i, val));
            }

            String value=linear_answearray.get(i);
            BarDataSet set1 = new BarDataSet(yVals1,value );
            Random rnd = new Random();
            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));


            set1.setColor(color);
            set1.setValueTextColor(Color.rgb(60, 220, 78));
            set1.setValueTextSize(10f);
            set1.setAxisDependency(YAxis.AxisDependency.LEFT);
            iBarDataSets.add(set1);
        }







        float groupSpace = 0.06f;
        float barSpace = 0.02f; // x2 dataset
        float barWidth = 0.45f; // x2 dataset
        // (0.45 + 0.02) * 2 + 0.06 = 1.00 -> interval per "group"

        BarData d = new BarData(iBarDataSets);
//        d.setBarWidth(barWidth);

//        // make this BarData object grouped
//        d.groupBars(0, groupSpace, barSpace); // start at x = 0

        return d;


    }










    private void setDataDropDown(ArrayList<String> drop_countarray, ArrayList<String> drop_answearray) {
        int count=drop_answearray.size();



        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();



        for (int i = 0; i < count ; i++) {
            int count_int= Integer.parseInt(drop_countarray.get(i));

            String name_string = drop_answearray.get(i);

            entries.add(new PieEntry((float) count_int,
                    name_string,
                    getResources().getDrawable(R.drawable.ic_add_white)));

        }




        PieDataSet dataSet = new PieDataSet(entries, " Results");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);

        dropChart.setData(data);


        // undo all highlights
        dropChart.highlightValues(null);

        dropChart.invalidate();

    }

    @Override
    public void onHttpAnalysisFailed(String message) {

        cancelProgress();
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}
