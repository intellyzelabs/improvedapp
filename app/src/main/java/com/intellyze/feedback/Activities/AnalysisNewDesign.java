package com.intellyze.feedback.Activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.renderer.YAxisRenderer;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.intellyze.feedback.Api.AnalysisModel.AnalysisDataModel;
import com.intellyze.feedback.Api.AnalysisModel.Answer;
import com.intellyze.feedback.Api.AnalysisModel.AnswerGrid;
import com.intellyze.feedback.Api.AnalysisModel.AnswersList;
import com.intellyze.feedback.Api.AnalysisModel.ColumnsResult;
import com.intellyze.feedback.Api.AnalysisModel.HttpRequestForListAnalysis;
import com.intellyze.feedback.Api.AnalysisModel.OnHttpRequestAnalysis;
import com.intellyze.feedback.Api.AnalysisModel.QuestionMore;
import com.intellyze.feedback.Api.AnalysisModel.SurveyData;
import com.intellyze.feedback.Custom.DayAxisValueFormatter;
import com.intellyze.feedback.Custom.MyAxisValueFormatter;
import com.intellyze.feedback.Custom.MyValueFormatter;
import com.intellyze.feedback.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class AnalysisNewDesign extends BaseActivity implements OnHttpRequestAnalysis, OnChartValueSelectedListener {
    LinearLayout linear_base;
    private PieChart mChart;
    List<String> mult_answearray;
    List<String> mult_countarray;
    private ArrayList<String> drop_answearray;
    private ArrayList<String> drop_countarray;


    private ArrayList<String> linear_answearray;
    private ArrayList<String> linear_countarray;
    ArrayList<BarEntry> checkboxlist;
    private PieChart dropChart;
    private BarChart linear;

    private ArrayList<String> choice_grid_answearray;
    private ArrayList<String> choice_grid_countarray;
    private ArrayList<String> column_name_array;
    private ArrayList<String> answer_count_array;
    private BarChart mulgridlinear;
    private LinearLayout.LayoutParams lp;
    List<IBarDataSet> iBarDataSets;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);


        setContentView(R.layout.activity_analysis_new_design);
        Intent i = getIntent();
        setTitle("Analysis");
        String uid = i.getStringExtra("UID");
        Log.e( "uid: ",""+uid );
        setProgressBar();
        initilize();

        showProgress("Loading");
        getData(uid);
    }

    private void initilize() {
        linear_base = findViewById(R.id.linear);
    }

    private void getData(String surveyid) {

        AnalysisDataModel analysisDataModel = new AnalysisDataModel();
        analysisDataModel.setSurveyId(surveyid);
        HttpRequestForListAnalysis httpRequestForListAnalysis = new HttpRequestForListAnalysis(this);
        httpRequestForListAnalysis.getList(analysisDataModel);
    }

    @Override
    public void onHttpAnalysisSuccess(String Message, final SurveyData surveyData) {

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                setData(surveyData);
                //Your code to run in GUI thread here
            }//public void run() {
        });


    }

    private void setData(SurveyData surveyData) {
        Gson gson = new GsonBuilder().create();
        String payloadStr = gson.toJson(surveyData);
        System.out.println(payloadStr + "surveyid");
        BarDataSet barset;

        List<QuestionMore> questionMores = surveyData.getQuestionMore();



        for (int i = 0; i < questionMores.size(); i++) {
            final String Question_Title = questionMores.get(i).getQuestionTitle();
            String Question_type = questionMores.get(i).getQuestionType();
            if (Question_type.equals("Short answer")) {
                TextView t = new TextView(this);
                t.setText(i + 1 + "." + Question_Title);
                t.setPadding(5,15,5,25);
                t.setTextSize(20);
                t.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                t.setTypeface(null, Typeface.BOLD);
                setMargins(t,10,10,10,10);
                this.linear_base.addView(t);
                final List<AnswersList> answersList = questionMores.get(i).getAnswersList();
                TextView t2 = new TextView(this);
                t2.setText(answersList.size() + " Response");
                t2.setPadding(20,0,0,0);
                t2.setGravity(Gravity.RIGHT);
                t2.setTextSize(12);
                t2.setTextColor(Color.BLACK);
                this.linear_base.addView(t2);



                int count;
                int countfull=answersList.size();
                if (answersList.size()>=4)
                {
                    count=4;
                }
                else
                {
                    count=answersList.size();
                }
                for (int s = 0; s < count; s++) {
                    String answer = answersList.get(s).getAnswrValue();
                    TextView tv = new TextView(this);
                    tv.setText(answer);
                    tv.setPadding(30, 10, 10, 10);



                    if (!answer.equals("")) {

                        this.linear_base.addView(tv);
                    }



                }

                if (countfull>count)
                {


                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            AbsListView.LayoutParams.WRAP_CONTENT,AbsListView.LayoutParams.WRAP_CONTENT);
                    final Button btn = new Button(this);
                    // Give button an ID
                    btn.setText("Read More");
                    btn.setBackgroundColor(Color.TRANSPARENT);
                    btn.setPadding(20,0,0,0);
                    btn.setGravity(Gravity.RIGHT);
                    btn.setTextSize(12);
                    btn.setTextColor(Color.BLUE);
                    // set the layoutParams on the button
                    btn.setLayoutParams(params);
                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {



                            setNewData(Question_Title,answersList);

                            Log.e( "onClick: ","SIZE OF ARRAY"+answersList.size() );
                        }
                    });


                    linear_base.addView(btn);
                }

            }

            else if (Question_type.equals("Paragraph")) {
                TextView t = new TextView(this);
                t.setText(i + 1 + "." + Question_Title);
                t.setPadding(5,15,5,25);
                t.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                t.setTypeface(null, Typeface.BOLD);
                this.linear_base.addView(t);


                final List<AnswersList> answersList = questionMores.get(i).getAnswersList();
                TextView t2 = new TextView(this);
                t2.setPadding(20,0,0,0);
                t2.setTextSize(12);
                t2.setTextColor(Color.BLACK);
                t.setTextSize(20);
                t2.setGravity(Gravity.RIGHT);
                t2.setText(answersList.size() + " Response");

                this.linear_base.addView(t2);

                int count;
                int countfull=answersList.size();

                if (answersList.size()>=2)
                {
                    count=2;
                }
                else
                {
                    count=answersList.size();
                }
                for (int s = 0; s < count; s++) {
                    String answer = answersList.get(s).getAnswrValue();
                    TextView tv = new TextView(this);
                    tv.setText(answer);
                    tv.setPadding(30, 10, 10, 10);

                    if (!answer.equals(""))
                    {
                    this.linear_base.addView(tv);
                    }
                }

                if (countfull>count)
                {


                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            AbsListView.LayoutParams.MATCH_PARENT,AbsListView.LayoutParams.WRAP_CONTENT);
                    final Button btn = new Button(this);
                    // Give button an ID
                    btn.setText("Read More");
                    // set the layoutParams on the button
                    btn.setBackgroundColor(Color.TRANSPARENT);
                    btn.setPadding(20,0,0,0);
                    btn.setGravity(Gravity.RIGHT);
                    btn.setTextSize(12);
                    btn.setTextColor(Color.BLUE);
                    btn.setLayoutParams(params);
                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override

                        public void onClick(View view) {



                            setNewData(Question_Title,answersList);

                            Log.e( "onClick: ","SIZE OF ARRAY"+answersList.size() );
                        }
                    });


                    linear_base.addView(btn);
                }


            } else if (Question_type.equals("Multiple choice")) {
                mult_answearray = new ArrayList<>();
                mult_countarray = new ArrayList<>();
                TextView t = new TextView(this);
                t.setText(i + 1 + "." + Question_Title);
                t.setTextSize(20);
                t.setPadding(5,15,5,25);
                     t.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                t.setTypeface(null, Typeface.BOLD);
                this.linear_base.addView(t);

                List<Answer> answersList = questionMores.get(i).getAnswer();



                for (int s = 0; s < answersList.size(); s++) {
                    String answer = answersList.get(s).getOptionName();
                    String count = answersList.get(s).getOptionCount();
                    mult_countarray.add(count);
                    mult_answearray.add(answer);
                }
                setData(mult_countarray, mult_answearray);


            } else if (Question_type.equals("Dropdown")) {
                drop_answearray = new ArrayList<>();
                drop_countarray = new ArrayList<>();
                TextView t = new TextView(this);
                t.setText(i + 1 + "." + Question_Title);
                t.setPadding(5,15,5,25);
                t.setTextSize(20);
                     t.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                t.setTypeface(null, Typeface.BOLD);
                this.linear_base.addView(t);

                List<Answer> answersList = questionMores.get(i).getAnswer();



                for (int s = 0; s < answersList.size(); s++) {
                    String answer = answersList.get(s).getOptionName();
                    String count = answersList.get(s).getOptionCount();
                    drop_countarray.add(count);
                    drop_answearray.add(answer);
                }
                setDataDropDown(drop_countarray, drop_answearray);


            } else if (Question_type.equals("Multiple choice grid")) {

                TextView t = new TextView(this);
                t.setText(i + 1 + "." + Question_Title);
                t.setPadding(5,15,5,25);
                     t.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                     t.setTextSize(20);
                t.setTypeface(null, Typeface.BOLD);
                this.linear_base.addView(t);


                List<IBarDataSet> dataSets = new ArrayList<>();
                BarDataSet set1;
                List<AnswerGrid> answersList = questionMores.get(i).getAnswerGrid();


                Random rnd = new Random();
                List<ColumnsResult> columnsResults;

                List<BarEntry> entriesGroup1 = new ArrayList<>();




                for (int s = 0; s < answersList.size(); s++) {
                    columnsResults = new ArrayList<>();
                    columnsResults = answersList.get(s).getColumnsResult();
                    String answer = answersList.get(s).getRowTitle();
                    int count = answersList.get(s).getColumnsResult().size();


                    for (int j = 0; j < count; j++) {

                        String column = columnsResults.get(j).getColumnName();
                        int answertcount = Integer.parseInt(columnsResults.get(j).getAnswerCount());
                        entriesGroup1.add(new BarEntry(j, answertcount));


                    }

                    set1 = new BarDataSet(entriesGroup1, answer);
                    dataSets.add(set1);


                }

                BarData data = new BarData(dataSets);

                data.setValueFormatter(new MyValueFormatter());
                data.setValueTextSize(10f);
                data.setBarWidth(0.9f);
                data.setValueTextColor(Color.WHITE);
                mulgridlinear = new BarChart(this);
                setMultiChartNew();
                mulgridlinear.setData(data);


                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 800);
                layoutParams.gravity = Gravity.CENTER;


                this.linear_base.addView(mulgridlinear, layoutParams);
//    setBarGroupData(choice_grid_countarray,choice_grid_answearray);

            }




            else if (Question_type.equals("Checkboxes")) {
                ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
                TextView t = new TextView(this);
                t.setText(i + 1 + "." + Question_Title);
                     t.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                t.setPadding(5,15,5,25);
                t.setTypeface(null, Typeface.BOLD);
                t.setTextSize(20);
                this.linear_base.addView(t);
                float spaceForBar = 2f;
                List<Answer> answersList = questionMores.get(i).getAnswer();
                for (int s = 0; s < answersList.size(); s++) {
                    String answer = answersList.get(s).getOptionName();
                    String counts = answersList.get(s).getOptionCount();
                    checkboxlist = new ArrayList<>();
                    int answertcount = Integer.parseInt(counts);
                    checkboxlist.add(new BarEntry(s, (float) answertcount));
                    barset = new BarDataSet(checkboxlist, answer);
                    Random rnd = new Random();
                    int color = Color.rgb(77, rnd.nextInt(182), rnd.nextInt(172));
                    barset.setColor(color);
                    dataSets.add(barset);
                }

                float barWidth = 0.45f; // x2 dataset
                BarData data = new BarData(dataSets);
                data.setBarWidth(barWidth);
                data.setValueFormatter(new MyValueFormatter());
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 800);
                layoutParams.gravity = Gravity.CENTER;


                HorizontalBarChart horizontalBarChart = new HorizontalBarChart(this);
                horizontalBarChart.setDrawBarShadow(false);

                horizontalBarChart.setDrawValueAboveBar(false);

                horizontalBarChart.getDescription().setEnabled(true);

                horizontalBarChart.setMaxVisibleValueCount(60);

                // scaling can now only be done on x- and y-axis separately
                horizontalBarChart.setPinchZoom(false);

                // draw shadows for each bar that show the maximum value
//                horizontalBarChart.setDrawBarShadow(true);

                horizontalBarChart.setDrawGridBackground(false);
                horizontalBarChart.setData(data);
                horizontalBarChart.setFitBars(true);
                horizontalBarChart.animateY(2500);

                Legend l = horizontalBarChart.getLegend();
                l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
                l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
                l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
                l.setDrawInside(false);
                l.setFormSize(8f);
                l.setXEntrySpace(4f);


                XAxis xl = horizontalBarChart.getXAxis();
                xl.setPosition(XAxis.XAxisPosition.BOTTOM);
                xl.setDrawAxisLine(false);
                xl.setDrawGridLines(false);
                xl.setGranularity(10f);

////
//                YAxis yl = horizontalBarChart.getAxisLeft();
//                yl.setDrawAxisLine(true);
//                yl.setDrawGridLines(true);
//
//                yl.setAxisMinimum(0f);
//                // this replaces setStartAtZero(true)
////        yl.setInverted(true);
//
//                YAxis yr = horizontalBarChart.getAxisRight();
//                yr.setDrawAxisLine(true);
//                yr.setDrawGridLines(false);
//                yr.setAxisMinimum(0f); // this replaces setStartAtZero(true)
////        yr.setInverted(true);


                linear_base.addView(horizontalBarChart, layoutParams);


            } else if (Question_type.equals("Linear scale")) {


                ArrayList<BarEntry> yVals1;
                BarDataSet set1;
                List<IBarDataSet> sets;
                sets = new ArrayList<>();
                linear_answearray = new ArrayList<>();
                linear_countarray = new ArrayList<>();
                TextView t = new TextView(this);
                t.setText(i + 1 + "." + Question_Title);
                t.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                t.setTypeface(null, Typeface.BOLD);
                t.setTextSize(20);
                this.linear_base.addView(t);

                List<Answer> answersList = questionMores.get(i).getAnswer();



                for (int s = 0; s < answersList.size(); s++) {
                    yVals1 = new ArrayList<>();
                    String answer = answersList.get(s).getOptionName();
                    String count = answersList.get(s).getOptionCount();
                    linear_countarray.add(count);
                    linear_answearray.add(answer);
                    int answertcount = Integer.parseInt(answersList.get(s).getOptionCount());
                    String column = answersList.get(s).getOptionName();
                    yVals1.add(new BarEntry(s, (float) answertcount));
                    set1 = new BarDataSet(yVals1, column);
                    Random rnd = new Random();
                    int colors = Color.rgb(0, rnd.nextInt(121), rnd.nextInt(107));
//                    int colors = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                    set1.setColor(colors);
                    sets.add(set1);


                }


                BarData data = new BarData(sets);


                float barWidth = 0.78f; // x2 dataset
                data.setValueFormatter(new MyValueFormatter());

                data.setValueTextSize(10f);
                data.setValueTextColor(Color.WHITE);
                data.setBarWidth(barWidth);


                linear = new BarChart(this);
                setlinerChartNews();
//                linear.setData(data);


                Legend l = linear.getLegend();
                l.setVerticalAlignment(Legend.LegendVerticalAlignment.CENTER);
                l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
                l.setOrientation(Legend.LegendOrientation.VERTICAL);
                l.setDrawInside(false);
                l.setForm(Legend.LegendForm.SQUARE);

                linear.setData(data);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 800);
                layoutParams.gravity = Gravity.CENTER;


                linear_base.addView(linear, layoutParams);

            }


        }


        cancelProgress();

    }

    private void setNewData(String question_title, List<AnswersList> answersList) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
// ...Irrelevant code for customizing the buttons and title
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alertdialog_lay, null);
        dialogBuilder.setView(dialogView);

        LinearLayout layoutid=dialogView.findViewById(R.id.layoutid);

        layoutid.setPadding(20,20,20,20);

            TextView t = new TextView(this);
            t.setText(question_title);
            t.setPadding(5,15,5,25);
            t.setTextSize(20);
            t.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            t.setTypeface(null, Typeface.BOLD);
            setMargins(t,10,10,10,10);
            layoutid.addView(t);
            TextView t2 = new TextView(this);
            t2.setText(answersList.size() + " Response");
            t2.setPadding(20,0,0,0);
            t2.setGravity(Gravity.RIGHT);
            t2.setTextSize(12);
            t2.setTextColor(Color.BLACK);
            layoutid.addView(t2);


            for (int s = 0; s < answersList.size(); s++) {
                String answer = answersList.get(s).getAnswrValue();
                TextView tv = new TextView(this);
                tv.setText(answer);
                tv.setPadding(30, 10, 10, 10);



                if (!answer.equals("")) {

                    layoutid.addView(tv);
                }




        }


        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }

    private void setlinerChartNews() {


        linear.setDrawBarShadow(false);
        linear.setDrawGridBackground(false);

        XAxis xAxis = linear.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);


        linear.getAxisLeft().setDrawGridLines(false);

        linear.getLegend().setEnabled(true);

        linear.setFitBars(true);
        linear.setOnChartValueSelectedListener(this);

//        mulgridlinear.setDrawBarShadow(true);
        linear.setDrawValueAboveBar(false);

        linear.getDescription().setEnabled(false);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        linear.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        linear.setPinchZoom(true);

        linear.setDrawGridBackground(false);





    }

    @Override
    public void onHttpAnalysisFailed(String message) {
        cancelProgress();
    }

    private BarData setLinearData(ArrayList<String> linear_countarray, ArrayList<String> linear_answearray) {


        int count = linear_answearray.size();
        float start = 0;

        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();
        iBarDataSets = new ArrayList<>();

        for (int i = (int) start; i < count; i++) {

            int val = Integer.parseInt(linear_countarray.get(i));


            yVals1.add(new BarEntry(i, val, getResources().getDrawable(R.drawable.ic_add_white)));


            String value = linear_answearray.get(i);
            BarDataSet set1 = new BarDataSet(yVals1, value);
            Random rnd = new Random();
//            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
            int color = Color.rgb(77, rnd.nextInt(182), rnd.nextInt(172));

            set1.setColor(color);
            set1.setValueTextColor(Color.rgb(60, 220, 78));
            set1.setValueTextSize(10f);
            set1.setAxisDependency(YAxis.AxisDependency.LEFT);
            iBarDataSets.add(set1);
        }


        float groupSpace = 0.06f;
        float barSpace = 0.02f; // x2 dataset
        float barWidth = 0.45f; // x2 dataset
        // (0.45 + 0.02) * 2 + 0.06 = 1.00 -> interval per "group"

        BarData d = new BarData(iBarDataSets);
        d.setBarWidth(barWidth);

//        // make this BarData object grouped
        d.groupBars(0, groupSpace, barSpace); // start at x = 0

        return d;


    }

    private void setMultiChartNew() {





        mulgridlinear.setOnChartValueSelectedListener(this);

        mulgridlinear.setDrawBarShadow(true);
        mulgridlinear.setDrawValueAboveBar(false);

        mulgridlinear.getDescription().setEnabled(true);

        // if more than 60 entries are displayed in the chart, no values will be
        // drawn
        mulgridlinear.setMaxVisibleValueCount(60);

        // scaling can now only be done on x- and y-axis separately
        mulgridlinear.setPinchZoom(true);

        mulgridlinear.setDrawGridBackground(false);


        IAxisValueFormatter xAxisFormatter = new MyAxisValueFormatter();

        XAxis xAxis = mulgridlinear.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setGranularity(1f); // only intervals of 1 day

        xAxis.setValueFormatter(xAxisFormatter);

        IAxisValueFormatter custom = new MyAxisValueFormatter();
//
        YAxis leftAxis = mulgridlinear.getAxisLeft();
//        leftAxis.setValueFormatter(custom);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.INSIDE_CHART);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = mulgridlinear.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setLabelCount(8, false);
//        rightAxis.setValueFormatter(custom);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)
//
        Legend l = mulgridlinear.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);
        l.setForm(Legend.LegendForm.SQUARE);
        l.setFormSize(9f);
        l.setTextSize(11f);
        l.setXEntrySpace(4f);

    }

    private void setData(List<String> mult_countarray, List<String> mult_answearray) {
        int count = mult_answearray.size();


        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();
        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int i = 0; i < count; i++) {
            int count_int = Integer.parseInt(mult_countarray.get(i));

            String name_string = mult_answearray.get(i);
            String drop_countarrays = mult_countarray.get(i);

            if (!drop_countarrays.equals("0"))
            {
                entries.add(new PieEntry((float) count_int,
                        name_string,
                        getResources().getDrawable(R.drawable.ic_add_white)));


                Log.e( "Name String: ",""+name_string );
                Log.e( "drop_countarrays: ",""+drop_countarrays );
                Random rnd = new Random();
//            int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
//                int color = Color.rgb(77, rnd.nextInt(182), rnd.nextInt(172));
//                colors.add(color);




                for (int c : ColorTemplate.VORDIPLOM_COLORS)
                    colors.add(c);
            }



        }


        PieDataSet dataSet = new PieDataSet(entries, " ");

        dataSet.setDrawIcons(false);
        dataSet.setColors(colors);
        dataSet.setValueTextSize(11f);

        dataSet.setSliceSpace(3f);

        dataSet.setSelectionShift(5f);


        dataSet.setColors(colors);
        dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);


        mChart = new PieChart(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 800);
        layoutParams.gravity = Gravity.CENTER;
        setMultchart();
        mChart.setData(data);

        mChart.invalidate();
        this.linear_base.addView(mChart, layoutParams);


    }

    private void setDataDropDown(ArrayList<String> drop_countarray, ArrayList<String> drop_answearray) {
        int count = drop_answearray.size();


        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        ArrayList<Integer> colors = new ArrayList<Integer>();
        for (int i = 0; i < count; i++) {
            int count_int = Integer.parseInt(drop_countarray.get(i));

            String name_string = drop_answearray.get(i);
            String drop_countarrays = drop_countarray.get(i);
            Log.e( "Name String: ",""+name_string );
            Log.e( "drop_countarrays: ",""+drop_countarrays );

            if (!drop_countarrays.equals("0")) {

                Log.e( "setDataDropDown: ","data" );
                entries.add(new PieEntry((float) count_int,
                        name_string,
                        getResources().getDrawable(R.drawable.ic_add_white)));
                Random rnd = new Random();

                for (int c : ColorTemplate.VORDIPLOM_COLORS)
                    colors.add(c);
            }


        }


        PieDataSet dataSet = new PieDataSet(entries, " ");
        dataSet.setDrawIcons(false);
        dataSet.setColors(colors);
        dataSet.setValueTextSize(11f);

        dataSet.setSliceSpace(3f);

        dataSet.setSelectionShift(5f);


        dataSet.setColors(colors);
        dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.WHITE);



        dropChart = new PieChart(this);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 800);
        layoutParams.gravity = Gravity.CENTER;
        // You might want to tweak these to WRAP_CONTENT
        setDropchart();

        dropChart.setData(data);


        // undo all highlights
        dropChart.highlightValues(null);
//        dropChart.setLayoutParams(new LinearLayout.LayoutParams(400, 400));

        dropChart.invalidate();
        linear_base.addView(dropChart, layoutParams);


    }

    private void setDropchart() {

        dropChart.setUsePercentValues(true);
        dropChart.getDescription().setEnabled(false);
//        dropChart.setExtraOffsets(5, 10, 5, 5);

        dropChart.setDragDecelerationFrictionCoef(0.95f);


        dropChart.setCenterText("Improved");

        dropChart.setDrawHoleEnabled(true);
        dropChart.setHoleColor(Color.WHITE);

        dropChart.setTransparentCircleColor(Color.WHITE);
        dropChart.setTransparentCircleAlpha(110);

        dropChart.setHoleRadius(58f);
        dropChart.setTransparentCircleRadius(61f);

        dropChart.setDrawCenterText(true);

        dropChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        dropChart.setRotationEnabled(true);
        dropChart.setHighlightPerTapEnabled(true);
        Legend l = dropChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setFormSize(8f);
        l.setXEntrySpace(4f);
        l.setDrawInside(false);
        l.setXEntrySpace(0f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);
        l.setWordWrapEnabled(true);

        // entry label styling
        dropChart.setEntryLabelColor(Color.WHITE);

        dropChart.setEntryLabelTextSize(12f);


        // add a selection listener
        dropChart.setOnChartValueSelectedListener(this);

//        setData(4, 100);

        dropChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);


    }

    private void setMultchart() {


        mChart.setUsePercentValues(true);
        mChart.getDescription().setEnabled(false);
//        dropChart.setExtraOffsets(5, 10, 5, 5);

        mChart.setDragDecelerationFrictionCoef(0.95f);


        mChart.setCenterText("Improved");

        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(Color.WHITE);

        mChart.setTransparentCircleColor(Color.WHITE);
        mChart.setTransparentCircleAlpha(110);

        mChart.setHoleRadius(58f);
        mChart.setTransparentCircleRadius(61f);

        mChart.setDrawCenterText(true);

        mChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChart.setRotationEnabled(true);
        mChart.setHighlightPerTapEnabled(true);
        Legend l = mChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setFormSize(8f);
        l.setDrawInside(false);
        l.setXEntrySpace(5f);
        l.setYEntrySpace(5f);
        l.setYOffset(5f);
        l.setWordWrapEnabled(true);

        // entry label styling
        mChart.setEntryLabelColor(Color.WHITE);

        mChart.setEntryLabelTextSize(12f);

        mChart.setOnChartValueSelectedListener(this);



        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);




    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }

    public void onBackClick(View view) {
        finish();
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void setMargins (View view, int left, int top, int right, int bottom) {
        if (view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams) {
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
            p.setMargins(left, top, right, bottom);
            view.requestLayout();
        }
    }
}
