package com.intellyze.feedback.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.intellyze.feedback.R;
import com.intellyze.feedback.Utils.UserPref;

/**
 * Created by work on 9/25/2017.
 */

public class SplashScreen extends Activity {
    public static final int TIME_OUT = 3000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

//        checkIntent(getIntent());
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                UserPref userPref= new UserPref(getApplicationContext());
                String flage = userPref.getUserName();
                if (flage.equals(""))
//
                {
                    Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                }


            }
        }, TIME_OUT);

    }
//    public void checkIntent(Intent intent) {
//        Log.e( "checkIntent: ","on recive intent" );
//
//        if (intent.hasExtra("click_action")) {
//
//
//            Log.e("checkIntent: ","click_action"+intent.getStringExtra("click_action") );
//


//            sessionManager.SaveNotification(true);
//            int studentid= Integer.parseInt(intent.getStringExtra("student_id"));
//            Log.e("STUDENT ID",""+studentid);
//            if(studentid!=0)
//            {
//                sessionManager.saveStudentId(studentid);
//
//            }
//            else
//            {
//                Log.e("STUDENT ID","empty");
//            }

//            int classid= Integer.parseInt(intent.getStringExtra("class_id"));
//            Log.e("CLASS_ID",""+classid);
//            if(classid!=0)
//            {
//                sessionManager.saveClassId(classid);
//
//            }
//            else
//            {
//                Log.e("STUDENT ID","empty");
//            }


//            String admissionNo= intent.getStringExtra("admissionNo");
//            Log.e("CLASS_ID",""+admissionNo);
//            if(!admissionNo.equals("0"))
//            {
//                sessionManager.setAdmissinNumber(admissionNo);
//
//            }
//            else
//            {
//                Log.e("admissionNo  ID","empty");
//            }


//
//            String examId= intent.getStringExtra("examId") ;
//            Log.e("CLASS_ID",""+examId);
//            if(!examId.equals("0"))
//            {
//                sessionManager.setExamId(examId);
//
//            }
//            else
//            {
//                Log.e("STUDENT ID","empty");
//            }

//            ClickActionHelper.startActivity(intent.getStringExtra("click_action"), intent.getExtras(), this);
////            flag = false;
//
//        } else {
//
////            flag = true;
//        }


}

//    @Override
//    protected void onNewIntent(Intent intent) {
//        super.onNewIntent(intent);
//
//        Log.e("NEW intent","Splash Screen");
//        checkIntent(intent);
//    }
//}