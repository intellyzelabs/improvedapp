package com.intellyze.feedback.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Scroller;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.intellyze.feedback.Api.AnswerDetails.Answer;
import com.intellyze.feedback.Api.AnswerDetails.AnswerDetailModelParameter;
import com.intellyze.feedback.Api.AnswerDetails.ColumnsResult;
import com.intellyze.feedback.Api.AnswerDetails.HttpRequestForAnswerDetailsSurvey;
import com.intellyze.feedback.Api.AnswerDetails.LinearQuestionInfo;
import com.intellyze.feedback.Api.AnswerDetails.OnHttpRequestForSurveyAnswerDetaisSurvey;
import com.intellyze.feedback.Api.AnswerDetails.QuestionMore;
import com.intellyze.feedback.Api.AnswerDetails.SurveyData;
import com.intellyze.feedback.Custom.EditTextRegular;
import com.intellyze.feedback.Custom.TextViewRegular;
import com.intellyze.feedback.Custom.TextViewSemiBold;
import com.intellyze.feedback.Models.CheckBoxDetails;
import com.intellyze.feedback.Models.ColumnDetailsId;
import com.intellyze.feedback.Models.DropDownAnswerModel;
import com.intellyze.feedback.Models.GridRadioAnswerMultipleModel;
import com.intellyze.feedback.Models.LinearAnswerModells;
import com.intellyze.feedback.Models.LongAnswerModel;
import com.intellyze.feedback.Models.MultipleChoiceCheckBoxModel;
import com.intellyze.feedback.Models.MultipleChoiceRadioModel;
import com.intellyze.feedback.Models.RadioGroupDetails;
import com.intellyze.feedback.R;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.Column;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.Linear;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.MultipleChoice;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

public class SurveyDetailsPerCustomer extends BaseActivity implements OnHttpRequestForSurveyAnswerDetaisSurvey {
    TextView tvPrev, tvFinish, tvSkip, iv, tvShortAnwer;
    private ImageView imgSurveyImage;
    TextView tvCreatedOn, tv_lastEntry, tvSurveyName, tvSurveyDescription;
    LinearLayout contentPanel;
    private String first;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_details_per_customer);
        initViews();
        Intent intent = getIntent();
        String surveyid = intent.getStringExtra("surveyid");
        String answerid = intent.getStringExtra("answerid");
        setProgressBar();
        showProgress("Loading");
        getData(surveyid,answerid);
    }
    private void initViews() {
   
        imgSurveyImage = (ImageView) findViewById(R.id.imgSurveyImage);
        contentPanel = (LinearLayout) findViewById(R.id.contentPanel);
        tvSurveyDescription = (TextView) findViewById(R.id.tvSurveyDescription);
        tvSurveyName = (TextView) findViewById(R.id.tvSurveyName);
        tvCreatedOn = (TextView)findViewById(R.id.tvCreatedOn);
        tv_lastEntry = (TextView) findViewById(R.id.tv_lastEntry);

    }
    private void getData(String surveyid, String answerid) {
        AnswerDetailModelParameter answerDetailModelParameter=new AnswerDetailModelParameter();
        answerDetailModelParameter.setAnswerId(answerid);
        answerDetailModelParameter.setSurveyId(surveyid);
        HttpRequestForAnswerDetailsSurvey httpRequestForAnswerDetailsSurvey=new HttpRequestForAnswerDetailsSurvey(this);
        httpRequestForAnswerDetailsSurvey.getAnswerDetails(answerDetailModelParameter);
    }

    @Override
    public void onHttpAddForSurveyAnswerDetaisSurveySuccess(SurveyData list, String Message) {

        setData(list);

cancelProgress();
        Toast.makeText(this, ""+Message, Toast.LENGTH_SHORT).show();
    }
    private CardView drawCard(int id) {
        CardView card = new CardView(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(10, 10, 10, 10);
        card.setLayoutParams(params);
        card.setRadius(9);
        card.setContentPadding(15, 15, 15, 45);
        card.setCardBackgroundColor(getResources().getColor(R.color.colorWhite));
        card.setMaxCardElevation(10);
        card.setCardElevation(5);
        card.setId(id);
        return card;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void setData(SurveyData list) {


        tvSurveyDescription.setText(list.getDetails().get(0).getSurveyDescr());
        tvSurveyName.setText(list.getDetails().get(0).getSurveyTitle());
        tvCreatedOn.setText(list.getDetails().get(0).getSurveyDate());
        tv_lastEntry.setText(list.getDetails().get(0).getSurveyTime());


        List<QuestionMore> mQuestionMore=list.getQuestionMore();


for (int a=0;a<mQuestionMore.size();a++)
{
    LinearLayout ds = new LinearLayout(this);
    ds.setGravity(Gravity.TOP);
    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


    ds.setLayoutParams(params);
    ds.setGravity(Gravity.CENTER);

    String mQuestionType=mQuestionMore.get(a).getQuestionType();
    if(mQuestionType.equals("Short answer"))
    {
        CardView card = drawCard(Integer.parseInt(mQuestionMore.get(a).getQuestionId()));

        LinearLayout layout = new LinearLayout(this);
        layout.setGravity(Gravity.CENTER);
        layout.setOrientation(LinearLayout.VERTICAL);

        tvShortAnwer = newtextViewQuestion();
        first = mQuestionMore.get(a).getQuestionNumber() + ". " + mQuestionMore.get(a).getQuestionTitle() + " ?";
        tvShortAnwer.setText(first);
        layout.addView(tvShortAnwer);
        if (!mQuestionMore.get(a).getQuestionImage().equals("")) {
            Bitmap imageToBind = decodeImage(mQuestionMore.get(a).getQuestionImage());
            ImageView nkj = newImageview(this, imageToBind);
            layout.addView(nkj);
        }
        EditText et = newedittextz("Short Answer");
        et.setScroller(new Scroller(this));
        et.setMinLines(2);
        et.setVerticalScrollBarEnabled(true);
        et.setEnabled(false);


        if (mQuestionMore.get(a).getAnswersList().size()!=0) {
            et.setText(mQuestionMore.get(a).getAnswersList().get(0).getAnswrValue());
        }

        layout.addView(et);


        if (mQuestionMore.get(a).getIsRequired().equals("1")) {
            View view = new View(this);
            LinearLayout.LayoutParams viewparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2);
            viewparams.setMargins(10, 0, 10, 0);
            viewparams.gravity=Gravity.CENTER;

            view.setBackgroundColor(Color.parseColor("#dedede"));
            layout.addView(view);
            TextView textView = newtextViewRequired();
            textView.setText("Required");
            layout.addView(textView);
        }

        card.addView(layout);
//                                ds.setGravity(Gravity.CENTER);

        ds.addView(card);


        contentPanel.addView(ds);

    }
    else
        if(mQuestionType.equals("Paragraph"))
    {
        CardView card = drawCard(Integer.parseInt(mQuestionMore.get(a).getQuestionId()));

        LinearLayout layout = new LinearLayout(this);
        layout.setGravity(Gravity.CENTER);
        layout.setOrientation(LinearLayout.VERTICAL);

        tvShortAnwer = newtextViewQuestion();
        first = mQuestionMore.get(a).getQuestionNumber() + ". " + mQuestionMore.get(a).getQuestionTitle() + " ?";
        tvShortAnwer.setText(first);
        layout.addView(tvShortAnwer);
        if (!mQuestionMore.get(a).getQuestionImage().equals("")) {
            Bitmap imageToBind = decodeImage(mQuestionMore.get(a).getQuestionImage());
            ImageView nkj = newImageview(this, imageToBind);
            layout.addView(nkj);
        }
        EditText et = newedittextz("Long Answer");
        et.setScroller(new Scroller(this));
        et.setMinLines(2);
        et.setVerticalScrollBarEnabled(true);
        et.setEnabled(false);
        if (mQuestionMore.get(a).getAnswersList().size()!=0) {
            et.setText(mQuestionMore.get(a).getAnswersList().get(0).getAnswrValue());
        }
        layout.addView(et);


        if (mQuestionMore.get(a).getIsRequired().equals("1")) {
            View view = new View(this);
            LinearLayout.LayoutParams viewparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2);
            viewparams.setMargins(30, 0, 30, 0);
            viewparams.gravity=Gravity.CENTER;

            view.setBackgroundColor(Color.parseColor("#dedede"));
            layout.addView(view);
            TextView textView = newtextViewRequired();
            textView.setText("Required");
            layout.addView(textView);
        }

        card.addView(layout);
//                                ds.setGravity(Gravity.CENTER);

        ds.addView(card);

        contentPanel.addView(ds);



    }
    else if(mQuestionType.equals("Multiple choice"))
        {
            CardView card = drawCard(Integer.parseInt(mQuestionMore.get(a).getQuestionId()));
            LinearLayout layout = new LinearLayout(this);
            layout.setOrientation(LinearLayout.VERTICAL);
            iv = newtextViewQuestion();
            iv.setText(mQuestionMore.get(a).getQuestionNumber() + ". " + mQuestionMore.get(a).getQuestionTitle() );
            layout.addView(iv);

            if (!mQuestionMore.get(a).getQuestionImage().equals("")) {
                Bitmap imageToBind = decodeImage(mQuestionMore.get(a).getQuestionImage());
                ImageView nkj = newImageview(this, imageToBind);
                layout.addView(nkj);
            }

            RadioGroup rg = createRadioGroup(mQuestionMore.get(a).getAnswer().size(), mQuestionMore.get(a).getAnswer(), mQuestionMore.get(a).getQuestionId(), Integer.parseInt(mQuestionMore.get(a).getIsOther()), layout);


            if (mQuestionMore.get(a).getIsRequired().equals("1")) {
                View view = new View(this);
                LinearLayout.LayoutParams viewparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2);
                viewparams.setMargins(30, 100, 30, 0);
                view.setLayoutParams(viewparams);
                view.setBackgroundColor(Color.parseColor("#dedede"));
                layout.addView(view);
                TextView textView = newtextViewRequired();
                textView.setText("Required");
                layout.addView(textView);
            }

            card.addView(layout);
            ds.addView(card);
            contentPanel.addView(ds);


        } else if (mQuestionType.equals("Checkboxes")) {
            CardView card = drawCard(Integer.parseInt(mQuestionMore.get(a).getQuestionId()));
            LinearLayout layout = new LinearLayout(this);
            layout.setOrientation(LinearLayout.VERTICAL);
            iv = newtextViewQuestion();
            iv.setText(mQuestionMore.get(a).getQuestionNumber() + ". " + mQuestionMore.get(a).getQuestionTitle());
            layout.addView(iv);
            if (!mQuestionMore.get(a).getQuestionImage().equals("")) {
                Bitmap imageToBind = decodeImage(mQuestionMore.get(a).getQuestionImage());
                ImageView nkj = newImageview(this, imageToBind);
                layout.addView(nkj);
            }
            LinearLayout.LayoutParams radioparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            radioparams.setMargins(0, 30, 0, 0);
            LinearLayout fdh = new LinearLayout(this);
            fdh.setOrientation(LinearLayout.VERTICAL);
            fdh.setLayoutParams(radioparams);
            MultipleChoiceCheckBoxModel multipleChoiceCheckBoxModel = new MultipleChoiceCheckBoxModel();
            multipleChoiceCheckBoxModel.setQuestionId(Integer.parseInt(mQuestionMore.get(a).getQuestionId()));
            multipleChoiceCheckBoxModel.setQuestionType("Multiple choice");
            List<CheckBoxDetails> checkBoxDetailss = new ArrayList<CheckBoxDetails>();
            List<Integer> answerIds = new ArrayList<>();
            for (int ij = 0; ij < mQuestionMore.get(a).getAnswer().size(); ij++) {
                CheckBox cb = new CheckBox(this);
                CheckBoxDetails checkBoxDetails = new CheckBoxDetails();
                if(mQuestionMore.get(a).getAnswer().get(ij).getIsAnswer().equals("1"))
                {
                    cb.setChecked(true);
                    cb.setTextColor(Color.BLACK);
                }
                cb.setEnabled(false);
                checkBoxDetails.setCheckBoxes(cb);
                cb.setText(mQuestionMore.get(a).getAnswer().get(ij).getOptionName());
                Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/WorkSans-Regular.ttf");
                cb.setTypeface(font);
                fdh.addView(cb);
                checkBoxDetailss.add(checkBoxDetails);
                answerIds.add(Integer.valueOf(mQuestionMore.get(a).getAnswer().get(ij).getIsAnswer()));
            }
            layout.addView(fdh);
            multipleChoiceCheckBoxModel.setCheckBoxes(checkBoxDetailss);
            if (mQuestionMore.get(a).getIsOther().equals("1")) {
                EditText otherOpt = newedittextz("Other");
                multipleChoiceCheckBoxModel.setEditText(otherOpt);
                layout.addView(otherOpt);
            }
            multipleChoiceCheckBoxModel.setOptionIds(answerIds);
//            multipleChoiceCheckBoxModels.add(multipleChoiceCheckBoxModel);


            if (mQuestionMore.get(a).getIsRequired().equals("1")) {
                View view = new View(this);
                LinearLayout.LayoutParams viewparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2);
                viewparams.setMargins(30, 100, 30, 0);
                view.setLayoutParams(viewparams);
                view.setBackgroundColor(Color.parseColor("#dedede"));

                layout.addView(view);
                TextView textView = newtextViewRequired();
                textView.setText("Required");
                layout.addView(textView);
            }

            card.addView(layout);
            ds.addView(card);
            contentPanel.addView(ds);
        } else if (mQuestionType.equals("Dropdown")) {
            CardView card = drawCard(Integer.parseInt(mQuestionMore.get(a).getQuestionId()));
            LinearLayout layout = new LinearLayout(this);
            layout.setOrientation(LinearLayout.VERTICAL);
            iv = newtextViewQuestion();
            iv.setText(mQuestionMore.get(a).getQuestionNumber() + ". " + mQuestionMore.get(a).getQuestionTitle());
            layout.addView(iv);
            if (!mQuestionMore.get(a).getQuestionImage().equals("")) {
                Bitmap imageToBind = decodeImage(mQuestionMore.get(a).getQuestionImage());
                ImageView nkj = newImageview(this, imageToBind);
                layout.addView(nkj);
            }

            LinearLayout.LayoutParams radioparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            radioparams.setMargins(0, 30, 0, 0);
            LinearLayout fdh = new LinearLayout(this);
            ArrayList<String> spinnerArray = new ArrayList<String>();
            List<Integer> answerId = new ArrayList<Integer>();
            for (int ij = 0; ij < mQuestionMore.get(a).getAnswer().size(); ij++) {

                if (mQuestionMore.get(a).getAnswer().get(ij).getIsAnswer().equals("1"))
                {
                    tvShortAnwer = newtextNormalViewQuestion();
                    first = mQuestionMore.get(a).getAnswer().get(ij).getOptionName();
                    tvShortAnwer.setText(first);
                    layout.addView(tvShortAnwer);
                }

            }



            layout.addView(fdh);
            int jjsdf = Integer.parseInt(mQuestionMore.get(a).getIsOther());
            EditText otherOpt = newedittextz("Other");
            if (jjsdf == 1) {
                layout.addView(otherOpt);
            }
            if (mQuestionMore.get(a).getIsRequired().equals("1")) {
                View view = new View(this);
                LinearLayout.LayoutParams viewparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2);
                viewparams.setMargins(30, 100, 30, 0);
                view.setLayoutParams(viewparams);
                view.setBackgroundColor(Color.parseColor("#dedede"));
                layout.addView(view);
                TextView textView = newtextViewRequired();
                textView.setText("Required");
                layout.addView(textView);
            }

            card.addView(layout);
            ds.addView(card);
            contentPanel.addView(ds);
        } else if (mQuestionType.equals("Linear scale")) {
            CardView card = drawCard(Integer.parseInt(mQuestionMore.get(a).getQuestionId()));
            LinearLayout layout = new LinearLayout(this);
            layout.setOrientation(LinearLayout.VERTICAL);
            iv = newtextViewQuestion();
            iv.setText(mQuestionMore.get(a).getQuestionNumber() + ". " + mQuestionMore.get(a).getQuestionTitle() );
            layout.addView(iv);

            if (!mQuestionMore.get(a).getQuestionImage().equals("")) {
                Bitmap imageToBind = decodeImage(mQuestionMore.get(a).getQuestionImage());
                ImageView nkj = newImageview(this, imageToBind);
                layout.addView(nkj);
            }
            LinearLayout layoutInner = new LinearLayout(this);
            layoutInner.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams Lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            Lparams.setMargins(0, 0, 10, 0);
            LinearLayout layoutOutInner = new LinearLayout(this);
            layoutOutInner.setOrientation(LinearLayout.HORIZONTAL);
            layoutOutInner.setLayoutParams(Lparams);
            TextView Upv = newtextViewWrap();


            List<LinearQuestionInfo> linearQuestionInfo=mQuestionMore.get(a).getLinearQuestionInfo();



            String startText=linearQuestionInfo.get(0).getStartText();
            String endText=linearQuestionInfo.get(0).getEndText();

            Upv.setText(startText);
            layoutOutInner.addView(Upv);
            TextView Lpv = newtextViewWrap();
            Lpv.setGravity(Gravity.RIGHT);
            Lpv.setText(endText);
            layoutOutInner.addView(Lpv);
            layoutInner.addView(layoutOutInner);


            int startValue = Integer.parseInt(mQuestionMore.get(a).getLinearQuestionInfo().get(0).getStartValue());
            int endValue = Integer.parseInt(mQuestionMore.get(a).getLinearQuestionInfo().get(0).getEndValue());
            int questAnswerId = Integer.parseInt(mQuestionMore.get(a).getLinearQuestionInfo().get(0).getQlinearId());
            List<Answer> answeristick=mQuestionMore.get(a).getAnswer();
            RadioGroup rg = drawRadioGroupTextTop(startValue, endValue, questAnswerId, mQuestionMore.get(a).getQuestionId(),answeristick);
            layoutInner.addView(rg);
            layout.addView(layoutInner);
            int jjsdf = Integer.parseInt(mQuestionMore.get(a).getIsOther());
            EditText otherOpt = newedittextz("Other");
            if (jjsdf == 1) {
                layout.addView(otherOpt);
            }
            if (mQuestionMore.get(a).getIsRequired().equals("1")) {
                View view = new View(this);
                LinearLayout.LayoutParams viewparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2);
                viewparams.setMargins(30, 100, 30, 0);
                view.setLayoutParams(viewparams);
                view.setBackgroundColor(Color.parseColor("#dedede"));

                layout.addView(view);
                TextView textView = newtextViewRequired();
                textView.setText("Required");
                layout.addView(textView);
            }

            card.addView(layout);
            ds.addView(card);
            contentPanel.addView(ds);
        }

        else if (mQuestionType.equals("Multiple choice grid")) {
            CardView card = drawCard(Integer.parseInt(mQuestionMore.get(a).getQuestionId()));
            LinearLayout layout = new LinearLayout(this);
            layout.setOrientation(LinearLayout.VERTICAL);
            iv = newtextViewQuestion();
            iv.setText(mQuestionMore.get(a).getQuestionNumber() + ". " + mQuestionMore.get(a).getQuestionTitle() + " ?");
            layout.addView(iv);
            if (!mQuestionMore.get(a).getQuestionImage().equals("")) {
                Bitmap imageToBind = decodeImage(mQuestionMore.get(a).getQuestionImage());
                ImageView nkj = newImageview(this, imageToBind);
                layout.addView(nkj);
            }

            HorizontalScrollView scroll = new HorizontalScrollView(this);
            scroll.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            scroll.setFillViewport(true);
            scroll.setHorizontalScrollBarEnabled(false);
            LinearLayout layoutOuter = new LinearLayout(this);
            layoutOuter.setOrientation(LinearLayout.HORIZONTAL);
            LinearLayout.LayoutParams LparamsRowName = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            LparamsRowName.setMargins(10, 40, 0, 0);
            LinearLayout layoutInner = new LinearLayout(this);
            layoutInner.setOrientation(LinearLayout.VERTICAL);
            GridRadioAnswerMultipleModel gridRadioAnswerMultipleModel = new GridRadioAnswerMultipleModel();
            gridRadioAnswerMultipleModel.setQuestionType("Multiple choice grid");
            List<RadioGroupDetails> radioGroupDetailslist = new ArrayList<RadioGroupDetails>();
            for (int o = 0; o < mQuestionMore.get(a).getAnswerGrid().size(); o++) {
                List<ColumnDetailsId> columnDetailsIdsList = new ArrayList<ColumnDetailsId>();

                RadioGroupDetails radioGroupDetails = new RadioGroupDetails();
                RadioGroup rg = drawRadioGroupGridNEw(o, mQuestionMore.get(a).getAnswerGrid().get(o).getColumnsResult(),mQuestionMore.get(a).getAnswerGrid().get(o).getRowTitle(), radioGroupDetails, columnDetailsIdsList);
                radioGroupDetails.setGroup(rg);
//                radioGroupDetails.setRowId(Integer.parseInt(mQuestionMore.get(a).getAnswerGrid().get(0).getRowTitle()));
                radioGroupDetailslist.add(radioGroupDetails);
                layoutInner.addView(rg);
                gridRadioAnswerMultipleModel.setRadioGroupDetails(radioGroupDetailslist);
                gridRadioAnswerMultipleModel.setQuestionId(Integer.parseInt(mQuestionMore.get(a).getQuestionId()));
                gridRadioAnswerMultipleModel.setColumnDetailsIds(columnDetailsIdsList);
            }

            layoutOuter.addView(layoutInner);
            scroll.addView(layoutOuter);
            layout.addView(scroll);
            int jjsdf = Integer.parseInt(mQuestionMore.get(a).getIsOther());
            EditText otherOpt = newedittextz("Other");
            if (jjsdf == 1) {
                layout.addView(otherOpt);


            }
            gridRadioAnswerMultipleModel.setEditText(otherOpt);
//            gridRadioAnswerMultipleModels.add(gridRadioAnswerMultipleModel);
            if (mQuestionMore.get(a).getIsRequired().equals("1")) {
                View view = new View(this);
                LinearLayout.LayoutParams viewparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2);
                viewparams.setMargins(30, 100, 30, 0);
                view.setLayoutParams(viewparams);
                view.setBackgroundColor(Color.parseColor("#dedede"));

                layout.addView(view);
                TextView textView = newtextViewRequired();
                textView.setText("Required");
                layout.addView(textView);
            }

            card.addView(layout);
            ds.addView(card);
            contentPanel.addView(ds);
        }



}





    }

    private RadioGroup drawRadioGroupTextTop(int startValue, int endValue, int questAnswerId, String questionId,List<Answer> istic) {
        int limit = 0;
        if (startValue == 0) {
            limit = endValue + 1;
        }
        if (startValue == 1) {
            limit = endValue;
        }
        LinearLayout.LayoutParams radioparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        radioparams.setMargins(0, 30, 0, 0);
        final RadioButton[] rb = new RadioButton[limit];
        RadioGroup rg = new RadioGroup(this); //create the RadioGroup
        rg.setGravity(Gravity.CENTER);
        rg.setOrientation(RadioGroup.HORIZONTAL);//or RadioGroup.VERTICAL
        rg.setLayoutParams(radioparams);
        LinearAnswerModells linearAnswerModells = new LinearAnswerModells();
        linearAnswerModells.setQuestionId(Integer.parseInt(questionId));
        linearAnswerModells.setAnswerId(questAnswerId);
        linearAnswerModells.setQuestionType("Linear scale");
        linearAnswerModells.setRadioGroup(rg);
//        linearAnswerModellss.add(linearAnswerModells);
        int i = 0;
        while (startValue <= endValue) {
            rb[i] = new RadioButton(this);
            rb[i].setText("" + startValue);
            rb[i].setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
            rb[i].setButtonDrawable(null);
            String check=istic.get(i).getIsAnswer();
            if(check.equals("1"))
            {
                rb[i].setChecked(true);
                rb[i].setTextColor(Color.BLACK);
            }
            rb[i].setEnabled(false);
            Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/WorkSans-Regular.ttf");
            rb[i].setTypeface(font);
            TypedArray a = this.getTheme().obtainStyledAttributes(R.style.AppTheme, new int[]{android.R.attr.listChoiceIndicatorSingle});
            int attributeResourceId = a.getResourceId(0, 0);
            Drawable drawable = ContextCompat.getDrawable(this,attributeResourceId);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorView));
            rb[i].setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable);
            rb[i].setGravity(Gravity.CENTER | Gravity.BOTTOM);
            rg.addView(rb[i]);
            i++;
            startValue++;
        }
        return rg;
    }

    @Override
    public void onHttpAddFoListSurveyAnswerDetaisSurveyFailed(String message) {
        Toast.makeText(this, ""+message, Toast.LENGTH_SHORT).show();
cancelProgress();
    }

    public Bitmap decodeImage(String encodedImage) {
        byte[] decodedString = Base64.decode(encodedImage, Base64.NO_WRAP);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }



    private TextView newtextViewWrap() {
        final TextViewRegular textView = new TextViewRegular(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        params.setMargins(10, 30, 0, 0);
        textView.setLayoutParams(params);
        textView.setTextSize(14);
        textView.setTextColor(Color.rgb(0, 0, 0));
        return textView;
    }

    private TextView newtextViewWrapGrid(String j) {
        final TextViewRegular textView = new TextViewRegular(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(10, 30, 0, 0);
        textView.setLayoutParams(params);
        textView.setTextSize(14);
        textView.setText(j);
        textView.setTextColor(Color.rgb(0, 0, 0));
        return textView;
    }

    private RadioGroup drawRadioGroupGrid(int o, List<ColumnsResult> columnDetails, String row_name, RadioGroupDetails radioGroupDetails, List<ColumnDetailsId> columnDetailsIdsList) {
        final RadioButton[] rb = new RadioButton[columnDetails.size()];
        RadioGroup rg = new RadioGroup(this); //create the RadioGroup
        rg.setGravity(Gravity.CENTER);
        TextView txt = newtextViewWrapGrid(row_name);
        radioGroupDetails.setTextViews(row_name);
        LinearLayout.LayoutParams ledt = new LinearLayout.LayoutParams(250, LinearLayout.LayoutParams.WRAP_CONTENT);
        txt.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        txt.setLayoutParams(ledt);
        txt.setTextSize(12);
        rg.addView(txt);
        rg.setOrientation(RadioGroup.HORIZONTAL);//or RadioGroup.VERTICAL
        LinearLayout.LayoutParams LparamsRowNamee = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        LparamsRowNamee.setMargins(0, 0, 0, 0);
        for (int ii = 0; ii < columnDetails.size(); ii++) {
            ColumnDetailsId columnDetailsId = new ColumnDetailsId();
//            columnDetailsId.setColIds(Integer.parseInt(columnDetails.get(ii).getColumnName()));
            rb[ii] = new RadioButton(this);
            rb[ii].setText(columnDetails.get(ii).getColumnName());
            if (o == 0) {
                rb[ii].setTextColor(getResources().getColor(R.color.colorTextBlack));
            } else {
                rb[ii].setTextColor(getResources().getColor(R.color.colorWhite));
            }
            rb[ii].setLayoutParams(LparamsRowNamee);
            rb[ii].setTextSize(12);

            if(columnDetails.get(ii).getIsAnswer().equals("1"))
            {
                rb[ii].setChecked(true);
                rb[ii].setTextColor(Color.BLACK);
            }
            rb[ii].setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
            rb[ii].setId(ii + 100);
            Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/WorkSans-Regular.ttf");
            rb[ii].setTypeface(font);
            rb[ii].setPadding(20, 0, 20, 0);
            rb[ii].setButtonDrawable(null);
            TypedArray a = this.getTheme().obtainStyledAttributes(R.style.AppTheme, new int[]{android.R.attr.listChoiceIndicatorSingle});
            int attributeResourceId = a.getResourceId(0, 0);
            Drawable drawable = ContextCompat.getDrawable(this,attributeResourceId);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorView));
            rb[ii].setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable);
            rb[ii].setGravity(Gravity.CENTER | Gravity.BOTTOM);
            rg.addView(rb[ii]);
            columnDetailsIdsList.add(columnDetailsId);
        }
        return rg;
    }
    private RadioGroup drawRadioGroupGridNEw(int o, List<ColumnsResult> columnDetails, String row_name, RadioGroupDetails radioGroupDetails, List<ColumnDetailsId> columnDetailsIdsList) {
        final RadioButton[] rb = new RadioButton[columnDetails.size()];
        RadioGroup rg = new RadioGroup(this); //create the RadioGroup
        rg.setGravity(Gravity.CENTER);
        TextView txt = newtextViewWrapGrid(row_name);
        radioGroupDetails.setTextViews(row_name);
        LinearLayout.LayoutParams ledt = new LinearLayout.LayoutParams(250, LinearLayout.LayoutParams.WRAP_CONTENT);
        txt.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        txt.setLayoutParams(ledt);
        txt.setTextSize(12);
        rg.addView(txt);
        rg.setOrientation(RadioGroup.HORIZONTAL);//or RadioGroup.VERTICAL
        LinearLayout.LayoutParams LparamsRowNamee = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        LparamsRowNamee.setMargins(0, 0, 0, 0);
        for (int ii = 0; ii < columnDetails.size(); ii++) {
            ColumnDetailsId columnDetailsId = new ColumnDetailsId();
//            columnDetailsId.setColIds(Integer.parseInt(columnDetails.get(ii).getColumnName()));
            rb[ii] = new RadioButton(this);
            rb[ii].setText(columnDetails.get(ii).getColumnName());
            if (o == 0) {
                rb[ii].setTextColor(getResources().getColor(R.color.colorTextBlack));
            } else {
                rb[ii].setTextColor(getResources().getColor(R.color.colorWhite));
            }
            rb[ii].setLayoutParams(LparamsRowNamee);
            rb[ii].setTextSize(12);

            if(columnDetails.get(ii).getIsAnswer().equals("1"))
            {
                rb[ii].setChecked(true);
                rb[ii].setTextColor(Color.BLACK);
            }
            rb[ii].setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
            rb[ii].setId(ii + 100);
            Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/WorkSans-Regular.ttf");
            rb[ii].setTypeface(font);
            rb[ii].setPadding(20, 0, 20, 0);
            rb[ii].setButtonDrawable(null);
            TypedArray a = this.getTheme().obtainStyledAttributes(R.style.AppTheme, new int[]{android.R.attr.listChoiceIndicatorSingle});
            int attributeResourceId = a.getResourceId(0, 0);
            Drawable drawable = ContextCompat.getDrawable(this,attributeResourceId);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorView));
            rb[ii].setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable);
            rb[ii].setGravity(Gravity.CENTER | Gravity.BOTTOM);
            rg.addView(rb[ii]);
            columnDetailsIdsList.add(columnDetailsId);
        }
        return rg;
    }

    private RadioGroup createRadioGroup(int size, List<Answer> multiplechoicesingleDetails, String questId, int isOther, LinearLayout layout) {
        LinearLayout.LayoutParams radioparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        radioparams.setMargins(0, 30, 0, 0);
        RadioGroup rg = new RadioGroup(this); //create the RadioGroup
        final RadioButton[] rb = new RadioButton[size];
        rg.setOrientation(RadioGroup.VERTICAL);//or RadioGroup.VERTICAL
        rg.setLayoutParams(radioparams);
        MultipleChoiceRadioModel multipleChoiceRadioModel = new MultipleChoiceRadioModel();
        multipleChoiceRadioModel.setQuestionType("Multiple choice");
        List<Integer> dcv = new ArrayList<>();
        for (int ii = 0; ii < size; ii++) {
            rb[ii] = new RadioButton(this);
            if(multiplechoicesingleDetails.get(ii).getIsAnswer().equals("1"))
            {
                rb[ii].setChecked(true);
                rb[ii].setTextColor(Color.BLACK);
            }
            rb[ii].setEnabled(false);
            rb[ii].setText(multiplechoicesingleDetails.get(ii).getOptionName());
            rb[ii].setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER);
            rb[ii].setId(ii + 100);
            Typeface font = Typeface.createFromAsset(this.getAssets(), "fonts/WorkSans-Regular.ttf");
            rb[ii].setTypeface(font);
            rg.addView(rb[ii]);
            dcv.add(Integer.valueOf(multiplechoicesingleDetails.get(ii).getIsAnswer()));
        }

        layout.addView(rg);
        if (isOther == 1) {
            EditText otherOpt = newedittextz("Other");

            multipleChoiceRadioModel.setEditText(otherOpt);


            layout.addView(otherOpt);
        }
        multipleChoiceRadioModel.setQuestionId(dcv);
        multipleChoiceRadioModel.setQuestId(Integer.parseInt(questId));
        multipleChoiceRadioModel.setRadioGroup(rg);
//        multipleChoiceRadioModels.add(multipleChoiceRadioModel);
        return rg;
    }

    private TextView newtextViewQuestion() {
        final TextViewSemiBold textView = new TextViewSemiBold(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(10, 100, 0, 30);
        textView.setLayoutParams(params);
        textView.setTextSize(17);
        textView.setTextColor(this.getResources().getColor(R.color.colorPrimaryDark));
        textView.setMaxEms(2);
        return textView;
    }


    private TextView newtextNormalViewQuestion() {
        final TextViewSemiBold textView = new TextViewSemiBold(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(20, 10, 0, 30);
        textView.setLayoutParams(params);
        textView.setTextSize(17);
        textView.setTextColor(this.getResources().getColor(R.color.colorTextBlack));
        textView.setMaxEms(2);
        return textView;
    }

    private TextView newtextViewRequired() {
        final TextViewRegular textView = new TextViewRegular(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 30, 0, 20);
        textView.setLayoutParams(params);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(12);
        textView.setTextColor(this.getResources().getColor(R.color.colorTextBlack));

        return textView;
    }

    public ImageView newImageview(Context context, Bitmap imageToBind) {
        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 150);
        params.setMargins(10, 20, 0, 0);
        final ImageView imgView = new ImageView(context);
        imgView.setLayoutParams(params);
        imgView.setScaleType(ImageView.ScaleType.FIT_XY);
        imgView.setImageBitmap(imageToBind);
        return imgView;
    }


    private EditText newedittextz(String j) {
        final EditTextRegular editText = new EditTextRegular(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(10, 30, 0, 10);
        editText.setLayoutParams(params);
        editText.setHint(j);
        editText.setTextSize(14);
        editText.setTextColor(Color.rgb(0, 0, 0));
        return editText;
    }

    public void onBackClick(View view) {
   finish();
    }


}
