package com.intellyze.feedback.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.intellyze.feedback.Api.ResetPassword.HttpRequestForResetPasswrord;
import com.intellyze.feedback.Api.ResetPassword.OnHttpRequestResetPassword;
import com.intellyze.feedback.R;

public class ResetPassword extends BaseActivity implements OnHttpRequestResetPassword {
EditText passwordedt,confirmpasswordedt;
Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_reset_password);
        setProgressBar();
        passwordedt=findViewById(R.id.passwordedt);
        confirmpasswordedt=findViewById(R.id.confirmpassword);
        button=findViewById(R.id.reset);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate())
                {
                    resetPassword();
                }
            }
        });




    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        finish();
    }

    private void resetPassword() {
        showProgress("Loading");

        HttpRequestForResetPasswrord  httpRequestForResetPasswrord=new HttpRequestForResetPasswrord(this);
        httpRequestForResetPasswrord.resetPassword("");
    }

    private boolean validate() {

        if(!passwordedt.getText().toString().equals(""))
        {
            if(!confirmpasswordedt.getText().toString().equals(""))
            {
if(passwordedt.getText().toString().equals(confirmpasswordedt.getText().toString()))
{
    return true;
}
else
{
    passwordedt.setError("Passwrod not mactching");
    return false;
}


            }
            else
            {
                confirmpasswordedt.setError("Required confirm password" );
                return false;
            }
        }
        else
        {passwordedt.setError("Required password");

            return false;
        }

    }


    @Override
    public void onHttpResetPassSuccess(String Message) {
        Toast.makeText(this, Message, Toast.LENGTH_SHORT).show();

        finish();
    }

    @Override
    public void onHttpResetPassFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }
}
