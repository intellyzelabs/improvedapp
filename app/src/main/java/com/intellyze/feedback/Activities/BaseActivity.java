package com.intellyze.feedback.Activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.intellyze.feedback.Bus.BusFactory;
import com.intellyze.feedback.Bus.Events.BackPressRefreshLocalData;
import com.intellyze.feedback.R;

import java.io.ByteArrayOutputStream;


/**
 * Created by work on 7/21/2017.
 */

public class BaseActivity extends AppCompatActivity {


    private static String SHARE_PREFERENCE = "SHARE_PREFERENCE";
    boolean doubleBackToExitPressedOnce = false;
    private ProgressDialog progressBar;
    //private Toolbar mToolBar;
    //TODO: handle other permission groups

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusFactory.getBus().register(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        BusFactory.getBus().unregister(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    public String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 50, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);
        return encImage;
    }

    public Bitmap decodeImage(String encodedImage) {
        byte[] decodedString = Base64.decode(encodedImage, Base64.NO_WRAP);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

    /**
     * Method to add fragment to the container.
     *
     * @param containerId:    The id of the container.
     * @param fragment:       The fragment to add.
     * @param addToBackStack: Whether to add to backstack or not.
     */
    public void addFragment(int containerId, Fragment fragment, boolean addToBackStack) {

        if (isCurrentFragment(fragment)) {//no need to add a fragment twice
            return;
        }

        FragmentTransaction fTransaction = getSupportFragmentManager().beginTransaction();
        fTransaction.add(containerId, fragment, fragment.getClass().getSimpleName());//Tag : used for findFragmentByTag
        if (addToBackStack) {
            fTransaction.addToBackStack(null);
        }
        fTransaction.commit();
    }

    /**
     * Method to add fragment to the container with id R.id.container.
     *
     * @param fragment:       The fragment to add.
     * @param addToBackStack: Whether to add to backstack or not.
     */
    public void addFragment(Fragment fragment, boolean addToBackStack) {
        addFragment(R.id.container, fragment, addToBackStack);
    }

    /**
     * Method to replace fragment in a container.
     *
     * @param containerId:    The id of the container.
     * @param fragment:       The fragment to replace.
     * @param addToBackStack: Whether to add to backstack or not.
     */
    public void replaceFragment(int containerId, Fragment fragment, boolean addToBackStack, boolean animate) {

        if (isCurrentFragment(fragment)) {//no need to add a fragment twice
            return;
        }

        FragmentTransaction fTransaction = getSupportFragmentManager().beginTransaction();
        if (animate) {
//            fTransaction.setCustomAnimations(R.anim.slide_left_in, R.anim.slide_right_out, R.anim.slide_right_in, R.anim.slide_left_out);
        }
        fTransaction.replace(containerId, fragment, fragment.getClass().getSimpleName());//Tag : used for findFragmentByTag
        if (addToBackStack) {
            fTransaction.addToBackStack(null);
        }
        fTransaction.commit();

    }

    public void replaceFragment(Fragment fragment, boolean addToBackStack) {
        replaceFragment(R.id.container, fragment, addToBackStack, true);
    }

    public boolean isConnectedToNet(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;

    }

    public Fragment getFragment(Class clazz) {
        return getSupportFragmentManager().findFragmentByTag(clazz.getSimpleName());
    }

    public void setProgressBar() {

        this.progressBar = new ProgressDialog(this,
                ProgressDialog.STYLE_SPINNER);
        this.progressBar.setIndeterminate(false);
        this.progressBar.setCancelable(false);
        this.progressBar.cancel();

    }


    public void showProgress(String message) {
        Log.e ("showProgress: ","showProgress" );
        this.progressBar.setMessage(message);
        this.progressBar.show();
    }

    public void cancelProgress()
    {
        Log.e ("cancelProgress: ","cancelProgress" );
        this.progressBar.cancel();
    }

    public void showAlert(String message) {
        Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show();
    }

    public void showAlertSucces(String message) {
        Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show();
    }

    public void popBackStack() {
        getSupportFragmentManager().popBackStack();
    }

    /**
     * To check whether the given fragment is the current fragment.
     *
     * @param fragment
     * @return
     */
    public boolean isCurrentFragment(Fragment fragment) {
        Fragment tFragment = getFragment(fragment.getClass());
        if (tFragment != null && tFragment.isVisible()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //to handel the actionbar back button
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {


        final Fragment fr = getSupportFragmentManager().findFragmentById(R.id.container);
        if (fr != null) {


            if (fr.getClass().getSimpleName().equals("CreateEditSurveyFirstFragment")) {

                getSupportFragmentManager().popBackStack();
            } else if (!fr.getClass().getSimpleName().equals("HomeFragment")) {
                if (fr.getClass().getSimpleName().equals("StartQuestionsFragment") || fr.getClass().getSimpleName().equals("CreateSurveyFirstFragment") || fr.getClass().getSimpleName().equals("ContinueSurveyFragment") || fr.getClass().getSimpleName().equals("EditQuestionFragment") || fr.getClass().getSimpleName().equals("UserSurveyQuestionsFragment")) {
                    final Dialog dialog = new Dialog(this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.dialog_status_update_confirm);
                    Button btnConfirm = (Button) dialog.findViewById(R.id.btnConfirm);
                    final Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
                    btnConfirm.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {


                                for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
                                    if (!fr.getClass().getSimpleName().equals("HomeFragment")) {
                                        getSupportFragmentManager().popBackStack();
                                    }
                                }
                                BusFactory.getBus().post(new BackPressRefreshLocalData());
                            }
                            dialog.dismiss();
                        }

                    });
                    btnCancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    dialog.show();
                } else {
                    if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {


                        for (int i = 0; i < getSupportFragmentManager().getBackStackEntryCount(); i++) {
                            if (!fr.getClass().getSimpleName().equals("HomeFragment")) {
                                getSupportFragmentManager().popBackStack();
                            }
                        }
                        BusFactory.getBus().post(new BackPressRefreshLocalData());
                    }
                }

            } else {
                if (getSupportFragmentManager().getBackStackEntryCount() >= 1) {
//            Fragment fr = getSupportFragmentManager().findFragmentById(R.id.container);
//            if (fr != null) {
//                Log.e("fragment=", fr.getClass().getSimpleName());
//            }
//            for(int i=0;i<getSupportFragmentManager().getBackStackEntryCount();i++){
//                if (!fr.getClass().getSimpleName().equals("HomeFragment")) {
//                    getSupportFragmentManager().popBackStack();
//
//                }
//
//
//
//            }
//            BusFactory.getBus().post(new BackPressRefreshLocalData());


                } else if (!doubleBackToExitPressedOnce) {
                    this.doubleBackToExitPressedOnce = true;
                    Toast.makeText(this, "Please click BACK again to exit.", Toast.LENGTH_SHORT).show();

                    new Handler().postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            doubleBackToExitPressedOnce = false;
                        }
                    }, 2000);
                } else {
                    finish();
                }
            }
        }

    }

    public void put_SharePreference(String key, String value) {
        SharedPreferences.Editor preferences = getSharedPreferences(SHARE_PREFERENCE, Context.MODE_PRIVATE).edit();
        preferences.putString(key, value).commit();
    }

    public String get_SharePreference(String key) {
        SharedPreferences preferences = getSharedPreferences(SHARE_PREFERENCE, Context.MODE_PRIVATE);
        return preferences.getString(key, null);
    }

    public void deleteSharePreference() {
        SharedPreferences preferences = getSharedPreferences(SHARE_PREFERENCE, MODE_PRIVATE);
        preferences.edit().clear().commit();

    }

    public interface OnBackPressedListener {
        boolean onBackPressed();
    }

}
