package com.intellyze.feedback.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.intellyze.feedback.Api.OTP.HttpRequestForOtp;
import com.intellyze.feedback.Api.OTP.OnHttpRequestOtp;
import com.intellyze.feedback.Interfaces.SmsListener;
import com.intellyze.feedback.R;
import com.intellyze.feedback.Services.SmsReceiver;

public class FrogetPassword extends BaseActivity implements OnHttpRequestOtp,SmsListener {
    TextView one,two,three,four,five;
    TextView six,seven,eight,nine,zero;
    String getotp;
    TextView enterotp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_froget_password);
        setProgressBar();


//        SmsReceiver.bindListener(new SmsListener() {
//            @Override
//            public void messageReceived(String messageText) {
//                Log.d("Text",messageText);
//                Toast.makeText(FrogetPassword.this,"Message: "+messageText,Toast.LENGTH_LONG).show();
//            }
//        });
        enterotp=(TextView)findViewById(R.id.otpenter);
        one=(TextView)findViewById(R.id.one);
        two=(TextView)findViewById(R.id.two);
        three=(TextView)findViewById(R.id.three);
        four=(TextView)findViewById(R.id.four);
        five=(TextView)findViewById(R.id.five);
        six=(TextView)findViewById(R.id.six);
        seven=(TextView)findViewById(R.id.seven);
        eight=(TextView)findViewById(R.id.eight);
        nine=(TextView)findViewById(R.id.nine);
        zero=(TextView)findViewById(R.id.zero);


        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendnum("1");
            }
        });
        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendnum("2");
            }
        });
        three.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendnum("3");
            }
        });
        four.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendnum("4");
            }
        });
        five.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendnum("5");
            }
        });
        six.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendnum("6");
            }
        });
        seven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendnum("7");
            }
        });
        eight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendnum("8");
            }
        });
        nine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendnum("9");
            }
        });
        zero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                appendnum("0");
            }
        });



    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void appendnum(String n){
        int count=0;
        getotp=enterotp.getText().toString();

        for (int i = 0, len = getotp.length(); i < len; i++) {
            if (Character.isDigit(getotp.charAt(i))) {
                count++;
            }
        }
        if(count==0){
            enterotp.setText(n+" - - -");
        }else if(count==1){
            String otpnew=getotp.substring(0,1);
            enterotp.setText(otpnew+" "+n+" - -");
        }
        else if(count==2){
            String otpnew=getotp.substring(0,3);
            enterotp.setText(otpnew+" "+n+" -");
        }
        else if(count==3){
            String otpnew=getotp.substring(0,5);
            enterotp.setText(otpnew+" "+n);
   sendDatatoServer(otpnew);

//            Toast.makeText(FrogetPassword.this,"create password page",Toast.LENGTH_LONG).show();
        }
        else
        {}
    }

    private void sendDatatoServer(String otpnew) {
        showProgress("Loading");
        HttpRequestForOtp httpRequestForOtp=new HttpRequestForOtp(this);
        httpRequestForOtp.sendtOtp(otpnew);
    }

    @Override
    public void onHttpOtpisSuccess(String Message) {
        cancelProgress();
        Intent intent=new Intent(this,ResetPassword.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onHttpOtpisFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void messageReceived(String messageText) {
        Toast.makeText(this, ""+messageText, Toast.LENGTH_SHORT).show();
    }
}
