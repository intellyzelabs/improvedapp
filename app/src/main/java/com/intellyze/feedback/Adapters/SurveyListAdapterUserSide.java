package com.intellyze.feedback.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.intellyze.feedback.Interfaces.SurveyItemClick;
import com.intellyze.feedback.Interfaces.SurveyQuestionUserItemClick;
import com.intellyze.feedback.Models.SurveyDetailsModel;
import com.intellyze.feedback.Models.SurveyDetailsUserFeedbackQuestionModel;
import com.intellyze.feedback.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by INTELLYZE-202 on 19-12-2017.
 */

public class SurveyListAdapterUserSide extends RecyclerView.Adapter<SurveyListAdapterUserSide.NavigationViewHolder> {

    private Context context;
    SurveyQuestionUserItemClick surveyItemClick;
    private final SparseBooleanArray mCollapsedStatus;
    List<SurveyDetailsUserFeedbackQuestionModel> surveyDetailsModels;

    public SurveyListAdapterUserSide(Context context, SurveyQuestionUserItemClick surveyItemClick, List<SurveyDetailsUserFeedbackQuestionModel> surveyDetailsModels) {
        this.context = context;
        this.surveyItemClick = surveyItemClick;
        this.surveyDetailsModels = surveyDetailsModels;
        mCollapsedStatus = new SparseBooleanArray();
    }

    @Override
    public SurveyListAdapterUserSide.NavigationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_survey_list, parent, false);
        return new SurveyListAdapterUserSide.NavigationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SurveyListAdapterUserSide.NavigationViewHolder holder, final int position) {

        holder.tvSurveyName.setText(surveyDetailsModels.get(position).getSurveyName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                surveyItemClick.onNavigateSurveyUserResult(surveyDetailsModels.get(position).getSurveyId());
            }
        });
        holder.tvStatus.setText(surveyDetailsModels.get(position).getCount()+"");
        String s=surveyDetailsModels.get(position).getSurveyImage();

        if(s!=null) {


            if (surveyDetailsModels.get(position).getSurveyImage().equals("")) {
                holder.imgProfile.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_intellyze_logo));
            } else {
                Bitmap bitmap = decodeImage(surveyDetailsModels.get(position).getSurveyImage());
                holder.imgProfile.setImageBitmap(bitmap);
            }
        }
        else
        { holder.imgProfile.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_intellyze_logo));


        }



        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat outputFormat = new SimpleDateFormat("d EEE");
        DateFormat outputFormatMonth = new SimpleDateFormat("MMM");
        Date date = null;
        try {
            date = inputFormat.parse(surveyDetailsModels.get(position).getSurveyDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputDateStr = outputFormat.format(date);
        String outputDateStrMonth = outputFormatMonth.format(date);
        holder.tv_dayDate.setText(outputDateStr);
        holder.tvMonth.setText(outputDateStrMonth.toUpperCase());
    }

    private Bitmap decodeImage(String encodedImage) {
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;

    }

    @Override
    public int getItemCount() {
        return surveyDetailsModels.size();
    }


    class NavigationViewHolder extends RecyclerView.ViewHolder {
        TextView tvSurveyName, tvMonth, tv_dayDate;
        ImageView imgProfile;
        TextView tvStatus;
        public NavigationViewHolder(View itemView) {
            super(itemView);
            tvSurveyName = (TextView) itemView.findViewById(R.id.tvSurveyName);
            tvStatus = (TextView) itemView.findViewById(R.id.tvStatus);
            imgProfile = (ImageView) itemView.findViewById(R.id.imgProfile);
            tv_dayDate = (TextView) itemView.findViewById(R.id.tv_dayDate);
            tvMonth = (TextView) itemView.findViewById(R.id.tvMonth);
        }
    }
}
