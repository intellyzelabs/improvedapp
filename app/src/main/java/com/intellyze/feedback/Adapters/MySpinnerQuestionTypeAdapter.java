package com.intellyze.feedback.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.intellyze.feedback.Models.SpinnerQuestionTypeModel;
import com.intellyze.feedback.R;

import java.util.ArrayList;

/**
 * Created by INTELLYZE-202 on 01-12-2017.
 */

public class MySpinnerQuestionTypeAdapter extends BaseAdapter {

    Context _context;
    ArrayList<SpinnerQuestionTypeModel>  _list;

    public MySpinnerQuestionTypeAdapter(Context context, ArrayList<SpinnerQuestionTypeModel> list)
    {
        _context = context;
        _list = list;
    }

    @Override
    public int getCount() {
        return (_list == null)?0:  _list.size();
    }

    @Override
    public Object getItem(int i) {
        return _list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if(view == null)
        {
            LayoutInflater inflater = (LayoutInflater)_context.getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.custom_spinner_item_questions,viewGroup,false);
        }
        SpinnerQuestionTypeModel country = _list.get(i);

        TextView tvQuestionTypeName = (TextView)view.findViewById(R.id.tvQuestionTypeName);
        View vView = (View) view.findViewById(R.id.vView);
        if(i==1||i==4){
            vView.setVisibility(View.VISIBLE);
        }else {
            vView.setVisibility(View.GONE);
        }
        ImageView imgQuestionIcon = (ImageView)view.findViewById(R.id.imgQuestionIcon);
        tvQuestionTypeName.setText(country.getTypeName());
        imgQuestionIcon.setImageResource(country.getTypeIcon());
        return view;
    }

}