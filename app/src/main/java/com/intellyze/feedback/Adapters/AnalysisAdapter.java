package com.intellyze.feedback.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.intellyze.feedback.Api.AnalysisModel.SurveyData;
import com.intellyze.feedback.Custom.TextViewBold;
import com.intellyze.feedback.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AnalysisAdapter extends RecyclerView.Adapter<AnalysisAdapter.ViewHolder> {

    private final List<SurveyData> mValues;
    Context context;


    public AnalysisAdapter(List<SurveyData> items, Context context) {
        mValues = items;
        this.context=context;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_analysis, parent, false);
        return new ViewHolder(view);
    }

    private Bitmap decodeImage(String encodedImage) {
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {


    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextViewBold heading_shotanswer;
        public SurveyData mItem;
        CircleImageView profile;
        public LinearLayout shortanswer_linear;
        public ViewHolder(View view) {
            super(view);
            mView = view;
            heading_shotanswer = (TextViewBold) view.findViewById(R.id.heading);
            shortanswer_linear = view.findViewById(R.id.shortanswer_linear);


        }


    }
}
