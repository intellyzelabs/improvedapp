package com.intellyze.feedback.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.intellyze.feedback.Fragments.ViewResponseFragment.OnListFragmentInteractionListener;
import com.intellyze.feedback.Fragments.dummy.DummyContent.DummyItem;
import com.intellyze.feedback.Models.SurveyAnswerListForRecyclerView;
import com.intellyze.feedback.R;

import java.util.ArrayList;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MySurveyResponcesListAdminRecyclerViewAdapter extends RecyclerView.Adapter<MySurveyResponcesListAdminRecyclerViewAdapter.ViewHolder> {

    ArrayList<SurveyAnswerListForRecyclerView> itemsnew;
    private final OnListFragmentInteractionListener mListener;

    public MySurveyResponcesListAdminRecyclerViewAdapter(ArrayList<SurveyAnswerListForRecyclerView> items, OnListFragmentInteractionListener listener) {
        itemsnew = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_surveyresponceslistadmin, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        holder.mItem = itemsnew.get(position).getSurveyId();
        holder.survey_Title.setText(itemsnew.get(position).getSurveyTitle());
        holder.Answer_Id.setText(itemsnew.get(position).getAnswerId());
        holder.Full_name.setText(itemsnew.get(position).getFullName());
        holder.mobile_no.setText(itemsnew.get(position).getMobileNo());
        holder.profession.setText(itemsnew.get(position).getProfession());
        holder.email.setText(itemsnew.get(position).getEmail());
        holder.age.setText(itemsnew.get(position).getAge());
        holder.answer_date.setText(itemsnew.get(position).getAnswerDate());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(itemsnew.get(position).getAnswerId(),itemsnew.get(position).getSurveyId());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemsnew.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView survey_Title;
        public final TextView Answer_Id;
        public final TextView Full_name,mobile_no,profession,email,age,answer_date;
        public DummyItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            survey_Title = (TextView) view.findViewById(R.id.survey_Title);
            Answer_Id = (TextView) view.findViewById(R.id.Answer_Id);
            Full_name = (TextView) view.findViewById(R.id.Full_name);
            mobile_no = (TextView) view.findViewById(R.id.mobile_no);
            profession = (TextView) view.findViewById(R.id.profession);
            email = (TextView) view.findViewById(R.id.email);
            age = (TextView) view.findViewById(R.id.age);
            answer_date = (TextView) view.findViewById(R.id.answer_date);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + Answer_Id.getText() + "'";
        }
    }
}
