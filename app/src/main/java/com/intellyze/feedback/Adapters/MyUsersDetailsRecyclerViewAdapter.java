package com.intellyze.feedback.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.intellyze.feedback.Api.UserModel.Result;

import com.intellyze.feedback.Interfaces.UserDeleteItemClick;
import com.intellyze.feedback.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class MyUsersDetailsRecyclerViewAdapter extends RecyclerView.Adapter<MyUsersDetailsRecyclerViewAdapter.ViewHolder> {

    private final List<Result> mValues;
    Context context;

    UserDeleteItemClick userDeleteItemClick;

    public MyUsersDetailsRecyclerViewAdapter(List<Result> items, Context context,UserDeleteItemClick userDeleteItemClick) {
        mValues = items;
        this.context=context;
        this.userDeleteItemClick=userDeleteItemClick;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_user_design, parent, false);
        return new ViewHolder(view);
    }

    private Bitmap decodeImage(String encodedImage) {
        byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).getUsername());

        if (mValues.get(position).getURole().equals("S_Admin"))
        {
            holder.mContentView.setText("Admin");

        }
        else
        {
            holder.mContentView.setText("User");
        }


        if (mValues.get(position).getProfilepic().equals("")) {
            holder.profile.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_intellyze_logo));
        } else {
            Bitmap bitmap = decodeImage(mValues.get(position).getProfilepic());
            holder.profile.setImageBitmap(bitmap);
        }



holder.supper.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        userDeleteItemClick.onUserDeleteClicked(position);
    }
});


    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        ImageView supper;
        public Result mItem;
        CircleImageView profile;
        public RelativeLayout  viewForeground;
        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.item_number);
            mContentView = (TextView) view.findViewById(R.id.content);
            viewForeground = view.findViewById(R.id.view_foreground);
            supper = view.findViewById(R.id.supper);
            profile = view.findViewById(R.id.thumbnail);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
