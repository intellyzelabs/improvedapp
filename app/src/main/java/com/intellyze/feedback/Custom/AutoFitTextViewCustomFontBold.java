package com.intellyze.feedback.Custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import me.grantland.widget.AutofitTextView;

/**
 * Created by INTELLYZE-202 on 01-12-2017.
 */

public class AutoFitTextViewCustomFontBold extends AutofitTextView {

    public AutoFitTextViewCustomFontBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public AutoFitTextViewCustomFontBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AutoFitTextViewCustomFontBold(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/WorkSans-SemiBold.ttf");
            setTypeface(tf);
        }
    }


}