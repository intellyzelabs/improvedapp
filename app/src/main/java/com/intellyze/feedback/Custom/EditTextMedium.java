package com.intellyze.feedback.Custom;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

/**
 * Created by INTELLYZE-202 on 29-11-2017.
 */

public class EditTextMedium extends android.support.v7.widget.AppCompatEditText {

    public EditTextMedium(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public EditTextMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EditTextMedium(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/WorkSans-Medium.ttf");
            setTypeface(tf);
        }
    }

}