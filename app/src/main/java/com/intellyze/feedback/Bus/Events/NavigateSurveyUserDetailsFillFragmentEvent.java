package com.intellyze.feedback.Bus.Events;

/**
 * Created by INTELLYZE-202 on 01-12-2017.
 */

public class NavigateSurveyUserDetailsFillFragmentEvent {
    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public NavigateSurveyUserDetailsFillFragmentEvent(int id) {
        this.id=id;
    }
}
