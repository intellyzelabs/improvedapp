package com.intellyze.feedback.Bus.Events;

/**
 * Created by INTELLYZE-202 on 01-12-2017.
 */

public class NavigateSurveyResultFragmentEvent {
    String survey;
    int completeId;

    public NavigateSurveyResultFragmentEvent(String survey, int completeId) {
        this.survey = survey;
        this.completeId = completeId;
    }

    public int getCompleteId() {
        return completeId;
    }

    public void setCompleteId(int completeId) {
        this.completeId = completeId;
    }

    public String getSurvey() {
        return survey;
    }

    public void setSurvey(String survey) {
        this.survey = survey;
    }
}
