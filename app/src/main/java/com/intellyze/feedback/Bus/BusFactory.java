package com.intellyze.feedback.Bus;

import com.squareup.otto.Bus;

/**
 * Created by INTELLYZE-202 on 30-11-2017.
 */

public class BusFactory {

    public static final Bus bus = new Bus();

    public static Bus getBus(){
        return bus;
    }
}
