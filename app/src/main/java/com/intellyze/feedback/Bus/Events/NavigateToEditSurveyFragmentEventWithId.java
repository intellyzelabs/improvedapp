package com.intellyze.feedback.Bus.Events;

public class NavigateToEditSurveyFragmentEventWithId {

    String survey;

    public NavigateToEditSurveyFragmentEventWithId(String survey) {
        this.survey = survey;

    }


    public String getSurvey() {
        return survey;
    }

    public void setSurvey(String survey) {
        this.survey = survey;
    }
}
