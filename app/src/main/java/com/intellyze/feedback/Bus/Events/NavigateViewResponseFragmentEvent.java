package com.intellyze.feedback.Bus.Events;

/**
 * Created by INTELLYZE-202 on 01-12-2017.
 */

public class NavigateViewResponseFragmentEvent {
    String mId;
    int totResponse;
    String statusSurvey;
    String surveyname;

    public String getSurveyname() {
        return surveyname;
    }

    public void setSurveyname(String surveyname) {
        this.surveyname = surveyname;
    }

    public String getOpernorclose() {
        return opernorclose;
    }

    public void setOpernorclose(String opernorclose) {
        this.opernorclose = opernorclose;
    }

    String lastEntry;
    String opernorclose;
    public NavigateViewResponseFragmentEvent(String mId, int totResponse, String statusSurvey, String lastEntry, String opernorclose,String surveyname) {
        this.mId = mId;
        this.totResponse = totResponse;
        this.statusSurvey = statusSurvey;
        this.lastEntry = lastEntry;
        this.opernorclose = opernorclose;
        this.surveyname = surveyname;
    }

    public String getLastEntry() {
        return lastEntry;
    }

    public void setLastEntry(String lastEntry) {
        this.lastEntry = lastEntry;
    }

    public String getStatusSurvey() {
        return statusSurvey;
    }

    public void setStatusSurvey(String statusSurvey) {
        this.statusSurvey = statusSurvey;
    }

    public int getTotResponse() {
        return totResponse;
    }

    public void setTotResponse(int totResponse) {
        this.totResponse = totResponse;
    }


    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }
}
