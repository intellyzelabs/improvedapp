package com.intellyze.feedback.Bus.Events;

/**
 * Created by INTELLYZE-202 on 21-12-2017.
 */

public class NavigateToSurveyLastPagFragmentEvent {
    int mId;

    public NavigateToSurveyLastPagFragmentEvent(int mId) {
        this.mId = mId;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }
}
