package com.intellyze.feedback.Bus.Events;

/**
 * Created by INTELLYZE-202 on 12-12-2017.
 */

public class NavigateToEditQuestionEvent {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public NavigateToEditQuestionEvent(int id) {
        this.id = id;

    }
}
