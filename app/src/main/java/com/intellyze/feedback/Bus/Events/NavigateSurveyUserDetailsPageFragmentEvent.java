package com.intellyze.feedback.Bus.Events;

/**
 * Created by INTELLYZE-202 on 20-12-2017.
 */

public class NavigateSurveyUserDetailsPageFragmentEvent {
    int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public NavigateSurveyUserDetailsPageFragmentEvent(int id) {
        this.id=id;
    }
}
