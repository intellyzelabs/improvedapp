package com.intellyze.feedback.Bus.Events;

/**
 * Created by INTELLYZE-202 on 09-01-2018.
 */

public class NavigateToDeleteSurveyFragmentEvent {
    String survey;

    public NavigateToDeleteSurveyFragmentEvent(String survey) {
        this.survey = survey;

    }


    public String getSurvey() {
        return survey;
    }

    public void setSurvey(String survey) {
        this.survey = survey;
    }
}
