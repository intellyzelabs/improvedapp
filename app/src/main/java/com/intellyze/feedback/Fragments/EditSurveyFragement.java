package com.intellyze.feedback.Fragments;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextDirectionHeuristic;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.intellyze.feedback.Api.AddAnswerMultipleSurveys.AddMultplieAnswerSurveyRequestModel;
import com.intellyze.feedback.Api.AddAnswerMultipleSurveys.Answer;
import com.intellyze.feedback.Api.AddMultpileSurveys.AddMultipleSurveyRequestModel;
import com.intellyze.feedback.Api.AddMultpileSurveys.HttpRequestForAddMultipleSurvey;
import com.intellyze.feedback.Api.AddMultpileSurveys.OnHttpRequestAddMultipleSurvey;
import com.intellyze.feedback.Api.AddMultpileSurveys.Survey;
import com.intellyze.feedback.Bus.BusFactory;
import com.intellyze.feedback.Bus.Events.NavigateBack;
import com.intellyze.feedback.Bus.Events.NavigateEditStartQuestionFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateEditStartQuestionFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateStartQuestionFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateToEditQuestionEvent;
import com.intellyze.feedback.Bus.Events.NavigateToSurveyEditQuestionEvent;
import com.intellyze.feedback.Bus.Events.RefreshEvent;
import com.intellyze.feedback.Bus.Events.RefreshListFeedback;
import com.intellyze.feedback.Bus.Events.SyncSurveyAfterSubmit;
import com.intellyze.feedback.Custom.EditTextRegular;
import com.intellyze.feedback.Custom.TextViewRegular;
import com.intellyze.feedback.Interfaces.QuestionItemClick;
import com.intellyze.feedback.Models.SurveyDetailsModel;
import com.intellyze.feedback.Models.SurveyDetailsUserFeedbackQuestionModel;
import com.intellyze.feedback.R;
import com.intellyze.feedback.Realm.AddAnswerMaster;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.Column;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.FeedQuestUserSide;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.Linear;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.MultipleChoice;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.MultipleChoiceGrid;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.Row;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.SurveyQuestion;
import com.intellyze.feedback.Realm.SurveyDetails;
import com.intellyze.feedback.Realm.SurveyQuestionsDetails;
import com.intellyze.feedback.Realm.columnDetails;
import com.intellyze.feedback.Realm.linearquestionDetalis;
import com.intellyze.feedback.Realm.matrixquestionDetails;
import com.intellyze.feedback.Realm.multiplechoicesingleDetails;
import com.intellyze.feedback.Realm.rowDetails;
import com.intellyze.feedback.Utils.UserPref;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by intellyelabs on 05/02/18.
 */

public class EditSurveyFragement  extends BaseFragment implements QuestionItemClick, View.OnClickListener, OnHttpRequestAddMultipleSurvey {

    LinearLayout lnr_QuestionList;
    TextView tvAdd, tvSubmit, tvSurveyName;
    Realm mRealm;
    SurveyDetails surveyDetails;

    List<CardView> cardIds = new ArrayList<CardView>();

    private RealmResults<SurveyDetails> dataOfflineToOnline;

    private List<Survey> mSurveysList;

    private Survey mSurvey;

    private AddMultipleSurveyRequestModel addMultipleSurveyRequestModel;
    private UserPref userPref;




    public EditSurveyFragement() {
        // Required empty public constructor


    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EditSurveyFragement newInstance() {
        EditSurveyFragement fragment = new EditSurveyFragement();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.edit_survey_fragment, container, false);
        initViews(v);

        userPref = new UserPref(getActivity());
        dataFromReaalm();

        return v;
    }

    @Subscribe
    public void NavigateQuestionFragmen(RefreshEvent event) {
        lnr_QuestionList.removeAllViews();
        dataFromReaalm();
    }

    /**
     * Use this  method to FETCH details
     * of a particular survey.
     */
    private void dataFromReaalm() {
        String vv = userPref.getSurveyId();


        surveyDetails=new SurveyDetails();

        mRealm = Realm.getDefaultInstance();
        surveyDetails = mRealm.where(SurveyDetails.class)
                .equalTo("SurveyId", vv).findFirstAsync();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (surveyDetails != null) {
                        userPref.SaveCount(surveyDetails.getSurveyQuestionsDetails().size() + 1);
                        tvSurveyName.setText(surveyDetails.getSurveyName());

                        for (int i = 0; i < surveyDetails.getSurveyQuestionsDetails().size(); i++) {

                            if (surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionType().equals("Short answer")) {
                                CardView card = drawCard(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestion_Id());
                                LinearLayout layout = new LinearLayout(getActivity());
                                layout.setOrientation(LinearLayout.VERTICAL);
                                TextView iv = newtextView();
                                String first = surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionNumber() + ". " + surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionName();
                                String next = "";
                                if (surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionRequired() == 1)
                                    next = "<font color='#EE0000'>*</font>";
                                iv.setText(Html.fromHtml(first + next));
                                layout.addView(iv);
                                if (!surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage().equals("")) {
                                    Bitmap imageToBind = loadImageFromStorage(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage());
                                    ImageView nkj = newImageview(getActivity(), imageToBind);
                                    layout.addView(nkj);
                                }
                                EditText et = newedittextz("Short Answer");
                                layout.addView(et);
                                ImageView ivs = newEditButton();
                                ivs.setImageResource(R.drawable.edit_icons);
                                layout.addView(ivs);

                                card.addView(layout);
                                lnr_QuestionList.addView(card);
                            } else if (surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionType().equals("Paragraph")) {
                                CardView card = drawCard(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestion_Id());
                                LinearLayout layout = new LinearLayout(getActivity());
                                layout.setOrientation(LinearLayout.VERTICAL);
                                String first = surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionNumber() + ". " + surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionName();
                                TextView iv = newtextView();
                                String next = "";
                                if (surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionRequired() == 1)
                                    next = "<font color='#EE0000'>*</font>";
                                iv.setText(Html.fromHtml(first + next));
                                layout.addView(iv);
                                if (!surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage().equals("")) {
                                    Bitmap imageToBind = loadImageFromStorage(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage());
                                    ImageView nkj = newImageview(getActivity(), imageToBind);
                                    layout.addView(nkj);
                                }
                                EditText et = newedittextz("Long Answer");
                                layout.addView(et);
                                ImageView ivs = newEditButton();
                                ivs.setImageResource(R.drawable.edit_icons);
                                layout.addView(ivs);
                                card.addView(layout);
                                lnr_QuestionList.addView(card);
                            } else if (surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionType().equals("Multiple choice")) {
                                CardView card = drawCard(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestion_Id());
                                LinearLayout layout = new LinearLayout(getActivity());
                                layout.setOrientation(LinearLayout.VERTICAL);
                                String first = surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionNumber() + ". " + surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionName();
                                TextView iv = newtextView();
                                String next = "";
                                if (surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionRequired() == 1)
                                    next = "<font color='#EE0000'>*</font>";
                                iv.setText(Html.fromHtml(first + next));
                                layout.addView(iv);
                                if (!surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage().equals("")) {

                                    Bitmap imageToBind = loadImageFromStorage(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage());
                                    ImageView nkj = newImageview(getActivity(), imageToBind);
                                    layout.addView(nkj);
                                }
                                RadioGroup rg = createRadioGroup(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMultiplechoicesingleDetails().size(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMultiplechoicesingleDetails());
                                layout.addView(rg);
                                ImageView ivs = newEditButton();
                                ivs.setImageResource(R.drawable.edit_icons);
                                layout.addView(ivs);
                                card.addView(layout);
                                lnr_QuestionList.addView(card);
                            } else if (surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionType().equals("Checkboxes")) {
                                CardView card = drawCard(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestion_Id());
                                LinearLayout layout = new LinearLayout(getActivity());
                                layout.setOrientation(LinearLayout.VERTICAL);
                                String first = surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionNumber() + ". " + surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionName();
                                TextView iv = newtextView();
                                String next = "";
                                if (surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionRequired() == 1)
                                    next = "<font color='#EE0000'>*</font>";
                                iv.setText(Html.fromHtml(first + next));
                                layout.addView(iv);
                                if (!surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage().equals("")) {
                                    Bitmap imageToBind = loadImageFromStorage(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage());
                                    ImageView nkj = newImageview(getActivity(), imageToBind);
                                    layout.addView(nkj);
                                }
                                LinearLayout.LayoutParams radioparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                radioparams.setMargins(0, 30, 0, 0);
                                LinearLayout fdh = new LinearLayout(getActivity());
                                fdh.setOrientation(LinearLayout.VERTICAL);
                                fdh.setLayoutParams(radioparams);
                                for (int ij = 0; ij < surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMultiplechoicesingleDetails().size(); ij++) {
                                    CheckBox cb = new CheckBox(getActivity());
                                    cb.setText(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMultiplechoicesingleDetails().get(ij).getTextChoice());
                                    cb.setEnabled(false);
                                    Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/WorkSans-Regular.ttf");
                                    cb.setTypeface(font);
                                    fdh.addView(cb);
                                }
                                layout.addView(fdh);

                                ImageView ivs = newEditButton();
                                ivs.setImageResource(R.drawable.edit_icons);
                                layout.addView(ivs);
                                card.addView(layout);
                                lnr_QuestionList.addView(card);
                            } else if (surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionType().equals("Dropdown")) {
                                CardView card = drawCard(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestion_Id());
                                LinearLayout layout = new LinearLayout(getActivity());
                                layout.setOrientation(LinearLayout.VERTICAL);
                                String first = surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionNumber() + ". " + surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionName();
                                TextView iv = newtextView();
                                String next = "";
                                if (surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionRequired() == 1)
                                    next = "<font color='#EE0000'>*</font>";
                                iv.setText(Html.fromHtml(first + next));
                                layout.addView(iv);
                                if (!surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage().equals("")) {
                                    Bitmap imageToBind = loadImageFromStorage(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage());
                                    ImageView nkj = newImageview(getActivity(), imageToBind);
                                    layout.addView(nkj);
                                }
                                LinearLayout.LayoutParams radioparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                radioparams.setMargins(0, 30, 0, 0);
                                LinearLayout fdh = new LinearLayout(getActivity());
                                fdh.setOrientation(LinearLayout.VERTICAL);
                                fdh.setLayoutParams(radioparams);
                                for (int ij = 0; ij < surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMultiplechoicesingleDetails().size(); ij++) {
                                    LinearLayout.LayoutParams tvparam = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                    tvparam.setMargins(20, 15, 0, 0);
                                    TextView cb = new TextView(getActivity());
                                    cb.setText(ij + ".  " + surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMultiplechoicesingleDetails().get(ij).getTextChoice());
                                    cb.setLayoutParams(tvparam);
                                    cb.setEnabled(false);
                                    Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/WorkSans-Regular.ttf");
                                    cb.setTypeface(font);
                                    fdh.addView(cb);
                                }
                                layout.addView(fdh);
                                ImageView ivs = newEditButton();
                                ivs.setImageResource(R.drawable.edit_icons);
                                layout.addView(ivs);
                                card.addView(layout);
                                lnr_QuestionList.addView(card);
                            } else if (surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionType().equals("Linear scale")) {
                                CardView card = drawCard(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestion_Id());
                                LinearLayout layout = new LinearLayout(getActivity());
                                layout.setOrientation(LinearLayout.VERTICAL);
                                String first = surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionNumber() + ". " + surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionName();
                                TextView iv = newtextView();
                                String next = "";
                                if (surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionRequired() == 1)
                                    next = "<font color='#EE0000'>*</font>";
                                iv.setText(Html.fromHtml(first + next));
                                layout.addView(iv);
                                if (!surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage().equals("")) {
                                    Bitmap imageToBind = loadImageFromStorage(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage());
                                    ImageView nkj = newImageview(getActivity(), imageToBind);
                                    layout.addView(nkj);
                                }
                                LinearLayout layoutInner = new LinearLayout(getActivity());
                                layoutInner.setOrientation(LinearLayout.VERTICAL);
                                LinearLayout.LayoutParams Lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                Lparams.setMargins(0, 0, 10, 0);
                                LinearLayout layoutOutInner = new LinearLayout(getActivity());
                                layoutOutInner.setOrientation(LinearLayout.HORIZONTAL);
                                layoutOutInner.setLayoutParams(Lparams);
                                TextView Upv = newtextViewWrap();
                                Upv.setText(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getLinearquestionDetalis().get(0).getStartText());
                                layoutOutInner.addView(Upv);
                                TextView Lpv = newtextViewWrap();
                                Lpv.setGravity(Gravity.RIGHT);
                                Lpv.setText(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getLinearquestionDetalis().get(0).getEndText());
                                layoutOutInner.addView(Lpv);
                                layoutInner.addView(layoutOutInner);
                                RadioGroup rg = drawRadioGroupTextTop(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getLinearquestionDetalis().get(0).getStartValue(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getLinearquestionDetalis().get(0).getEndValue());
                                layoutInner.addView(rg);
                                layout.addView(layoutInner);
                                ImageView ivs = newEditButton();
                                ivs.setImageResource(R.drawable.edit_icons);
                                layout.addView(ivs);
                                card.addView(layout);
                                lnr_QuestionList.addView(card);
                            } else if (surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionType().equals("Multiple choice grid")) {
                                CardView card = drawCard(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestion_Id());
                                LinearLayout layout = new LinearLayout(getActivity());
                                layout.setOrientation(LinearLayout.VERTICAL);
                                String first = surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionNumber() + ". " + surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionName();
                                TextView iv = newtextView();
                                String next = "";
                                if (surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionRequired() == 1)
                                    next = "<font color='#EE0000'>*</font>";
                                iv.setText(Html.fromHtml(first + next));
                                layout.addView(iv);
                                if (!surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage().equals("")) {
                                    Bitmap imageToBind = loadImageFromStorage(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage());
                                    ImageView nkj = newImageview(getActivity(), imageToBind);
                                    layout.addView(nkj);
                                }
                                HorizontalScrollView scroll = new HorizontalScrollView(getActivity());
                                scroll.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                        ViewGroup.LayoutParams.WRAP_CONTENT));
                                scroll.setFillViewport(true);
                                scroll.setHorizontalScrollBarEnabled(false);
                                LinearLayout layoutOuter = new LinearLayout(getActivity());
                                layoutOuter.setOrientation(LinearLayout.HORIZONTAL);
                                LinearLayout.LayoutParams LparamsRowName = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                LparamsRowName.setMargins(10, 40, 0, 0);
                                LinearLayout layoutInner = new LinearLayout(getActivity());
                                layoutInner.setOrientation(LinearLayout.VERTICAL);
                                for (int o = 0; o < surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMatrixquestionDetails().get(0).getRowDetails().size(); o++) {
                                    RadioGroup rg = drawRadioGroupGrid(o, surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMatrixquestionDetails().get(0).getColumnDetails(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMatrixquestionDetails().get(0).getRowDetails().get(o).getRow_name());
                                    layoutInner.addView(rg);
                                }
                                layoutOuter.addView(layoutInner);
                                scroll.addView(layoutOuter);
                                layout.addView(scroll);
                                ImageView ivs = newEditButton();
                                ivs.setImageResource(R.drawable.edit_icons);
                                layout.addView(ivs);
                                card.addView(layout);
                                lnr_QuestionList.addView(card);
                            }
                        }

                    }
                }


            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }
    }

    /**
     * Use this  method to create a new instance of
     * RadioGroup (Horizontal) using the provided parameters.
     *
     * @param o             index of the row
     * @param columnDetails instance of the columnDetails realmList
     * @param row_name      String name of the row
     * @return instance of RadioGroup.
     */
    private RadioGroup drawRadioGroupGrid(int o, RealmList<columnDetails> columnDetails, String row_name) {
        final RadioButton[] rb = new RadioButton[columnDetails.size()];
        RadioGroup rg = new RadioGroup(getActivity()); //create the RadioGroup
        rg.setGravity(Gravity.CENTER);
        TextView txt = newtextViewWrapGrid(row_name);
        LinearLayout.LayoutParams ledt = new LinearLayout.LayoutParams(250, LinearLayout.LayoutParams.WRAP_CONTENT);
        txt.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        txt.setLayoutParams(ledt);
        txt.setTextSize(12);
        rg.addView(txt);
        rg.setOrientation(RadioGroup.HORIZONTAL);//or RadioGroup.VERTICAL
        LinearLayout.LayoutParams LparamsRowNamee = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        LparamsRowNamee.setMargins(0, 0, 0, 0);

        for (int ii = 0; ii < columnDetails.size(); ii++) {
            rb[ii] = new RadioButton(getActivity());
            rb[ii].setText(columnDetails.get(ii).getColumn_name());
            if (o == 0) {
                rb[ii].setTextColor(getResources().getColor(R.color.colorTextBlack));
            } else {
                rb[ii].setTextColor(getResources().getColor(R.color.colorWhite));
            }
            rb[ii].setLayoutParams(LparamsRowNamee);
            rb[ii].setTextSize(12);
            rb[ii].setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
            rb[ii].setId(ii + 100);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/WorkSans-Regular.ttf");
            rb[ii].setTypeface(font);
            rb[ii].setPadding(20, 0, 20, 0);
            rb[ii].setEnabled(false);
            rb[ii].setButtonDrawable(null);
            TypedArray a = getActivity().getTheme().obtainStyledAttributes(R.style.AppTheme, new int[]{android.R.attr.listChoiceIndicatorSingle});
            int attributeResourceId = a.getResourceId(0, 0);
//            Drawable drawable = getResources().getDrawable(attributeResourceId);
            Drawable drawable = ContextCompat.getDrawable(getContext(),attributeResourceId);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorView));
            rb[ii].setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable);
            rb[ii].setGravity(Gravity.CENTER | Gravity.BOTTOM);
            rg.addView(rb[ii]);
        }
        return rg;
    }

    /**
     * Use this  method to create a new instance of
     * RadioGroup (Horizontal) with the label on top of the
     * radioButtons using the provided parameters.
     *
     * @param startValue
     * @param endValue
     * @return instance of RadioGroup.
     */
    private RadioGroup drawRadioGroupTextTop(int startValue, int endValue) {
        int limit = 0;
        if (startValue == 0) {
            limit = endValue + 1;
        }
        if (startValue == 1) {
            limit = endValue;
        }
        LinearLayout.LayoutParams radioparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        radioparams.setMargins(0, 30, 0, 0);
        final RadioButton[] rb = new RadioButton[limit];
        RadioGroup rg = new RadioGroup(getActivity()); //create the RadioGroup
        rg.setGravity(Gravity.CENTER);
        rg.setOrientation(RadioGroup.HORIZONTAL);//or RadioGroup.VERTICAL
        rg.setLayoutParams(radioparams);
        int i = 0;
        while (startValue <= endValue) {

            rb[i] = new RadioButton(getActivity());
            rb[i].setText("" + startValue);
            rb[i].setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
            rb[i].setEnabled(false);
            rb[i].setButtonDrawable(null);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/WorkSans-Regular.ttf");
            rb[i].setTypeface(font);
            TypedArray a = getActivity().getTheme().obtainStyledAttributes(R.style.AppTheme, new int[]{android.R.attr.listChoiceIndicatorSingle});
            int attributeResourceId = a.getResourceId(0, 0);
            Drawable drawable = ContextCompat.getDrawable(getContext(),attributeResourceId);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorView));
            rb[i].setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable);
            rb[i].setGravity(Gravity.CENTER | Gravity.BOTTOM);
            rg.addView(rb[i]);
            i++;
            startValue++;

        }

        return rg;
    }

    private Bitmap loadImageFromStorage(String path) {
        byte[] decodedString = Base64.decode(path, Base64.NO_WRAP);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

    /**
     * Use this  method to create a new instance of
     * RadioGroup (Vertical) using the provided parameters.
     *
     * @param size                        number of RadioButton in the RadioGroup
     * @param multiplechoicesingleDetails instance of RealmList to get the text of RadioButton
     * @return instance of RadioGroup.
     */
    private RadioGroup createRadioGroup(int size, RealmList<multiplechoicesingleDetails> multiplechoicesingleDetails) {
        LinearLayout.LayoutParams radioparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        radioparams.setMargins(0, 30, 0, 0);
        RadioGroup rg = new RadioGroup(getActivity()); //create the RadioGroup
        final RadioButton[] rb = new RadioButton[size];
        rg.setOrientation(RadioGroup.VERTICAL);//or RadioGroup.VERTICAL
        rg.setLayoutParams(radioparams);
        for (int ii = 0; ii < size; ii++) {
            rb[ii] = new RadioButton(getActivity());
            rb[ii].setText(multiplechoicesingleDetails.get(ii).getTextChoice());
            rb[ii].setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER);
            rb[ii].setId(ii + 100);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/WorkSans-Regular.ttf");
            rb[ii].setTypeface(font);
            rb[ii].setEnabled(false);
            rg.addView(rb[ii]);
        }
        return rg;
    }

    /**
     * Use this  method to create a new instance of
     * CardView using the provided parameters.
     *
     * @param id question ID
     * @return instance of CardView.
     */
    private CardView drawCard(int id) {
        CardView card = new CardView(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        cardIds.add(card);
        params.setMargins(30, 30, 30, 30);
        card.setLayoutParams(params);
        card.setRadius(9);
        card.setContentPadding(15, 15, 15, 15);
        card.setCardBackgroundColor(getResources().getColor(R.color.colorWhite));
        card.setMaxCardElevation(10);
        card.setOnClickListener(this);
        card.setCardElevation(5);
        card.setId(id);
        return card;
    }

    /**
     * Use this  method to create a new instance of
     * EditText using the provided parameters.
     *
     * @param hintText Hint text
     * @return instance of EditText.
     */
    private EditText newedittextz(String hintText) {
        final EditTextRegular editText = new EditTextRegular(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(10, 30, 0, 10);
        editText.setLayoutParams(params);
        editText.setEnabled(false);
        editText.setHint(hintText);
        editText.setTextSize(14);
        editText.setTextColor(Color.rgb(0, 0, 0));
        return editText;
    }

    /**
     * Use this  method to create a new instance of
     * TextView (Width Match) .
     *
     * @param
     * @return instance of TextView.
     */
    private TextView newtextView() {
        final TextViewRegular textView = new TextViewRegular(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(10, 30, 0, 0);
        textView.setLayoutParams(params);
        textView.setTextSize(14);
        textView.setTextColor(Color.rgb(0, 0, 0));
        textView.setMaxEms(2);
        return textView;
    }

    /**
     * Use this  method to create a new instance of
     * TextView (Width Wrap) using the provided parameters.
     *
     * @param rowName Row text
     * @return instance of TextView.
     */
    private TextView newtextViewWrapGrid(String rowName) {
        final TextViewRegular textView = new TextViewRegular(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(10, 30, 0, 0);
        textView.setLayoutParams(params);
        textView.setTextSize(14);
        textView.setText(rowName);
        textView.setTextColor(Color.rgb(0, 0, 0));
        textView.setMaxEms(2);
        return textView;
    }

    /**
     * Use this  method to create a new instance of
     * TextView (Width 0) using the provided parameters.
     *
     * @param
     * @return instance of TextView.
     */
    private TextView newtextViewWrap() {
        final TextViewRegular textView = new TextViewRegular(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        params.setMargins(10, 30, 0, 0);
        textView.setLayoutParams(params);
        textView.setTextSize(14);
        textView.setTextColor(Color.rgb(0, 0, 0));
        textView.setMaxEms(2);
        return textView;
    }

    /**
     * Use this  method to create a new instance of
     * ImageView (Width 150) using the provided parameters.
     *
     * @param context     context of app
     * @param imageToBind Bitmap of the image
     * @return instance of ImageView.
     */
    public ImageView newImageview(Context context, Bitmap imageToBind) {
        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 150);
        params.setMargins(10, 20, 0, 0);
        final ImageView imgView = new ImageView(context);
        imgView.setLayoutParams(params);
        imgView.setScaleType(ImageView.ScaleType.FIT_XY);
        imgView.setImageBitmap(imageToBind);
        return imgView;
    }

    /**
     * Use this  method to initialize
     * the views using the provided parameters.
     *
     * @param v instance of View
     */
    private void initViews(View v) {
        setProgressBar();
        lnr_QuestionList = (LinearLayout) v.findViewById(R.id.lnr_QuestionList);
        tvAdd = (TextView) v.findViewById(R.id.tvAdd);
        tvSubmit = (TextView) v.findViewById(R.id.tvSubmit);
        tvSurveyName = (TextView) v.findViewById(R.id.tvSurveyName);

        tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BusFactory.getBus().post(new NavigateEditStartQuestionFragmentEvent());


            }
        });
        tvSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callSubmit();
            }
        });
    }

    /**
     * Use this  method to change the status
     * of the survey to completed
     */
    private void callSubmit() {


        mRealm = Realm.getDefaultInstance();
        surveyDetails = mRealm.where(SurveyDetails.class)
                .equalTo("SurveyId", userPref.getSurveyId()).findFirst();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (surveyDetails != null) {
                        surveyDetails.setSurveyComplete(1);
                        surveyDetails.setSurveySync(0);
                        surveyDetails.setEditflag("true");
                    }

                }
            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }

        sendDataToserver();




//        mRealm = Realm.getDefaultInstance();
//        surveyDetails = mRealm.where(SurveyDetails.class)
//                .equalTo("SurveyId", userPref.getSurveyId()).findFirst();
//        try {
//            mRealm.executeTransaction(new Realm.Transaction() {
//                @Override
//                public void execute(Realm realm) {
//                    if (surveyDetails != null) {
//                        surveyDetails.setSurveyComplete(1);
//                    }
//                }
//            });
//        } catch (Exception em) {
//            em.printStackTrace();
//        } finally {
//            if (mRealm != null)
//                mRealm.close();
//        }
//        userPref.saveSurveyId("");
//        BusFactory.getBus().post(new NavigateBack());
//        BusFactory.getBus().post(new NavigateBack());
//        BusFactory.getBus().post(new NavigateBack());
//        BusFactory.getBus().post(new NavigateBack());
//        BusFactory.getBus().post(new RefreshListFeedback());

    }

    private void sendDataToserver() {
        userPref.saveSurveyId("");
        BusFactory.getBus().post(new NavigateBack());
        BusFactory.getBus().post(new NavigateBack());
        BusFactory.getBus().post(new NavigateBack());
        BusFactory.getBus().post(new NavigateBack());
        BusFactory.getBus().post(new NavigateBack());
        BusFactory.getBus().post(new RefreshListFeedback());








    }



    @Override
    public void onQuestionClicked() {
        Toast.makeText(getActivity(), "" + "cliked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        Log.e( "ID OF THE QUESTION: ", ""+v.getId());
        BusFactory.getBus().post(new NavigateToSurveyEditQuestionEvent(v.getId()));
    }



    @Override
    public void onHttpAddMultipleSurveySuccess(String Message) {
        cancelProgress();

        Toast.makeText(getActivity(), ""+Message, Toast.LENGTH_SHORT).show();
                userPref.saveSurveyId("");
        BusFactory.getBus().post(new NavigateBack());
        BusFactory.getBus().post(new NavigateBack());
        BusFactory.getBus().post(new NavigateBack());
        BusFactory.getBus().post(new NavigateBack());
        BusFactory.getBus().post(new NavigateBack());
        BusFactory.getBus().post(new SyncSurveyAfterSubmit());
        BusFactory.getBus().post(new RefreshListFeedback());

    }




    private ImageView newEditButton() {
        final ImageView textView = new ImageView(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        params.setMargins(40, 10, 10, 0);
        params.gravity=Gravity.RIGHT;
//        textView.getLayoutParams().height = 20;
//        textView.getLayoutParams().width = 20;
        textView.setLayoutParams(params);
        return textView;
    }

    @Override
    public void onHttpAddMultipleSurveyFailed(String message) {
        cancelProgress();
        Toast.makeText(getActivity(), ""+message, Toast.LENGTH_SHORT).show();
    }
}

