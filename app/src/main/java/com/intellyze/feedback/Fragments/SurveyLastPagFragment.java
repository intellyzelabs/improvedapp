package com.intellyze.feedback.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;

import com.intellyze.feedback.Bus.BusFactory;
import com.intellyze.feedback.Bus.Events.NavigateToMainScreenEvent;
import com.intellyze.feedback.R;

/**
 * Created by INTELLYZE-202 on 21-12-2017.
 */

public class SurveyLastPagFragment extends BaseFragment {
    Button btnFinish;
    public static final String ARG_PAGE = "ARG_PAGE";

    public SurveyLastPagFragment() {
        // Required empty public constructor
    }

    public static SurveyLastPagFragment newInstance(int surveyid) {
        SurveyLastPagFragment fragment = new SurveyLastPagFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, surveyid);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_survey_last_page, container, false);
        initViews(v);
        btnFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                BusFactory.getBus().post(new NavigateToMainScreenEvent());
            }
        });
        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void initViews(View v) {
        btnFinish = (Button) v.findViewById(R.id.btnFinish);
    }

}
