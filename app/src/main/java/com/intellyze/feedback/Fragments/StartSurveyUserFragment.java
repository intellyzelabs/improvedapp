package com.intellyze.feedback.Fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.intellyze.feedback.Bus.BusFactory;
import com.intellyze.feedback.Bus.Events.NavigateSurveyUserDetailsFillFragmentEvent;
import com.intellyze.feedback.R;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.FeedQuestUserSide;

import io.realm.Realm;

/**
 * Created by INTELLYZE-202 on 01-12-2017.
 */

public class StartSurveyUserFragment extends BaseFragment {
    LinearLayout lnrStartQuestions;
    public static final String ARG_ID = "ARG_ID";
    private int mId;
    FeedQuestUserSide feedQuestUserSides;
    ImageView imgSurveyImage;
    Realm mRealm;
    TextView tvFeedbackTitle, tvSurveyDescription;

    public StartSurveyUserFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StartSurveyUserFragment newInstance(int id) {
        StartSurveyUserFragment fragment = new StartSurveyUserFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mId = getArguments().getInt(ARG_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_start_survey_user, container, false);
        initViews(v);
        getDataFromLocal();
        lnrStartQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BusFactory.getBus().post(new NavigateSurveyUserDetailsFillFragmentEvent(mId));
            }
        });
        return v;
    }

    private void getDataFromLocal() {

        mRealm = Realm.getDefaultInstance();
        String sId = String.valueOf(mId);
        feedQuestUserSides = mRealm.where(FeedQuestUserSide.class).equalTo("mSurveyId", sId).findFirst();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (feedQuestUserSides != null) {
                        tvFeedbackTitle.setText(feedQuestUserSides.getSurveyTitle());
                        tvSurveyDescription.setText(feedQuestUserSides.getSurveyDescr());
                        if (feedQuestUserSides.getmSurveyImage().equals("")) {
                        } else {
                            Bitmap bitmap = decodeImage(feedQuestUserSides.getmSurveyImage());
                            imgSurveyImage.setImageBitmap(bitmap);
                        }
                    }
                }

            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }
    }

    private void initViews(View v) {
        lnrStartQuestions = (LinearLayout) v.findViewById(R.id.lnrStartQuestions);
        tvFeedbackTitle = (TextView) v.findViewById(R.id.tvFeedbackTitle);
        tvSurveyDescription = (TextView) v.findViewById(R.id.tvSurveyDescription);
        imgSurveyImage = (ImageView) v.findViewById(R.id.imgSurveyImage);
    }
}