package com.intellyze.feedback.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.intellyze.feedback.Activities.AnalysisActivity;
import com.intellyze.feedback.Activities.AnalysisNewDesign;
import com.intellyze.feedback.Adapters.MySurveyResponcesListAdminRecyclerViewAdapter;
import com.intellyze.feedback.Adapters.SurveyListAdapter;
import com.intellyze.feedback.Api.ListSurveyAnswerAdmin.HttpRequestForLiostAnswerSurvey;
import com.intellyze.feedback.Api.ListSurveyAnswerAdmin.OnHttpRequestForListSurveyAnswerSurvey;
import com.intellyze.feedback.Api.ListSurveyAnswerAdmin.ResultDatum;
import com.intellyze.feedback.Api.ListSurveyAnswerAdmin.SurveyAnswerAdminListBean;
import com.intellyze.feedback.Api.Login.HttpLogin;
import com.intellyze.feedback.Api.UpdateSurveyStatus.HttpResponseForServerStatusUpdate;
import com.intellyze.feedback.Api.UpdateSurveyStatus.OnHttpResponseForServerStatusUpdate;
import com.intellyze.feedback.Bus.BusFactory;
import com.intellyze.feedback.Bus.Events.NavigateToMainScreenEvent;
import com.intellyze.feedback.Custom.ButtonRegular;
import com.intellyze.feedback.Custom.TextViewMedium;
import com.intellyze.feedback.Fragments.dummy.DummyContent;
import com.intellyze.feedback.Models.SurveyAnswerListForRecyclerView;
import com.intellyze.feedback.Models.SurveyDetailsModel;
import com.intellyze.feedback.R;
import com.intellyze.feedback.Realm.AddAnswerMaster;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.FeedQuestUserSide;
import com.intellyze.feedback.Realm.QuestionMoreDetails;
import com.intellyze.feedback.Realm.SurveyAnswerListAdmin;
import com.intellyze.feedback.Realm.SurveyDetails;
import com.intellyze.feedback.Realm.SurveyQuestionsDetails;
import com.intellyze.feedback.Realm.multiplechoicesingleDetails;
import com.intellyze.feedback.Utils.UserPref;
import com.zcw.togglebutton.ToggleButton;

import java.util.ArrayList;
import java.util.List;

import javax.security.auth.login.LoginException;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by INTELLYZE-202 on 01-12-2017.
 */

public class ViewResponseFragment extends BaseFragment implements OnHttpResponseForServerStatusUpdate, OnHttpRequestForListSurveyAnswerSurvey {
    TextView tvLastEntry, tvStatuss, tvTotalResponse;
    TextViewMedium text_surveyname;
    ToggleButton toggleStatus;
    String Surveystatus = "",mId,status = "",lastEntry = "",opernorclose = "";
    public static final String ARG_PAGE = "ARG_PAGE";
    public static final String ARG_RES = "ARG_RES";
    public static final String ARG_STATUS = "ARG_STATUS";
    public static final String ARG_LAST = "ARG_LAST";
    public static final String ARG_OPEN = "ARG_OPEN";
    public static final String ARG_NAME = "ARG_NAME";
    int totResponse;
    Realm mRealm;
    FeedQuestUserSide addFeedQuestUserSide;
    SurveyDetails surveyDetails;
    private ButtonRegular analysis_button;
RecyclerView recyclerView;
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private UserPref userPref;
    private SurveyAnswerListAdmin surveyAnswerListAdmin;
    private ArrayList<SurveyAnswerListForRecyclerView> surveyAnswerAdminModel;
    private RealmResults<SurveyAnswerListAdmin> surveyAnswerListAdminModel;
    private RealmResults<SurveyAnswerListAdmin> surveyAnswerListAdminnewdata;
    private String surveynames;

    public ViewResponseFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param
     * @param totResponse
     * @param statusSurvey
     * @param lastEntry
     * @param opernorclose
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ViewResponseFragment newInstance(String survey, int totResponse, String statusSurvey, String lastEntry, String opernorclose,String surveyname) {
        ViewResponseFragment fragment = new ViewResponseFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PAGE, survey);
        args.putInt(ARG_RES, totResponse);
        args.putString(ARG_STATUS, statusSurvey);
        args.putString(ARG_LAST, lastEntry);
        args.putString(ARG_OPEN, opernorclose);
        args.putString(ARG_NAME, surveyname);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mId = getArguments().getString(ARG_PAGE);
        totResponse = getArguments().getInt(ARG_RES);
        totResponse = getArguments().getInt(ARG_RES);
        Surveystatus = getArguments().getString(ARG_STATUS);
        lastEntry = getArguments().getString(ARG_LAST);
        opernorclose = getArguments().getString(ARG_OPEN);
        surveynames = getArguments().getString(ARG_NAME);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_view_response, container, false);
        initViews(v);
        userPref = new UserPref(getActivity());
        showProgress("Loading");
        listSurveyDetails();

        if (lastEntry.equals("")) {
            tvLastEntry.setText("No entry");
        } else {
            tvLastEntry.setText(lastEntry + "");
        }

//        if (opernorclose.equals("Active")) {
//            tvStatuss.setText("OPEN");
//            toggleStatus.setToggleOn();

//        }
//        else
//        {
//
//            tvStatuss.setText("CLOSE");
//            toggleStatus.setToggleOff();
//        }






        if(totResponse==0)
        {
            analysis_button.setVisibility(View.GONE);
        }
        tvTotalResponse.setText(totResponse + "");
        text_surveyname.setText(surveynames);

        toggleStatus.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on) {
                    status = "open";
                    tvStatuss.setText("OPEN");
                    callStatusUpdate("Active");
                    Surveystatus = "Active";

                } else {
                    status = "close";
                    tvStatuss.setText("CLOSE");
                    callStatusUpdate("De-active");
                    Surveystatus = "De-active";
                }
            }
        });
        if (Surveystatus.equals("De-active")) {
            toggleStatus.setToggleOff();
        } else {
            toggleStatus.setToggleOn();
        }
        analysis_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), AnalysisNewDesign.class);
                intent.putExtra("UID",mId);
                startActivity(intent);
            }
        });
        return v;
    }

    private void listSurveyDetails() {



        if(isConnectedToNet(getActivity()))
        {
            SurveyAnswerAdminListBean surveyAnswerAdminListBean = new SurveyAnswerAdminListBean();
            surveyAnswerAdminListBean.setBusinessId(userPref.getBusinessId());
            surveyAnswerAdminListBean.setSurveyId(Long.valueOf(mId));


            HttpRequestForLiostAnswerSurvey httpRequestForLiostAnswerSurvey = new HttpRequestForLiostAnswerSurvey(this);
            httpRequestForLiostAnswerSurvey.getAnswerSurveyList(surveyAnswerAdminListBean);

        }
        else {

            getDataFromRelamAndList();



        }
    }

    private void getDataFromRelamAndList() {


            surveyAnswerAdminModel = new ArrayList<SurveyAnswerListForRecyclerView>();
            mRealm = Realm.getDefaultInstance();
            surveyAnswerListAdminModel = mRealm.where(SurveyAnswerListAdmin.class).equalTo("mSurveyId", mId).findAll();
            try {
                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        for (int i = 0; i < surveyAnswerListAdminModel.size(); i++) {
                            SurveyAnswerListForRecyclerView surveyAnswerListForRecyclerView = new SurveyAnswerListForRecyclerView();
                            surveyAnswerListForRecyclerView.setAge(surveyAnswerListAdminModel.get(i).getAge());
                            surveyAnswerListForRecyclerView.setAnswerDate(surveyAnswerListAdminModel.get(i).getAnswerDate());
                            surveyAnswerListForRecyclerView.setAnswerId(surveyAnswerListAdminModel.get(i).getAnswerId());
                            surveyAnswerListForRecyclerView.setAnswerTime(surveyAnswerListAdminModel.get(i).getAnswerTime());
                            surveyAnswerListForRecyclerView.setEmail(surveyAnswerListAdminModel.get(i).getEmail());
                            surveyAnswerListForRecyclerView.setFullName(surveyAnswerListAdminModel.get(i).getFullName());
                            surveyAnswerListForRecyclerView.setGender(surveyAnswerListAdminModel.get(i).getGender());
                            surveyAnswerListForRecyclerView.setMobileNo(surveyAnswerListAdminModel.get(i).getMobileNo());
                            surveyAnswerListForRecyclerView.setProfession(surveyAnswerListAdminModel.get(i).getProfession());
                            surveyAnswerListForRecyclerView.setSurveyId(surveyAnswerListAdminModel.get(i).getSurveyId());
                            surveyAnswerListForRecyclerView.setSurveyTitle(surveyAnswerListAdminModel.get(i).getSurveyTitle());

                            surveyAnswerAdminModel.add(surveyAnswerListForRecyclerView);
                        }
                    }
                });
            } catch (Exception em) {
                em.printStackTrace();
            } finally {
                if (mRealm != null)
                    mRealm.close();
            }
            if (surveyAnswerAdminModel.size() == 0) {
//                lnrEmptyScreen.setVisibility(View.VISIBLE);
//                Toast.makeText(getActivity(), "Item nis null", Toast.LENGTH_SHORT).show();
            } else {
//                rv_SurveyList.setHasFixedSize(true);
//                mLayoutManager = new LinearLayoutManager(getActivity());
//                rv_SurveyList.setLayoutManager(mLayoutManager);
//                SurveyListAdapter surveyListAdapter = new SurveyListAdapter(getActivity(), this, surveyAnswerAdminModel);
//                rv_SurveyList.setAdapter(surveyListAdapter);
//                lnr_createSurvey.setVisibility(View.VISIBLE);
//                rv_SurveyList.setVisibility(View.VISIBLE);
//                rv_SurveyListQuestion.setVisibility(View.GONE);


                if (mColumnCount <= 1) {
                    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
                } else {
                    recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), mColumnCount));
                }
                recyclerView.setAdapter(new MySurveyResponcesListAdminRecyclerViewAdapter(surveyAnswerAdminModel, mListener));

            }




    }

    private void updateInLocal() {
        mRealm = Realm.getDefaultInstance();
        addFeedQuestUserSide = mRealm.where(FeedQuestUserSide.class).equalTo("mSurveyId", mId).findFirst();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (addFeedQuestUserSide != null) {
                        addFeedQuestUserSide.setSStatus(Surveystatus);
                        analysis_button.setVisibility(View.VISIBLE);
                    }

                }
            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }

    }

    private void checkStatus() {
        if (status.equals("open")) {
            toggleStatus.setToggleOff();
            tvStatuss.setText("OPEN");
        } else {
            toggleStatus.setToggleOn();
            tvStatuss.setText("CLOSE");
        }
    }

    private void callStatusUpdate(String status) {
        int afd = Integer.parseInt(mId);
        HttpResponseForServerStatusUpdate httpResponseForServerStatusUpdate = new HttpResponseForServerStatusUpdate(this);
        httpResponseForServerStatusUpdate.UpdateStaus(afd, status);
    }

    private void initViews(View v) {
        tvTotalResponse = (TextView) v.findViewById(R.id.tvTotalResponse);
        text_surveyname =  v.findViewById(R.id.text_surveyname);
        tvLastEntry = (TextView) v.findViewById(R.id.tvLastEntry);
        tvStatuss = (TextView) v.findViewById(R.id.tvStatuss);
        toggleStatus = (ToggleButton) v.findViewById(R.id.toggleStatus);
        analysis_button = (ButtonRegular) v.findViewById(R.id.analysis_button);
        recyclerView = (RecyclerView) v.findViewById(R.id.list_responces);
        setProgressBar();
    }


    @Override
    public void onHttpServerStatusUpdateSuccess(String Message) {
        updateInLocal();
        updateInLocalSurveyDetails();
//        Toast.makeText(getActivity(), "" + Message, Toast.LENGTH_SHORT).show();
    }

    private void updateInLocalSurveyDetails() {
        mRealm = Realm.getDefaultInstance();
        surveyDetails = mRealm.where(SurveyDetails.class).equalTo("SurveyId", mId).findFirst();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (addFeedQuestUserSide != null) {
                        surveyDetails.setSurveyStatus(Surveystatus);
                        analysis_button.setVisibility(View.VISIBLE);
                    }

                }
            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }

    }

    @Override
    public void onHttpServerStatusUpdateFailed(String message) {
//        Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
        checkStatus();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
             mListener = (OnListFragmentInteractionListener) context;;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onHttpAddForListSurveyAnswerSurveySuccess(List<ResultDatum> list, String Message) {
//        Toast.makeText(getActivity(), ""+Message, Toast.LENGTH_SHORT).show();

sendDatatoRelam(list);
        getDataFromRelamAndList();
        cancelProgress();

    }

    private void callDeleteLocal() {
        mRealm = Realm.getDefaultInstance();
        surveyAnswerListAdminnewdata = mRealm.where(SurveyAnswerListAdmin.class).findAll();

//        surveyQuestionsDetails=mRealm.where(SurveyQuestionsDetails.class).findAll();

        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (surveyAnswerListAdminnewdata.size() != 0) {
                        surveyAnswerListAdminnewdata.deleteAllFromRealm();

                    }
                }
            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }
//        getDataFromServer();
    }
    private void sendDatatoRelam(final List<ResultDatum> list) {



        callDeleteLocal();
        mRealm = Realm.getDefaultInstance();





        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    for (int o = 0; o < list.size(); o++) {
                        surveyAnswerListAdmin = mRealm.copyToRealm(new SurveyAnswerListAdmin());
                        surveyAnswerListAdmin.setAge(list.get(o).getAge());
                        surveyAnswerListAdmin.setAnswerDate(list.get(o).getAnswerDate());
                        surveyAnswerListAdmin.setAnswerId(list.get(o).getAnswerId());
                        surveyAnswerListAdmin.setAnswerTime(list.get(o).getAnswerTime());
                        surveyAnswerListAdmin.setEmail(list.get(o).getEmail());
                        surveyAnswerListAdmin.setFullName(list.get(o).getFullName());
                        surveyAnswerListAdmin.setGender(list.get(o).getGender());
                        surveyAnswerListAdmin.setMobileNo(list.get(o).getMobileNo());
                        surveyAnswerListAdmin.setProfession(list.get(o).getProfession());
                        surveyAnswerListAdmin.setSurveyId(list.get(o).getSurveyId());
                        surveyAnswerListAdmin.setSurveyTitle(list.get(o).getSurveyTitle());
                    }


//                    Toast.makeText(getActivity(), "" + "Updated successfully!", Toast.LENGTH_SHORT).show();

                }
            });
        }
        catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }
    }

    @Override
    public void onHttpAddForListSurveyAnswerSurveyFailed(String message) {
//        Toast.makeText(getActivity(), ""+message, Toast.LENGTH_SHORT).show();

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name


        void onListFragmentInteraction(String answerId, String surveyId);
    }

}
