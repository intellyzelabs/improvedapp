package com.intellyze.feedback.Fragments;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Scroller;
import android.widget.Toast;

import com.intellyze.feedback.Bus.BusFactory;
import com.intellyze.feedback.Bus.Events.NavigateStartQuestionFragmentEvent;
import com.intellyze.feedback.R;
import com.intellyze.feedback.Realm.SurveyDetails;
import com.intellyze.feedback.Utils.UserPref;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import io.realm.Realm;

import static android.app.Activity.RESULT_OK;

/**
 * Created by INTELLYZE-202 on 30-11-2017.
 */

public class CreateSurveyFirstFragment extends BaseFragment {
    Realm mRealm;
    SurveyDetails surveyDetails;
    EditText edtAddress,edtSurveyName;
    ImageView imgSurveyImage;
    FrameLayout frmAddSurveyImage;
    LinearLayout tvAdd;
    private static final int REQUEST_WRITE_PERMISSION = 786;
    int select_photo = 1;
    Bitmap selectedImage ;
    String encodedImage = "";

    public CreateSurveyFirstFragment() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CreateSurveyFirstFragment newInstance() {
        CreateSurveyFirstFragment fragment = new CreateSurveyFirstFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_create_survey_first_page, container, false);
        mRealm = Realm.getDefaultInstance();
        initViews(v);
        frmAddSurveyImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermission();

            }
        });
        return v;
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        } else {
            openFilePicker();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openFilePicker();
        }
    }

    private void openFilePicker() {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, select_photo);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == select_photo && resultCode == RESULT_OK && null != data) {
            Uri selectedImageuri = data.getData();
            final InputStream imageStream;
            try {
                imageStream = getActivity().getContentResolver().openInputStream(selectedImageuri);
                selectedImage = BitmapFactory.decodeStream(imageStream);
                imgSurveyImage.setImageBitmap(selectedImage);


            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
//            loadImageFromStorage(encodedImage);
            selectedImage =getResizedBitmap(selectedImage,400,400);
            encodedImage = encodeImage(selectedImage);
        }
    }
    public Bitmap getResizedBitmap(Bitmap image, int bitmapWidth, int bitmapHeight) {
        return Bitmap.createScaledBitmap(image, bitmapWidth, bitmapHeight, true);
    }
    private Bitmap loadImageFromStorage(String path) {
        Bitmap b = null;
        try {
            File f = new File(path, "profile.jpg");
            b = BitmapFactory.decodeStream(new FileInputStream(f));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return b;
    }
    private String saveToInternalStorage(Bitmap bitmapImage){
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,System.currentTimeMillis()+"_improved.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String s= String.valueOf(mypath);
        return s;
    }
//
    private void initViews(View v) {
        imgSurveyImage = (ImageView) v.findViewById(R.id.imgSurveyImage);
        frmAddSurveyImage = (FrameLayout) v.findViewById(R.id.frmAddSurveyImage);
        tvAdd = (LinearLayout) v.findViewById(R.id.tvAdd);
        edtSurveyName = (EditText) v.findViewById(R.id.edtSurveyName);
        edtAddress = (EditText) v.findViewById(R.id.edtAddress);
        edtAddress.setScroller(new Scroller(getActivity()));
        edtAddress.setMinLines(2);
        edtAddress.setVerticalScrollBarEnabled(true);
        tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtSurveyName.getText().toString().trim().equals("")) {
                    edtSurveyName.setError(getActivity().getResources().getString(R.string.cant_be_blank));
                } else {
                    mRealm = Realm.getDefaultInstance();
                    surveyDetails = mRealm.where(SurveyDetails.class)
                            .equalTo("SurveyName", edtSurveyName.getText().toString().trim()).findFirst();
                    try {
                        mRealm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                if (surveyDetails != null) {
                                    Toast.makeText(getActivity(), "" + "Survey Name already exist! please try another name", Toast.LENGTH_SHORT).show();

                                }
                                else
                                {

                                    SurveyDetails surveyDetails = mRealm.createObject(SurveyDetails.class);
                                    String ids=UUID.randomUUID().toString();
                                    surveyDetails.setSurveyId(ids);
                                    surveyDetails.setEditflag("false");
                                    surveyDetails.setSurveyName(edtSurveyName.getText().toString().trim());
                                    surveyDetails.setSurveyStatus("Active");
                                    DateFormat df1=new SimpleDateFormat("yyyy-MM-dd");//foramt date
                                    String date=df1.format(Calendar.getInstance().getTime());
                                    DateFormat df = new SimpleDateFormat("HH:mm"); //format time
                                    String time = df.format(Calendar.getInstance().getTime());
                                    surveyDetails.setSurveyDate(date);
                                    surveyDetails.setSurveyTime(time);
                                    surveyDetails.setSurveyComplete(0);
                                    surveyDetails.setSurveySync(0);
                                    surveyDetails.setSurveyImage(encodedImage);
                                    surveyDetails.setSurveyDescription(edtAddress.getText().toString().trim());
                                    mRealm.copyToRealm(surveyDetails);
                                    UserPref userPref = new UserPref(getActivity());
                                    userPref.saveSurveyName(edtSurveyName.getText().toString().trim());
                                    userPref.SaveCount(1);
                                    userPref.saveSurveyId(ids);
                                    View view = getActivity().getCurrentFocus();
                                    if (view != null) {
                                        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                                    }
                                    BusFactory.getBus().post(new NavigateStartQuestionFragmentEvent());

                                }
                            }
                        });
                    } catch (Exception em) {
                        em.printStackTrace();
                    } finally {
                        if (mRealm != null)
                            mRealm.close();
                    }
                }
            }
        });
    }
}
