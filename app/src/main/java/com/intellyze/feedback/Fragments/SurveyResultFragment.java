package com.intellyze.feedback.Fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.intellyze.feedback.Bus.BusFactory;
import com.intellyze.feedback.Bus.Events.NavigateStartSurveyUserFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateToContinueFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateToContinueSurveyFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateToDeleteSurveyFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateToEditSurveyFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateToSurveySurveyFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateViewResponseFragmentEvent;
import com.intellyze.feedback.R;
import com.intellyze.feedback.Realm.SurveyDetails;

import io.realm.Realm;

/**
 * Created by INTELLYZE-202 on 01-12-2017.
 */

public class SurveyResultFragment extends BaseFragment {
    LinearLayout lnrViewResponse, lnrStartSurveyUserPage,edit_surway,delete_surway;
    public static final String ARG_PAGE = "ARG_PAGE";
    ImageView imgSurveyImage;
    TextView tvCreatedOn, tv_lastEntry, tvSurveyName, tvSurveyDescription;
    Realm mRealm;
    SurveyDetails surveyDetails;
    LinearLayout lnr_GetFeedback;
    LinearLayout lnr_GetRePort;
    int complete = 0,sync = 0,totResponse=0;
    String mId,statusSurvey="",lastEntry = "",surveyName = "";
    private String opernorclose;

    public SurveyResultFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param survey
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SurveyResultFragment
    newInstance(String survey) {
        SurveyResultFragment fragment = new SurveyResultFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PAGE, survey);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mId = getArguments().getString(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_survey_result, container, false);
        initViews(v);
        dataFromReaalm();
        lnr_GetFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (complete == 1) {
                    if (sync == 1) {
                        int fgg = Integer.parseInt(mId);
                        BusFactory.getBus().post(new NavigateStartSurveyUserFragmentEvent(fgg));
                    } else {
                        Toast.makeText(getActivity(), surveyName + " Not synched with server, please try again later!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), surveyName + " is incomplete, please complete and try again later!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        lnr_GetRePort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mId.equals("") ) {
                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                    BusFactory.getBus().post(new NavigateToSurveySurveyFragmentEvent(mId));

                } else {
                    Toast.makeText(getActivity(),  " No item !", Toast.LENGTH_SHORT).show();
                }

            }
        });



        lnrViewResponse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (complete == 1) {
                    if (sync == 1) {
                        BusFactory.getBus().post(new NavigateViewResponseFragmentEvent(mId,totResponse,statusSurvey,lastEntry,opernorclose,surveyName));
                    } else {
                        Toast.makeText(getActivity(), surveyName + " Not synched with server, please try again later!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), surveyName + " is incomplete, please complete and try again later!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        edit_surway.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mId.equals("") ) {
                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                    BusFactory.getBus().post(new NavigateToEditSurveyFragmentEvent(mId));

                } else {
                    Toast.makeText(getActivity(),  " No item !", Toast.LENGTH_SHORT).show();
                }
            }
        });
        delete_surway.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mId.equals("") ) {
                    View view = getActivity().getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                    BusFactory.getBus().post(new NavigateToDeleteSurveyFragmentEvent(mId));

                } else {
                    Toast.makeText(getActivity(),  " No item !", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return v;
    }

    private void dataFromReaalm() {
        mRealm = Realm.getDefaultInstance();
        surveyDetails = mRealm.where(SurveyDetails.class)
                .equalTo("SurveyId", mId).findFirst();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (surveyDetails != null) {
                        surveyName = surveyDetails.getSurveyName();
                        statusSurvey=surveyDetails.getSurveyStatus();
                        tvSurveyName.setText(surveyDetails.getSurveyName());
                        tv_lastEntry.setText(surveyDetails.getLastEntryDate() + "  " + surveyDetails.getLastEntryTime());
                        lastEntry=surveyDetails.getLastEntryDate();
                        tvCreatedOn.setText(surveyDetails.getSurveyDate() + "  " + surveyDetails.getSurveyTime());
                        tvSurveyDescription.setText(surveyDetails.getSurveyDescription());
                        totResponse=surveyDetails.getCount();
                        sync = surveyDetails.getSurveySync();
                        complete = surveyDetails.getSurveyComplete();
                        opernorclose = surveyDetails.getSurveyStatus();
                        if(surveyDetails.getSurveyImage().equals("")){
                            imgSurveyImage.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.ic_intellyze_logo));
                        }else {
                            Bitmap bitmap = decodeImage(surveyDetails.getSurveyImage());
                            imgSurveyImage.setImageBitmap(bitmap);
                        }

                        if (surveyDetails.getCount()>0)
                        {


                            delete_surway.setVisibility(View.GONE);
                            lnrViewResponse.setVisibility(View.VISIBLE);
                        }
                        else
                        {


                            lnrViewResponse.setVisibility(View.GONE);
                            delete_surway.setVisibility(View.VISIBLE);
                        }
                    }
                }


            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }
    }

    private void initViews(View v) {
        lnrViewResponse = (LinearLayout) v.findViewById(R.id.lnrViewResponse);
        lnr_GetRePort = (LinearLayout) v.findViewById(R.id.lnr_GetRePort);
        edit_surway = (LinearLayout) v.findViewById(R.id.edit_surway);
        delete_surway = (LinearLayout) v.findViewById(R.id.delete_surway);
        lnr_GetFeedback = (LinearLayout) v.findViewById(R.id.lnr_GetFeedback);
        imgSurveyImage = (ImageView) v.findViewById(R.id.imgSurveyImage);
        tvSurveyDescription = (TextView) v.findViewById(R.id.tvSurveyDescription);
        tvSurveyName = (TextView) v.findViewById(R.id.tvSurveyName);
        tvCreatedOn = (TextView) v.findViewById(R.id.tvCreatedOn);
        tv_lastEntry = (TextView) v.findViewById(R.id.tv_lastEntry);
    }
}
