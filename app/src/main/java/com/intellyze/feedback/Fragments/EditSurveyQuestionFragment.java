package com.intellyze.feedback.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.intellyze.feedback.Adapters.MySpinnerQuestionTypeAdapter;
import com.intellyze.feedback.Bus.BusFactory;
import com.intellyze.feedback.Bus.Events.RefreshEventContinueFragment;
import com.intellyze.feedback.Custom.EditTextMedium;
import com.intellyze.feedback.Models.ColumnDetailsModel;
import com.intellyze.feedback.Models.LinearQuestionDetalisModel;
import com.intellyze.feedback.Models.MatrixQuestionDetailModel;
import com.intellyze.feedback.Models.MultipleChoicesingleDetailsModel;
import com.intellyze.feedback.Models.QuestionMoreDetailsModel;
import com.intellyze.feedback.Models.RowDetailsModel;
import com.intellyze.feedback.Models.SpinnerQuestionTypeModel;
import com.intellyze.feedback.R;
import com.intellyze.feedback.Realm.QuestionMoreDetails;
import com.intellyze.feedback.Realm.SurveyDetails;
import com.intellyze.feedback.Realm.SurveyQuestionsDetails;
import com.intellyze.feedback.Realm.columnDetails;
import com.intellyze.feedback.Realm.linearquestionDetalis;
import com.intellyze.feedback.Realm.matrixquestionDetails;
import com.intellyze.feedback.Realm.multiplechoicesingleDetails;
import com.intellyze.feedback.Realm.rowDetails;
import com.intellyze.feedback.Utils.UserPref;
import com.zcw.togglebutton.ToggleButton;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

import static android.app.Activity.RESULT_OK;

/**
 * Created by INTELLYZE-202 on 12-12-2017.
 */

public class EditSurveyQuestionFragment extends BaseFragment implements View.OnClickListener {
    View child;
    SurveyQuestionsDetails surveyQuestionsDetails;
    List<EditText> allEds = new ArrayList<EditText>();
    List<EditText> allEdsRow = new ArrayList<EditText>();
    List<EditText> allEdsCol = new ArrayList<EditText>();
    List<MultipleChoicesingleDetailsModel> MultipleChoicesingleDetailsModelList = new ArrayList<MultipleChoicesingleDetailsModel>();
    MultipleChoicesingleDetailsModel multipleChoicesingleDetailsModel;
    QuestionMoreDetails qstMoreDetailsObj;
    multiplechoicesingleDetails qstMultipleChoiceObj;
    RealmList<QuestionMoreDetails> qstDetailsMoreList;
    RealmList<multiplechoicesingleDetails> qstDetailsMultipleChoiceSingleList;
    List<LinearQuestionDetalisModel> LinearQuestionDetalisModelList = new ArrayList<LinearQuestionDetalisModel>();
    LinearQuestionDetalisModel linearQuestionDetalisModel;
    List<MatrixQuestionDetailModel> matrixQuestionDetailModelsList = new ArrayList<MatrixQuestionDetailModel>();
    MatrixQuestionDetailModel matrixQuestionDetailModel;
    List<RowDetailsModel> rowDetailsModelList = new ArrayList<RowDetailsModel>();
    RowDetailsModel rowDetailsModel;
    List<ColumnDetailsModel> columnDetailsModelList = new ArrayList<ColumnDetailsModel>();
    ColumnDetailsModel columnDetailsModel;
    FrameLayout flContainer;
    ImageView imgTakeImage, imgAttached;
    EditText edtColumn1, edtCh1, edtColumn2, edtRow1, edtRow2, edt_Upper, edt_Lower;
    LinearLayout lnrView, lnrChoice;
    private static final int REQUEST_WRITE_PERMISSION = 786;
    int required = 0, questionNumber = 0, count = 0, counttv = 100, select_photo = 1;
    Uri selectedImage = Uri.parse("");
    TextView tvSave, tvSurveyName, tvCancel, tvLowerLimit, tvUpperlimit;
    Spinner spQuestionType;
    String encodedImage = "", SpinnerQuestionTypeSelected = "Short answer", PreviousAnswerType = "";
    Realm mRealm;
    UserPref userPref;
    SurveyDetails surveyDetails;
    ToggleButton toggleRequired;
    MySpinnerQuestionTypeAdapter arrayAdapter;
    List<String> options = new ArrayList<String>();
    List<String> optionsid = new ArrayList<String>();
    List<String> optionsRow = new ArrayList<String>();
    List<String> optionsCol = new ArrayList<String>();
    public static final String ARG_PAGE = "ARG_PAGE";
    private int mPageNo;
    EditText tvQuestion;
    QuestionMoreDetailsModel questionMoreDetails;

    public EditSurveyQuestionFragment() {
        // Required empty public constructor

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EditSurveyQuestionFragment newInstance(int id) {
        EditSurveyQuestionFragment fragment = new EditSurveyQuestionFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, id);
        fragment.setArguments(args);


        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNo = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_edit_question, container, false);
        userPref = new UserPref(getActivity());
        initViews(v);
        questionMoreDetails = new QuestionMoreDetailsModel();
        imgTakeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermission();
            }
        });
        mRealm = Realm.getDefaultInstance();
        ArrayList<SpinnerQuestionTypeModel> spinnerQuestionTypeModels = new ArrayList<SpinnerQuestionTypeModel>();
        spinnerQuestionTypeModels.add(new SpinnerQuestionTypeModel("Short answer", R.drawable.ic_short_answer));
        spinnerQuestionTypeModels.add(new SpinnerQuestionTypeModel("Paragraph", R.drawable.ic_paragraph));
        spinnerQuestionTypeModels.add(new SpinnerQuestionTypeModel("Multiple choice", R.drawable.ic_multiple_choice));
        spinnerQuestionTypeModels.add(new SpinnerQuestionTypeModel("Checkboxes", R.drawable.ic_check_box));
        spinnerQuestionTypeModels.add(new SpinnerQuestionTypeModel("Dropdown", R.drawable.ic_drop_down));
        spinnerQuestionTypeModels.add(new SpinnerQuestionTypeModel("Linear scale", R.drawable.ic_linear_scale));
        spinnerQuestionTypeModels.add(new SpinnerQuestionTypeModel("Multiple choice grid", R.drawable.ic_multiple_choice_grid));
//        spinnerQuestionTypeModels.add(new SpinnerQuestionTypeModel("Checkbox grid", R.drawable.ic_check_box_grid));
        arrayAdapter = new MySpinnerQuestionTypeAdapter(getActivity(), spinnerQuestionTypeModels);
        spQuestionType.setAdapter(arrayAdapter);
        spQuestionType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    LinearQuestionDetalisModelList.clear();
                    MultipleChoicesingleDetailsModelList.clear();
                    SpinnerQuestionTypeSelected = "Short answer";
                    flContainer.removeAllViews();
                    tvQuestion.setHint("Short Answer Question");
                } else if (position == 1) {
                    rowDetailsModelList.clear();
                    columnDetailsModelList.clear();
                    LinearQuestionDetalisModelList.clear();
                    MultipleChoicesingleDetailsModelList.clear();
                    SpinnerQuestionTypeSelected = "Paragraph";
                    flContainer.removeAllViews();
                    tvQuestion.setHint("Long Answer Question");
                } else if (position == 2) {
                    LinearQuestionDetalisModelList.clear();
                    SpinnerQuestionTypeSelected = "Multiple choice";
                    rowDetailsModelList.clear();
                    columnDetailsModelList.clear();
                    flContainer.removeAllViews();
                    tvQuestion.setHint("Multiple Choice Question");
                    child = getLayoutInflater().inflate(R.layout.edit_multiple_choice_question, null);
                    lnrChoice = (LinearLayout) child.findViewById(R.id.lnrChoice);
                    edtCh1 = (EditText) child.findViewById(R.id.edtCh1);
                    TextView btnad = (TextView) child.findViewById(R.id.btnad);
                    flContainer.addView(child);
                    btnad.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            LinearLayout layout = new LinearLayout(getActivity());
                            layout.setOrientation(LinearLayout.HORIZONTAL);
                            EditText et = newedittext();
                            allEds.add(et);
                            layout.addView(et);
                            ImageView iv = newImageview(getActivity());
                            iv.setTag(et.getId());
                            layout.addView(iv);
                            lnrChoice.addView(layout);
                        }
                    });
                    if (MultipleChoicesingleDetailsModelList.size() >= 1) {
                        edtCh1.setText(MultipleChoicesingleDetailsModelList.get(0).getTextChoice());
                    }
                    for (int i = 1; i < MultipleChoicesingleDetailsModelList.size(); i++) {
                        LinearLayout layout = new LinearLayout(getActivity());
                        layout.setOrientation(LinearLayout.HORIZONTAL);
                        EditText et = newedittext();
                        et.setText(MultipleChoicesingleDetailsModelList.get(i).getTextChoice());
                        allEds.add(et);
                        layout.addView(et);
                        ImageView iv = newImageview(getActivity());
                        iv.setTag(et.getId());
                        layout.addView(iv);
                        lnrChoice.addView(layout);
                    }
                } else if (position == 3) {
                    LinearQuestionDetalisModelList.clear();
                    SpinnerQuestionTypeSelected = "Checkboxes";
                    rowDetailsModelList.clear();
                    columnDetailsModelList.clear();
                    flContainer.removeAllViews();
                    tvQuestion.setHint("Multiple Choice Question");
                    child = getLayoutInflater().inflate(R.layout.edit_multiple_choice_question, null);
                    lnrChoice = (LinearLayout) child.findViewById(R.id.lnrChoice);
                    TextView btnad = (TextView) child.findViewById(R.id.btnad);
                    edtCh1 = (EditText) child.findViewById(R.id.edtCh1);
                    flContainer.addView(child);
                    btnad.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            LinearLayout layout = new LinearLayout(getActivity());
                            layout.setOrientation(LinearLayout.HORIZONTAL);
                            EditText et = newedittextCoulmn();
                            layout.addView(et);
                            allEdsCol.add(et);
                            ImageView iv = newImageview(getActivity());
                            iv.setTag(et.getId());
                            layout.addView(iv);
                            lnrChoice.addView(layout);
                        }
                    });
                    if (MultipleChoicesingleDetailsModelList.size() >= 1) {
                        edtCh1.setText(MultipleChoicesingleDetailsModelList.get(0).getTextChoice());
                    }
                    for (int i = 1; i < MultipleChoicesingleDetailsModelList.size(); i++) {
                        LinearLayout layout = new LinearLayout(getActivity());
                        layout.setOrientation(LinearLayout.HORIZONTAL);
                        EditText et = newedittext();
                        et.setText(MultipleChoicesingleDetailsModelList.get(i).getTextChoice());
                        allEds.add(et);
                        layout.addView(et);
                        ImageView iv = newImageview(getActivity());
                        iv.setTag(et.getId());
                        layout.addView(iv);
                        lnrChoice.addView(layout);

                    }
                } else if (position == 4) {
                    LinearQuestionDetalisModelList.clear();
                    SpinnerQuestionTypeSelected = "Dropdown";
                    rowDetailsModelList.clear();
                    columnDetailsModelList.clear();
                    allEds.clear();
                    flContainer.removeAllViews();
                    child = getLayoutInflater().inflate(R.layout.drop_down_questions, null);
                    tvQuestion.setHint("Dropdown Question");
                    flContainer.addView(child);
                    lnrView = (LinearLayout) child.findViewById(R.id.lnrView);
                    TextView btnad = (TextView) child.findViewById(R.id.btnad);
                    final TextView txtAnswerChoice = (TextView) child.findViewById(R.id.txtAnswerChoice);
                    btnad.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            LinearLayout layout = new LinearLayout(getActivity());
                            layout.setOrientation(LinearLayout.HORIZONTAL);
                            EditText et = newedittext();
                            txtAnswerChoice.setVisibility(View.VISIBLE);
                            allEds.add(et);
                            layout.addView(et);
                            ImageView iv = newImageview(getActivity());
                            iv.setTag(et.getId());
                            layout.addView(iv);
                            lnrView.addView(layout);
                        }
                    });
                    for (int i = 0; i < MultipleChoicesingleDetailsModelList.size(); i++) {
                        txtAnswerChoice.setVisibility(View.VISIBLE);
                        LinearLayout layout = new LinearLayout(getActivity());
                        layout.setOrientation(LinearLayout.HORIZONTAL);
                        EditText et = newedittext();
                        et.setText(MultipleChoicesingleDetailsModelList.get(i).getTextChoice());
                        allEds.add(et);
                        layout.addView(et);
                        ImageView iv = newImageview(getActivity());
                        iv.setTag(et.getId());
                        layout.addView(iv);
                        lnrView.addView(layout);

                    }
                } else if (position == 5) {
                    MultipleChoicesingleDetailsModelList.clear();
                    rowDetailsModelList.clear();
                    columnDetailsModelList.clear();
                    SpinnerQuestionTypeSelected = "Linear scale";
                    flContainer.removeAllViews();
                    child = getLayoutInflater().inflate(R.layout.linear_scale_questions, null);
                    tvQuestion.setHint("Dropdown Question");
                    edt_Lower = (EditText) child.findViewById(R.id.edt_Lower);
                    edt_Upper = (EditText) child.findViewById(R.id.edt_Upper);
                    flContainer.addView(child);
                    Spinner spinnerlower = (Spinner) child.findViewById(R.id.spinnerlower);
                    Spinner spinnerUpper = (Spinner) child.findViewById(R.id.spinnerUpper);
                    tvLowerLimit = (TextView) child.findViewById(R.id.tvLowerLimit);
                    tvUpperlimit = (TextView) child.findViewById(R.id.tvUpperlimit);

                    spinnerUpper.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            tvUpperlimit.setText(parent.getSelectedItem().toString());
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                    if (LinearQuestionDetalisModelList.size() > 0) {
                        if (LinearQuestionDetalisModelList.get(0).getEndValue() == 0) {
                            spinnerlower.setSelection(0);
                        } else {
                            spinnerlower.setSelection(1);
                        }
                        spinnerUpper.setSelection(LinearQuestionDetalisModelList.get(0).getEndValue() - 1);
                        edt_Lower.setText(LinearQuestionDetalisModelList.get(0).getStartText());
                        edt_Upper.setText(LinearQuestionDetalisModelList.get(0).getEndText());
                    }
                    spinnerlower.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            tvLowerLimit.setText(parent.getSelectedItem().toString());
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });

                } else if (position == 6) {
                    LinearQuestionDetalisModelList.clear();
                    MultipleChoicesingleDetailsModelList.clear();
                    SpinnerQuestionTypeSelected = "Multiple choice grid";
                    flContainer.removeAllViews();
                    child = getLayoutInflater().inflate(R.layout.multiple_choice_grid_radio_button, null);
                    flContainer.addView(child);
                    TextView TvAddNewColumn = (TextView) child.findViewById(R.id.TvAddNewColumn);
                    tvQuestion.setHint("Radio button Grid");
                    edtRow1 = (EditText) child.findViewById(R.id.edtRow1);
                    edtRow2 = (EditText) child.findViewById(R.id.edtRow2);
                    edtColumn1 = (EditText) child.findViewById(R.id.edtColumn1);
                    edtColumn2 = (EditText) child.findViewById(R.id.edtColumn2);
                    TextView tvAddNewRow = (TextView) child.findViewById(R.id.tvAddNewRow);
                    final LinearLayout lnrRow = (LinearLayout) child.findViewById(R.id.lnrRow);
                    final LinearLayout lnrCoulmns = (LinearLayout) child.findViewById(R.id.lnrCoulmns);
                    tvAddNewRow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            LinearLayout layout = new LinearLayout(getActivity());
                            layout.setOrientation(LinearLayout.HORIZONTAL);
                            EditText et = newedittextRow();
                            layout.addView(et);
                            allEdsRow.add(et);
                            ImageView iv = newImageview(getActivity());
                            iv.setTag(et.getId());
                            layout.addView(iv);
                            lnrRow.addView(layout);
                        }
                    });
                    TvAddNewColumn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            LinearLayout layout = new LinearLayout(getActivity());
                            layout.setOrientation(LinearLayout.HORIZONTAL);
                            EditText et = newedittextCoulmn();
                            layout.addView(et);
                            allEdsCol.add(et);
                            ImageView iv = newImageview(getActivity());
                            iv.setTag(et.getId());
                            layout.addView(iv);
                            lnrCoulmns.addView(layout);
                        }
                    });
                    for (int i = 0; i < columnDetailsModelList.size(); i++) {
                        if (i == 0 || i == 1) {
                            if (i == 0) {
                                edtColumn1.setText(columnDetailsModelList.get(0).getColumn_name());
                            } else {
                                edtColumn2.setText(columnDetailsModelList.get(1).getColumn_name());
                            }
                        } else {
                            LinearLayout layout = new LinearLayout(getActivity());
                            layout.setOrientation(LinearLayout.HORIZONTAL);
                            EditText et = newedittextCoulmn();
                            layout.addView(et);
                            allEdsCol.add(et);
                            et.setText(columnDetailsModelList.get(i).getColumn_name());
                            ImageView iv = newImageview(getActivity());
                            iv.setTag(et.getId());
                            layout.addView(iv);
                            lnrCoulmns.addView(layout);
                        }
                    }

                    for (int i = 0; i < rowDetailsModelList.size(); i++) {
                        if (i == 0 || i == 1) {
                            if (i == 0) {
                                edtRow1.setText(rowDetailsModelList.get(0).getRow_name());
                            } else {
                                edtRow2.setText(rowDetailsModelList.get(1).getRow_name());
                            }
                        } else {
                            LinearLayout layout = new LinearLayout(getActivity());
                            layout.setOrientation(LinearLayout.HORIZONTAL);
                            EditText et = newedittextRow();
                            layout.addView(et);
                            allEdsRow.add(et);
                            et.setText(rowDetailsModelList.get(i).getRow_name());
                            ImageView iv = newImageview(getActivity());
                            iv.setTag(et.getId());
                            layout.addView(iv);
                            lnrRow.addView(layout);
                        }
                    }
                } else if (position == 7) {
                    LinearQuestionDetalisModelList.clear();
                    MultipleChoicesingleDetailsModelList.clear();
                    SpinnerQuestionTypeSelected = "Checkbox grid";
                    flContainer.removeAllViews();
                    child = getLayoutInflater().inflate(R.layout.multiple_choice_grid_checkbox, null);
                    flContainer.addView(child);
                    tvQuestion.setHint("Checkbox Grid Question");
                    TextView TvAddNewColumn = (TextView) child.findViewById(R.id.TvAddNewColumn);
                    TextView tvAddNewRow = (TextView) child.findViewById(R.id.tvAddNewRow);
                    final LinearLayout lnrRow = (LinearLayout) child.findViewById(R.id.lnrRow);
                    final LinearLayout lnrCoulmns = (LinearLayout) child.findViewById(R.id.lnrCoulmns);
                    tvAddNewRow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            LinearLayout layout = new LinearLayout(getActivity());
                            layout.setOrientation(LinearLayout.HORIZONTAL);
                            EditText et = newedittextRow();
                            layout.addView(et);
                            ImageView iv = newImageview(getActivity());
                            iv.setTag(et.getId());
                            layout.addView(iv);
                            lnrRow.addView(layout);
                        }
                    });
                    TvAddNewColumn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            LinearLayout layout = new LinearLayout(getActivity());
                            layout.setOrientation(LinearLayout.HORIZONTAL);
                            EditText et = newedittextCoulmn();
                            layout.addView(et);
                            ImageView iv = newImageview(getActivity());
                            iv.setTag(et.getId());
                            layout.addView(iv);
                            lnrCoulmns.addView(layout);
                        }
                    });
                }
            } // to close the onItemSelected

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mRealm = Realm.getDefaultInstance();
        surveyDetails = mRealm.where(SurveyDetails.class)
                .equalTo("SurveyId", userPref.getSurveyId()).findFirst();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (surveyDetails != null) {
                        for (int i = 0; i < surveyDetails.getSurveyQuestionsDetails().size(); i++) {
                            if (surveyDetails.getSurveyQuestionsDetails().get(i).getQuestion_Id() == mPageNo) {
                                Toast.makeText(getActivity(), "" + surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionType(), Toast.LENGTH_SHORT).show();
                                switch (surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionType()) {
                                    case "Short answer":
                                        initializeViewz(0, "Short answer", surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionName(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionRequired(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionNumber());
                                        break;
                                    case "Paragraph":
                                        initializeViewz(1, "Paragraph", surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionName(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionRequired(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionNumber());
                                        break;
                                    case "Multiple choice":
                                        initializeViewz(2, "Multiple choice", surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionName(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionRequired(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionNumber());
                                        child = getLayoutInflater().inflate(R.layout.edit_multiple_choice_question, null);
                                        int sizee = surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMultiplechoicesingleDetails().size();
                                        for (int m = 0; m < sizee; m++) {
                                            multipleChoicesingleDetailsModel = new MultipleChoicesingleDetailsModel();
                                            multipleChoicesingleDetailsModel.setTextChoice(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMultiplechoicesingleDetails().get(m).getTextChoice());
                                            MultipleChoicesingleDetailsModelList.add(multipleChoicesingleDetailsModel);
                                        }
                                        flContainer.addView(child);
                                        break;
                                    case "Checkboxes":
                                        initializeViewz(3, "Checkboxes", surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionName(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionRequired(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionNumber());
                                        int sizecheck = surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMultiplechoicesingleDetails().size();
                                        for (int m = 0; m < sizecheck; m++) {
                                            multipleChoicesingleDetailsModel = new MultipleChoicesingleDetailsModel();
                                            multipleChoicesingleDetailsModel.setTextChoice(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMultiplechoicesingleDetails().get(m).getTextChoice());
                                            MultipleChoicesingleDetailsModelList.add(multipleChoicesingleDetailsModel);
                                        }
                                        break;
                                    case "Dropdown":


                                        initializeViewz(4, "Dropdown", surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionName(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionRequired(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionNumber());
                                        int sizedrop = surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMultiplechoicesingleDetails().size();
                                        for (int m = 0; m < sizedrop; m++) {
                                            multipleChoicesingleDetailsModel = new MultipleChoicesingleDetailsModel();
                                            multipleChoicesingleDetailsModel.setTextChoice(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMultiplechoicesingleDetails().get(m).getTextChoice());
                                            multipleChoicesingleDetailsModel.setTextChoice_id(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMultiplechoicesingleDetails().get(m).getQuestions_options_Id());
                                            MultipleChoicesingleDetailsModelList.add(multipleChoicesingleDetailsModel);
                                        }
                                        break;
                                    case "Linear scale":
                                        initializeViewz(5, "Linear scale", surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionName(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionRequired(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionNumber());
                                        linearQuestionDetalisModel = new LinearQuestionDetalisModel();
                                        linearQuestionDetalisModel.setEndText(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getLinearquestionDetalis().get(0).getEndText());
                                        linearQuestionDetalisModel.setStartText(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getLinearquestionDetalis().get(0).getStartText());
                                        linearQuestionDetalisModel.setEndValue(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getLinearquestionDetalis().get(0).getEndValue());
                                        linearQuestionDetalisModel.setStartValue(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getLinearquestionDetalis().get(0).getStartValue());
                                        LinearQuestionDetalisModelList.add(linearQuestionDetalisModel);
                                        break;
                                    case "Multiple choice grid":
                                        initializeViewz(6, "Multiple choice grid", surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionName(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionRequired(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionNumber());

                                        for (int l = 0; l < surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMatrixquestionDetails().get(0).getRowDetails().size(); l++) {
                                            rowDetailsModel = new RowDetailsModel();
                                            rowDetailsModel.setRow_name(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMatrixquestionDetails().get(0).getRowDetails().get(l).getRow_name());
                                            rowDetailsModelList.add(rowDetailsModel);
                                        }

                                        for (int l = 0; l < surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMatrixquestionDetails().get(0).getColumnDetails().size(); l++) {
                                            columnDetailsModel = new ColumnDetailsModel();
                                            columnDetailsModel.setColumn_name(surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionMoreDetails().get(0).getMatrixquestionDetails().get(0).getColumnDetails().get(l).getColumn_name());
                                            columnDetailsModelList.add(columnDetailsModel);
                                        }
                                        matrixQuestionDetailModel.setColumnDetails(columnDetailsModelList);
                                        matrixQuestionDetailModel.setRowDetails(rowDetailsModelList);
                                        matrixQuestionDetailModelsList.add(matrixQuestionDetailModel);
                                        break;
                                    case "Checkbox grid":
                                        initializeViewz(7, "Checkbox grid", surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionName(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionRequired(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionImage(), surveyDetails.getSurveyQuestionsDetails().get(i).getQuestionNumber());
                                        break;
                                }
                            }
                        }
                    }
                }
            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }
        return v;
    }

    private void initializeViewz(int pos, String questType, String questionName, int questionRequired, String questionImage, int questionNo) {
        spQuestionType.setSelection(pos);
        SpinnerQuestionTypeSelected = questType;
        PreviousAnswerType = questType;
        required = questionRequired;

        questionNumber = questionNo;
        encodedImage = questionImage;
        tvQuestion.setText(questionName);
        if (required == 1) {
            toggleRequired.setToggleOn();
        } else {
            toggleRequired.setToggleOff();
        }
        if (encodedImage.equals("")) {
            encodedImage = "";
            imgAttached.setVisibility(View.GONE);
        } else {
            imgAttached.setVisibility(View.VISIBLE);
            Bitmap bb = decodeImage(encodedImage);
            imgAttached.setImageBitmap(bb);
        }

        questionMoreDetails.setQuestion(questionName);

    }

    private EditText newedittext() {
        final EditTextMedium editText = new EditTextMedium(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        params.setMargins(0, 30, 0, 0);
        editText.setLayoutParams(params);
        counttv = counttv + 1;
        editText.setId(counttv);
        editText.setTag("Count" + counttv);
        editText.setTextSize(14);
        editText.setTextColor(Color.rgb(0, 0, 0));
        editText.setMaxEms(2);
        editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        return editText;
    }

    private EditText newedittextRow() {
        final EditTextMedium editText = new EditTextMedium(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        params.setMargins(0, 30, 0, 0);
        editText.setLayoutParams(params);
        editText.setHint("Enter Row ");
        counttv = counttv + 1;
        editText.setId(counttv);
        editText.setTag("Count" + counttv);
        editText.setTextSize(14);
        editText.setTextColor(Color.rgb(0, 0, 0));
        editText.setMaxEms(2);
        editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        return editText;
    }

    private EditText newedittextCoulmn() {
        final EditTextMedium editText = new EditTextMedium(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        params.setMargins(0, 30, 0, 0);
        editText.setLayoutParams(params);
        editText.setHint("Enter Coulmn ");
        counttv = counttv + 1;
        editText.setId(counttv);
        editText.setTag("Count" + counttv);
        editText.setTextSize(14);
        editText.setTextColor(Color.rgb(0, 0, 0));
        editText.setMaxEms(2);
        editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        return editText;
    }

    public ImageView newImageview(Context context) {
        final ImageView imgView = new ImageView(context);
        imgView.setOnClickListener(this);
        imgView.setId(count++);
        imgView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_close_red));
        return imgView;
    }


    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        } else {
            openFilePicker();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openFilePicker();
        }
    }

    private void openFilePicker() {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, select_photo);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == select_photo && resultCode == RESULT_OK && null != data) {
            selectedImage = data.getData();
            final InputStream imageStream;
            try {
                imageStream = getActivity().getContentResolver().openInputStream(selectedImage);
                Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                selectedImage = getResizedBitmap(selectedImage, 150, 150);
                encodedImage = encodeImage(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            imgAttached.setImageURI(selectedImage);
            imgAttached.setVisibility(View.VISIBLE);
            imgAttached.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedImage = Uri.parse("");

                }
            });
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int bitmapWidth, int bitmapHeight) {
        return Bitmap.createScaledBitmap(image, bitmapWidth, bitmapHeight, true);
    }

    private void initViews(View v) {
        imgTakeImage = (ImageView) v.findViewById(R.id.imgTakeImage);
        tvQuestion = (EditText) v.findViewById(R.id.tvQuestion);
        imgAttached = (ImageView) v.findViewById(R.id.imgAttached);
        toggleRequired = (ToggleButton) v.findViewById(R.id.toggleRequired);
        toggleRequired.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on) {
                    required = 1;
                } else {
                    required = 0;
                }
            }
        });
        flContainer = (FrameLayout) v.findViewById(R.id.flContainer);
        tvCancel = (TextView) v.findViewById(R.id.tvCancel);
        tvSurveyName = (TextView) v.findViewById(R.id.tvSurveyName);
        tvSave = (TextView) v.findViewById(R.id.tvSave);
        spQuestionType = (Spinner) v.findViewById(R.id.spQuestionType);
        tvSurveyName.setText(userPref.getSurveyName());
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                BusFactory.getBus().post(new RefreshEventContinueFragment());
            }
        });
        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                if (tvQuestion.getText().toString().trim().equals("")) {
                    tvQuestion.setError(getActivity().getResources().getString(R.string.cant_be_blank));
                } else {
                    switch (spQuestionType.getSelectedItemPosition()) {
                        case 0:
                            setValueToRealmFillAnswerQuestion(tvQuestion.getText().toString().trim());
                            break;
                        case 1:
                            setValueToRealmFillAnswerQuestion(tvQuestion.getText().toString().trim());
                            break;
                        case 2:
                            if (!edtCh1.getText().toString().trim().equals("")) {
                                options.add(edtCh1.getText().toString().trim());
                            }
                            for (int i = 0; i < allEds.size(); i++) {
                                EditText editText = allEds.get(i);
                                if (!editText.getText().toString().trim().equals("")) {
                                    options.add(editText.getText().toString().trim());
                                }
                            }
                            if (options.size() >= 2) {
                                setValueToRealmMultipleChoice("radio", tvQuestion.getText().toString().trim());

                            } else {
                                Toast.makeText(getActivity(), "Must contain atleast 2 options", Toast.LENGTH_SHORT).show();
                            }

                            break;
                        case 3:
                            if (!edtCh1.getText().toString().trim().equals("")) {
                                options.add(edtCh1.getText().toString().trim());
                            }
                            for (int i = 0; i < allEds.size(); i++) {
                                EditText editText = allEds.get(i);
                                if (!editText.getText().toString().trim().equals("")) {
                                    options.add(editText.getText().toString().trim());
                                }
                            }
                            if (options.size() >= 2) {
                                setValueToRealmMultipleChoice("checkbox", tvQuestion.getText().toString().trim());

                            } else {
                                Toast.makeText(getActivity(), "Must contain atleast 2 options", Toast.LENGTH_SHORT).show();
                            }
                            break;
                        case 4:
                            for (int i = 0; i < allEds.size(); i++) {
                                EditText editText = allEds.get(i);
                                if (!editText.getText().toString().trim().equals("")) {
                                    options.add(editText.getText().toString().trim());
                                }
                            }
                            if (options.size() >= 2) {
                                setValueToRealmMultipleChoice("Dropdown", tvQuestion.getText().toString().trim());
                            } else {
                                Toast.makeText(getActivity(), "Must contain atleast 2 options", Toast.LENGTH_SHORT).show();
                            }
                            break;
                        case 5:
                            if (tvLowerLimit.getText().toString().trim() != tvUpperlimit.getText().toString().trim()) {
                                setValueToLinearScale(tvLowerLimit.getText().toString().trim(), tvUpperlimit.getText().toString().trim(), edt_Upper.getText().toString().trim(), edt_Lower.getText().toString().trim());
                            } else {
                                Toast.makeText(getActivity(), "Must contain atleast 2 options", Toast.LENGTH_SHORT).show();
                            }
                            break;
                        case 6:
                            if (!edtRow1.getText().toString().trim().equals("")) {
                                optionsRow.add(edtRow1.getText().toString().trim());
                            }
                            if (!edtRow2.getText().toString().trim().equals("")) {
                                optionsRow.add(edtRow2.getText().toString().trim());
                            }
                            if (!edtColumn1.getText().toString().trim().equals("")) {
                                optionsCol.add(edtColumn1.getText().toString().trim());
                            }
                            if (!edtColumn2.getText().toString().trim().equals("")) {
                                optionsCol.add(edtColumn2.getText().toString().trim());
                            }
                            for (int i = 0; i < allEdsRow.size(); i++) {
                                EditText editText = allEdsRow.get(i);
                                if (!editText.getText().toString().trim().equals("")) {
                                    optionsRow.add(editText.getText().toString().trim());
                                }
                            }
                            for (int i = 0; i < allEdsCol.size(); i++) {
                                EditText editText = allEdsCol.get(i);
                                if (!editText.getText().toString().trim().equals("")) {
                                    optionsCol.add(editText.getText().toString().trim());
                                }
                            }
                            setValueMultipleGridRadio(optionsCol, optionsRow, tvQuestion.getText().toString().trim());
                            break;
                    }
                }
            }
        });
    }

    private void setValueToRealmMultipleChoice(final String choiceType, final String question) {
        if (options.size() >= 2) {
            mRealm = Realm.getDefaultInstance();
            surveyDetails = mRealm.where(SurveyDetails.class)
                    .equalTo("SurveyId", userPref.getSurveyId()).findFirst();
            try {
                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        if (surveyDetails != null) {




                            RealmResults<SurveyQuestionsDetails> result = realm.where(SurveyQuestionsDetails.class).equalTo("Question_Id", mPageNo).findAll();
                            result.deleteAllFromRealm();




                            surveyQuestionsDetails = mRealm.copyToRealm(new SurveyQuestionsDetails());
                            Number maxId = mRealm.where(SurveyQuestionsDetails.class).max("Question_Id");
                            int nextId = (maxId == null) ? 1 : maxId.intValue() + 1;
                            surveyQuestionsDetails.setQuestion_Id(mPageNo);
                              surveyQuestionsDetails.setAdded_flag("false");

                            surveyQuestionsDetails.setQuestionImage(encodedImage);
                            surveyQuestionsDetails.setQuestionName(question);
                            surveyQuestionsDetails.setQuestionNumber(questionNumber);
                            surveyQuestionsDetails.setQuestionRequired(required);
                            surveyQuestionsDetails.setQuestionType(SpinnerQuestionTypeSelected);
                            qstMoreDetailsObj = mRealm.copyToRealm(new QuestionMoreDetails());
                            qstMoreDetailsObj.setChoiceType(choiceType);
                            qstDetailsMoreList = new RealmList<QuestionMoreDetails>();
                            qstDetailsMultipleChoiceSingleList = new RealmList<multiplechoicesingleDetails>();
                            for (int o = 0; o < options.size(); o++) {
                                qstMultipleChoiceObj = mRealm.copyToRealm(new multiplechoicesingleDetails());
                                qstMultipleChoiceObj.setQuestions_options_Id(0);
                                qstMultipleChoiceObj.setTextChoice(options.get(o));
                                qstDetailsMultipleChoiceSingleList.add(qstMultipleChoiceObj);
                            }
                            qstMoreDetailsObj.setMultiplechoicesingleDetails(qstDetailsMultipleChoiceSingleList);
                            qstDetailsMoreList.add(qstMoreDetailsObj);
                            surveyQuestionsDetails.setQuestionMoreDetails(qstDetailsMoreList);
                            surveyDetails.getSurveyQuestionsDetails().add(surveyQuestionsDetails);
                            mRealm.copyToRealm(surveyDetails);






                            Toast.makeText(getActivity(), "" + "Updated successfully!", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), "Something went wrong, please try again later!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } catch (Exception em) {
                em.printStackTrace();
            } finally {
                if (mRealm != null) {
                    mRealm.close();
                    BusFactory.getBus().post(new RefreshEventContinueFragment());
                }
            }
        }
    }

    private void setValueMultipleGridRadio(final List<String> optionsCol, final List<String> optionsRow, final String question) {
        if (optionsCol.size() >= 2 && optionsRow.size() >= 1) {
            mRealm = Realm.getDefaultInstance();
            surveyDetails = mRealm.where(SurveyDetails.class)
                    .equalTo("SurveyId", userPref.getSurveyId()).findFirst();
            try {
                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        if (surveyDetails != null) {

                            RealmResults<SurveyQuestionsDetails> result = realm.where(SurveyQuestionsDetails.class).equalTo("Question_Id", mPageNo).findAll();
                            result.deleteAllFromRealm();


                            surveyQuestionsDetails = new SurveyQuestionsDetails();
                            Number maxId = mRealm.where(SurveyQuestionsDetails.class).max("Question_Id");
                            int nextId = (maxId == null) ? 1 : maxId.intValue() + 1;
                         surveyQuestionsDetails.setQuestion_Id(mPageNo);
                           surveyQuestionsDetails.setAdded_flag("false");
                            surveyQuestionsDetails.setQuestionImage(encodedImage);
                            surveyQuestionsDetails.setQuestionName(question);
                            surveyQuestionsDetails.setQuestionNumber(questionNumber);
                            surveyQuestionsDetails.setQuestionRequired(required);
                            surveyQuestionsDetails.setQuestionType(SpinnerQuestionTypeSelected);
                            RealmList<QuestionMoreDetails> questionMoreDetailsRealmlist = new RealmList<QuestionMoreDetails>();
                            QuestionMoreDetails questionMoreDetails = new QuestionMoreDetails();
                            questionMoreDetails.setChoiceType("radio");
                            RealmList<matrixquestionDetails> matrixquestionDetailsrealmlist = new RealmList<matrixquestionDetails>();
                            matrixquestionDetails m = new matrixquestionDetails();
                            RealmList<rowDetails> rowDetailsrealmlist = new RealmList<rowDetails>();
                            for (int o = 0; o < optionsRow.size(); o++) {
                                rowDetails rowDetails = new rowDetails();
                                rowDetails.setQmultiple_grid_row_Id(0);
                                rowDetails.setRow_name(optionsRow.get(o));
                                rowDetailsrealmlist.add(rowDetails);
                            }
                            RealmList<columnDetails> columnDetailsrealmlist = new RealmList<columnDetails>();
                            for (int o = 0; o < optionsCol.size(); o++) {
                                columnDetails columnDetail = new columnDetails();
                                columnDetail.setQmultiple_grid_col_Id(0);
                                columnDetail.setColumn_name(optionsCol.get(o));
                                columnDetailsrealmlist.add(columnDetail);
                            }
                            m.setColumnDetails(columnDetailsrealmlist);
                            m.setRowDetails(rowDetailsrealmlist);
                            matrixquestionDetailsrealmlist.add(m);
                            questionMoreDetails.setMatrixquestionDetails(matrixquestionDetailsrealmlist);
                            questionMoreDetailsRealmlist.add(questionMoreDetails);
                            surveyQuestionsDetails.setQuestionMoreDetails(questionMoreDetailsRealmlist);
                            surveyDetails.getSurveyQuestionsDetails().add(surveyQuestionsDetails);
                            mRealm.copyToRealm(surveyDetails);







                            Toast.makeText(getActivity(), "" + "Updated successfully!", Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(getActivity(), "Something went wrong, please try again later!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } catch (Exception em) {
                em.printStackTrace();
            } finally {
                if (mRealm != null) {
                    mRealm.close();
                    BusFactory.getBus().post(new RefreshEventContinueFragment());
                }
            }
        }
    }

    private void setValueToLinearScale(final String lowerlimit, final String upperlimit, final String uppertext, final String lowertext) {
        mRealm = Realm.getDefaultInstance();
        surveyDetails = mRealm.where(SurveyDetails.class)
                .equalTo("SurveyId", userPref.getSurveyId()).findFirst();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (surveyDetails != null) {
                        RealmResults<SurveyQuestionsDetails> result = realm.where(SurveyQuestionsDetails.class).equalTo("Question_Id", mPageNo).findAll();
                        result.deleteAllFromRealm();


                        surveyQuestionsDetails = new SurveyQuestionsDetails();
                        Number maxId = mRealm.where(SurveyQuestionsDetails.class).max("Question_Id");
                        int nextId = (maxId == null) ? 1 : maxId.intValue() + 1;
                     surveyQuestionsDetails.setQuestion_Id(mPageNo);
                     surveyQuestionsDetails.setAdded_flag("false");
                        surveyQuestionsDetails.setQuestionImage(encodedImage);
                        surveyQuestionsDetails.setQuestionName(tvQuestion.getText().toString().trim());
                        surveyQuestionsDetails.setQuestionNumber(questionNumber);
                        surveyQuestionsDetails.setQuestionRequired(required);
                        surveyQuestionsDetails.setQuestionType(SpinnerQuestionTypeSelected);
                        RealmList<QuestionMoreDetails> questionMoreDetailsRealmlist = new RealmList<QuestionMoreDetails>();
                        QuestionMoreDetails questionMoreDetails = new QuestionMoreDetails();
                        questionMoreDetails.setChoiceType("linear");
                        RealmList<linearquestionDetalis> linearquestionDetalisrealmlist = new RealmList<linearquestionDetalis>();
                        linearquestionDetalis linearquestionDetalis = new linearquestionDetalis();
                        linearquestionDetalis.setEndText(uppertext);
                        linearquestionDetalis.setStartText(lowertext);
                        linearquestionDetalis.setEndValue(Integer.parseInt(upperlimit));
                        linearquestionDetalis.setStartValue(Integer.parseInt(lowerlimit));
                        linearquestionDetalisrealmlist.add(linearquestionDetalis);
                        questionMoreDetails.setLinearquestionDetalis(linearquestionDetalisrealmlist);
                        questionMoreDetailsRealmlist.add(questionMoreDetails);
                        surveyQuestionsDetails.setQuestionMoreDetails(questionMoreDetailsRealmlist);
                        surveyDetails.getSurveyQuestionsDetails().add(surveyQuestionsDetails);
                        mRealm.copyToRealm(surveyDetails);


                        Toast.makeText(getActivity(), "" + "Updated successfully!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "Something went wrong, please try again later!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
                BusFactory.getBus().post(new RefreshEventContinueFragment());
            }
        }
    }

    private void setValueToRealmFillAnswerQuestion(final String question) {
        mRealm = Realm.getDefaultInstance();
        surveyDetails = mRealm.where(SurveyDetails.class)
                .equalTo("SurveyId", userPref.getSurveyId()).findFirst();
        try {
                mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (surveyDetails != null) {
                        RealmResults<SurveyQuestionsDetails> result = mRealm.where(SurveyQuestionsDetails.class).equalTo("Question_Id", mPageNo).findAll();
                        result.deleteAllFromRealm();

                        surveyQuestionsDetails = new SurveyQuestionsDetails();
                        Number maxId = mRealm.where(SurveyQuestionsDetails.class).max("Question_Id");
                        int nextId = (maxId == null) ? 1 : maxId.intValue() + 1;
                     surveyQuestionsDetails.setQuestion_Id(mPageNo);
                           surveyQuestionsDetails.setAdded_flag("false");
                        surveyQuestionsDetails.setQuestionImage(encodedImage);
                        surveyQuestionsDetails.setQuestionName(question);
                        surveyQuestionsDetails.setQuestionNumber(questionNumber);
                        surveyQuestionsDetails.setQuestionRequired(required);
                        surveyQuestionsDetails.setQuestionType(SpinnerQuestionTypeSelected);
                        surveyDetails.getSurveyQuestionsDetails().add(surveyQuestionsDetails);
                        mRealm.copyToRealm(surveyDetails);




                        tvQuestion.setText("");
                        Toast.makeText(getActivity(), "" + "Updated successfully!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "Something went wrong, please try again later!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
                BusFactory.getBus().post(new RefreshEventContinueFragment());
            }
        }
    }

    @Override
    public void onClick(View v) {

        Object editTextId = v.getTag();
        if (editTextId == null) {
            return;
        }
        ViewParent parent = v.getParent();
        if (parent instanceof ViewGroup) {
            EditText et = ((ViewGroup) parent).findViewById((Integer) editTextId);
            Object s = et.getTag();
            for (int i = 0; i < allEds.size(); i++) {
                EditText J = allEds.get(i);
                Object k = J.getTag();
                if (s.equals(k)) {
                    allEds.remove(i);
                }
            }
            for (int i = 0; i < allEdsCol.size(); i++) {
                EditText J = allEdsCol.get(i);
                Object k = J.getTag();
                if (s.equals(k)) {
                    allEdsCol.remove(i);
                }
            }
            for (int i = 0; i < allEdsRow.size(); i++) {
                EditText J = allEdsRow.get(i);
                Object k = J.getTag();
                if (s.equals(k)) {
                    allEdsRow.remove(i);
                }
            }
            ((ViewGroup) parent).removeView(et);
            ((ViewGroup) parent).removeView(v);

        }
    }
}