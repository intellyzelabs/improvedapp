package com.intellyze.feedback.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.intellyze.feedback.Adapters.SurveyListAdapter;
import com.intellyze.feedback.Adapters.SurveyListAdapterUserSide;
import com.intellyze.feedback.Api.AddAnswerMultipleSurveys.AddMultplieAnswerSurveyRequestModel;
import com.intellyze.feedback.Api.AddAnswerMultipleSurveys.Answer;
import com.intellyze.feedback.Api.AddAnswerMultipleSurveys.HttpRequestForAddMultipleAnswerSurvey;
import com.intellyze.feedback.Api.AddAnswerMultipleSurveys.OnHttpRequestForAddMultipleAnswerSurvey;
import com.intellyze.feedback.Api.AddMultpileSurveys.AddMultipleSurveyRequestModel;
import com.intellyze.feedback.Api.AddMultpileSurveys.DeleteSurveryParameters;
import com.intellyze.feedback.Api.AddMultpileSurveys.HttpRequestForAddMultipleSurvey;
import com.intellyze.feedback.Api.AddMultpileSurveys.HttpRequestForSurveryDelete;
import com.intellyze.feedback.Api.AddMultpileSurveys.OnHttpRequestAddMultipleSurvey;
import com.intellyze.feedback.Api.AddMultpileSurveys.OnHttpSurveryDeleteResponce;
import com.intellyze.feedback.Api.AddMultpileSurveys.Survey;
import com.intellyze.feedback.Api.FeedbackSurveyQuestions.FeedbackQuestionRequestModel;
import com.intellyze.feedback.Api.FeedbackSurveyQuestions.HttpRequestSurveyQuestion;
import com.intellyze.feedback.Api.FeedbackSurveyQuestions.OnHttpRequestSurveyQuestion;
import com.intellyze.feedback.Api.FeedbackSurveyQuestions.SurveyDatum;
import com.intellyze.feedback.Bus.BusFactory;
import com.intellyze.feedback.Bus.Events.BackPressRefreshLocalData;
import com.intellyze.feedback.Bus.Events.NavigateStartSurveyUserFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateSurveyFirstFragmentEvent;
import com.intellyze.feedback.Bus.Events.NavigateSurveyResultFragmentEvent;
import com.intellyze.feedback.Bus.Events.RefreshListFeedback;
import com.intellyze.feedback.Bus.Events.SyncDataWithServer;
import com.intellyze.feedback.Custom.TextViewSemiBold;
import com.intellyze.feedback.Interfaces.SurveyItemClick;
import com.intellyze.feedback.Interfaces.SurveyQuestionUserItemClick;
import com.intellyze.feedback.Models.SurveyDetailsModel;
import com.intellyze.feedback.Models.SurveyDetailsUserFeedbackQuestionModel;
import com.intellyze.feedback.R;
import com.intellyze.feedback.Realm.AddAnswerMaster;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.Column;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.FeedQuestUserSide;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.Linear;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.MultipleChoice;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.MultipleChoiceGrid;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.QuestionMore;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.Row;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.SurveyQuestion;
import com.intellyze.feedback.Realm.QuestionMoreDetails;
import com.intellyze.feedback.Realm.SurveyDetails;
import com.intellyze.feedback.Realm.SurveyQuestionsDetails;
import com.intellyze.feedback.Realm.columnDetails;
import com.intellyze.feedback.Realm.linearquestionDetalis;
import com.intellyze.feedback.Realm.matrixquestionDetails;
import com.intellyze.feedback.Realm.multiplechoicesingleDetails;
import com.intellyze.feedback.Realm.rowDetails;
import com.intellyze.feedback.Utils.UserPref;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by INTELLYZE-202 on 30-11-2017.
 */

public class HomeFragment extends BaseFragment implements SurveyItemClick, SurveyQuestionUserItemClick {
    private Realm mRealm;
    private RealmResults<SurveyDetails> surveyDetailss;
    private RealmResults<SurveyDetails> dataOfflineToOnline;
    TextViewSemiBold errormessage;

    private RealmResults<SurveyQuestionsDetails> surveyQuestionsDetails;
    private RealmResults<AddAnswerMaster> addAnswerMaster;
    private SurveyDetails surveyDetailsdelete;
    private List<SurveyDetailsModel> surveyDetailsModels;
    private List<SurveyDetailsUserFeedbackQuestionModel> surveyDetailsUserFeedbackQuestionModels;
    private List<FeedQuestUserSide> feedQuestUserSides;
    private List<Survey> mSurveysList;
    private List<Answer> answerListObj;

    private RealmList<SurveyQuestion> surveyQuestions;
    private RealmList<SurveyQuestionsDetails> surveyQuestionsdetai;
    private RealmList<MultipleChoice> multipleChoices;
    private RealmList<multiplechoicesingleDetails> multipleChoicesdetails;
    private RealmList<columnDetails> columnsdetails;
    private RealmList<Column> columns;
    private RealmList<rowDetails> rowsdetils;
    private RealmList<Row> rows;
    private RealmList<MultipleChoiceGrid> multipleChoiceGrids;
    private RealmList<matrixquestionDetails> multipleChoiceGridsdetails;
    private RealmList<Linear> linears;
    private RealmList<linearquestionDetalis> linearsdetails;

    private AddMultplieAnswerSurveyRequestModel addMultplieAnswerSurveyRequestModel;
    private Survey mSurvey;
    private Answer answerModel;
    private SurveyDetails surveySingleDetails;
    private FeedQuestUserSide sdf;
    private MultipleChoiceGrid multipleChoiceGrid;
    private AddMultipleSurveyRequestModel addMultipleSurveyRequestModel;
    private UserPref userPref;

    private TextView tv_BusinessName, tv_BusinessGst;
    private ImageView imgProfile;
    private LinearLayout lnr_createSurvey, lnrEmptyScreen;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView rv_SurveyList, rv_SurveyListQuestion;
    private Button btnCreateSurvey;


    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        initViews(v);
        ButterKnife.bind(getActivity());
        setProgressBar();
        mRealm = Realm.getDefaultInstance();
        userPref = new UserPref(getActivity());
        tv_BusinessName.setText(userPref.getNameOfUser());
        tv_BusinessGst.setText("GST : " + userPref.getGst());
//        if (userPref.getRole().equals("S_Admin")) {
//
//            lnr_createSurvey.setVisibility(View.VISIBLE);
//            if (isConnectedToNet(getActivity())) {
//                BusFactory.getBus().post(new SyncDataWithServer());
//            }
//        }
//        else
//        {
//            getSurveyFeedBackUserist();
//        }

        if (isConnectedToNet(getActivity())) {
            BusFactory.getBus().post(new SyncDataWithServer());
        }
        else
        {
            if (userPref.getRole().equals("S_Admin")) {
                lnr_createSurvey.setVisibility(View.VISIBLE);
            getFeedbackQuestionListFromLocal();
        } else {
            getSurveyFeedBackUserist();
        }

            }

        if (userPref.getUserImage().equals("")) {
        } else {
            Bitmap image = decodeImage(userPref.getUserImage());
            imgProfile.setImageBitmap(image);
        }

//        if (userPref.getRole().equals("S_Admin")) {
//            lnr_createSurvey.setVisibility(View.VISIBLE);
//            getFeedbackQuestionListFromLocal();
//        } else {
//            getSurveyFeedBackUserist();
//        }



        btnCreateSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    BusFactory.getBus().post(new NavigateSurveyFirstFragmentEvent());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        return v;
    }




    /* Method to FETCH Admin side survey list from Local database */
    private void getFeedbackQuestionListFromLocal() {
        surveyDetailsModels = new ArrayList<SurveyDetailsModel>();
        mRealm = Realm.getDefaultInstance();
        surveyDetailss = mRealm.where(SurveyDetails.class).findAll();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    for (int i = 0; i < surveyDetailss.size(); i++) {
                        SurveyDetailsModel surveyQuestionsDetails = new SurveyDetailsModel();
                        surveyQuestionsDetails.setSurveyImage(surveyDetailss.get(i).getSurveyImage());
                        surveyQuestionsDetails.setSurveyDate(surveyDetailss.get(i).getSurveyDate());
                        surveyQuestionsDetails.setSurveyCompleted(surveyDetailss.get(i).getSurveyComplete());
                        surveyQuestionsDetails.setCount((surveyDetailss.get(i).getCount()));
                        surveyQuestionsDetails.setSurveyTime((surveyDetailss.get(i).getSurveyTime()));
                        surveyQuestionsDetails.setLastEntryDate((surveyDetailss.get(i).getLastEntryDate()));
                        surveyQuestionsDetails.setLastEntryTime((surveyDetailss.get(i).getLastEntryTime()));
                        surveyQuestionsDetails.setSurveyDescription(surveyDetailss.get(i).getSurveyDescription());
                        surveyQuestionsDetails.setSurveyId(surveyDetailss.get(i).getSourveyId());
                        surveyQuestionsDetails.setSurveyName(surveyDetailss.get(i).getSurveyName());
                        surveyQuestionsDetails.setSurveyStatus(surveyDetailss.get(i).getSurveyStatus());
                        surveyQuestionsDetails.setSurveyTime(surveyDetailss.get(i).getSurveyTime());
                        surveyDetailsModels.add(surveyQuestionsDetails);
                    }
                }
            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }
        if (surveyDetailsModels.size() == 0) {



            SurveyListAdapter surveyListAdapter = new SurveyListAdapter(getActivity(), this, surveyDetailsModels);
            rv_SurveyList.setAdapter(surveyListAdapter);
            surveyListAdapter.notifyDataSetChanged();
            lnrEmptyScreen.setVisibility(View.VISIBLE);
            rv_SurveyList.setVisibility(View.GONE);
            lnr_createSurvey.setVisibility(View.VISIBLE);
        } else {
            rv_SurveyList.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(getActivity());
            rv_SurveyList.setLayoutManager(mLayoutManager);
            SurveyListAdapter surveyListAdapter = new SurveyListAdapter(getActivity(), this, surveyDetailsModels);
            rv_SurveyList.setAdapter(surveyListAdapter);
            surveyListAdapter.notifyDataSetChanged();
            lnr_createSurvey.setVisibility(View.VISIBLE);
            rv_SurveyList.setVisibility(View.VISIBLE);
            rv_SurveyListQuestion.setVisibility(View.GONE);

        }
    }

    /* Method to FETCH User Side survey details list from server to local database */
    private void getFeedbackQuestionListUserSide(final List<SurveyDatum> result) {
        try {
            mRealm = Realm.getDefaultInstance();
            for (int i = 0; i < result.size(); i++) {
                sdf = mRealm.where(FeedQuestUserSide.class).equalTo("mSurveyId", result.get(i).getSurveyId()).findFirst();
                final int finalI = i;
                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        if (sdf != null) {
                            String gh = result.get(finalI).getCount();
                            sdf.setmSurveyCount(result.get(finalI).getCount());

                            mRealm.copyToRealm(sdf);
                        } else {
                            FeedQuestUserSide feedQuestUserSide = new FeedQuestUserSide();
                            feedQuestUserSide.setSStatus(result.get(finalI).getSStatus());
                            feedQuestUserSide.setmSurveyImage(result.get(finalI).getSurvey_image());
                            feedQuestUserSide.setmSurveyCount(result.get(finalI).getCount());
                            feedQuestUserSide.setSurveyDate(result.get(finalI).getSurveyDate());
                            feedQuestUserSide.setSurveyTime(result.get(finalI).getSurveyTime());
                            feedQuestUserSide.setSurveyDescr(result.get(finalI).getSurveyDescr());
                            feedQuestUserSide.setSurveyId(result.get(finalI).getSurveyId());
                            feedQuestUserSide.setSurveyTitle(result.get(finalI).getSurveyTitle());
                            surveyQuestions = new RealmList<SurveyQuestion>();
                            for (int k = 0; k < result.get(finalI).getSurveyQuestions().size(); k++) {
                                SurveyQuestion question = new SurveyQuestion();
                                question.setIsRequired(result.get(finalI).getSurveyQuestions().get(k).getIsRequired());
                                question.setQuestionId(result.get(finalI).getSurveyQuestions().get(k).getQuestionId());
//                                question.setIsOther(Integer.parseInt(result.get(finalI).getSurveyQuestions().get(k).getIsOther()));
                                String s=result.get(finalI).getSurveyQuestions().get(k).getIsOther();
                                if(!s.equals(null))
                                {
                                    question.setIsOther(Integer.parseInt(s));
                                }
                                else
                                {
                                    question.setIsOther(0);
                                }

                                question.setQuestionImage(result.get(finalI).getSurveyQuestions().get(k).getQuestionImage());
                                question.setQuestionNumber(result.get(finalI).getSurveyQuestions().get(k).getQuestionNumber());
                                question.setQuestionTitle(result.get(finalI).getSurveyQuestions().get(k).getQuestionTitle());
                                question.setQuestionType(result.get(finalI).getSurveyQuestions().get(k).getQuestionType());
                                question.setSurveyId(result.get(finalI).getSurveyQuestions().get(k).getSurveyId());
                                QuestionMore questionMore = new QuestionMore();
                                multipleChoices = new RealmList<MultipleChoice>();
                                for (int p = 0; p < result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getMultipleChoice().size(); p++) {
                                    MultipleChoice questionMultiple = new MultipleChoice();
                                    questionMultiple.setOptionTitle(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getMultipleChoice().get(p).getOptionTitle());
                                    questionMultiple.setQuestionId(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getMultipleChoice().get(p).getQuestionId());
                                    questionMultiple.setQuestionsOptionsId(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getMultipleChoice().get(p).getQuestionsOptionsId());

                                    multipleChoices.add(questionMultiple);
                                }
                                questionMore.setMultipleChoice(multipleChoices);
                                linears = new RealmList<Linear>();
                                for (int p = 0; p < result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getLinear().size(); p++) {
                                    Linear linear = new Linear();
                                    linear.setEndText(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getLinear().get(p).getEndText());
                                    linear.setEndValue(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getLinear().get(p).getEndValue());
                                    linear.setQlinearId(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getLinear().get(p).getQlinearId());
                                    linear.setQuestionId(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getLinear().get(p).getQuestionId());
                                    linear.setStartText(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getLinear().get(p).getStartText());
                                    linear.setStartValue(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getLinear().get(p).getStartValue());
                                    linears.add(linear);
                                }
                                questionMore.setLinear(linears);
                                multipleChoiceGrid = new MultipleChoiceGrid();
                                int ghh = result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getmMultipleChoiceGrid().size();
                                multipleChoiceGrids = new RealmList<MultipleChoiceGrid>();
                                if (ghh != 0) {
                                    rows = new RealmList<Row>();
                                    for (int p = 0; p < result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getmMultipleChoiceGrid().get(0).getRow().size(); p++) {
                                        Row row = new Row();
                                        row.setRowTitle(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getmMultipleChoiceGrid().get(0).getRow().get(p).getRowTitle());
                                        row.setQuestionId(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getmMultipleChoiceGrid().get(0).getRow().get(p).getQuestionId());
                                        row.setQmultipleGridRowId(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getmMultipleChoiceGrid().get(0).getRow().get(p).getQmultipleGridRowId());
                                        rows.add(row);
                                    }
                                    multipleChoiceGrid.setRow(rows);
                                    columns = new RealmList<Column>();
                                    for (int p = 0; p < result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getmMultipleChoiceGrid().get(0).getColumn().size(); p++) {
                                        Column column = new Column();
                                        column.setColTitle(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getmMultipleChoiceGrid().get(0).getColumn().get(p).getColTitle());
                                        column.setQuestionId(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getmMultipleChoiceGrid().get(0).getColumn().get(p).getQuestionId());
                                        column.setQmultipleGridColId(result.get(finalI).getSurveyQuestions().get(k).getQuestionMore().getmMultipleChoiceGrid().get(0).getColumn().get(p).getQmultipleGridColId());
                                        columns.add(column);
                                    }
                                    multipleChoiceGrid.setColumn(columns);
                                    multipleChoiceGrids.add(multipleChoiceGrid);
                                }
                                questionMore.setmMultipleChoiceGrid(multipleChoiceGrids);
                                question.setQuestionMore(questionMore);
                                surveyQuestions.add(question);
                            }
                            feedQuestUserSide.setSurveyQuestions(surveyQuestions);
                            mRealm.copyToRealm(feedQuestUserSide);
                        }
                    }
                });
            }
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }
        if (userPref.getRole().equals("S_User"))
            getSurveyFeedBackUserist();
    }

    /* Method to FETCH User side survey list from Local database */
    private void getSurveyFeedBackUserist() {
        surveyDetailsUserFeedbackQuestionModels = new ArrayList<SurveyDetailsUserFeedbackQuestionModel>();
        mRealm = Realm.getDefaultInstance();
        feedQuestUserSides = mRealm.where(FeedQuestUserSide.class).findAll();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    for (int i = 0; i < feedQuestUserSides.size(); i++) {
                        if (!feedQuestUserSides.get(i).getSStatus().equals("De-active")) {
                            SurveyDetailsUserFeedbackQuestionModel surveyQuestionsDetails = new SurveyDetailsUserFeedbackQuestionModel();
                            surveyQuestionsDetails.setSurveyImage(feedQuestUserSides.get(i).getmSurveyImage());
                            surveyQuestionsDetails.setSurveyDate(feedQuestUserSides.get(i).getSurveyDate());
                            surveyQuestionsDetails.setSurveyDescription(feedQuestUserSides.get(i).getSurveyDescr());
                            surveyQuestionsDetails.setSurveyId(Integer.parseInt(feedQuestUserSides.get(i).getSurveyId()));
                            surveyQuestionsDetails.setSurveyName(feedQuestUserSides.get(i).getSurveyTitle());
                            surveyQuestionsDetails.setSurveyStatus(feedQuestUserSides.get(i).getSStatus());
                            surveyQuestionsDetails.setCount(Integer.parseInt(feedQuestUserSides.get(i).getmSurveyCount()));
                            surveyQuestionsDetails.setSurveyTime(feedQuestUserSides.get(i).getSurveyTime());
                            surveyDetailsUserFeedbackQuestionModels.add(surveyQuestionsDetails);
                        }
                    }
                }
            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }
        if (surveyDetailsUserFeedbackQuestionModels.size() == 0) {

            SurveyListAdapterUserSide surveyListAdapter = new SurveyListAdapterUserSide(getActivity(), this, surveyDetailsUserFeedbackQuestionModels);
            rv_SurveyListQuestion.setAdapter(surveyListAdapter);
            surveyListAdapter.notifyDataSetChanged();
            lnrEmptyScreen.setVisibility(View.VISIBLE);

        } else {
            rv_SurveyListQuestion.setHasFixedSize(true);
            mLayoutManager = new LinearLayoutManager(getActivity());
            rv_SurveyListQuestion.setLayoutManager(mLayoutManager);
            SurveyListAdapterUserSide surveyListAdapter = new SurveyListAdapterUserSide(getActivity(), this, surveyDetailsUserFeedbackQuestionModels);
            rv_SurveyListQuestion.setAdapter(surveyListAdapter);

            lnr_createSurvey.setVisibility(View.GONE);
            rv_SurveyList.setVisibility(View.GONE);

            rv_SurveyListQuestion.setVisibility(View.VISIBLE);

        }

    }

    /* Method to INITIALISE views */
    private void initViews(View v) {
        rv_SurveyList = (RecyclerView) v.findViewById(R.id.rv_SurveyList);
        rv_SurveyListQuestion = (RecyclerView) v.findViewById(R.id.rv_SurveyListQuestion);
        btnCreateSurvey = (Button) v.findViewById(R.id.btnCreateSurvey);
        tv_BusinessName = (TextView) v.findViewById(R.id.tv_BusinessName);
        errormessage = v.findViewById(R.id.errormessage);
        tv_BusinessGst = (TextView) v.findViewById(R.id.tv_BusinessGst);
        imgProfile = (ImageView) v.findViewById(R.id.imgProfile);
        lnr_createSurvey = (LinearLayout) v.findViewById(R.id.lnr_createSurvey);
        lnrEmptyScreen = (LinearLayout) v.findViewById(R.id.lnrEmptyScreen);
    }



    @Override
    public void onNavigateSurveyResult(String survey, int completeId) {
        BusFactory.getBus().post(new NavigateSurveyResultFragmentEvent(survey, completeId));
    }






    @Override
    public void onNavigateSurveyUserResult(int id) {

        BusFactory.getBus().post(new NavigateStartSurveyUserFragmentEvent(id));

    }








    @Subscribe
    public void BackPressRefreshLocalDat(BackPressRefreshLocalData event) {

        if (userPref.getRole().equals("S_Admin")) {
            getFeedbackQuestionListFromLocal();
        } else {
getSurveyFeedBackUserist();
        }
    }

    @Subscribe
    public void RefreshListFeedback(RefreshListFeedback event) {


        Log.e( "RefreshListFeedback: ","Reload list " );
        if (userPref.getRole().equals("S_Admin")) {
            getFeedbackQuestionListFromLocal();
        } else {
            getSurveyFeedBackUserist();
        }


    }



}
