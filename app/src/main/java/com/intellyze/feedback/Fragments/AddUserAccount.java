package com.intellyze.feedback.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.intellyze.feedback.Adapters.MyUsersDetailsRecyclerViewAdapter;
import com.intellyze.feedback.Api.AddUserModel.AdduserModel;
import com.intellyze.feedback.Api.AddUserModel.HttpRequestForAddUser;
import com.intellyze.feedback.Api.AddUserModel.OnHttpAddUserRequest;
import com.intellyze.feedback.Api.UserModel.HttpRequestForUserDelete;
import com.intellyze.feedback.Api.UserModel.HttpRequestForUserListing;
import com.intellyze.feedback.Api.UserModel.ListUserItem;
import com.intellyze.feedback.Api.UserModel.OnHttpUserDeleteResponce;
import com.intellyze.feedback.Api.UserModel.OnHttpUserRequest;
import com.intellyze.feedback.Api.UserModel.Result;
import com.intellyze.feedback.Api.UserModel.UserDeleteModel;
import com.intellyze.feedback.Interfaces.UserDeleteItemClick;
import com.intellyze.feedback.R;
import com.intellyze.feedback.Utils.RecyclerItemTouchHelper;
import com.intellyze.feedback.Utils.UserPref;
import com.nispok.snackbar.Snackbar;
import com.nispok.snackbar.SnackbarManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

import static android.app.Activity.RESULT_OK;
import static com.intellyze.feedback.Activities.LoginActivity.isValidEmail;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListenerAdduser} interface
 * to handle interaction events.
 * Use the {@link AddUserAccount#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddUserAccount extends BaseFragment  implements OnHttpAddUserRequest, OnHttpUserRequest, RecyclerItemTouchHelper.RecyclerItemTouchHelperListener, OnHttpUserDeleteResponce,UserDeleteItemClick {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
TextView user_roledescriptiom;
    EditText username, role, edtPassword, confirmpassword;
    String encodedImage = "";
    LinearLayout lnrStartQuestions;
    String ds;
    public static final String ARG_ID = "ARG_ID";
    private int mId;
    AdduserModel adduserModel;
    UserPref userPref;
    Spinner rolespinner;

    private OnFragmentInteractionListenerAdduser mListener;
    private Realm mRealm;
    private MyUsersDetailsRecyclerViewAdapter myUsersDetailsRecyclerViewAdapter;

    public AddUserAccount() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddUserAccount.
     */
    // TODO: Rename and change types and number of parameters
    public static AddUserAccount newInstance(String param1, String param2) {
        AddUserAccount fragment = new AddUserAccount();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }
    ImageView imgSurveyImage;
    private int mColumnCount = 1;
    RecyclerView recyclerView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }
    int select_photo = 1;
    FrameLayout frmAddSurveyImage;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View v = inflater.inflate(R.layout.fragment_add_user_account, container, false);

        frmAddSurveyImage = (FrameLayout) v.findViewById(R.id.frmAddSurveyImage);
        mRealm = Realm.getDefaultInstance();
        initViews(v);
        setProgressBar();
        final List<String> list = new ArrayList<String>();
        list.add("User");
        list.add("Admin");

        Context context = v.getContext();
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL));

        // adding item touch helper
        // only ItemTouchHelper.LEFT added to detect Right to Left swipe
        // if you want both Right -> Left and Left -> Right
        // add pass ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT as param
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);
        frmAddSurveyImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermission();

            }
        });

        showProgress("Loading");
       setRecycler();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, list);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        rolespinner.setAdapter(dataAdapter);
        lnrStartQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(!isValidEmail(username.getText().toString().trim()))
                {
                    username.setError("Enter valid email");
                    username.setFocusable(true);


return;
                }

                else if (confirmpassword.getText().toString().trim().equals("") || edtPassword.getText().toString().trim().equals("") || role.getText().toString().trim().equals("Select Role")|| username.getText().toString().trim().equals("") || role.getText().toString().trim().equals(""))
                    {

                        if (confirmpassword.getText().toString().trim().equals(""))
                            confirmpassword.setError(getActivity().getResources().getString(R.string.cant_be_blank));



                        if (username.getText().toString().trim().equals(""))
                            username.setError(getActivity().getResources().getString(R.string.cant_be_blank));

                        if (role.getText().toString().trim().equals("Select Role") || role.getText().toString().trim().equals(""))
                            role.setError(getActivity().getResources().getString(R.string.cant_be_blank));

                    }
                    else
                    {

                        if(edtPassword.getText().toString().length()>6)
                        {
                            if (edtPassword.getText().toString().equals(confirmpassword.getText().toString())) {
                                showProgress("Saving ");
                                saveDataToServer();

                            } else {
                                confirmpassword.setError("Password Miss match");
                                return;

                            }
                        }
                        else{
                            edtPassword.setError("Password is too short");
                            return;
                        }

                    }

            }
        });
        rolespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String s=list.get(i);

                role.setText(s);




                if (role.getText().toString().equals("Admin"))
                {
                    user_roledescriptiom.setText("Admin permisson description content goes here");
                }
                else
                {
                    user_roledescriptiom.setText("User  permisson description content goes here");
                }






            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {


            }
        });
        return v;
    }
    Bitmap selectedImage ;
    Bitmap selectedImagefull ;
    private static final int REQUEST_WRITE_PERMISSION = 786;
    private void setRecycler() {
        ListUserItem listUserItem=new ListUserItem();
        listUserItem.setBusinessId(Long.valueOf(userPref.getBusinessId()));
        HttpRequestForUserListing httpRequestForUserListing=new HttpRequestForUserListing(this);
        httpRequestForUserListing.getList(listUserItem);
    }
    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        } else {
            openFilePicker();
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openFilePicker();
        }
    }
    private void openFilePicker() {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, select_photo);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == select_photo && resultCode == RESULT_OK && null != data) {
            Uri selectedImageuri = data.getData();
            final InputStream imageStream;
            try {
                imageStream = getActivity().getContentResolver().openInputStream(selectedImageuri);
                selectedImage = BitmapFactory.decodeStream(imageStream);

                imgSurveyImage.setImageBitmap(selectedImage);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
//            loadImageFromStorage(encodedImage);
            selectedImage =getResizedBitmap(selectedImage,400,400);
            encodedImage = saveToInternalStorage(selectedImage);
        }
    }
    // TODO: Rename method, update argument and hook method into UI event
    public static String encodeFromString(Bitmap bm){
        byte[] b = new byte[0];
        if (bm != null)
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            bm.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
             b = baos.toByteArray();


        }
        return Base64.encodeToString(b, Base64.DEFAULT);

    }
    private void saveDataToServer() {
        String encodedImages = encodeFromString(selectedImage);
        adduserModel=new AdduserModel();
        adduserModel.setBusinessId(Long.valueOf(userPref.getBusinessId()));
        adduserModel.setFrid("frid");
        adduserModel.setProfilepic(encodedImages);
        adduserModel.setUpassword(edtPassword.getText().toString());
        adduserModel.setUsername(username.getText().toString());
        String roles = "";
        if (role.getText().toString().equals("Admin"))
        {
            roles = "S_Admin";
            user_roledescriptiom.setText("Admin permisson description content goes here");
        }
        else
        {
            user_roledescriptiom.setText("User  permisson description content goes here");
            roles = "S_User";
        }
        adduserModel.setUrole(roles);
        adduserModel.setUserId(Long.valueOf("0"));
        HttpRequestForAddUser httpRequestForAddUser=new HttpRequestForAddUser(this);
        httpRequestForAddUser.addUserDetails(adduserModel);

    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    public Bitmap getResizedBitmap(Bitmap image, int bitmapWidth, int bitmapHeight) {
        return Bitmap.createScaledBitmap(image, bitmapWidth, bitmapHeight, true);
    }
    private Bitmap loadImageFromStorage(String path) {
        Bitmap b = null;
        try {
            File f = new File(path, "profile.jpg");
            b = BitmapFactory.decodeStream(new FileInputStream(f));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return b;
    }
    private String saveToInternalStorage(Bitmap bitmapImage){
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath=new File(directory,System.currentTimeMillis()+"_improved.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String s= String.valueOf(mypath);
        return s;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListenerAdduser) {
            mListener = (OnFragmentInteractionListenerAdduser) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListenerAdduser");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onHttpAddUserDetailsSuccess(String Message) {
        username.setText("");
        role.setText("");
        edtPassword.setText("");

        imgSurveyImage.setImageBitmap(null);
        confirmpassword.setText("");
        username.setText("");
        SnackbarManager.show(
                Snackbar.with(getActivity())
                        .text(Message));
        setRecycler();
        cancelProgress();
        if(selectedImage!=null)
        {
            selectedImage.recycle();
        }
    }

    @Override
    public void onHttpAddUserDetailsFailed(String message) {
        SnackbarManager.show(
                Snackbar.with(getActivity())
                        .text(message));
cancelProgress();
    }

    List<String> ids;
    @Override
    public void onHttpUserDetailsSuccess(String Message, List<Result> result) {



        ids=new ArrayList<>();


        for (int s=0;s<result.size();s++)
        {
            ids.add(result.get(s).getUserId());
        }

         myUsersDetailsRecyclerViewAdapter =  new MyUsersDetailsRecyclerViewAdapter(result,getActivity(),this);
        recyclerView.setAdapter(myUsersDetailsRecyclerViewAdapter);
        cancelProgress();
    }

    @Override
    public void onHttpUserDetailsFailed(String message) {
cancelProgress();

    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, final int position) {
showAlertAndDelete(position,viewHolder);
    }

    private void showAlertAndDelete(final int position, final RecyclerView.ViewHolder viewHolder) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Are you sure want to delete the user");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                showProgress("Deleting");
                String userid=ids.get(position);
                Deleteitem(userid);
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {


                dialog.cancel();


            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }



    private void Deleteitem(String userid) {

        UserDeleteModel userDeleteModel=new UserDeleteModel();
        userDeleteModel.setBusinessId(Long.valueOf(userPref.getBusinessId()));
        userDeleteModel.setUserId(Long.valueOf(userid));

        HttpRequestForUserDelete httpRequestForUserDelete=new HttpRequestForUserDelete(this);
        httpRequestForUserDelete.getList(userDeleteModel);
    }

    @Override
    public void onHttpUserDeleteSuccess(String Message) {


        setRecycler();
        SnackbarManager.show(
                Snackbar.with(getActivity())
                        .text(Message));
        cancelProgress();

    }

    @Override
    public void onHttpUserDeleteFailed(String message) {
        SnackbarManager.show(
                Snackbar.with(getActivity())
                        .text(message));
        cancelProgress();
    }

    @Override
    public void onUserDeleteClicked(int position) {
        showAlertAndDelete(position);

    }

    private void showAlertAndDelete(final int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Improved");
        builder.setMessage("Are you sure want to delete the user");
        builder.setIcon(getResources().getDrawable(R.drawable.ic_logo_improved));
        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                showProgress("Deleting");
                String userid=ids.get(position);
                Deleteitem(userid);
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {


                dialog.cancel();


            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListenerAdduser {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void initViews(View v) {
        userPref = new UserPref(getActivity());
        recyclerView=v.findViewById(R.id.recyclerview_new);
        imgSurveyImage = (ImageView) v.findViewById(R.id.imgSurveyImage);
        rolespinner = (Spinner) v.findViewById(R.id.rolespinner);
        username = (EditText) v.findViewById(R.id.username);
        role = (EditText) v.findViewById(R.id.role);
        role.setEnabled(false);
        edtPassword = (EditText) v.findViewById(R.id.edtPassword);
        confirmpassword = (EditText) v.findViewById(R.id.confirmpassword);
        lnrStartQuestions = (LinearLayout) v.findViewById(R.id.lnrStartQuestions);
        user_roledescriptiom = (TextView) v.findViewById(R.id.user_roledescriptiom);
    }


}
