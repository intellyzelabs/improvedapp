package com.intellyze.feedback.Fragments;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.intellyze.feedback.Adapters.MySpinnerQuestionTypeAdapter;
import com.intellyze.feedback.Bus.BusFactory;
import com.intellyze.feedback.Bus.Events.NavigateToContinueFragmentEvent;
import com.intellyze.feedback.Bus.Events.RefreshEventContinueFragment;
import com.intellyze.feedback.Bus.Events.RefreshQuestionEvent;
import com.intellyze.feedback.Custom.EditTextMedium;
import com.intellyze.feedback.Custom.EditTextRegular;
import com.intellyze.feedback.Models.SpinnerQuestionTypeModel;
import com.intellyze.feedback.R;
import com.intellyze.feedback.Realm.QuestionMoreDetails;
import com.intellyze.feedback.Realm.SurveyDetails;
import com.intellyze.feedback.Realm.SurveyQuestionsDetails;
import com.intellyze.feedback.Realm.columnDetails;
import com.intellyze.feedback.Realm.linearquestionDetalis;
import com.intellyze.feedback.Realm.matrixquestionDetails;
import com.intellyze.feedback.Realm.multiplechoicesingleDetails;
import com.intellyze.feedback.Realm.rowDetails;
import com.intellyze.feedback.Utils.UserPref;
import com.squareup.otto.Subscribe;
import com.zcw.togglebutton.ToggleButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

import static android.app.Activity.RESULT_OK;

/**
 * Created by INTELLYZE-202 on 30-11-2017.
 */

public class StartQuestionsFragment extends BaseFragment implements View.OnClickListener {
    View child;
    int isOther = 0;
    SurveyQuestionsDetails surveyQuestionsDetails;
    List<EditText> allEds = new ArrayList<EditText>();
    List<EditText> allEdsRow = new ArrayList<EditText>();
    List<EditText> allEdsCol = new ArrayList<EditText>();
    FrameLayout flContainer;
    QuestionMoreDetails qstMoreDetailsObj;
    multiplechoicesingleDetails qstMultipleChoiceObj;
    RealmList<QuestionMoreDetails> qstDetailsMoreList;
    RealmList<multiplechoicesingleDetails> qstDetailsMultipleChoiceSingleList;
    ImageView imgCh3, imgTakeImage, imgAttached, imgCh4, imgCh5, imgCh6, imgCh7, imgCh8;
    EditText tvQuestion, edtColumn1, edtColumn2, edtRow1, edtRow2, edt_Upper, edt_Lower, edtCh1, edtCh2, edtCh3, edtCh4, edtCh5, edtCh6, edtCh7, edtCh8;
    LinearLayout lnrView;
    private static final int REQUEST_WRITE_PERMISSION = 786;
    int required = 0, count = 0, counttv = 100, select_photo = 1;
    Uri selectedImage = Uri.parse("");
    TextView tvSave, tvSurveyName, tvCancel, tvLowerLimit, tvUpperlimit;
    Spinner spQuestionType;
    String encodedImage = "", SpinnerQuestionTypeSelected = "Short answer";
    Realm mRealm;
    UserPref userPref;
    SurveyDetails surveyDetails;
    ToggleButton toggleRequired;
    MySpinnerQuestionTypeAdapter arrayAdapter;
    List<String> options = new ArrayList<String>();
    List<String> optionsRow = new ArrayList<String>();
    List<String> optionsCol = new ArrayList<String>();

    public StartQuestionsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StartQuestionsFragment newInstance() {
        StartQuestionsFragment fragment = new StartQuestionsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_start_question, container, false);
        userPref = new UserPref(getActivity());
        initViews(v);
        imgTakeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestPermission();
            }
        });
        mRealm = Realm.getDefaultInstance();
        ArrayList<SpinnerQuestionTypeModel> spinnerQuestionTypeModels = new ArrayList<SpinnerQuestionTypeModel>();
        spinnerQuestionTypeModels.add(new SpinnerQuestionTypeModel("Short answer", R.drawable.ic_short_answer));
        spinnerQuestionTypeModels.add(new SpinnerQuestionTypeModel("Paragraph", R.drawable.ic_paragraph));
        spinnerQuestionTypeModels.add(new SpinnerQuestionTypeModel("Multiple choice", R.drawable.ic_multiple_choice));
        spinnerQuestionTypeModels.add(new SpinnerQuestionTypeModel("Checkboxes", R.drawable.ic_check_box));
        spinnerQuestionTypeModels.add(new SpinnerQuestionTypeModel("Dropdown", R.drawable.ic_drop_down));
        spinnerQuestionTypeModels.add(new SpinnerQuestionTypeModel("Linear scale", R.drawable.ic_linear_scale));
        spinnerQuestionTypeModels.add(new SpinnerQuestionTypeModel("Multiple choice grid", R.drawable.ic_multiple_choice_grid));
//        spinnerQuestionTypeModels.add(new SpinnerQuestionTypeModel("Checkbox grid", R.drawable.ic_check_box_grid));
        arrayAdapter = new MySpinnerQuestionTypeAdapter(getActivity(), spinnerQuestionTypeModels);
        spQuestionType.setAdapter(arrayAdapter);
        spQuestionType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    SpinnerQuestionTypeSelected = "Short answer";
                    clearMethod();
                    flContainer.removeAllViews();
                    isOther = 0;
                    tvQuestion.setHint("Short Answer Question");
                } else if (position == 1) {
                    SpinnerQuestionTypeSelected = "Paragraph";
                    clearMethod();
                    flContainer.removeAllViews();
                    isOther = 0;
                    tvQuestion.setHint("Long Answer Question");
                } else if (position == 2) {
                    SpinnerQuestionTypeSelected = "Multiple choice";
                    clearMethod();
                    flContainer.removeAllViews();
                    child = getLayoutInflater().inflate(R.layout.multiple_choice_question, null);
                    flContainer.addView(child);
                    tvQuestion.setHint("Multiple Choice Question");
                    isOther = 0;
                    setChildViewsMultipleChoice(child);
                } else if (position == 3) {
                    SpinnerQuestionTypeSelected = "Checkboxes";
                    clearMethod();
                    flContainer.removeAllViews();
                    child = getLayoutInflater().inflate(R.layout.multiple_choice_question, null);
                    tvQuestion.setHint("Multiple Choice Question");
                    flContainer.addView(child);
                    isOther = 0;
                    setChildViewsMultipleChoice(child);
                } else if (position == 4) {
                    SpinnerQuestionTypeSelected = "Dropdown";
                    clearMethod();
                    allEds.clear();
                    flContainer.removeAllViews();
                    child = getLayoutInflater().inflate(R.layout.drop_down_questions, null);
                    tvQuestion.setHint("Dropdown Question");
                    flContainer.addView(child);
                    lnrView = (LinearLayout) child.findViewById(R.id.lnrView);
                    TextView btnad = (TextView) child.findViewById(R.id.btnad);
                    btnad.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            LinearLayout layout = new LinearLayout(getActivity());
                            layout.setOrientation(LinearLayout.HORIZONTAL);
                            EditText et = newedittext();
                            allEds.add(et);
                            layout.addView(et);
                            ImageView iv = newImageview(getActivity());
                            iv.setTag(et.getId());
                            layout.addView(iv);
                            lnrView.addView(layout);
                        }
                    });
                } else if (position == 5) {
                    SpinnerQuestionTypeSelected = "Linear scale";
                    clearMethod();
                    flContainer.removeAllViews();
                    child = getLayoutInflater().inflate(R.layout.linear_scale_questions, null);
                    tvQuestion.setHint("Linear Scale Question");
                    edt_Lower = (EditText) child.findViewById(R.id.edt_Lower);
                    edt_Upper = (EditText) child.findViewById(R.id.edt_Upper);
                    flContainer.addView(child);
                    Spinner spinnerlower = (Spinner) child.findViewById(R.id.spinnerlower);
                    Spinner spinnerUpper = (Spinner) child.findViewById(R.id.spinnerUpper);
                    tvLowerLimit = (TextView) child.findViewById(R.id.tvLowerLimit);
                    tvUpperlimit = (TextView) child.findViewById(R.id.tvUpperlimit);
                    spinnerUpper.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            tvUpperlimit.setText(parent.getSelectedItem().toString());
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {
                        }
                    });
                    spinnerlower.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            tvLowerLimit.setText(parent.getSelectedItem().toString());
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                } else if (position == 6) {
                    SpinnerQuestionTypeSelected = "Multiple choice grid";
                    clearMethod();
                    flContainer.removeAllViews();
                    child = getLayoutInflater().inflate(R.layout.multiple_choice_grid_radio_button, null);
                    flContainer.addView(child);
                    tvQuestion.setHint("Multiple Choice Grid");
                    TextView TvAddNewColumn = (TextView) child.findViewById(R.id.TvAddNewColumn);
                    edtRow1 = (EditText) child.findViewById(R.id.edtRow1);
                    edtRow2 = (EditText) child.findViewById(R.id.edtRow2);
                    edtColumn1 = (EditText) child.findViewById(R.id.edtColumn1);
                    edtColumn2 = (EditText) child.findViewById(R.id.edtColumn2);
                    TextView tvAddNewRow = (TextView) child.findViewById(R.id.tvAddNewRow);
                    final LinearLayout lnrRow = (LinearLayout) child.findViewById(R.id.lnrRow);
                    final LinearLayout lnrCoulmns = (LinearLayout) child.findViewById(R.id.lnrCoulmns);
                    tvAddNewRow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            LinearLayout layout = new LinearLayout(getActivity());
                            layout.setOrientation(LinearLayout.HORIZONTAL);
                            EditText et = newedittextRow();
                            layout.addView(et);
                            allEdsRow.add(et);
                            ImageView iv = newImageview(getActivity());
                            iv.setTag(et.getId());
                            layout.addView(iv);
                            lnrRow.addView(layout);
                        }
                    });
                    TvAddNewColumn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            LinearLayout layout = new LinearLayout(getActivity());
                            layout.setOrientation(LinearLayout.HORIZONTAL);
                            EditText et = newedittextCoulmn();
                            layout.addView(et);
                            allEdsCol.add(et);
                            ImageView iv = newImageview(getActivity());
                            iv.setTag(et.getId());
                            layout.addView(iv);
                            lnrCoulmns.addView(layout);
                        }
                    });
                    ToggleButton toggleOther = (ToggleButton) child.findViewById(R.id.toggleOther);
                    toggleOther.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
                        @Override
                        public void onToggle(boolean on) {
                            if (on) {
                                isOther = 1;
                            } else {
                                isOther = 0;
                            }
//                Toast.makeText(getContext(), "On" + on, Toast.LENGTH_SHORT).show();
                        }

                    });
                } else if (position == 7) {
                    SpinnerQuestionTypeSelected = "Checkbox grid";
                    clearMethod();
                    flContainer.removeAllViews();
                    child = getLayoutInflater().inflate(R.layout.multiple_choice_grid_checkbox, null);
                    flContainer.addView(child);
                    tvQuestion.setHint("Multiple Choice Grid");
                    TextView TvAddNewColumn = (TextView) child.findViewById(R.id.TvAddNewColumn);
                    TextView tvAddNewRow = (TextView) child.findViewById(R.id.tvAddNewRow);
                    final LinearLayout lnrRow = (LinearLayout) child.findViewById(R.id.lnrRow);
                    final LinearLayout lnrCoulmns = (LinearLayout) child.findViewById(R.id.lnrCoulmns);
                    tvAddNewRow.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            LinearLayout layout = new LinearLayout(getActivity());
                            layout.setOrientation(LinearLayout.HORIZONTAL);
                            EditText et = newedittextRow();
                            layout.addView(et);
                            ImageView iv = newImageview(getActivity());
                            iv.setTag(et.getId());
                            layout.addView(iv);
                            lnrRow.addView(layout);
                        }
                    });
                    TvAddNewColumn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            LinearLayout layout = new LinearLayout(getActivity());
                            layout.setOrientation(LinearLayout.HORIZONTAL);
                            EditText et = newedittextCoulmn();
                            layout.addView(et);
                            ImageView iv = newImageview(getActivity());
                            iv.setTag(et.getId());
                            layout.addView(iv);
                            lnrCoulmns.addView(layout);
                        }
                    });
                }
            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return v;
    }

    @Subscribe
    public void RefreshQuestionEvent(RefreshQuestionEvent event) {
        spQuestionType.setSelection(0);
        clearMethod();
        tvQuestion.setText("");
    }

    public void clearMethod() {
        imgAttached.setVisibility(View.GONE);
        selectedImage = Uri.parse("");
        encodedImage = "";

    }

    private EditText newedittext() {
        final EditTextRegular editText = new EditTextRegular(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        params.setMargins(0, 30, 0, 0);
        editText.setLayoutParams(params);
        editText.setId(counttv++);
        editText.setTag("Count" + counttv++);
        editText.setTextSize(14);
        editText.setTextColor(Color.rgb(0, 0, 0));
        editText.setMaxEms(2);
        editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        return editText;
    }

    private EditText newedittextRow() {
        final EditTextMedium editText = new EditTextMedium(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        params.setMargins(0, 30, 0, 0);
        editText.setLayoutParams(params);
        editText.setHint("Enter Row ");
        editText.setId(counttv++);
        editText.setTextSize(14);
        editText.setTextColor(Color.rgb(0, 0, 0));
        editText.setMaxEms(2);
        editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        return editText;
    }

    private EditText newedittextCoulmn() {
        final EditTextMedium editText = new EditTextMedium(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        params.setMargins(0, 30, 0, 0);
        editText.setLayoutParams(params);
        editText.setHint("Enter Coulmn ");
        editText.setId(counttv++);
        editText.setTextSize(14);
        editText.setTextColor(Color.rgb(0, 0, 0));
        editText.setMaxEms(2);
        editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        return editText;
    }

    public ImageView newImageview(Context context) {
        final ImageView imgView = new ImageView(context);
        imgView.setOnClickListener(this);
        imgView.setId(count++);
        imgView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_close_red));
        return imgView;
    }

    private void setChildViewsMultipleChoice(View child) {
        final LinearLayout lnr_ch3 = (LinearLayout) child.findViewById(R.id.lnr_ch3);
        final LinearLayout lnr_ch4 = (LinearLayout) child.findViewById(R.id.lnr_ch4);
        final LinearLayout lnr_ch5 = (LinearLayout) child.findViewById(R.id.lnr_ch5);
        final LinearLayout lnr_ch6 = (LinearLayout) child.findViewById(R.id.lnr_ch6);
        final LinearLayout lnr_ch7 = (LinearLayout) child.findViewById(R.id.lnr_ch7);
        final LinearLayout lnr_ch8 = (LinearLayout) child.findViewById(R.id.lnr_ch8);
        edtCh1 = (EditText) child.findViewById(R.id.edtCh1);
        edtCh2 = (EditText) child.findViewById(R.id.edtCh2);
        edtCh3 = (EditText) child.findViewById(R.id.edtCh3);
        edtCh4 = (EditText) child.findViewById(R.id.edtCh4);
        edtCh5 = (EditText) child.findViewById(R.id.edtCh5);
        edtCh6 = (EditText) child.findViewById(R.id.edtCh6);
        edtCh7 = (EditText) child.findViewById(R.id.edtCh7);
        edtCh8 = (EditText) child.findViewById(R.id.edtCh8);
        imgCh3 = (ImageView) child.findViewById(R.id.imgCh3);
        imgCh4 = (ImageView) child.findViewById(R.id.imgCh4);
        imgCh5 = (ImageView) child.findViewById(R.id.imgCh5);
        imgCh6 = (ImageView) child.findViewById(R.id.imgCh6);
        imgCh7 = (ImageView) child.findViewById(R.id.imgCh7);
        imgCh8 = (ImageView) child.findViewById(R.id.imgCh8);
        ToggleButton toggleOther = (ToggleButton) child.findViewById(R.id.toggleOther);
        toggleOther.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on) {
                    isOther = 1;
                } else {
                    isOther = 0;
                }
//                Toast.makeText(getContext(), "On" + on, Toast.LENGTH_SHORT).show();
            }

        });
        imgCh3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lnr_ch3.setVisibility(View.GONE);
                edtCh3.setText("");
                edtCh2.clearFocus();
                imgCh3.setVisibility(View.INVISIBLE);
            }
        });
        imgCh4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lnr_ch4.setVisibility(View.GONE);
                edtCh4.setText("");
                edtCh3.clearFocus();
                imgCh4.setVisibility(View.INVISIBLE);

            }
        });
        imgCh5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lnr_ch5.setVisibility(View.GONE);
                edtCh5.setText("");
                edtCh4.clearFocus();
                imgCh5.setVisibility(View.INVISIBLE);
            }
        });
        imgCh6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lnr_ch6.setVisibility(View.GONE);
                edtCh6.setText("");
                edtCh5.clearFocus();
                imgCh6.setVisibility(View.INVISIBLE);
            }
        });
        imgCh7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lnr_ch7.setVisibility(View.GONE);
                edtCh7.setText("");
                edtCh6.clearFocus();
                imgCh7.setVisibility(View.INVISIBLE);
            }
        });
        imgCh8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lnr_ch8.setVisibility(View.GONE);
                edtCh8.setText("");
                edtCh7.clearFocus();
                imgCh8.setVisibility(View.INVISIBLE);
            }
        });
        edtCh2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    lnr_ch3.setVisibility(View.VISIBLE);
                    imgCh3.setVisibility(View.VISIBLE);
                }
            }
        });
        edtCh3.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    lnr_ch4.setVisibility(View.VISIBLE);
                    imgCh4.setVisibility(View.VISIBLE);
                }
            }
        });
        edtCh4.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    lnr_ch5.setVisibility(View.VISIBLE);
                    imgCh5.setVisibility(View.VISIBLE);
                }
            }
        });
        edtCh5.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    lnr_ch6.setVisibility(View.VISIBLE);
                    imgCh6.setVisibility(View.VISIBLE);
                }
            }
        });
        edtCh6.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    lnr_ch7.setVisibility(View.VISIBLE);
                    imgCh7.setVisibility(View.VISIBLE);
                }
            }
        });
        edtCh7.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    lnr_ch8.setVisibility(View.VISIBLE);
                    imgCh8.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        } else {
            openFilePicker();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openFilePicker();
        }
    }

    private void openFilePicker() {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, select_photo);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == select_photo && resultCode == RESULT_OK && null != data) {
            selectedImage = data.getData();
            final InputStream imageStream;
            try {


                imageStream = getActivity().getContentResolver().openInputStream(selectedImage);
                Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                selectedImage = getResizedBitmap(selectedImage, 500, 500);
                encodedImage = encodeImage(selectedImage);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            imgAttached.setImageURI(selectedImage);
            imgAttached.setVisibility(View.VISIBLE);
            imgAttached.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clearMethod();
                }
            });
        }
    }

    private String saveToInternalStorage(Bitmap bitmapImage) {
        ContextWrapper cw = new ContextWrapper(getActivity());
        // path to /data/data/yourapp/app_data/imageDir
        File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
        // Create imageDir
        File mypath = new File(directory, System.currentTimeMillis() + "_improved.jpg");

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(mypath);
            // Use the compress method on the BitMap object to write image to the OutputStream
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String hh = String.valueOf(mypath);
        System.out.println("aamruth" + hh);
        return hh;
    }

    public Bitmap getResizedBitmap(Bitmap image, int bitmapWidth, int bitmapHeight) {
        return Bitmap.createScaledBitmap(image, bitmapWidth, bitmapHeight, true);
    }

    private void initViews(View v) {
        imgTakeImage = (ImageView) v.findViewById(R.id.imgTakeImage);
        tvQuestion = (EditText) v.findViewById(R.id.tvQuestion);
        imgAttached = (ImageView) v.findViewById(R.id.imgAttached);
        toggleRequired = (ToggleButton) v.findViewById(R.id.toggleRequired);
        toggleRequired.setOnToggleChanged(new ToggleButton.OnToggleChanged() {
            @Override
            public void onToggle(boolean on) {
                if (on) {
                    required = 1;
                } else {
                    required = 0;
                }
            }
        });
        flContainer = (FrameLayout) v.findViewById(R.id.flContainer);
        tvCancel = (TextView) v.findViewById(R.id.tvCancel);
        tvSurveyName = (TextView) v.findViewById(R.id.tvSurveyName);
        tvSave = (TextView) v.findViewById(R.id.tvSave);
        spQuestionType = (Spinner) v.findViewById(R.id.spQuestionType);
        tvSurveyName.setText(userPref.getSurveyName());


        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                BusFactory.getBus().post(new RefreshEventContinueFragment());
            }
        });

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tvQuestion.getText().toString().trim().equals("")) {
                    tvQuestion.setError(getActivity().getResources().getString(R.string.cant_be_blank));
                } else {
                    switch (spQuestionType.getSelectedItemPosition()) {
                        case 0:
                            setValueToRealmFillAnswerQuestion(tvQuestion.getText().toString().trim());
                            break;
                        case 1:
                            setValueToRealmFillAnswerQuestion(tvQuestion.getText().toString().trim());
                            break;
                        case 2:
                            fetchValuesFromEditText();
                            setValueToRealmMultipleChoice("radio", tvQuestion.getText().toString().trim());
                            options.clear();
                            break;
                        case 3:
                            fetchValuesFromEditText();
                            setValueToRealmMultipleChoice("checkbox", tvQuestion.getText().toString().trim());
                            options.clear();
                            break;
                        case 4:
//int ss=allEds.size();
//                            Log.e( "Size of items: ",""+allEds.size() );
//                            if(ss==0)
//                            {
//                                Toast.makeText(getActivity(), "Please Add options", Toast.LENGTH_SHORT).show();
//return;
//                            }
//                            else {

                                for (int i = 0; i < allEds.size(); i++) {
                                    EditText editText = allEds.get(i);
                                    if (!editText.getText().toString().trim().equals("")) {
                                        options.add(editText.getText().toString().trim());
                                    }
// else {
//                                        editText.setError(getActivity().getResources().getString(R.string.cant_be_blank));
//                                        return;
//                                    }
                                }

                                setValueToRealmMultipleChoice("Dropdown", tvQuestion.getText().toString().trim());
                                options.clear();

                                allEds.clear();
//                            }
                            break;
                        case 5:

                            if (edt_Upper.getText().toString().equals("")) {

                                edt_Upper.setError(getActivity().getResources().getString(R.string.cant_be_blank));
                                return;

                            } else if (edt_Lower.getText().toString().equals("")) {
                                edt_Lower.setError(getActivity().getResources().getString(R.string.cant_be_blank));
                                return;
                            } else {

                                setValueToLinearScale(tvLowerLimit.getText().toString().trim(), tvUpperlimit.getText().toString().trim(), edt_Upper.getText().toString().trim(), edt_Lower.getText().toString().trim());
                                options.clear();
                                allEds.clear();

                            }

                            break;
                        case 6:
                            if (!edtRow1.getText().toString().trim().equals("")) {
                                optionsRow.add(edtRow1.getText().toString().trim());
                            }
                            else
                            {
                                edtRow1.setError("Required");
                                return;
                            }
                            if (!edtRow2.getText().toString().trim().equals("")) {
                                optionsRow.add(edtRow2.getText().toString().trim());
                            }
                            else
                            {
                                edtRow2.setError("Required");
                                return;
                            }
                            if (!edtColumn1.getText().toString().trim().equals("")) {
                                optionsCol.add(edtColumn1.getText().toString().trim());
                            }
                            else
                            {
                                edtColumn1.setError("Required");
                                return;
                            }
                            if (!edtColumn2.getText().toString().trim().equals("")) {
                                optionsCol.add(edtColumn2.getText().toString().trim());
                            }
                            else
                            {
                                edtColumn2.setError("Required");
                                return;
                            }
                            for (int i = 0; i < allEdsRow.size(); i++) {
                                EditText editText = allEdsRow.get(i);
                                if (!editText.getText().toString().trim().equals("")) {
                                    optionsRow.add(editText.getText().toString().trim());
                                }
                            }
                            for (int i = 0; i < allEdsCol.size(); i++) {
                                EditText editText = allEdsCol.get(i);
                                if (!editText.getText().toString().trim().equals("")) {
                                    optionsCol.add(editText.getText().toString().trim());
                                }
                            }
                            setValueMultipleGridRadio(optionsCol, optionsRow, tvQuestion.getText().toString().trim());
                            optionsCol.clear();
                            optionsRow.clear();
                            allEdsRow.clear();
                            allEdsCol.clear();
                            break;
                    }
                }
            }
        });
    }

    private void setValueMultipleGridRadio(final List<String> optionsCol, final List<String> optionsRow, final String question) {
        if (optionsCol.size() >= 2 && optionsRow.size() >= 1) {
            mRealm = Realm.getDefaultInstance();
            surveyDetails = mRealm.where(SurveyDetails.class)
                    .equalTo("SurveyId", userPref.getSurveyId()).findFirst();
            try {
                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        if (surveyDetails != null) {
                            surveyQuestionsDetails = new SurveyQuestionsDetails();
                            Number maxId = mRealm.where(SurveyQuestionsDetails.class).max("Question_Id");
                            int nextId = (maxId == null) ? 1 : maxId.intValue() + 1;
                            surveyQuestionsDetails.setQuestion_Id(nextId);

                            surveyQuestionsDetails.setAdded_flag("true");
                            surveyQuestionsDetails.setQuestionImage(encodedImage);
                            surveyQuestionsDetails.setIsOther(isOther);
                            surveyQuestionsDetails.setQuestionName(question);
                            int k = userPref.GetCount();
                            surveyQuestionsDetails.setQuestionNumber(k++);
                            userPref.SaveCount(k++);
                            surveyQuestionsDetails.setQuestionRequired(required);
                            surveyQuestionsDetails.setQuestionType(SpinnerQuestionTypeSelected);
                            RealmList<QuestionMoreDetails> questionMoreDetailsRealmlist = new RealmList<QuestionMoreDetails>();
                            QuestionMoreDetails questionMoreDetails = new QuestionMoreDetails();
                            questionMoreDetails.setChoiceType("radio");
                            RealmList<matrixquestionDetails> matrixquestionDetailsrealmlist = new RealmList<matrixquestionDetails>();
                            matrixquestionDetails m = new matrixquestionDetails();
                            RealmList<rowDetails> rowDetailsrealmlist = new RealmList<rowDetails>();
                            for (int o = 0; o < optionsRow.size(); o++) {
                                rowDetails rowDetails = new rowDetails();
                                rowDetails.setQmultiple_grid_row_Id(0);
                                rowDetails.setRow_name(optionsRow.get(o));
                                rowDetailsrealmlist.add(rowDetails);
                            }
                            RealmList<columnDetails> columnDetailsrealmlist = new RealmList<columnDetails>();
                            for (int o = 0; o < optionsCol.size(); o++) {
                                columnDetails columnDetail = new columnDetails();
                                columnDetail.setQmultiple_grid_col_Id(0);
                                columnDetail.setColumn_name(optionsCol.get(o));
                                columnDetailsrealmlist.add(columnDetail);
                            }
                            m.setColumnDetails(columnDetailsrealmlist);
                            m.setRowDetails(rowDetailsrealmlist);
                            matrixquestionDetailsrealmlist.add(m);
                            questionMoreDetails.setMatrixquestionDetails(matrixquestionDetailsrealmlist);
                            questionMoreDetailsRealmlist.add(questionMoreDetails);
                            surveyQuestionsDetails.setQuestionMoreDetails(questionMoreDetailsRealmlist);
                            surveyDetails.getSurveyQuestionsDetails().add(surveyQuestionsDetails);
                            mRealm.copyToRealm(surveyDetails);
                            Toast.makeText(getActivity(), "" + "Added successfully!", Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(getActivity(), "Something went wrong, please try again later!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } catch (Exception em) {
                em.printStackTrace();
            } finally {
                if (mRealm != null)
                    mRealm.close();
            }
            View view = getActivity().getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            BusFactory.getBus().post(new NavigateToContinueFragmentEvent());

        }
    }

    private void setValueToLinearScale(final String lowerlimit, final String upperlimit, final String uppertext, final String lowertext) {
        mRealm = Realm.getDefaultInstance();
        surveyDetails = mRealm.where(SurveyDetails.class)
                .equalTo("SurveyId", userPref.getSurveyId()).findFirst();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (surveyDetails != null) {
                        surveyQuestionsDetails = new SurveyQuestionsDetails();
                        Number maxId = mRealm.where(SurveyQuestionsDetails.class).max("Question_Id");
                        int nextId = (maxId == null) ? 1 : maxId.intValue() + 1;
                        surveyQuestionsDetails.setQuestion_Id(nextId);

                        surveyQuestionsDetails.setAdded_flag("true");
                        surveyQuestionsDetails.setQuestionImage(encodedImage);
                        surveyQuestionsDetails.setIsOther(isOther);
                        surveyQuestionsDetails.setQuestionName(tvQuestion.getText().toString().trim());
                        int k = userPref.GetCount();
                        surveyQuestionsDetails.setQuestionNumber(k++);
                        userPref.SaveCount(k++);
                        surveyQuestionsDetails.setQuestionRequired(required);
                        surveyQuestionsDetails.setQuestionType(SpinnerQuestionTypeSelected);
                        RealmList<QuestionMoreDetails> questionMoreDetailsRealmlist = new RealmList<QuestionMoreDetails>();
                        QuestionMoreDetails questionMoreDetails = new QuestionMoreDetails();
                        questionMoreDetails.setChoiceType("linear");
                        RealmList<linearquestionDetalis> linearquestionDetalisrealmlist = new RealmList<linearquestionDetalis>();
                        linearquestionDetalis linearquestionDetalis = new linearquestionDetalis();
                        linearquestionDetalis.setEndText(uppertext);
                        linearquestionDetalis.setStartText(lowertext);
                        linearquestionDetalis.setEndValue(Integer.parseInt(upperlimit));
                        linearquestionDetalis.setStartValue(Integer.parseInt(lowerlimit));
                        linearquestionDetalisrealmlist.add(linearquestionDetalis);
                        questionMoreDetails.setLinearquestionDetalis(linearquestionDetalisrealmlist);
                        questionMoreDetailsRealmlist.add(questionMoreDetails);
                        surveyQuestionsDetails.setQuestionMoreDetails(questionMoreDetailsRealmlist);
                        surveyDetails.getSurveyQuestionsDetails().add(surveyQuestionsDetails);
                        mRealm.copyToRealm(surveyDetails);
                        Toast.makeText(getActivity(), "" + "Added successfully!", Toast.LENGTH_SHORT).show();
//
                    } else {
                        Toast.makeText(getActivity(), "Something went wrong, please try again later!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        BusFactory.getBus().post(new NavigateToContinueFragmentEvent());

    }

    private void setValueToRealmFillAnswerQuestion(final String question) {
        mRealm = Realm.getDefaultInstance();
        surveyDetails = mRealm.where(SurveyDetails.class)
                .equalTo("SurveyId", userPref.getSurveyId()).findFirst();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (surveyDetails != null) {

                        surveyQuestionsDetails = new SurveyQuestionsDetails();
                        Number maxId = mRealm.where(SurveyQuestionsDetails.class).max("Question_Id");
                        int nextId = (maxId == null) ? 1 : maxId.intValue() + 1;
                        surveyQuestionsDetails.setQuestion_Id(nextId);

                        surveyQuestionsDetails.setAdded_flag("true");
                        surveyQuestionsDetails.setQuestionImage(encodedImage);
                        surveyQuestionsDetails.setQuestionName(question);
                        surveyQuestionsDetails.setIsOther(isOther);
                        int k = userPref.GetCount();
                        surveyQuestionsDetails.setQuestionNumber(k++);
                        userPref.SaveCount(k++);
                        surveyQuestionsDetails.setQuestionRequired(required);
                        surveyQuestionsDetails.setQuestionType(SpinnerQuestionTypeSelected);
                        surveyDetails.getSurveyQuestionsDetails().add(surveyQuestionsDetails);
                        mRealm.copyToRealm(surveyDetails);
                        Toast.makeText(getActivity(), "" + "Added successfully!", Toast.LENGTH_SHORT).show();

                        tvQuestion.setText("");
                    } else {
                        Toast.makeText(getActivity(), "Something went wrong, please try again later!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        BusFactory.getBus().post(new NavigateToContinueFragmentEvent());
    }

    private void setValueToRealmMultipleChoice(final String choiceType, final String question) {
        if (options.size() >= 2) {
            mRealm = Realm.getDefaultInstance();
            surveyDetails = mRealm.where(SurveyDetails.class)
                    .equalTo("SurveyId", userPref.getSurveyId()).findFirst();
            try {
                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        if (surveyDetails != null) {
                            surveyQuestionsDetails = mRealm.copyToRealm(new SurveyQuestionsDetails());
                            Number maxId = mRealm.where(SurveyQuestionsDetails.class).max("Question_Id");
                            int nextId = (maxId == null) ? 1 : maxId.intValue() + 1;
                            surveyQuestionsDetails.setQuestion_Id(nextId);

                            surveyQuestionsDetails.setAdded_flag("true");


                            surveyQuestionsDetails.setQuestionImage(encodedImage);
                            surveyQuestionsDetails.setQuestionName(question);
                            surveyQuestionsDetails.setIsOther(isOther);
                            int k = userPref.GetCount();
                            surveyQuestionsDetails.setQuestionNumber(k++);
                            userPref.SaveCount(k++);
                            surveyQuestionsDetails.setQuestionRequired(required);
                            surveyQuestionsDetails.setQuestionType(SpinnerQuestionTypeSelected);
                            qstMoreDetailsObj = mRealm.copyToRealm(new QuestionMoreDetails());
                            qstMoreDetailsObj.setChoiceType(choiceType);
                            qstDetailsMoreList = new RealmList<QuestionMoreDetails>();
                            qstDetailsMultipleChoiceSingleList = new RealmList<multiplechoicesingleDetails>();
                            for (int o = 0; o < options.size(); o++) {
                                qstMultipleChoiceObj = mRealm.copyToRealm(new multiplechoicesingleDetails());
                                qstMultipleChoiceObj.setQuestions_options_Id(0);
                                qstMultipleChoiceObj.setTextChoice(options.get(o));
                                qstDetailsMultipleChoiceSingleList.add(qstMultipleChoiceObj);
                            }
                            qstMoreDetailsObj.setMultiplechoicesingleDetails(qstDetailsMultipleChoiceSingleList);
                            qstDetailsMoreList.add(qstMoreDetailsObj);
                            surveyQuestionsDetails.setQuestionMoreDetails(qstDetailsMoreList);
                            surveyDetails.getSurveyQuestionsDetails().add(surveyQuestionsDetails);
                            mRealm.copyToRealm(surveyDetails);
                            Toast.makeText(getActivity(), "" + "Added successfully!", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), "Something went wrong, please try again later!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } catch (Exception em) {
                em.printStackTrace();
            } finally {
                if (mRealm != null)
                    mRealm.close();
            }
            View view = getActivity().getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            BusFactory.getBus().post(new NavigateToContinueFragmentEvent());

        }
    }


    private void fetchValuesFromEditText() {
        if (!edtCh1.getText().toString().trim().equals("")) {
            options.add(edtCh1.getText().toString().trim());
        } else {
            edtCh1.setError("Required");
            return;
        }
        if (!edtCh2.getText().toString().trim().equals("")) {
            options.add(edtCh2.getText().toString().trim());
        } else {
            edtCh2.setError("Required");
            return;
        }
        if (!edtCh3.getText().toString().trim().equals("")) {
            options.add(edtCh3.getText().toString().trim());
        }
        if (!edtCh4.getText().toString().trim().equals("")) {
            options.add(edtCh4.getText().toString().trim());
        }
        if (!edtCh5.getText().toString().trim().equals("")) {
            options.add(edtCh5.getText().toString().trim());
        }
        if (!edtCh6.getText().toString().trim().equals("")) {
            options.add(edtCh6.getText().toString().trim());
        }
        if (!edtCh7.getText().toString().trim().equals("")) {
            options.add(edtCh7.getText().toString().trim());
        }
        if (!edtCh8.getText().toString().trim().equals("")) {
            options.add(edtCh8.getText().toString().trim());
        }
    }

    @Override
    public void onClick(View v) {
        Object editTextId = v.getTag();
        if (editTextId == null) {
            return;
        }
        ViewParent parent = v.getParent();
        if (parent instanceof ViewGroup) {
            EditText et = ((ViewGroup) parent).findViewById((Integer) editTextId);
            ((ViewGroup) parent).removeView(et);
            ((ViewGroup) parent).removeView(v);
        }
    }

}
