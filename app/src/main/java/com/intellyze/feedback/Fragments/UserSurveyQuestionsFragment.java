package com.intellyze.feedback.Fragments;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Scroller;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.intellyze.feedback.Adapters.MainPagerAdapter;
import com.intellyze.feedback.Bus.BusFactory;
import com.intellyze.feedback.Bus.Events.NavigateToSurveyLastPagFragmentEvent;
import com.intellyze.feedback.Custom.EditTextRegular;
import com.intellyze.feedback.Custom.TextViewRegular;
import com.intellyze.feedback.Custom.TextViewSemiBold;
import com.intellyze.feedback.Models.CheckBoxDetails;
import com.intellyze.feedback.Models.ColumnDetailsId;
import com.intellyze.feedback.Models.DropDownAnswerModel;
import com.intellyze.feedback.Models.GridRadioAnswerMultipleModel;
import com.intellyze.feedback.Models.LinearAnswerModells;
import com.intellyze.feedback.Models.LongAnswerModel;
import com.intellyze.feedback.Models.MultipleChoiceCheckBoxModel;
import com.intellyze.feedback.Models.MultipleChoiceRadioModel;
import com.intellyze.feedback.Models.RadioGroupDetails;
import com.intellyze.feedback.R;
import com.intellyze.feedback.Realm.AddAnswerMaster;
import com.intellyze.feedback.Realm.AnswersList;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.Column;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.FeedQuestUserSide;
import com.intellyze.feedback.Realm.FeedbackQuestionModel.MultipleChoice;
import com.intellyze.feedback.Utils.UserPref;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by INTELLYZE-202 on 01-12-2017.
 */

public class UserSurveyQuestionsFragment extends BaseFragment {
    private ViewPager pager = null;
    private MainPagerAdapter pagerAdapter = null;
    public static final String ARG_ID = "ARG_ID";
    private int mId, totalCount = 0;
    TextView tvPrev, tvFinish, tvSkip, iv, tvShortAnwer;
    String first;
    AddAnswerMaster AddAnswerMaster;
    FeedQuestUserSide feedQuestUserSides;
    RealmList<AnswersList> answersLists;
    List<LongAnswerModel> longAnswerModels = new ArrayList<LongAnswerModel>();
    List<MultipleChoiceRadioModel> multipleChoiceRadioModels = new ArrayList<MultipleChoiceRadioModel>();
    List<LinearAnswerModells> linearAnswerModellss = new ArrayList<LinearAnswerModells>();
    List<MultipleChoiceCheckBoxModel> multipleChoiceCheckBoxModels = new ArrayList<MultipleChoiceCheckBoxModel>();
    List<DropDownAnswerModel> dropDownAnswerModels = new ArrayList<DropDownAnswerModel>();
    List<GridRadioAnswerMultipleModel> gridRadioAnswerMultipleModels = new ArrayList<GridRadioAnswerMultipleModel>();
    Realm mRealm;

    public UserSurveyQuestionsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment LoginFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserSurveyQuestionsFragment newInstance(int id) {
        UserSurveyQuestionsFragment fragment = new UserSurveyQuestionsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mId = getArguments().getInt(ARG_ID);

    }

    private void saveSurveyDataTo() {

        showProgress("Saving data");
        mRealm = Realm.getDefaultInstance();
        UserPref userPref = new UserPref(getActivity());
        AddAnswerMaster = mRealm.where(AddAnswerMaster.class).equalTo("mSurveyUserId", userPref.getSurveyAnswerId()).findFirst();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (AddAnswerMaster != null) {
                        answersLists = new RealmList<AnswersList>();
//                        Toast.makeText(getActivity(), "" + "saving..", Toast.LENGTH_SHORT).show();
                        for (int i = 0; i < longAnswerModels.size(); i++) {
                            AnswersList answersList = mRealm.copyToRealm(new AnswersList());
                            EditText editText = longAnswerModels.get(i).getAnswerEditText();


                            String longanswer = editText.getText().toString();



                            answersList.setAnswrValue(editText.getText().toString());
                            answersList.setQuestionId(longAnswerModels.get(i).getQuestionId());
                            answersList.setAnswrValueId(0);
                            answersList.setAnswrValueRowId(0);
                            answersLists.add(answersList);

                        }

                        for (int i = 0; i < multipleChoiceCheckBoxModels.size(); i++) {
                            AnswersList answersList = mRealm.copyToRealm(new AnswersList());
                            answersList.setQuestionId(multipleChoiceCheckBoxModels.get(i).getQuestionId());

                            int flag = 0;
                            for (int p = 0; p < multipleChoiceCheckBoxModels.get(i).getCheckBoxes().size(); p++) {
                                CheckBox radioGroup = multipleChoiceCheckBoxModels.get(i).getCheckBoxes().get(p).getCheckBoxes();
                                if (radioGroup.isChecked()) {
                                    flag = 1;
                                    String kj = radioGroup.getText().toString();
                                    answersList.setAnswrValue(kj);
                                    answersList.setAnswrValueId(multipleChoiceCheckBoxModels.get(i).getOptionIds().get(p));
                                    answersList.setAnswrValueRowId(multipleChoiceCheckBoxModels.get(i).getOptionIds().get(p));
                                    answersLists.add(answersList);
                                }
                            }
                            if (flag == 0) {
                                EditText jsdf = multipleChoiceCheckBoxModels.get(i).getEditText();
                                if (jsdf != null) {
                                    if (jsdf.getText().toString().trim().length() != 0) {
                                        answersList.setAnswrValue(jsdf.getText().toString().trim());
                                        answersList.setAnswrValueId(0);
                                        answersList.setAnswrValueRowId(0);
                                        answersLists.add(answersList);

                                    }
                                }
                            }
                        }

                        for (int i = 0; i < multipleChoiceRadioModels.size(); i++) {
                            AnswersList answersList = mRealm.copyToRealm(new AnswersList());
                            RadioGroup radioGroup = multipleChoiceRadioModels.get(i).getRadioGroup();
                            int rf = radioGroup.getCheckedRadioButtonId();
                            View radioButton = radioGroup.findViewById(rf);
                            int radioId = radioGroup.indexOfChild(radioButton);
                            RadioButton btn = (RadioButton) radioGroup.getChildAt(radioId);
                            int flag = 0;
                            if (btn != null) {
                                flag = 1;
                                String selection = (String) btn.getText();
                                answersList.setAnswrValue(selection);
                                answersList.setAnswrValueId(multipleChoiceRadioModels.get(i).getQuestionId().get(radioId));
                            }
                            answersList.setQuestionId(multipleChoiceRadioModels.get(i).getQuestId());
                            answersList.setAnswrValueRowId(0);


                            EditText jsdf = multipleChoiceRadioModels.get(i).getEditText();

                            if (jsdf != null && flag == 0) {
                                if (jsdf.getText().toString().trim().length() != 0) {
                                    answersList.setAnswrValue(jsdf.getText().toString().trim());
                                    answersList.setAnswrValueId(0);

                                }
                            }

                            answersLists.add(answersList);
                        }
                        for (int i = 0; i < linearAnswerModellss.size(); i++) {
                            AnswersList answersList = mRealm.copyToRealm(new AnswersList());
                            RadioGroup radioGroup = linearAnswerModellss.get(i).getRadioGroup();
                            int rf = radioGroup.getCheckedRadioButtonId();
                            View radioButton = radioGroup.findViewById(rf);
                            int radioId = radioGroup.indexOfChild(radioButton);
                            Log.e( "radioId: ", ""+radioId);
                            RadioButton btn = (RadioButton) radioGroup.getChildAt(radioId);
                            Log.e( "btn: ", ""+btn);
                            String selection;
if (btn != null) {
     selection = btn.getText().toString();
}else
{
    selection="";

}
                            Log.e( "selection: ", ""+selection);
                            answersList.setAnswrValue(selection);
                            answersList.setQuestionId(linearAnswerModellss.get(i).getQuestionId());
                            answersList.setAnswrValueId(linearAnswerModellss.get(i).getAnswerId());
                            answersList.setAnswrValueRowId(0);
                            answersLists.add(answersList);
                        }
                        for (int i = 0; i < dropDownAnswerModels.size(); i++) {
                            AnswersList answersList = mRealm.copyToRealm(new AnswersList());
                            Spinner spinnera = dropDownAnswerModels.get(i).getSpinneres();
                            String s = spinnera.getSelectedItem().toString();
                            int indexSelected = spinnera.getSelectedItemPosition();
                            answersList.setAnswrValue(s);
                            answersList.setQuestionId(dropDownAnswerModels.get(i).getQuestionId());
                            answersList.setAnswrValueId(dropDownAnswerModels.get(i).getOptionId().get(indexSelected));
                            answersList.setAnswrValueRowId(0);
                            answersLists.add(answersList);
                        }
                        int flag = 0;
                        for (int i = 0; i < gridRadioAnswerMultipleModels.size(); i++) {

                            for (int p = 0; p < gridRadioAnswerMultipleModels.get(i).getRadioGroupDetails().size(); p++) {
                                AnswersList answersList = mRealm.copyToRealm(new AnswersList());
                                RadioGroup radioGroup = gridRadioAnswerMultipleModels.get(i).getRadioGroupDetails().get(p).getGroup();
                                int rf = radioGroup.getCheckedRadioButtonId();
                                View radioButton = radioGroup.findViewById(rf);
                                int radioId = radioGroup.indexOfChild(radioButton);
                                RadioButton btn = (RadioButton) radioGroup.getChildAt(radioId);
                                if (btn != null) {
                                    flag = 1;
                                    String selection = (String) btn.getText();
                                    answersList.setAnswrValue(selection);
                                    answersList.setAnswrValueId(gridRadioAnswerMultipleModels.get(i).getColumnDetailsIds().get(radioId - 1).getColIds());
                                    answersList.setAnswrValueRowId(gridRadioAnswerMultipleModels.get(i).getRadioGroupDetails().get(p).getRowId());
                                    answersList.setQuestionId(gridRadioAnswerMultipleModels.get(i).getQuestionId());
                                    answersLists.add(answersList);
                                }

                            }
                            if (flag == 0) {
                                EditText jsdf = gridRadioAnswerMultipleModels.get(i).getEditText();

                                if (jsdf != null) {
                                    if (jsdf.getText().toString().trim().length() != 0) {
                                        AnswersList answersList = mRealm.copyToRealm(new AnswersList());
                                        answersList.setAnswrValue(jsdf.getText().toString().trim());
                                        answersList.setAnswrValueId(0);
                                        answersList.setAnswrValueRowId(0);
                                        answersList.setQuestionId(gridRadioAnswerMultipleModels.get(i).getQuestionId());
                                        answersLists.add(answersList);
                                    }
                                }

                            }
                        }


                        AddAnswerMaster.setAnswersList(answersLists);
                        mRealm.copyToRealmOrUpdate(AddAnswerMaster);
                        cancelProgress();
                    } else {
                        cancelProgress();
                        Toast.makeText(getActivity(), "" + "not possible ", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
        cancelProgress();
        BusFactory.getBus().post(new NavigateToSurveyLastPagFragmentEvent(mId));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_user_survey_questions, container, false);
        initViews(v);
        setProgressBar();
        tvPrev.setVisibility(View.INVISIBLE);
        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (totalCount - 1 == pager.getCurrentItem() + 1) {
                    tvPrev.setVisibility(View.VISIBLE);
                    tvSkip.setVisibility(View.GONE);
                    tvFinish.setVisibility(View.VISIBLE);
                } else {
                    tvPrev.setVisibility(View.VISIBLE);
                    tvSkip.setVisibility(View.VISIBLE);
                }


                pager.setCurrentItem(pager.getCurrentItem() + 1);


            }
        });
        tvPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (0 == pager.getCurrentItem() - 1) {
                    tvPrev.setVisibility(View.GONE);
                    tvSkip.setVisibility(View.VISIBLE);
                    tvFinish.setVisibility(View.GONE);
                } else {
                    tvSkip.setVisibility(View.VISIBLE);
                    tvSkip.setVisibility(View.VISIBLE);
                }
                pager.setCurrentItem(pager.getCurrentItem() - 1);
            }
        });
        tvFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate()) {
                    saveSurveyDataTo();

                }


//


            }
        });


        pagerAdapter = new MainPagerAdapter();
        pager = (ViewPager) v.findViewById(R.id.view_pager);
        pager.setAdapter(pagerAdapter);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                if (0 == pager.getCurrentItem()) {
                    tvPrev.setVisibility(View.GONE);
                    tvSkip.setVisibility(View.VISIBLE);
                    tvFinish.setVisibility(View.GONE);
                } else if (totalCount - 1 == pager.getCurrentItem()) {
                    tvPrev.setVisibility(View.VISIBLE);
                    tvSkip.setVisibility(View.GONE);
                    tvFinish.setVisibility(View.VISIBLE);
                } else {
                    tvPrev.setVisibility(View.VISIBLE);
                    tvSkip.setVisibility(View.VISIBLE);
                    tvFinish.setVisibility(View.GONE);
                }
            }
        });
        mRealm = Realm.getDefaultInstance();
        String sId = String.valueOf(mId);
        feedQuestUserSides = mRealm.where(FeedQuestUserSide.class).equalTo("mSurveyId", sId).findFirst();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (feedQuestUserSides != null) {
                        totalCount = feedQuestUserSides.getSurveyQuestions().size();
                        if (totalCount == 1) {
                            tvFinish.setVisibility(View.VISIBLE);
                            tvPrev.setVisibility(View.GONE);
                            tvSkip.setVisibility(View.GONE);
                        }
                        for (int i = 0; i < feedQuestUserSides.getSurveyQuestions().size(); i++) {
                            LinearLayout ds = new LinearLayout(getActivity());
                            ds.setGravity(Gravity.TOP);
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);


                            ds.setLayoutParams(params);
                            ds.setGravity(Gravity.CENTER);

                            ds.setBackgroundColor(getActivity().getResources().getColor(R.color.background));


                            if (feedQuestUserSides.getSurveyQuestions().get(i).getQuestionType().equals("Short answer")) {
                                CardView card = drawCard(Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionId()));

                                LinearLayout layout = new LinearLayout(getActivity());
                                layout.setGravity(Gravity.CENTER);
                                layout.setOrientation(LinearLayout.VERTICAL);

                                tvShortAnwer = newtextViewQuestion();
                                first = feedQuestUserSides.getSurveyQuestions().get(i).getQuestionNumber() + ". " + feedQuestUserSides.getSurveyQuestions().get(i).getQuestionTitle() + " ?";
                                tvShortAnwer.setText(first);
                                layout.addView(tvShortAnwer);
                                if (!feedQuestUserSides.getSurveyQuestions().get(i).getQuestionImage().equals("")) {
                                    Bitmap imageToBind = decodeImage(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionImage());
                                    ImageView nkj = newImageview(getActivity(), imageToBind);
                                    layout.addView(nkj);
                                }
                                EditText et = newedittextz("Short Answer");
                                et.setScroller(new Scroller(getActivity()));
                                et.setMinLines(2);
                                et.setVerticalScrollBarEnabled(true);
                                layout.addView(et);


                                LongAnswerModel longAnswerModel = new LongAnswerModel();
                                longAnswerModel.setAnswerEditText(et);
                                longAnswerModel.setQuestionId(Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionId()));
                                longAnswerModel.setQuestionType("Short answer");
                                longAnswerModel.setQuestionNumber(Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionNumber()));
                                longAnswerModel.setIsrequired(feedQuestUserSides.getSurveyQuestions().get(i).getIsRequired());
                                longAnswerModels.add(longAnswerModel);

                                if (feedQuestUserSides.getSurveyQuestions().get(i).getIsRequired().equals("1")) {


                                    View view = new View(getActivity());
                                    LinearLayout.LayoutParams viewparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2);
                                    viewparams.setMargins(30, 0, 30, 0);
                                    viewparams.gravity = Gravity.CENTER;

                                    view.setLayoutParams(viewparams);
                                    view.setBackgroundColor(Color.parseColor("#dedede"));
                                    layout.addView(view);
                                    TextView textView = newtextViewRequired();
                                    textView.setText("Required");
                                    layout.addView(textView);
                                }

                                card.addView(layout);
//                                ds.setGravity(Gravity.CENTER);

                                ds.addView(card);

                                pagerAdapter.addView(ds, i);
                                pagerAdapter.notifyDataSetChanged();
                            } else if (feedQuestUserSides.getSurveyQuestions().get(i).getQuestionType().equals("Paragraph")) {

                                CardView card = drawCard(Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionId()));
                                LinearLayout layout = new LinearLayout(getActivity());
                                layout.setOrientation(LinearLayout.VERTICAL);
                                iv = newtextViewQuestion();
                                iv.setText(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionNumber() + ". " + feedQuestUserSides.getSurveyQuestions().get(i).getQuestionTitle() + " ?");
                                layout.addView(iv);
                                if (!feedQuestUserSides.getSurveyQuestions().get(i).getQuestionImage().equals("")) {
                                    Bitmap imageToBind = decodeImage(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionImage());
                                    ImageView nkj = newImageview(getActivity(), imageToBind);
                                    layout.addView(nkj);
                                }

                                EditText et = newedittextz("Long Answer");
                                et.setScroller(new Scroller(getActivity()));
                                et.setMinLines(2);
                                et.setVerticalScrollBarEnabled(true);
                                layout.addView(et);

                                LongAnswerModel longAnswerModel = new LongAnswerModel();
                                longAnswerModel.setAnswerEditText(et);
                                longAnswerModel.setQuestionId(Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionId()));
                                longAnswerModel.setQuestionType("Paragraph");
                                longAnswerModel.setQuestionNumber(Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionNumber()));

                                longAnswerModel.setIsrequired(feedQuestUserSides.getSurveyQuestions().get(i).getIsRequired());
                                longAnswerModels.add(longAnswerModel);

                                if (feedQuestUserSides.getSurveyQuestions().get(i).getIsRequired().equals("1")) {
                                    View view = new View(getActivity());
                                    LinearLayout.LayoutParams viewparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2);
                                    viewparams.setMargins(30, 0, 30, 0);
                                    view.setLayoutParams(viewparams);
                                    view.setBackgroundColor(Color.parseColor("#dedede"));
                                    layout.addView(view);
                                    TextView textView = newtextViewRequired();
                                    textView.setText("Required");
                                    layout.addView(textView);
                                }
                                card.addView(layout);
                                ds.addView(card);

                                pagerAdapter.addView(ds, i);
                                pagerAdapter.notifyDataSetChanged();
                            } else if (feedQuestUserSides.getSurveyQuestions().get(i).getQuestionType().equals("Multiple choice")) {
                                CardView card = drawCard(Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionId()));
                                LinearLayout layout = new LinearLayout(getActivity());
                                layout.setOrientation(LinearLayout.VERTICAL);
                                iv = newtextViewQuestion();
                                iv.setText(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionNumber() + ". " + feedQuestUserSides.getSurveyQuestions().get(i).getQuestionTitle() + " ?");
                                layout.addView(iv);

                                if (!feedQuestUserSides.getSurveyQuestions().get(i).getQuestionImage().equals("")) {
                                    Bitmap imageToBind = decodeImage(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionImage());
                                    ImageView nkj = newImageview(getActivity(), imageToBind);
                                    layout.addView(nkj);
                                }



                                RadioGroup rg = createRadioGroup(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionMore().getMultipleChoice().size(), feedQuestUserSides.getSurveyQuestions().get(i).getQuestionMore().getMultipleChoice(), feedQuestUserSides.getSurveyQuestions().get(i).getQuestionId(), feedQuestUserSides.getSurveyQuestions().get(i).getIsOther(), layout,feedQuestUserSides.getSurveyQuestions().get(i).getQuestionNumber(),feedQuestUserSides.getSurveyQuestions().get(i).getIsRequired(),feedQuestUserSides.getSurveyQuestions().get(i).getIsOther());

//

                                if (feedQuestUserSides.getSurveyQuestions().get(i).getIsRequired().equals("1")) {
                                    View view = new View(getActivity());
                                    LinearLayout.LayoutParams viewparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2);
                                    viewparams.setMargins(30, 100, 30, 0);
                                    view.setLayoutParams(viewparams);
                                    view.setBackgroundColor(Color.parseColor("#dedede"));

                                    layout.addView(view);
                                    TextView textView = newtextViewRequired();
                                    textView.setText("Required");
                                    layout.addView(textView);
                                }

                                card.addView(layout);
                                ds.addView(card);
                                pagerAdapter.addView(ds, i);
                                pagerAdapter.notifyDataSetChanged();
                            } else if (feedQuestUserSides.getSurveyQuestions().get(i).getQuestionType().equals("Checkboxes")) {
                                CardView card = drawCard(Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionId()));
                                LinearLayout layout = new LinearLayout(getActivity());
                                layout.setOrientation(LinearLayout.VERTICAL);
                                iv = newtextViewQuestion();
                                iv.setText(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionNumber() + ". " + feedQuestUserSides.getSurveyQuestions().get(i).getQuestionTitle() + " ?");
                                layout.addView(iv);
                                if (!feedQuestUserSides.getSurveyQuestions().get(i).getQuestionImage().equals("")) {
                                    Bitmap imageToBind = decodeImage(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionImage());
                                    ImageView nkj = newImageview(getActivity(), imageToBind);
                                    layout.addView(nkj);
                                }
                                LinearLayout.LayoutParams radioparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                radioparams.setMargins(0, 30, 0, 0);
                                LinearLayout fdh = new LinearLayout(getActivity());
                                fdh.setOrientation(LinearLayout.VERTICAL);
                                fdh.setLayoutParams(radioparams);
                                MultipleChoiceCheckBoxModel multipleChoiceCheckBoxModel = new MultipleChoiceCheckBoxModel();
                                multipleChoiceCheckBoxModel.setQuestionId(Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionId()));
                                multipleChoiceCheckBoxModel.setQuestionType("Checkboxes");
                                multipleChoiceCheckBoxModel.setQuestionNumber(Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionNumber()));
                                multipleChoiceCheckBoxModel.setIsother(feedQuestUserSides.getSurveyQuestions().get(i).getIsOther());

                                multipleChoiceCheckBoxModel.setIsrequired(feedQuestUserSides.getSurveyQuestions().get(i).getIsRequired());

                                List<CheckBoxDetails> checkBoxDetailss = new ArrayList<CheckBoxDetails>();
                                List<Integer> answerIds = new ArrayList<>();
                                for (int ij = 0; ij < feedQuestUserSides.getSurveyQuestions().get(i).getQuestionMore().getMultipleChoice().size(); ij++) {
                                    CheckBox cb = new CheckBox(getActivity());
                                    CheckBoxDetails checkBoxDetails = new CheckBoxDetails();
                                    checkBoxDetails.setCheckBoxes(cb);
                                    cb.setText(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionMore().getMultipleChoice().get(ij).getOptionTitle());
                                    Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/WorkSans-Regular.ttf");
                                    cb.setTypeface(font);
                                    fdh.addView(cb);
                                    checkBoxDetailss.add(checkBoxDetails);
                                    answerIds.add(Integer.valueOf(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionMore().getMultipleChoice().get(ij).getQuestionsOptionsId()));
                                }
                                layout.addView(fdh);
                                multipleChoiceCheckBoxModel.setCheckBoxes(checkBoxDetailss);
                                if (feedQuestUserSides.getSurveyQuestions().get(i).getIsOther() == 1) {
                                    EditText otherOpt = newedittextz("Other");
                                    multipleChoiceCheckBoxModel.setEditText(otherOpt);
                                    layout.addView(otherOpt);
                                }
                                multipleChoiceCheckBoxModel.setOptionIds(answerIds);
                                multipleChoiceCheckBoxModels.add(multipleChoiceCheckBoxModel);


                                if (feedQuestUserSides.getSurveyQuestions().get(i).getIsRequired().equals("1")) {
                                    View view = new View(getActivity());
                                    LinearLayout.LayoutParams viewparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2);
                                    viewparams.setMargins(30, 100, 30, 0);
                                    view.setLayoutParams(viewparams);
                                    view.setBackgroundColor(Color.parseColor("#dedede"));
                                    layout.addView(view);
                                    TextView textView = newtextViewRequired();
                                    textView.setText("Required");
                                    layout.addView(textView);
                                }

                                card.addView(layout);
                                ds.addView(card);
                                pagerAdapter.addView(ds, i);
                                pagerAdapter.notifyDataSetChanged();
                            } else if (feedQuestUserSides.getSurveyQuestions().get(i).getQuestionType().equals("Dropdown")) {
                                CardView card = drawCard(Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionId()));
                                LinearLayout layout = new LinearLayout(getActivity());
                                layout.setOrientation(LinearLayout.VERTICAL);
                                iv = newtextViewQuestion();
                                iv.setText(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionNumber() + ". " + feedQuestUserSides.getSurveyQuestions().get(i).getQuestionTitle() + " ?");
                                layout.addView(iv);
                                if (!feedQuestUserSides.getSurveyQuestions().get(i).getQuestionImage().equals("")) {
                                    Bitmap imageToBind = decodeImage(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionImage());
                                    ImageView nkj = newImageview(getActivity(), imageToBind);
                                    layout.addView(nkj);
                                }

                                LinearLayout.LayoutParams radioparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                radioparams.setMargins(0, 30, 0, 0);
                                LinearLayout fdh = new LinearLayout(getActivity());
                                ArrayList<String> spinnerArray = new ArrayList<String>();
                                List<Integer> answerId = new ArrayList<Integer>();
                                for (int ij = 0; ij < feedQuestUserSides.getSurveyQuestions().get(i).getQuestionMore().getMultipleChoice().size(); ij++) {
                                    spinnerArray.add(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionMore().getMultipleChoice().get(ij).getOptionTitle());
                                    answerId.add(Integer.valueOf(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionMore().getMultipleChoice().get(ij).getQuestionsOptionsId()));

                                }
                                Spinner spinner = new Spinner(getActivity());
                                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, spinnerArray);
                                spinner.setAdapter(spinnerArrayAdapter);
                                DropDownAnswerModel dropDownAnswerModel = new DropDownAnswerModel();
                                dropDownAnswerModel.setQuestionId(Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionId()));
                                dropDownAnswerModel.setQuestionType("Dropdown");
                                dropDownAnswerModel.setSpinneres(spinner);
                                dropDownAnswerModel.setOptionId(answerId);
                                dropDownAnswerModel.setQuestionNumber(Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionNumber()));

                                dropDownAnswerModel.setIsrequired(feedQuestUserSides.getSurveyQuestions().get(i).getIsRequired());
                                dropDownAnswerModels.add(dropDownAnswerModel);
                                layout.addView(spinner);
                                layout.addView(fdh);
                                int jjsdf = feedQuestUserSides.getSurveyQuestions().get(i).getIsOther();
                                EditText otherOpt = newedittextz("Other");
                                if (jjsdf == 1) {
                                    layout.addView(otherOpt);
                                }
                                if (feedQuestUserSides.getSurveyQuestions().get(i).getIsRequired().equals("1")) {
                                    View view = new View(getActivity());
                                    LinearLayout.LayoutParams viewparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2);
                                    viewparams.setMargins(30, 100, 30, 0);
                                    view.setLayoutParams(viewparams);
                                    view.setBackgroundColor(Color.parseColor("#dedede"));
                                    layout.addView(view);
                                    TextView textView = newtextViewRequired();
                                    textView.setText("Required");
                                    layout.addView(textView);
                                }

                                card.addView(layout);
                                ds.addView(card);
                                pagerAdapter.addView(ds, i);
                                pagerAdapter.notifyDataSetChanged();
                            } else if (feedQuestUserSides.getSurveyQuestions().get(i).getQuestionType().equals("Linear scale")) {
                                CardView card = drawCard(Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionId()));
                                LinearLayout layout = new LinearLayout(getActivity());
                                layout.setOrientation(LinearLayout.VERTICAL);
                                iv = newtextViewQuestion();
                                iv.setText(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionNumber() + ". " + feedQuestUserSides.getSurveyQuestions().get(i).getQuestionTitle() + " ?");
                                layout.addView(iv);

                                if (!feedQuestUserSides.getSurveyQuestions().get(i).getQuestionImage().equals("")) {
                                    Bitmap imageToBind = decodeImage(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionImage());
                                    ImageView nkj = newImageview(getActivity(), imageToBind);
                                    layout.addView(nkj);
                                }
                                LinearLayout layoutInner = new LinearLayout(getActivity());
                                layoutInner.setOrientation(LinearLayout.VERTICAL);
                                LinearLayout.LayoutParams Lparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                Lparams.setMargins(0, 0, 10, 0);
                                LinearLayout layoutOutInner = new LinearLayout(getActivity());
                                layoutOutInner.setOrientation(LinearLayout.HORIZONTAL);
                                layoutOutInner.setLayoutParams(Lparams);
                                TextView Upv = newtextViewWrap();
                                Upv.setText(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionMore().getLinear().get(0).getStartText());
                                layoutOutInner.addView(Upv);
                                TextView Lpv = newtextViewWrap();
                                Lpv.setGravity(Gravity.RIGHT);
                                Lpv.setText(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionMore().getLinear().get(0).getEndText());
                                layoutOutInner.addView(Lpv);
                                layoutInner.addView(layoutOutInner);
                                int startValue = Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionMore().getLinear().get(0).getStartValue());
                                int endValue = Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionMore().getLinear().get(0).getEndValue());
                                int questAnswerId = Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionMore().getLinear().get(0).getQlinearId());
                                RadioGroup rg = drawRadioGroupTextTop(startValue, endValue, questAnswerId, feedQuestUserSides.getSurveyQuestions().get(i).getQuestionId(),feedQuestUserSides.getSurveyQuestions().get(i).getIsRequired(),feedQuestUserSides.getSurveyQuestions().get(i).getQuestionNumber());
                                layoutInner.addView(rg);
                                layout.addView(layoutInner);
                                int jjsdf = feedQuestUserSides.getSurveyQuestions().get(i).getIsOther();
                                EditText otherOpt = newedittextz("Other");
                                if (jjsdf == 1) {
                                    layout.addView(otherOpt);
                                }
                                if (feedQuestUserSides.getSurveyQuestions().get(i).getIsRequired().equals("1")) {
                                    View view = new View(getActivity());
                                    LinearLayout.LayoutParams viewparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2);
                                    viewparams.setMargins(30, 100, 30, 0);
                                    view.setLayoutParams(viewparams);
                                    view.setBackgroundColor(Color.parseColor("#dedede"));

                                    layout.addView(view);
                                    TextView textView = newtextViewRequired();
                                    textView.setText("Required");
                                    layout.addView(textView);
                                }

                                card.addView(layout);
                                ds.addView(card);
                                pagerAdapter.addView(ds, i);
                                pagerAdapter.notifyDataSetChanged();
                            } else if (feedQuestUserSides.getSurveyQuestions().get(i).getQuestionType().equals("Multiple choice grid")) {
                                CardView card = drawCard(Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionId()));
                                LinearLayout layout = new LinearLayout(getActivity());
                                layout.setOrientation(LinearLayout.VERTICAL);
                                iv = newtextViewQuestion();
                                iv.setText(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionNumber() + ". " + feedQuestUserSides.getSurveyQuestions().get(i).getQuestionTitle() + " ?");
                                layout.addView(iv);
                                if (!feedQuestUserSides.getSurveyQuestions().get(i).getQuestionImage().equals("")) {
                                    Bitmap imageToBind = decodeImage(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionImage());
                                    ImageView nkj = newImageview(getActivity(), imageToBind);
                                    layout.addView(nkj);
                                }

                                HorizontalScrollView scroll = new HorizontalScrollView(getActivity());
                                scroll.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                        ViewGroup.LayoutParams.WRAP_CONTENT));
                                scroll.setFillViewport(true);
                                scroll.setHorizontalScrollBarEnabled(false);
                                LinearLayout layoutOuter = new LinearLayout(getActivity());
                                layoutOuter.setOrientation(LinearLayout.HORIZONTAL);
                                LinearLayout.LayoutParams LparamsRowName = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                                LparamsRowName.setMargins(10, 40, 0, 0);
                                LinearLayout layoutInner = new LinearLayout(getActivity());
                                layoutInner.setOrientation(LinearLayout.VERTICAL);
                                GridRadioAnswerMultipleModel gridRadioAnswerMultipleModel = new GridRadioAnswerMultipleModel();
                                gridRadioAnswerMultipleModel.setQuestionType("Multiple choice grid");
                                List<RadioGroupDetails> radioGroupDetailslist = new ArrayList<RadioGroupDetails>();
                                for (int o = 0; o < feedQuestUserSides.getSurveyQuestions().get(i).getQuestionMore().getmMultipleChoiceGrid().get(0).getRow().size(); o++) {
                                    List<ColumnDetailsId> columnDetailsIdsList = new ArrayList<ColumnDetailsId>();

                                    RadioGroupDetails radioGroupDetails = new RadioGroupDetails();
                                    RadioGroup rg = drawRadioGroupGrid(o, feedQuestUserSides.getSurveyQuestions().get(i).getQuestionMore().getmMultipleChoiceGrid().get(0).getColumn(), feedQuestUserSides.getSurveyQuestions().get(i).getQuestionMore().getmMultipleChoiceGrid().get(0).getRow().get(o).getRowTitle(), radioGroupDetails, columnDetailsIdsList);
                                    radioGroupDetails.setGroup(rg);
                                    radioGroupDetails.setRowId(Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionMore().getmMultipleChoiceGrid().get(0).getRow().get(o).getQmultipleGridRowId()));
                                    radioGroupDetailslist.add(radioGroupDetails);
                                    layoutInner.addView(rg);
                                    gridRadioAnswerMultipleModel.setRadioGroupDetails(radioGroupDetailslist);
                                    gridRadioAnswerMultipleModel.setQuestionId(Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionId()));
                                    gridRadioAnswerMultipleModel.setColumnDetailsIds(columnDetailsIdsList);
                                }

                                layoutOuter.addView(layoutInner);
                                scroll.addView(layoutOuter);
                                layout.addView(scroll);
                                int jjsdf = feedQuestUserSides.getSurveyQuestions().get(i).getIsOther();
                                EditText otherOpt = newedittextz("Other");
                                if (jjsdf == 1) {
                                    layout.addView(otherOpt);


                                }
                                gridRadioAnswerMultipleModel.setEditText(otherOpt);
                                gridRadioAnswerMultipleModel.setIsrequired(feedQuestUserSides.getSurveyQuestions().get(i).getIsRequired());
                                gridRadioAnswerMultipleModel.setIsother(feedQuestUserSides.getSurveyQuestions().get(i).getIsOther());
                                gridRadioAnswerMultipleModel.setQuestionNumber(Integer.parseInt(feedQuestUserSides.getSurveyQuestions().get(i).getQuestionNumber()));
                                gridRadioAnswerMultipleModels.add(gridRadioAnswerMultipleModel);
                                if (feedQuestUserSides.getSurveyQuestions().get(i).getIsRequired().equals("1")) {
                                    View view = new View(getActivity());
                                    LinearLayout.LayoutParams viewparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2);
                                    viewparams.setMargins(30, 100, 30, 0);
                                    view.setLayoutParams(viewparams);
                                    view.setBackgroundColor(Color.parseColor("#dedede"));

                                    layout.addView(view);
                                    TextView textView = newtextViewRequired();
                                    textView.setText("Required");
                                    layout.addView(textView);
                                }

                                card.addView(layout);
                                ds.addView(card);
                                pagerAdapter.addView(ds, i);
                                pagerAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                }
            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }
        return v;
    }

    private boolean validate() {
        boolean flags = true;

//validate short answer and long answer questiom
        for (int i = 0; i < longAnswerModels.size(); i++) {


            EditText editText = longAnswerModels.get(i).getAnswerEditText();
            String required = longAnswerModels.get(i).getIsrequired();
            String questiontype = longAnswerModels.get(i).getQuestionType();
            int id = longAnswerModels.get(i).getQuestionNumber();
            if (required.equals("1")) {

                String longanswer = editText.getText().toString();
                if (longanswer.equals("")) {
                    pager.setCurrentItem(id-1);
                    Toast.makeText(getActivity(), "Plesase answer " +questiontype +"Question" , Toast.LENGTH_SHORT).show();
                    flags = false;
                    return flags;
                }


            }
        }

        //validate multipple choice radio model

        for (int i = 0; i < multipleChoiceRadioModels.size(); i++) {
            Log.e( "multipleChoiceRadioModels.get(i).isIsother(): ", ""+multipleChoiceRadioModels.get(i).isIsother());
            if (multipleChoiceRadioModels.get(i).isIsother()==1) {
                EditText editText = multipleChoiceRadioModels.get(i).getEditText();
                RadioGroup radioGroup = multipleChoiceRadioModels.get(i).getRadioGroup();

                Log.e( "radioGroup.isSelected(): ", ""+radioGroup.isSelected());
                String required = multipleChoiceRadioModels.get(i).getIsrequired();
                int id = multipleChoiceRadioModels.get(i).getQuestionNumber();
                if (required.equals("1")) {

                    String longanswer = editText.getText().toString();
                    Log.e( "longanswer: ", ""+longanswer);

                    if (longanswer.equals("") && radioGroup.getCheckedRadioButtonId() == -1) {
                        pager.setCurrentItem(id - 1);

                        Toast.makeText(getActivity(), "Plesase Answer Multiple Choice Question", Toast.LENGTH_SHORT).show();
                        flags = false;
                        return flags;
                    }


                }
            }
            else
            {
                RadioGroup radioGroup = multipleChoiceRadioModels.get(i).getRadioGroup();
                String required = multipleChoiceRadioModels.get(i).getIsrequired();
                int id = multipleChoiceRadioModels.get(i).getQuestionNumber();
                if (required.equals("1")) {
                    if (radioGroup.getCheckedRadioButtonId() == -1) {
                        pager.setCurrentItem(id - 1);

                        Toast.makeText(getActivity(), "Plesase Answer Multiple Choice Question", Toast.LENGTH_SHORT).show();
                        flags = false;
                        return flags;
                    }
                }
            }


        }


        for (int i = 0; i < multipleChoiceCheckBoxModels.size(); i++) {
            if (multipleChoiceCheckBoxModels.get(i).isIsother()==1) {
                EditText editText = multipleChoiceCheckBoxModels.get(i).getEditText();
                String required = multipleChoiceCheckBoxModels.get(i).getIsrequired();
                String questiontype = multipleChoiceCheckBoxModels.get(i).getQuestionType();
                int id = multipleChoiceCheckBoxModels.get(i).getQuestionNumber();
                List<CheckBoxDetails> checkBoxes = multipleChoiceCheckBoxModels.get(i).getCheckBoxes();
                if (required.equals("1")) {
                    String longanswer = editText.getText().toString();
                    boolean ischecked = false;
                    for (int a = 0; a < checkBoxes.size(); a++) {
                        if (checkBoxes.get(a).getCheckBoxes().isChecked()) {
                            ischecked = true;
                        }
                    }
                    if (longanswer.equals("") && !ischecked) {
                        pager.setCurrentItem(id - 1);
                        Toast.makeText(getActivity(), "Plesase answer " + questiontype + "Question", Toast.LENGTH_SHORT).show();
                        flags = false;
                        return flags;
                    }
                }
            }
            else{
                String required = multipleChoiceCheckBoxModels.get(i).getIsrequired();
                String questiontype = multipleChoiceCheckBoxModels.get(i).getQuestionType();
                int id = multipleChoiceCheckBoxModels.get(i).getQuestionNumber();
                List<CheckBoxDetails> checkBoxes = multipleChoiceCheckBoxModels.get(i).getCheckBoxes();
                if (required.equals("1")) {



                    boolean ischecked = false;
                    for (int a = 0; a < checkBoxes.size(); a++) {
                        if (checkBoxes.get(a).getCheckBoxes().isChecked()) {
                            ischecked = true;
                        }

                    }

                    if (!ischecked) {
                        pager.setCurrentItem(id - 1);
                        Toast.makeText(getActivity(), "Plesase answer " + questiontype + "Question", Toast.LENGTH_SHORT).show();
                        flags = false;
                        return flags;
                    }


                }

            }


        }


        for (int i = 0; i < dropDownAnswerModels.size(); i++) {
            Spinner editText = dropDownAnswerModels.get(i).getSpinneres();
            String required = dropDownAnswerModels.get(i).getIsrequired();

            String questiontype = dropDownAnswerModels.get(i).getQuestionType();
            Log.e( "dropDownAnswerModels: ",""+questiontype );
            Log.e( "required: ",""+required );
            int id = dropDownAnswerModels.get(i).getQuestionNumber();
            if (required.equals("1")) {



                String longanswer = editText.getSelectedItem().toString();

                Log.e( "Selected item : ", ""+longanswer);
                if (longanswer.equals("")) {
                    pager.setCurrentItem(id-1);
                    Toast.makeText(getActivity(), "Plesase answer " +questiontype +"Question" , Toast.LENGTH_SHORT).show();
                    flags = false;
                    return flags;
                }


            }


        }


        for (int i = 0; i < linearAnswerModellss.size(); i++) {
            RadioGroup radioGroup = linearAnswerModellss.get(i).getRadioGroup();
            String required = linearAnswerModellss.get(i).getIsrequired();

            String questiontype = linearAnswerModellss.get(i).getQuestionType();
            int id = linearAnswerModellss.get(i).getQuestionNumber();
            if (required.equals("1")) {
                if (radioGroup.getCheckedRadioButtonId() == -1) {
                    pager.setCurrentItem(id-1);
                    Toast.makeText(getActivity(), "Plesase answer " +questiontype +"Question" , Toast.LENGTH_SHORT).show();
                    flags = false;
                    return flags;
                }


            }


        }
        for (int i = 0; i < gridRadioAnswerMultipleModels.size(); i++) {



            if (gridRadioAnswerMultipleModels.get(i).isIsother()==1) {
                EditText editText = gridRadioAnswerMultipleModels.get(i).getEditText();
                List<RadioGroupDetails> radioGroup = gridRadioAnswerMultipleModels.get(i).getRadioGroupDetails();
                String required = gridRadioAnswerMultipleModels.get(i).getIsrequired();

                String questiontype = gridRadioAnswerMultipleModels.get(i).getQuestionType();
                int id = gridRadioAnswerMultipleModels.get(i).getQuestionNumber();
                Log.e( "validate: ", "+id"+id);
                if (required.equals("1")) {

                    boolean checkflag = true;
                    for (int b = 0; b < radioGroup.size(); b++) {
                        if (radioGroup.get(b).getGroup().getCheckedRadioButtonId() == -1) {
                            checkflag = false;
                        }
                    }




                    String longanswer = editText.getText().toString();
                    Log.e( "longanswer: ", ""+longanswer);

                    if (longanswer.equals("") && !checkflag) {
                        pager.setCurrentItem(id - 1);

                        Toast.makeText(getActivity(), "Plesase Answer Multiple Choice Question", Toast.LENGTH_SHORT).show();
                        flags = false;
                        return flags;
                    }



                }
            }
            else
            {
                List<RadioGroupDetails> radioGroup = gridRadioAnswerMultipleModels.get(i).getRadioGroupDetails();

                String required = gridRadioAnswerMultipleModels.get(i).getIsrequired();

                String questiontype = gridRadioAnswerMultipleModels.get(i).getQuestionType();
                int id = gridRadioAnswerMultipleModels.get(i).getQuestionNumber();
                if (required.equals("1")) {

                    boolean checkflag = true;
                    for (int b = 0; b < radioGroup.size(); b++) {
                        if (radioGroup.get(b).getGroup().getCheckedRadioButtonId() == -1) {
                            checkflag = false;
                        }
                    }

                    if (!checkflag) {

                        pager.setCurrentItem(id - 1);
                        Toast.makeText(getActivity(), "Plesase answer " + questiontype + "Question", Toast.LENGTH_SHORT).show();
                        flags = false;
                        return flags;

                    }


                }

            }


        }





        return flags;


    }

    private RadioGroup drawRadioGroupTextTop(int startValue, int endValue, int questAnswerId, String questionId, String isRequired, String questionNumber) {
        int limit = 0;
        if (startValue == 0) {
            limit = endValue + 1;
        }
        if (startValue == 1) {
            limit = endValue;
        }
        LinearLayout.LayoutParams radioparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        radioparams.setMargins(0, 30, 0, 0);
        final RadioButton[] rb = new RadioButton[limit];
        RadioGroup rg = new RadioGroup(getActivity()); //create the RadioGroup
        rg.setGravity(Gravity.CENTER);
        rg.setOrientation(RadioGroup.HORIZONTAL);//or RadioGroup.VERTICAL
        rg.setLayoutParams(radioparams);
        LinearAnswerModells linearAnswerModells = new LinearAnswerModells();
        linearAnswerModells.setQuestionId(Integer.parseInt(questionId));
        linearAnswerModells.setAnswerId(questAnswerId);
        linearAnswerModells.setQuestionType("Linear scale");
        linearAnswerModells.setRadioGroup(rg);
        linearAnswerModells.setIsrequired(isRequired);
        linearAnswerModells.setQuestionNumber(Integer.parseInt(questionNumber));
        linearAnswerModellss.add(linearAnswerModells);
        int i = 0;
        while (startValue <= endValue) {
            rb[i] = new RadioButton(getActivity());
            rb[i].setText("" + startValue);
            rb[i].setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
            rb[i].setButtonDrawable(null);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/WorkSans-Regular.ttf");
            rb[i].setTypeface(font);
            TypedArray a = getActivity().getTheme().obtainStyledAttributes(R.style.AppTheme, new int[]{android.R.attr.listChoiceIndicatorSingle});
            int attributeResourceId = a.getResourceId(0, 0);
            Drawable drawable = ContextCompat.getDrawable(getActivity(), attributeResourceId);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorView));
            rb[i].setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable);
            rb[i].setGravity(Gravity.CENTER | Gravity.BOTTOM);
            rg.addView(rb[i]);
            i++;
            startValue++;
        }
        return rg;
    }

    private TextView newtextViewWrap() {
        final TextViewRegular textView = new TextViewRegular(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
        params.setMargins(10, 30, 0, 0);
        textView.setLayoutParams(params);
        textView.setTextSize(14);
        textView.setTextColor(Color.rgb(0, 0, 0));
        return textView;
    }

    private TextView newtextViewWrapGrid(String j) {
        final TextViewRegular textView = new TextViewRegular(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(10, 30, 0, 0);
        textView.setLayoutParams(params);
        textView.setTextSize(14);
        textView.setText(j);
        textView.setTextColor(Color.rgb(0, 0, 0));
        return textView;
    }

    private RadioGroup drawRadioGroupGrid(int o, RealmList<Column> columnDetails, String row_name, RadioGroupDetails radioGroupDetails, List<ColumnDetailsId> columnDetailsIdsList) {
        final RadioButton[] rb = new RadioButton[columnDetails.size()];
        RadioGroup rg = new RadioGroup(getActivity()); //create the RadioGroup
        rg.setGravity(Gravity.CENTER);
        TextView txt = newtextViewWrapGrid(row_name);
        radioGroupDetails.setTextViews(row_name);
        LinearLayout.LayoutParams ledt = new LinearLayout.LayoutParams(250, LinearLayout.LayoutParams.WRAP_CONTENT);
        txt.setGravity(Gravity.CENTER | Gravity.BOTTOM);
        txt.setLayoutParams(ledt);
        txt.setTextSize(12);
        rg.addView(txt);
        rg.setOrientation(RadioGroup.HORIZONTAL);//or RadioGroup.VERTICAL
        LinearLayout.LayoutParams LparamsRowNamee = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        LparamsRowNamee.setMargins(0, 0, 0, 0);
        for (int ii = 0; ii < columnDetails.size(); ii++) {
            ColumnDetailsId columnDetailsId = new ColumnDetailsId();
            columnDetailsId.setColIds(Integer.parseInt(columnDetails.get(ii).getQmultipleGridColId()));
            rb[ii] = new RadioButton(getActivity());
            rb[ii].setText(columnDetails.get(ii).getColTitle());
            if (o == 0) {
                rb[ii].setTextColor(getResources().getColor(R.color.colorTextBlack));
            } else {
                rb[ii].setTextColor(getResources().getColor(R.color.colorWhite));
            }
            rb[ii].setLayoutParams(LparamsRowNamee);
            rb[ii].setTextSize(12);
            rb[ii].setGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP);
            rb[ii].setId(ii + 100);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/WorkSans-Regular.ttf");
            rb[ii].setTypeface(font);
            rb[ii].setPadding(20, 0, 20, 0);
            rb[ii].setButtonDrawable(null);
            TypedArray a = getActivity().getTheme().obtainStyledAttributes(R.style.AppTheme, new int[]{android.R.attr.listChoiceIndicatorSingle});
            int attributeResourceId = a.getResourceId(0, 0);
//            Drawable drawable = ContextCompat.getDrawable(getActivity(), attributeResourceId);
            Drawable drawable = ContextCompat.getDrawable(getActivity(), attributeResourceId);
            DrawableCompat.setTint(drawable, getResources().getColor(R.color.colorView));
            rb[ii].setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable);
            rb[ii].setGravity(Gravity.CENTER | Gravity.BOTTOM);
            rg.addView(rb[ii]);
            columnDetailsIdsList.add(columnDetailsId);
        }
        return rg;
    }

    private RadioGroup createRadioGroup(int size, RealmList<MultipleChoice> multiplechoicesingleDetails, String questId, int isOther, LinearLayout layout, String questionNumber, String isRequired, int other) {


        Log.e( "is other: ", ""+other);
        LinearLayout.LayoutParams radioparams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        radioparams.setMargins(0, 30, 0, 0);
        RadioGroup rg = new RadioGroup(getActivity()); //create the RadioGroup
        final RadioButton[] rb = new RadioButton[size];
        rg.setOrientation(RadioGroup.VERTICAL);//or RadioGroup.VERTICAL
        rg.setLayoutParams(radioparams);
        MultipleChoiceRadioModel multipleChoiceRadioModel = new MultipleChoiceRadioModel();
        multipleChoiceRadioModel.setQuestionType("Multiple choice");
        List<Integer> dcv = new ArrayList<>();
        for (int ii = 0; ii < size; ii++) {
            rb[ii] = new RadioButton(getActivity());
            rb[ii].setText(multiplechoicesingleDetails.get(ii).getOptionTitle());
            rb[ii].setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER);

            rb[ii].setId(ii + 100);
            Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/WorkSans-Regular.ttf");
            rb[ii].setTypeface(font);
            rg.addView(rb[ii]);
            dcv.add(Integer.valueOf(multiplechoicesingleDetails.get(ii).getQuestionsOptionsId()));
        }

        layout.addView(rg);


        if (isOther == 1) {
            EditText otherOpt = newedittextz("Other");
            multipleChoiceRadioModel.setEditText(otherOpt);
            layout.addView(otherOpt);
        }
        multipleChoiceRadioModel.setQuestionId(dcv);
        multipleChoiceRadioModel.setQuestId(Integer.parseInt(questId));
        multipleChoiceRadioModel.setRadioGroup(rg);
        multipleChoiceRadioModel.setQuestionNumber(Integer.parseInt(questionNumber));
        multipleChoiceRadioModel.setIsrequired(isRequired);
        multipleChoiceRadioModel.setIsother(isOther);
        multipleChoiceRadioModels.add(multipleChoiceRadioModel);
        return rg;
    }

    private TextView newtextViewQuestion() {
        final TextViewSemiBold textView = new TextViewSemiBold(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(10, 100, 0, 30);
        textView.setLayoutParams(params);
        textView.setTextSize(17);
        textView.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
        textView.setMaxEms(2);
        return textView;
    }

    private TextView newtextViewQuestionNew() {
        final TextViewSemiBold textView = new TextViewSemiBold(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(10, 10, 0, 10);
        textView.setLayoutParams(params);
        textView.setTextSize(17);
        textView.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
        textView.setVisibility(View.VISIBLE);
        textView.setMaxEms(2);
        return textView;
    }

    private TextView newtextViewRequired() {
        final TextViewRegular textView = new TextViewRegular(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0, 30, 0, 20);
        textView.setLayoutParams(params);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(12);
        textView.setTextColor(getActivity().getResources().getColor(R.color.colorTextBlack));

        return textView;
    }

    public ImageView newImageview(Context context, Bitmap imageToBind) {
        final LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 250);
        params.setMargins(10, 20, 10, 0);
        final ImageView imgView = new ImageView(context);
        imgView.setLayoutParams(params);
        imgView.setImageBitmap(imageToBind);
        imgView.setScaleType(ImageView.ScaleType.FIT_XY);
        return imgView;
    }


    private EditText newedittextz(String j) {
        final EditTextRegular editText = new EditTextRegular(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(10, 30, 0, 10);
        editText.setLayoutParams(params);
        editText.setHint(j);
        editText.setTextSize(14);
        editText.setTextColor(Color.rgb(0, 0, 0));
        return editText;
    }

    private CardView drawCard(int id) {
        CardView card = new CardView(getActivity());
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(30, 30, 30, 30);
        card.setLayoutParams(params);
        card.setRadius(9);
        card.setContentPadding(15, 15, 15, 45);
        card.setCardBackgroundColor(getResources().getColor(R.color.colorWhite));
        card.setMaxCardElevation(10);
        card.setCardElevation(5);
        card.setId(id);
        return card;
    }

    private void initViews(View v) {
        tvSkip = (TextView) v.findViewById(R.id.tvSkip);
        tvPrev = (TextView) v.findViewById(R.id.tvPrev);
        tvFinish = (TextView) v.findViewById(R.id.tvFinish);
    }
}
