package com.intellyze.feedback.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.intellyze.feedback.Bus.BusFactory;
import com.intellyze.feedback.Bus.Events.NavigateSurveyUserDetailsPageFragmentEvent;
import com.intellyze.feedback.R;
import com.intellyze.feedback.Realm.AddAnswerMaster;
import com.intellyze.feedback.Utils.UserPref;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import io.realm.Realm;

/**
 * Created by INTELLYZE-202 on 20-12-2017.
 */

public class SurveyUserDetailsFillPageFragment extends BaseFragment {

    EditText edtName, edtMobileNo, edtProfession, edtAge, edtEmail;
    RadioGroup myRadioGroup;
    LinearLayout lnrStartQuestions;
    String ds;
    public static final String ARG_ID = "ARG_ID";
    private int mId;
    AddAnswerMaster AddAnswerMaster;
    Realm mRealm;
    UserPref userPref;

    public SurveyUserDetailsFillPageFragment() {
        // Required empty public constructor
    }

    public static SurveyUserDetailsFillPageFragment newInstance(int id) {
        SurveyUserDetailsFillPageFragment fragment = new SurveyUserDetailsFillPageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mId = getArguments().getInt(ARG_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_survey_user_fill_fragment, container, false);
        mRealm = Realm.getDefaultInstance();
        initViews(v);
//        setData();
        userPref = new UserPref(getActivity());
        myRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) v.findViewById(checkedId);
                ds = rb.getText().toString();
            }
        });
        lnrStartQuestions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edtEmail.getText().toString().trim().equals("") || edtAge.getText().toString().trim().equals("") || edtProfession.getText().toString().trim().equals("") || edtName.getText().toString().trim().equals("") || edtMobileNo.getText().toString().trim().equals("")) {
                    if (edtEmail.getText().toString().trim().equals(""))
                        edtEmail.setError(getActivity().getResources().getString(R.string.cant_be_blank));

                    if (edtAge.getText().toString().trim().equals(""))
                        edtAge.setError(getActivity().getResources().getString(R.string.cant_be_blank));

                    if (edtProfession.getText().toString().trim().equals(""))
                        edtProfession.setError(getActivity().getResources().getString(R.string.cant_be_blank));

                    if (edtName.getText().toString().trim().equals(""))
                        edtName.setError(getActivity().getResources().getString(R.string.cant_be_blank));


                    if (edtMobileNo.getText().toString().trim().equals(""))
                        edtMobileNo.setError(getActivity().getResources().getString(R.string.cant_be_blank));

                } else {
                    if (isValidEmail(edtEmail.getText().toString().trim()) && edtMobileNo.getText().toString().trim().length() >= 10) {
                        saveSurveyDataTo();
                    } else {
                        if (!isValidEmail(edtEmail.getText().toString().trim()))
                            edtEmail.setError(getActivity().getResources().getString(R.string.invalide_Entry));

                        if (edtMobileNo.getText().toString().trim().length() < 10)
                            edtMobileNo.setError(getActivity().getResources().getString(R.string.invalide_Entry));
                    }
                }
            }
        });
        return v;
    }

    private void setData()
    {
        edtName.setText("sanu feliz");
        edtMobileNo.setText("8606276916");
        edtProfession.setText("Developer");
        edtEmail.setText("sanu@gmail.com");
        edtAge.setText("25");
    }

    private void saveSurveyDataTo() {
        mRealm = Realm.getDefaultInstance();
        AddAnswerMaster = mRealm.where(AddAnswerMaster.class).equalTo("mSurveyUserId", 0).findFirst();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (AddAnswerMaster != null) {
                        Toast.makeText(getActivity(), "" + "Survey Name already exist! please try another name", Toast.LENGTH_SHORT).show();
                    } else {
                        Number maxId = mRealm.where(AddAnswerMaster.class).max("mSurveyUserId");
                        int nextId = (maxId == null) ? 1 : maxId.intValue() + 1;
                        userPref.saveSurveyAnswerId(nextId);
                        AddAnswerMaster AddAnswerMaster = mRealm.createObject(AddAnswerMaster.class, nextId);
                        AddAnswerMaster.setAge(edtAge.getText().toString().trim());
                        AddAnswerMaster.setmSurveyId(mId);
                        AddAnswerMaster.setmSurveySync(0);
                        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");//foramt date
                        String date = df1.format(Calendar.getInstance().getTime());
                        DateFormat df = new SimpleDateFormat("HH:mm"); //format time
                        String time = df.format(Calendar.getInstance().getTime());
                        AddAnswerMaster.setAnswerDate(date);
                        AddAnswerMaster.setEmail(edtEmail.getText().toString().trim());
                        AddAnswerMaster.setMobileNo(edtMobileNo.getText().toString().trim());
                        AddAnswerMaster.setGender(ds);
                        AddAnswerMaster.setAnswerTime(time);
                        AddAnswerMaster.setFullName(edtName.getText().toString().trim());
                        AddAnswerMaster.setProfession(edtProfession.getText().toString().trim());
                        mRealm.copyToRealm(AddAnswerMaster);
                        View view = getActivity().getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }
                        BusFactory.getBus().post(new NavigateSurveyUserDetailsPageFragmentEvent(mId));
                    }
                }
            });
        } catch (Exception em) {
            em.printStackTrace();
        } finally {
            if (mRealm != null)
                mRealm.close();
        }
    }

    private void initViews(View v) {
        edtName = (EditText) v.findViewById(R.id.edtName);
        edtMobileNo = (EditText) v.findViewById(R.id.edtMobileNo);
        edtProfession = (EditText) v.findViewById(R.id.edtProfession);
        edtEmail = (EditText) v.findViewById(R.id.edtEmail);
        edtAge = (EditText) v.findViewById(R.id.edtAge);
        myRadioGroup = (RadioGroup) v.findViewById(R.id.myRadioGroup);
        lnrStartQuestions = (LinearLayout) v.findViewById(R.id.lnrStartQuestions);





    }



}
