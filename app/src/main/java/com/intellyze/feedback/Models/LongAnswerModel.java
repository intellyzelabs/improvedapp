package com.intellyze.feedback.Models;

import android.widget.EditText;

/**
 * Created by INTELLYZE-202 on 21-12-2017.
 */

public class LongAnswerModel {
    String questionType;
    int QuestionId;
    int QuestionNumber;

    public int getQuestionNumber() {
        return QuestionNumber;
    }

    public void setQuestionNumber(int questionNumber) {
        QuestionNumber = questionNumber;
    }

    EditText answerEditText;
    String  isrequired;
    public String getIsrequired() {
        return isrequired;
    }

    public void setIsrequired(String isrequired) {
        this.isrequired = isrequired;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public int getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(int questionId) {
        QuestionId = questionId;
    }

    public EditText getAnswerEditText() {
        return answerEditText;
    }

    public void setAnswerEditText(EditText answerEditText) {
        this.answerEditText = answerEditText;
    }
}
