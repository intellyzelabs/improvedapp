package com.intellyze.feedback.Models;

import android.widget.EditText;
import android.widget.RadioGroup;

import java.util.List;

/**
 * Created by INTELLYZE-202 on 22-12-2017.
 */

public class GridRadioAnswerMultipleModel {
    String questionType;
    int QuestionId;
    List<ColumnDetailsId> columnDetailsIds;
    List<RadioGroupDetails> radioGroupDetails;
    EditText editText;
    String  isrequired;
    int QuestionNumber;
    int isother;

    public int isIsother() {
        return isother;
    }

    public void setIsother(int isother) {
        this.isother = isother;
    }


    public int getQuestionNumber() {
        return QuestionNumber;
    }

    public void setQuestionNumber(int questionNumber) {
        QuestionNumber = questionNumber;
    }

    public String getIsrequired() {
        return isrequired;
    }

    public void setIsrequired(String isrequired) {
        this.isrequired = isrequired;
    }

    public EditText getEditText() {
        return editText;
    }

    public void setEditText(EditText editText) {
        this.editText = editText;
    }

    public int getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(int questionId) {
        QuestionId = questionId;
    }

    public List<ColumnDetailsId> getColumnDetailsIds() {
        return columnDetailsIds;
    }

    public void setColumnDetailsIds(List<ColumnDetailsId> columnDetailsIds) {
        this.columnDetailsIds = columnDetailsIds;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }


    public List<RadioGroupDetails> getRadioGroupDetails() {
        return radioGroupDetails;
    }

    public void setRadioGroupDetails(List<RadioGroupDetails> radioGroupDetails) {
        this.radioGroupDetails = radioGroupDetails;
    }
}
