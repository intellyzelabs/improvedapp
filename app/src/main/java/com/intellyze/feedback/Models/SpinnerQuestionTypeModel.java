package com.intellyze.feedback.Models;

/**
 * Created by INTELLYZE-202 on 01-12-2017.
 */

public class SpinnerQuestionTypeModel {
    private String typeName;
    private int typeIcon;

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public int getTypeIcon() {
        return typeIcon;
    }

    public void setTypeIcon(int typeIcon) {
        this.typeIcon = typeIcon;
    }

    public SpinnerQuestionTypeModel(String typeName, int typeIcon) {
        this.typeName = typeName;
        this.typeIcon = typeIcon;
    }

}
