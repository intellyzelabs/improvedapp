package com.intellyze.feedback.Models;

import io.realm.RealmObject;

/**
 * Created by intellyelabs on 21/02/18.
 */

public class SurveyAnswerListForRecyclerView  {

    private String mAge;
    private String mAnswerDate;
    private String mAnswerId;
    private String mAnswerTime;
    private String mEmail;
    private String mFullName;
    private String mGender;
    private String mMobileNo;
    private String mProfession;
    private String mSurveyId;
    private String mSurveyTitle;

    public String getAge() {
        return mAge;
    }

    public void setAge(String age) {
        mAge = age;
    }

    public String getAnswerDate() {
        return mAnswerDate;
    }

    public void setAnswerDate(String answerDate) {
        mAnswerDate = answerDate;
    }

    public String getAnswerId() {
        return mAnswerId;
    }

    public void setAnswerId(String AnswerId) {
        mAnswerId = AnswerId;
    }

    public String getAnswerTime() {
        return mAnswerTime;
    }

    public void setAnswerTime(String answerTime) {
        mAnswerTime = answerTime;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String FullName) {
        mFullName = FullName;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mobileNo) {
        mMobileNo = mobileNo;
    }

    public String getProfession() {
        return mProfession;
    }

    public void setProfession(String profession) {
        mProfession = profession;
    }

    public String getSurveyId() {
        return mSurveyId;
    }

    public void setSurveyId(String surveyId) {
        mSurveyId = surveyId;
    }

    public String getSurveyTitle() {
        return mSurveyTitle;
    }

    public void setSurveyTitle(String surveyTitle) {
        mSurveyTitle = surveyTitle;
    }



}
