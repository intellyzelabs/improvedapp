package com.intellyze.feedback.Models;

/**
 * Created by INTELLYZE-202 on 13-12-2017.
 */

public class MultipleChoicesingleDetailsModel {
    private String textChoice;
    private int textChoice_id;

    public int getTextChoice_id() {
        return textChoice_id;
    }

    public void setTextChoice_id(int textChoice_id) {
        this.textChoice_id = textChoice_id;
    }

    public String getTextChoice() {
        return textChoice;
    }

    public void setTextChoice(String textChoice) {
        this.textChoice = textChoice;
    }
}
