package com.intellyze.feedback.Models;

/**
 * Created by INTELLYZE-202 on 13-12-2017.
 */

public class ColumnDetailsModel {
    private String column_name;

    public String getColumn_name() {
        return column_name;
    }

    public void setColumn_name(String column_name) {
        this.column_name = column_name;
    }
}
