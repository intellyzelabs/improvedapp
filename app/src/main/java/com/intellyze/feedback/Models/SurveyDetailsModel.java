package com.intellyze.feedback.Models;

/**
 * Created by INTELLYZE-202 on 15-12-2017.
 */

public class SurveyDetailsModel {
    private String SurveyId;
    private String SurveyName;
    private String SurveySync;
    private String SurveyDate;
    private String SurveyTime;
    private String SurveyStatus;
    private String SurveyDescription;
    private String LastEntryDate;
    private String LastEntryTime;

    public String getLastEntryDate() {
        return LastEntryDate;
    }

    public void setLastEntryDate(String lastEntryDate) {
        LastEntryDate = lastEntryDate;
    }

    public String getLastEntryTime() {
        return LastEntryTime;
    }

    public void setLastEntryTime(String lastEntryTime) {
        LastEntryTime = lastEntryTime;
    }

    private String SurveyImage;
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    private int SurveyCompleted;

    public int getSurveyCompleted() {
        return SurveyCompleted;
    }

    public void setSurveyCompleted(int surveyCompleted) {
        SurveyCompleted = surveyCompleted;
    }

    public String getSurveyId() {
        return SurveyId;
    }

    public void setSurveyId(String surveyId) {
        SurveyId = surveyId;
    }

    public String getSurveyName() {
        return SurveyName;
    }

    public void setSurveyName(String surveyName) {
        SurveyName = surveyName;
    }

    public String getSurveySync() {
        return SurveySync;
    }

    public void setSurveySync(String surveySync) {
        SurveySync = surveySync;
    }

    public String getSurveyDate() {
        return SurveyDate;
    }

    public void setSurveyDate(String surveyDate) {
        SurveyDate = surveyDate;
    }

    public String getSurveyTime() {
        return SurveyTime;
    }

    public void setSurveyTime(String surveyTime) {
        SurveyTime = surveyTime;
    }

    public String getSurveyStatus() {
        return SurveyStatus;
    }

    public void setSurveyStatus(String surveyStatus) {
        SurveyStatus = surveyStatus;
    }

    public String getSurveyDescription() {
        return SurveyDescription;
    }

    public void setSurveyDescription(String surveyDescription) {
        SurveyDescription = surveyDescription;
    }

    public String getSurveyImage() {
        return SurveyImage;
    }

    public void setSurveyImage(String surveyImage) {
        SurveyImage = surveyImage;
    }
}
