package com.intellyze.feedback.Models;

import android.widget.RadioGroup;
import android.widget.TextView;

/**
 * Created by INTELLYZE-202 on 22-12-2017.
 */

public class RadioGroupDetails {
    RadioGroup group;
    String textViews;
    int rowId;

    public int getRowId() {
        return rowId;
    }

    public void setRowId(int rowId) {
        this.rowId = rowId;
    }

    public String getTextViews() {
        return textViews;
    }

    public void setTextViews(String textViews) {
        this.textViews = textViews;
    }

    public RadioGroup getGroup() {
        return group;
    }

    public void setGroup(RadioGroup group) {
        this.group = group;
    }
}
