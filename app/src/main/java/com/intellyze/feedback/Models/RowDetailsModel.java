package com.intellyze.feedback.Models;

/**
 * Created by INTELLYZE-202 on 13-12-2017.
 */

public class RowDetailsModel {
    private String row_name;

    public String getRow_name() {
        return row_name;
    }

    public void setRow_name(String row_name) {
        this.row_name = row_name;
    }
}
