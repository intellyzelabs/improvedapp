package com.intellyze.feedback.Models;

/**
 * Created by INTELLYZE-202 on 13-12-2017.
 */

public class LinearQuestionDetalisModel {

    private String startText;
    private String endText;
    private int startValue;

    private int endValue;

    public int getEndValue() {
        return endValue;
    }

    public void setEndValue(int endValue) {
        this.endValue = endValue;
    }

    public String getStartText() {
        return startText;
    }

    public void setStartText(String startText) {
        this.startText = startText;
    }

    public String getEndText() {
        return endText;
    }

    public void setEndText(String endText) {
        this.endText = endText;
    }

    public int getStartValue() {
        return startValue;
    }

    public void setStartValue(int startValue) {
        this.startValue = startValue;
    }





}
