package com.intellyze.feedback.Models;

import android.widget.EditText;

import com.intellyze.feedback.Models.CheckBoxDetails;

import java.util.List;

/**
 * Created by INTELLYZE-202 on 21-12-2017.
 */

public class MultipleChoiceCheckBoxModel {
    String questionType;
    int isother;

    public int isIsother() {
        return isother;
    }

    public void setIsother(int isother) {
        this.isother = isother;
    }

    int QuestionId;
    List<CheckBoxDetails> checkBoxes;
    List<Integer> optionIds;
    EditText editText;
    String  isrequired;
    int QuestionNumber;

    public int getQuestionNumber() {
        return QuestionNumber;
    }

    public void setQuestionNumber(int questionNumber) {
        QuestionNumber = questionNumber;
    }
    public String getIsrequired() {
        return isrequired;
    }

    public void setIsrequired(String isrequired) {
        this.isrequired = isrequired;
    }


    public EditText getEditText() {
        return editText;
    }

    public void setEditText(EditText editText) {
        this.editText = editText;
    }

    public List<Integer> getOptionIds() {
        return optionIds;
    }

    public void setOptionIds(List<Integer> optionIds) {
        this.optionIds = optionIds;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public int getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(int questionId) {
        QuestionId = questionId;
    }

    public List<CheckBoxDetails> getCheckBoxes() {
        return checkBoxes;
    }

    public void setCheckBoxes(List<CheckBoxDetails> checkBoxes) {
        this.checkBoxes = checkBoxes;
    }
}
