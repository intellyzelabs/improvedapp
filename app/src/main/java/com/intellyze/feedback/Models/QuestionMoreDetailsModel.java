package com.intellyze.feedback.Models;


import java.util.List;

/**
 * Created by INTELLYZE-202 on 13-12-2017.
 */

public class QuestionMoreDetailsModel {
    private String question;
    private String questionType;
    private String choiceType;
    private List<MultipleChoicesingleDetailsModel> multiplechoicesingleDetails;
    private List<LinearQuestionDetalisModel> linearquestionDetalis;
    private List<MatrixQuestionDetailModel> matrixquestionDetails;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public String getChoiceType() {
        return choiceType;
    }

    public void setChoiceType(String choiceType) {
        this.choiceType = choiceType;
    }

    public List<MultipleChoicesingleDetailsModel> getMultiplechoicesingleDetails() {
        return multiplechoicesingleDetails;
    }

    public void setMultiplechoicesingleDetails(List<MultipleChoicesingleDetailsModel> multiplechoicesingleDetails) {
        this.multiplechoicesingleDetails = multiplechoicesingleDetails;
    }

    public List<LinearQuestionDetalisModel> getLinearquestionDetalis() {
        return linearquestionDetalis;
    }

    public void setLinearquestionDetalis(List<LinearQuestionDetalisModel> linearquestionDetalis) {
        this.linearquestionDetalis = linearquestionDetalis;
    }

    public List<MatrixQuestionDetailModel> getMatrixquestionDetails() {
        return matrixquestionDetails;
    }

    public void setMatrixquestionDetails(List<MatrixQuestionDetailModel> matrixquestionDetails) {
        this.matrixquestionDetails = matrixquestionDetails;
    }


}
