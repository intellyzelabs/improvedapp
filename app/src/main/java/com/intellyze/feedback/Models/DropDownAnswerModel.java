package com.intellyze.feedback.Models;

import android.widget.Spinner;

import java.util.List;

/**
 * Created by INTELLYZE-202 on 22-12-2017.
 */

public class DropDownAnswerModel {
    String questionType;
    int QuestionId;
    Spinner spinneres;
    List<Integer> optionId;

    String  isrequired;
    int QuestionNumber;

    public int getQuestionNumber() {
        return QuestionNumber;
    }

    public void setQuestionNumber(int questionNumber) {
        QuestionNumber = questionNumber;
    }
    public String getIsrequired() {
        return isrequired;
    }

    public void setIsrequired(String isrequired) {
        this.isrequired = isrequired;
    }



    public List<Integer> getOptionId() {
        return optionId;
    }

    public void setOptionId(List<Integer> optionId) {
        this.optionId = optionId;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public int getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(int questionId) {
        QuestionId = questionId;
    }

    public Spinner getSpinneres() {
        return spinneres;
    }

    public void setSpinneres(Spinner spinneres) {
        this.spinneres = spinneres;
    }
}
