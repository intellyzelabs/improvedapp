package com.intellyze.feedback.Models;

import android.widget.EditText;
import android.widget.RadioGroup;

import java.util.List;

/**
 * Created by INTELLYZE-202 on 21-12-2017.
 */

public class MultipleChoiceRadioModel {
    String questionType;
    List<Integer> QuestionId;
    RadioGroup radioGroup;
    EditText editText;
    String  isrequired;
    int QuestionNumber;

    int isother;

    public int isIsother() {
        return isother;
    }

    public void setIsother(int isother) {
        this.isother = isother;
    }



    public int getQuestionNumber() {
        return QuestionNumber;
    }

    public void setQuestionNumber(int questionNumber) {
        QuestionNumber = questionNumber;
    }
    public String getIsrequired() {
        return isrequired;
    }

    public void setIsrequired(String isrequired) {
        this.isrequired = isrequired;
    }

    public EditText getEditText() {
        return editText;
    }

    public void setEditText(EditText editText) {
        this.editText = editText;
    }

    int questId;

    public int getQuestId() {
        return questId;
    }

    public void setQuestId(int questId) {
        this.questId = questId;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public List<Integer> getQuestionId() {
        return QuestionId;
    }

    public void setQuestionId(List<Integer> questionId) {
        QuestionId = questionId;
    }

    public RadioGroup getRadioGroup() {
        return radioGroup;
    }

    public void setRadioGroup(RadioGroup radioGroup) {
        this.radioGroup = radioGroup;
    }
}
