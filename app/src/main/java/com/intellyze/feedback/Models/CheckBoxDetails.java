package com.intellyze.feedback.Models;

import android.widget.CheckBox;

import java.util.List;

/**
 * Created by INTELLYZE-202 on 21-12-2017.
 */

public class CheckBoxDetails {
     CheckBox checkBoxes;

    public CheckBox getCheckBoxes() {
        return checkBoxes;
    }

    public void setCheckBoxes(CheckBox checkBoxes) {
        this.checkBoxes = checkBoxes;
    }
}
