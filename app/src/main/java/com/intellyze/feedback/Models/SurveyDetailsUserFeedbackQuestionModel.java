package com.intellyze.feedback.Models;

/**
 * Created by INTELLYZE-202 on 26-12-2017.
 */

public class SurveyDetailsUserFeedbackQuestionModel {
    private int SurveyId;
    private String SurveyName;
    private String SurveyDate;
    private String SurveyTime;
    private String SurveyStatus;
    private String SurveyDescription;
    private String SurveyImage;
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getSurveyId() {
        return SurveyId;
    }

    public void setSurveyId(int surveyId) {
        SurveyId = surveyId;
    }

    public String getSurveyName() {
        return SurveyName;
    }

    public void setSurveyName(String surveyName) {
        SurveyName = surveyName;
    }

    public String getSurveyDate() {
        return SurveyDate;
    }

    public void setSurveyDate(String surveyDate) {
        SurveyDate = surveyDate;
    }

    public String getSurveyTime() {
        return SurveyTime;
    }

    public void setSurveyTime(String surveyTime) {
        SurveyTime = surveyTime;
    }

    public String getSurveyStatus() {
        return SurveyStatus;
    }

    public void setSurveyStatus(String surveyStatus) {
        SurveyStatus = surveyStatus;
    }

    public String getSurveyDescription() {
        return SurveyDescription;
    }

    public void setSurveyDescription(String surveyDescription) {
        SurveyDescription = surveyDescription;
    }

    public String getSurveyImage() {
        return SurveyImage;
    }

    public void setSurveyImage(String surveyImage) {
        SurveyImage = surveyImage;
    }
}
