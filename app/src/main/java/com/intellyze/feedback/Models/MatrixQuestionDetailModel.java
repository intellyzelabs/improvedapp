package com.intellyze.feedback.Models;


import java.util.List;

/**
 * Created by INTELLYZE-202 on 13-12-2017.
 */

public class MatrixQuestionDetailModel {
    private List<RowDetailsModel> rowDetails;
    private List<ColumnDetailsModel> columnDetails;

    public List<ColumnDetailsModel> getColumnDetails() {
        return columnDetails;
    }

    public void setColumnDetails(List<ColumnDetailsModel> columnDetails) {
        this.columnDetails = columnDetails;
    }

    public List<RowDetailsModel> getRowDetails() {

        return rowDetails;
    }

    public void setRowDetails(List<RowDetailsModel> rowDetails) {
        this.rowDetails = rowDetails;
    }
}
