package com.intellyze.feedback.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by INTELLYZE-202 on 07-12-2017.
 */

public class UserPref {
    SharedPreferences sp;
    Context activity;

    public String getBusinessId() {

        String token = sp.getString("businessId", "");
        return token;
    }

    public void setBusinessId(String businessId) {

        SharedPreferences.Editor token = sp.edit();
        token.putString("businessId", businessId);
        token.commit();
    }

    String businessId;

    public UserPref(Context context) {
        activity = context;
        sp = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void SaveCount(int id) {
        SharedPreferences.Editor token = sp.edit();
        token.putInt("count", id);
        token.commit();
    }

    public int GetCount() {
        int token = sp.getInt("count", 0);
        return token;
    }
    public void saveUserName(String uname) {
        SharedPreferences.Editor token = sp.edit();
        token.putString("uname", uname);
        token.commit();
    }

    public String getUserName() {
        String token = sp.getString("uname", "");
        return token;
    }

    public void saveSurveyAnswerId(int usurveyid) {
        SharedPreferences.Editor token = sp.edit();
        token.putInt("usurveyid", usurveyid);
        token.commit();
    }

    public int getSurveyAnswerId() {
        int token = sp.getInt("usurveyid", 0);
        return token;
    }

    public void saveSurveyName(String name) {
        SharedPreferences.Editor token = sp.edit();
        token.putString("name", name);
        token.commit();
    }
    public String getUserImage() {
        String token = sp.getString("uimage", "");
        return token;
    }
    public void saveUserImage(String uimage) {
        SharedPreferences.Editor token = sp.edit();
        token.putString("uimage", uimage);
        token.commit();
    }
    public String getNameOfUser() {
        String token = sp.getString("namess", "");
        return token;
    }
    public void saveNameOfUser(String namess) {
        SharedPreferences.Editor token = sp.edit();
        token.putString("namess", namess);
        token.commit();
    }
    public String getRole() {
        String token = sp.getString("u_role", "");
        return token;
    }
    public void saveRole(String u_role) {
        SharedPreferences.Editor token = sp.edit();
        token.putString("u_role", u_role);
        token.commit();
    }
    public String getGst() {
        String token = sp.getString("ugst", "");
        return token;
    }
    public void saveGst(String ugst) {
        SharedPreferences.Editor token = sp.edit();
        token.putString("ugst", ugst);
        token.commit();
    }
    public String getSurveyName() {
        String token = sp.getString("name", "");
        return token;
    }
    public void saveSurveyId(String id) {
        SharedPreferences.Editor token = sp.edit();
        token.putString("id", id);
        token.commit();
    }

    public String getSurveyId() {
        String token = sp.getString("id", "");
        return token;
    }
    public void Clear(){
        SharedPreferences.Editor ed = sp.edit();
        ed.clear();
        ed.commit();
    }


}