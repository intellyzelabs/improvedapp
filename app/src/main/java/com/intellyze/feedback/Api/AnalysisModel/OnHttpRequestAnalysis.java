package com.intellyze.feedback.Api.AnalysisModel;

import java.util.List;

/**
 * Created by INTELLYZE-202 on 01-01-2018.
 */

public interface OnHttpRequestAnalysis {
    public void onHttpAnalysisSuccess(String Message,SurveyData surveyData);
    public void onHttpAnalysisFailed(String message);
}
