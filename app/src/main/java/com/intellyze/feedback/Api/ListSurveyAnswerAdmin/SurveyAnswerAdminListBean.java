
package com.intellyze.feedback.Api.ListSurveyAnswerAdmin;


import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SurveyAnswerAdminListBean {

    @SerializedName("Business_Id")
    private String mBusinessId;
    @SerializedName("survey_Id")
    private Long mSurveyId;

    public String getBusinessId() {
        return mBusinessId;
    }

    public void setBusinessId(String BusinessId) {
        mBusinessId = BusinessId;
    }

    public Long getSurveyId() {
        return mSurveyId;
    }

    public void setSurveyId(Long surveyId) {
        mSurveyId = surveyId;
    }

}
