
package com.intellyze.feedback.Api.UserModel;

import com.google.gson.annotations.SerializedName;


public class UserDeleteModel {

    @SerializedName("BusinessId")
    private Long mBusinessId;
    @SerializedName("UserId")
    private Long mUserId;

    public Long getBusinessId() {
        return mBusinessId;
    }

    public void setBusinessId(Long BusinessId) {
        mBusinessId = BusinessId;
    }

    public Long getUserId() {
        return mUserId;
    }

    public void setUserId(Long UserId) {
        mUserId = UserId;
    }

}
