
package com.intellyze.feedback.Api.FeedbackSurveyQuestions;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Linear {

    @SerializedName("end_text")
    private String mEndText;
    @SerializedName("end_value")
    private String mEndValue;
    @SerializedName("Qlinear_Id")
    private String mQlinearId;
    @SerializedName("Question_Id")
    private String mQuestionId;
    @SerializedName("start_text")
    private String mStartText;
    @SerializedName("start_value")
    private String mStartValue;

    public String getEndText() {
        return mEndText;
    }

    public void setEndText(String endText) {
        mEndText = endText;
    }

    public String getEndValue() {
        return mEndValue;
    }

    public void setEndValue(String endValue) {
        mEndValue = endValue;
    }

    public String getQlinearId() {
        return mQlinearId;
    }

    public void setQlinearId(String QlinearId) {
        mQlinearId = QlinearId;
    }

    public String getQuestionId() {
        return mQuestionId;
    }

    public void setQuestionId(String QuestionId) {
        mQuestionId = QuestionId;
    }

    public String getStartText() {
        return mStartText;
    }

    public void setStartText(String startText) {
        mStartText = startText;
    }

    public String getStartValue() {
        return mStartValue;
    }

    public void setStartValue(String startValue) {
        mStartValue = startValue;
    }

}
