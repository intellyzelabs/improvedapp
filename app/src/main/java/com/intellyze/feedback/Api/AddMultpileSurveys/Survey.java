
package com.intellyze.feedback.Api.AddMultpileSurveys;

import java.util.List;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Survey {

    @SerializedName("SurveyDate")
    private String mSurveyDate;
    @SerializedName("SurveyDesc")
    private String mSurveyDesc;
    @SerializedName("SurveyId")
    private Long mSurveyId;
    @SerializedName("SurveyName")
    private String mSurveyName;
    @SerializedName("SurveyQuestions")
    private List<SurveyQuestion> mSurveyQuestions;
    @SerializedName("SurveyStatus")
    private String mSurveyStatus;
    @SerializedName("SurveyTime")
    private String mSurveyTime;
    @SerializedName("surveyImage")
    private String surveyImage;
    @SerializedName("Business_Id")
    private String Business_Id;

    public String getBusiness_Id() {
        return Business_Id;
    }

    public void setBusiness_Id(String business_Id) {
        Business_Id = business_Id;
    }

    public String getSurveyImage() {
        return surveyImage;
    }

    public void setSurveyImage(String surveyImage) {
        this.surveyImage = surveyImage;
    }

    public String getSurveyDate() {
        return mSurveyDate;
    }

    public void setSurveyDate(String SurveyDate) {
        mSurveyDate = SurveyDate;
    }

    public String getSurveyDesc() {
        return mSurveyDesc;
    }

    public void setSurveyDesc(String SurveyDesc) {
        mSurveyDesc = SurveyDesc;
    }

    public Long getSurveyId() {
        return mSurveyId;
    }

    public void setSurveyId(Long SurveyId) {
        mSurveyId = SurveyId;
    }

    public String getSurveyName() {
        return mSurveyName;
    }

    public void setSurveyName(String SurveyName) {
        mSurveyName = SurveyName;
    }

    public List<SurveyQuestion> getSurveyQuestions() {
        return mSurveyQuestions;
    }

    public void setSurveyQuestions(List<SurveyQuestion> SurveyQuestions) {
        mSurveyQuestions = SurveyQuestions;
    }

    public String getSurveyStatus() {
        return mSurveyStatus;
    }

    public void setSurveyStatus(String SurveyStatus) {
        mSurveyStatus = SurveyStatus;
    }

    public String getSurveyTime() {
        return mSurveyTime;
    }

    public void setSurveyTime(String SurveyTime) {
        mSurveyTime = SurveyTime;
    }

}
