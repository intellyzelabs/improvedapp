
package com.intellyze.feedback.Api.FeedbackSurveyQuestions;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class FeedbackQuestionsModel {

    @SerializedName("error")
    private Boolean mError;
    @SerializedName("Message")
    private String mMessage;
    @SerializedName("SurveyData")
    private List<SurveyDatum> mSurveyData;

    public Boolean getError() {
        return mError;
    }

    public void setError(Boolean error) {
        mError = error;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String Message) {
        mMessage = Message;
    }

    public List<SurveyDatum> getSurveyData() {
        return mSurveyData;
    }

    public void setSurveyData(List<SurveyDatum> SurveyData) {
        mSurveyData = SurveyData;
    }

}
