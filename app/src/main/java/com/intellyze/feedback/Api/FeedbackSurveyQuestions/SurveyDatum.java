
package com.intellyze.feedback.Api.FeedbackSurveyQuestions;

import java.util.List;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SurveyDatum {

    @SerializedName("s_status")
    private String mSStatus;
    @SerializedName("survey_date")
    private String mSurveyDate;
    @SerializedName("survey_Descr")
    private String mSurveyDescr;
    @SerializedName("survey_Id")
    private String mSurveyId;
    @SerializedName("SurveyQuestions")
    private List<SurveyQuestion> mSurveyQuestions;
    @SerializedName("survey_time")
    private String mSurveyTime;
    @SerializedName("survey_Title")
    private String mSurveyTitle;
    @SerializedName("count")
    private String count;
    @SerializedName("LastEntryDate")
    private String LastEntryDate;
    @SerializedName("LastEntryTime")
    private String LastEntryTime;
    @SerializedName("survey_image")
    private String survey_image;

    public String getSurvey_image() {
        return survey_image;
    }

    public void setSurvey_image(String survey_image) {
        this.survey_image = survey_image;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getLastEntryDate() {
        return LastEntryDate;
    }

    public void setLastEntryDate(String lastEntryDate) {
        LastEntryDate = lastEntryDate;
    }

    public String getLastEntryTime() {
        return LastEntryTime;
    }

    public void setLastEntryTime(String lastEntryTime) {
        LastEntryTime = lastEntryTime;
    }

    public String getSStatus() {
        return mSStatus;
    }

    public void setSStatus(String sStatus) {
        mSStatus = sStatus;
    }

    public String getSurveyDate() {
        return mSurveyDate;
    }

    public void setSurveyDate(String surveyDate) {
        mSurveyDate = surveyDate;
    }

    public String getSurveyDescr() {
        return mSurveyDescr;
    }

    public void setSurveyDescr(String surveyDescr) {
        mSurveyDescr = surveyDescr;
    }

    public String getSurveyId() {
        return mSurveyId;
    }

    public void setSurveyId(String surveyId) {
        mSurveyId = surveyId;
    }

    public List<SurveyQuestion> getSurveyQuestions() {
        return mSurveyQuestions;
    }

    public void setSurveyQuestions(List<SurveyQuestion> SurveyQuestions) {
        mSurveyQuestions = SurveyQuestions;
    }

    public String getSurveyTime() {
        return mSurveyTime;
    }

    public void setSurveyTime(String surveyTime) {
        mSurveyTime = surveyTime;
    }

    public String getSurveyTitle() {
        return mSurveyTitle;
    }

    public void setSurveyTitle(String surveyTitle) {
        mSurveyTitle = surveyTitle;
    }

}
