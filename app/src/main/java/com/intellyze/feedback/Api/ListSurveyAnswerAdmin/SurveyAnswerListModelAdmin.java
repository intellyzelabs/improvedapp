
package com.intellyze.feedback.Api.ListSurveyAnswerAdmin;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SurveyAnswerListModelAdmin {

    @SerializedName("error")
    private Boolean mError;
    @SerializedName("Message")
    private String mMessage;
    @SerializedName("ResultData")
    private List<ResultDatum> mResultData;

    public Boolean getError() {
        return mError;
    }

    public void setError(Boolean error) {
        mError = error;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String Message) {
        mMessage = Message;
    }

    public List<ResultDatum> getResultData() {
        return mResultData;
    }

    public void setResultData(List<ResultDatum> ResultData) {
        mResultData = ResultData;
    }

}
