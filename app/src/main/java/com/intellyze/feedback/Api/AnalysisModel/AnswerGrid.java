
package com.intellyze.feedback.Api.AnalysisModel;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class AnswerGrid {

    @SerializedName("ColumnsResult")
    private List<com.intellyze.feedback.Api.AnalysisModel.ColumnsResult> mColumnsResult;
    @SerializedName("RowTitle")
    private String mRowTitle;

    public List<com.intellyze.feedback.Api.AnalysisModel.ColumnsResult> getColumnsResult() {
        return mColumnsResult;
    }

    public void setColumnsResult(List<com.intellyze.feedback.Api.AnalysisModel.ColumnsResult> ColumnsResult) {
        mColumnsResult = ColumnsResult;
    }

    public String getRowTitle() {
        return mRowTitle;
    }

    public void setRowTitle(String RowTitle) {
        mRowTitle = RowTitle;
    }

}
