
package com.intellyze.feedback.Api.FeedbackSurveyQuestions;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class MultipleChoiceGrid {

    @SerializedName("column")
    private List<Column> mColumn;
    @SerializedName("row")
    private List<Row> mRow;

    public List<Column> getColumn() {
        return mColumn;
    }

    public void setColumn(List<Column> column) {
        mColumn = column;
    }

    public List<Row> getRow() {
        return mRow;
    }

    public void setRow(List<Row> row) {
        mRow = row;
    }

}
