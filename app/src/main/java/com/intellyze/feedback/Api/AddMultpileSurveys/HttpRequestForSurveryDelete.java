package com.intellyze.feedback.Api.AddMultpileSurveys;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.intellyze.feedback.Api.ApiClient;
import com.intellyze.feedback.Api.ApiInterfaces;
import com.intellyze.feedback.Api.UserModel.UserDeleteModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by intellyelabs on 23/01/18.
 */

public class HttpRequestForSurveryDelete {
    OnHttpSurveryDeleteResponce onHttpUserRequest;
    public HttpRequestForSurveryDelete(OnHttpSurveryDeleteResponce onHttpUserRequest) {
        this.onHttpUserRequest = onHttpUserRequest;
    }
    public void deleteUser(final DeleteSurveryParameters surveyid)
    {

        ApiInterfaces apiInterfaces= ApiClient.getClient().create(ApiInterfaces.class);
        Gson gson = new GsonBuilder().create();
        String payloadStr = gson.toJson(surveyid);
        System.out.println(payloadStr + "surveyid");
        Call<SurveryDeleteResponceBean> call=apiInterfaces.deleteSurvey(surveyid);



        call.enqueue(new Callback<SurveryDeleteResponceBean>() {
            @Override
            public void onResponse(Call<SurveryDeleteResponceBean> call, Response<SurveryDeleteResponceBean> response) {
                if(response.message().equals("OK"))
                {

                    if(!response.body().getError()) {
                        onHttpUserRequest.onHttpSurveryDeleteSuccess(response.body().getMessage(),surveyid);

                    }
                    else
                    {
                        onHttpUserRequest.onHttpSurveryDeleteFailed(response.body().getMessage());



                    }

                }
                else
                {
                    onHttpUserRequest.onHttpSurveryDeleteFailed("Server Error !");
                }
            }
            @Override
            public void onFailure(Call<SurveryDeleteResponceBean> call, Throwable t) {
                onHttpUserRequest.onHttpSurveryDeleteFailed("Server Error !");
            }
        });
    }
}
