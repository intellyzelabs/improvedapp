package com.intellyze.feedback.Api.FeedbackSurveyQuestions;

import com.intellyze.feedback.Api.ApiClient;
import com.intellyze.feedback.Api.ApiInterfaces;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by INTELLYZE-202 on 26-12-2017.
 */

public class HttpRequestSurveyQuestion {
    OnHttpRequestSurveyQuestion  onHttpRequestSurveyQuestion;
    public HttpRequestSurveyQuestion(OnHttpRequestSurveyQuestion onHttpRequestSurveyQuestion) {
        this.onHttpRequestSurveyQuestion = onHttpRequestSurveyQuestion;
    }
    public void SurveyQuest(FeedbackQuestionRequestModel feedbackQuestionRequestModel)
    {

        ApiInterfaces apiInterfaces= ApiClient.getClient().create(ApiInterfaces.class);
        Call<FeedbackQuestionsModel> call=apiInterfaces.ListSurvey(feedbackQuestionRequestModel);
        call.enqueue(new Callback<FeedbackQuestionsModel>() {
            @Override
            public void onResponse(Call<FeedbackQuestionsModel> call, Response<FeedbackQuestionsModel> response) {
                if(response.message().equals("OK"))
                {
                    if(!response.body().getError())
                    {


                        onHttpRequestSurveyQuestion.onHttpSurveyQuestionSuccess(response.body().getMessage(),response.body().getSurveyData());
                    }else {
                        onHttpRequestSurveyQuestion.onHttpSurveyQuestionFailed(response.body().getMessage());
                    }
                }
                else
                {
                    onHttpRequestSurveyQuestion.onHttpSurveyQuestionFailed("Server Error !");
                }
            }
            @Override
            public void onFailure(Call<FeedbackQuestionsModel> call, Throwable t) {
                onHttpRequestSurveyQuestion.onHttpSurveyQuestionFailed("Server Error !");
            }
        });
    }
}
