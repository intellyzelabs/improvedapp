
package com.intellyze.feedback.Api.FeedbackSurveyQuestions;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class QuestionMore {

    @SerializedName("Linear")
    private List<com.intellyze.feedback.Api.FeedbackSurveyQuestions.Linear> mLinear;
    @SerializedName("Multiple_Choice")
    private List<com.intellyze.feedback.Api.FeedbackSurveyQuestions.MultipleChoice> mMultipleChoice;
    @SerializedName("Multiple_Choice_Grid")
    private List<com.intellyze.feedback.Api.FeedbackSurveyQuestions.MultipleChoiceGrid> mMultipleChoiceGrid;

    public List<com.intellyze.feedback.Api.FeedbackSurveyQuestions.Linear> getLinear() {
        return mLinear;
    }

    public void setLinear(List<com.intellyze.feedback.Api.FeedbackSurveyQuestions.Linear> Linear) {
        mLinear = Linear;
    }

    public List<com.intellyze.feedback.Api.FeedbackSurveyQuestions.MultipleChoice> getMultipleChoice() {
        return mMultipleChoice;
    }

    public void setMultipleChoice(List<com.intellyze.feedback.Api.FeedbackSurveyQuestions.MultipleChoice> MultipleChoice) {
        mMultipleChoice = MultipleChoice;
    }

    public List<MultipleChoiceGrid> getmMultipleChoiceGrid() {
        return mMultipleChoiceGrid;
    }

    public void setmMultipleChoiceGrid(List<MultipleChoiceGrid> mMultipleChoiceGrid) {
        this.mMultipleChoiceGrid = mMultipleChoiceGrid;
    }
}
