package com.intellyze.feedback.Api.UserModel;


import java.util.List;

/**
 * Created by intellyelabs on 23/01/18.
 */

public interface OnHttpUserRequest {

        public void onHttpUserDetailsSuccess(String Message, List<Result> result);
        public void onHttpUserDetailsFailed(String message);

}
