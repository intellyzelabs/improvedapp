
package com.intellyze.feedback.Api.AnswerDetails;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SurveyAnswerModel {

    @SerializedName("error")
    private Boolean mError;
    @SerializedName("Message")
    private String mMessage;
    @SerializedName("SurveyData")
    private com.intellyze.feedback.Api.AnswerDetails.SurveyData mSurveyData;

    public Boolean getError() {
        return mError;
    }

    public void setError(Boolean error) {
        mError = error;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String Message) {
        mMessage = Message;
    }

    public com.intellyze.feedback.Api.AnswerDetails.SurveyData getSurveyData() {
        return mSurveyData;
    }

    public void setSurveyData(com.intellyze.feedback.Api.AnswerDetails.SurveyData SurveyData) {
        mSurveyData = SurveyData;
    }

}
