
package com.intellyze.feedback.Api.UpdateSurveyStatus;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ResultData {

    @SerializedName("s_status")
    private String mSStatus;
    @SerializedName("survey_date")
    private String mSurveyDate;
    @SerializedName("survey_Descr")
    private String mSurveyDescr;
    @SerializedName("survey_Id")
    private String mSurveyId;
    @SerializedName("survey_image")
    private String mSurveyImage;
    @SerializedName("survey_time")
    private String mSurveyTime;
    @SerializedName("survey_Title")
    private String mSurveyTitle;

    public String getSStatus() {
        return mSStatus;
    }

    public void setSStatus(String sStatus) {
        mSStatus = sStatus;
    }

    public String getSurveyDate() {
        return mSurveyDate;
    }

    public void setSurveyDate(String surveyDate) {
        mSurveyDate = surveyDate;
    }

    public String getSurveyDescr() {
        return mSurveyDescr;
    }

    public void setSurveyDescr(String surveyDescr) {
        mSurveyDescr = surveyDescr;
    }

    public String getSurveyId() {
        return mSurveyId;
    }

    public void setSurveyId(String surveyId) {
        mSurveyId = surveyId;
    }

    public String getSurveyImage() {
        return mSurveyImage;
    }

    public void setSurveyImage(String surveyImage) {
        mSurveyImage = surveyImage;
    }

    public String getSurveyTime() {
        return mSurveyTime;
    }

    public void setSurveyTime(String surveyTime) {
        mSurveyTime = surveyTime;
    }

    public String getSurveyTitle() {
        return mSurveyTitle;
    }

    public void setSurveyTitle(String surveyTitle) {
        mSurveyTitle = surveyTitle;
    }

}
