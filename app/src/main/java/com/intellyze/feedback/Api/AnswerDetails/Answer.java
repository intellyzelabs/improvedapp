
package com.intellyze.feedback.Api.AnswerDetails;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Answer {

    @SerializedName("isAnswer")
    private String mIsAnswer;
    @SerializedName("Option_Name")
    private String mOptionName;

    public String getIsAnswer() {
        return mIsAnswer;
    }

    public void setIsAnswer(String isAnswer) {
        mIsAnswer = isAnswer;
    }

    public String getOptionName() {
        return mOptionName;
    }

    public void setOptionName(String OptionName) {
        mOptionName = OptionName;
    }

}
