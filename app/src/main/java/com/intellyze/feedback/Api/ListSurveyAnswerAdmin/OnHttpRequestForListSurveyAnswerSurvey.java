package com.intellyze.feedback.Api.ListSurveyAnswerAdmin;

import java.util.List;

/**
 * Created by INTELLYZE-202 on 05-01-2018.
 */

public interface OnHttpRequestForListSurveyAnswerSurvey {
    public void onHttpAddForListSurveyAnswerSurveySuccess(List<ResultDatum> list,String Message);
    public void onHttpAddForListSurveyAnswerSurveyFailed(String message);
}
