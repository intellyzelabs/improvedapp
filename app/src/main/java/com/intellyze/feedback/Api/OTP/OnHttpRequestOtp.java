package com.intellyze.feedback.Api.OTP;

import com.intellyze.feedback.Api.AnalysisModel.SurveyData;

/**
 * Created by INTELLYZE-202 on 01-01-2018.
 */

public interface OnHttpRequestOtp {
    public void onHttpOtpisSuccess(String Message);
    public void onHttpOtpisFailed(String message);
}
