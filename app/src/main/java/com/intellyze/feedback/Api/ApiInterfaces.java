package com.intellyze.feedback.Api;

import com.intellyze.feedback.Api.AddAnswerMultipleSurveys.AddMultipleAnswerResponseModel;
import com.intellyze.feedback.Api.AddAnswerMultipleSurveys.AddMultplieAnswerSurveyRequestModel;
import com.intellyze.feedback.Api.AddMultpileSurveys.AddMultipleSurveyRequestModel;
import com.intellyze.feedback.Api.AddMultpileSurveys.AddMultipleSurveyResponseModel;
import com.intellyze.feedback.Api.AddMultpileSurveys.DeleteSurveryParameters;
import com.intellyze.feedback.Api.AddMultpileSurveys.SurveryDeleteResponceBean;
import com.intellyze.feedback.Api.AddUserModel.AddUserResponceModel;
import com.intellyze.feedback.Api.AddUserModel.AdduserModel;
import com.intellyze.feedback.Api.AnalysisModel.AnalysisDataModel;
import com.intellyze.feedback.Api.AnalysisModel.AnalysisResponceModel;
import com.intellyze.feedback.Api.AnswerDetails.AnswerDetailModelParameter;
import com.intellyze.feedback.Api.AnswerDetails.SurveyAnswerModel;
import com.intellyze.feedback.Api.FeedbackSurveyQuestions.FeedbackQuestionRequestModel;
import com.intellyze.feedback.Api.FeedbackSurveyQuestions.FeedbackQuestionsModel;
import com.intellyze.feedback.Api.ListSurveyAnswerAdmin.SurveyAnswerAdminListBean;
import com.intellyze.feedback.Api.ListSurveyAnswerAdmin.SurveyAnswerListModelAdmin;
import com.intellyze.feedback.Api.Login.LoginRequestModel;
import com.intellyze.feedback.Api.Login.LoginResponseModel;
import com.intellyze.feedback.Api.SignUp.SignUpRequestModel;
import com.intellyze.feedback.Api.SignUp.SignUpResponseModel;
import com.intellyze.feedback.Api.UpdateSurveyStatus.UpdateSurveyStatusResponseModel;
import com.intellyze.feedback.Api.UserModel.ListUserItem;
import com.intellyze.feedback.Api.UserModel.UserDeleteModel;
import com.intellyze.feedback.Api.UserModel.UserDeleteResponce;
import com.intellyze.feedback.Api.UserModel.UserDetailsModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by INTELLYZE-202 on 18-12-2017.
 */

public interface ApiInterfaces {
    @Headers({
            "Content-Type: application/json"
    })
    @POST("Add_Business")
    Call<SignUpResponseModel> signUp(@Body SignUpRequestModel data);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("user_login")
    Call<LoginResponseModel> Login(@Body LoginRequestModel data);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("List_Survey")
    Call<FeedbackQuestionsModel> ListSurvey(@Body FeedbackQuestionRequestModel data);

    @POST("Add_Business_UserAccount")
    Call<AddUserResponceModel> addUserAccount(@Body AdduserModel data);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("list_userAccounts_ForBusiness")
    Call<UserDetailsModel> ListUsers(@Body ListUserItem data);


    @POST("delete_userAccount")
    Call<UserDeleteResponce> deleteUserAccount(@Body UserDeleteModel data);

    @POST("Get_Analytics")
    Call<AnalysisResponceModel> getAnalysis(@Body AnalysisDataModel data);

    @FormUrlEncoded
    @POST("Get_Analytics")
    Call<AnalysisResponceModel> sendOtp(@Field("id") String data);


    @Headers({
            "Content-Type: application/json"
    })
    @POST("Add_Survey_Multiple")
    Call<AddMultipleSurveyResponseModel> Add_MultipleSurvey(@Body AddMultipleSurveyRequestModel data);

    @Headers({
            "Content-Type: application/json"
    })

    @POST("Add_Answer_Master_Multiple")
    Call<AddMultipleAnswerResponseModel> Add_MultipleAnswerSurvey(@Body AddMultplieAnswerSurveyRequestModel data);


    @POST("List_Survey_AnswersByUser")
    Call<SurveyAnswerListModelAdmin> List_Survey_AnswersByUser(@Body SurveyAnswerAdminListBean data);

    @POST("Get_Answers")
    Call<SurveyAnswerModel> Get_Answers(@Body AnswerDetailModelParameter data);

    @FormUrlEncoded
    @POST("Update_Survey_Status")
    Call<UpdateSurveyStatusResponseModel> getStatusResponse(@Field("survey_Id") int survey_Id, @Field("status") String status);

    @POST("Delete_Survey")
    Call<SurveryDeleteResponceBean> deleteSurvey(@Body DeleteSurveryParameters deleteSurveryParameters);
}
