package com.intellyze.feedback.Api.AnswerDetails;

import com.intellyze.feedback.Api.ListSurveyAnswerAdmin.ResultDatum;

import java.util.List;

/**
 * Created by INTELLYZE-202 on 05-01-2018.
 */

public interface OnHttpRequestForSurveyAnswerDetaisSurvey {
    public void onHttpAddForSurveyAnswerDetaisSurveySuccess(SurveyData list, String Message);
    public void onHttpAddFoListSurveyAnswerDetaisSurveyFailed(String message);
}
