package com.intellyze.feedback.Api.AddUserModel;

import com.intellyze.feedback.Api.ApiClient;
import com.intellyze.feedback.Api.ApiInterfaces;
import com.intellyze.feedback.Api.FeedbackSurveyQuestions.FeedbackQuestionRequestModel;
import com.intellyze.feedback.Api.UserModel.OnHttpUserRequest;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by intellyelabs on 23/01/18.
 */

public class HttpRequestForAddUser {
    OnHttpAddUserRequest onHttpUserRequest;
    public HttpRequestForAddUser(OnHttpAddUserRequest onHttpUserRequest) {
        this.onHttpUserRequest = onHttpUserRequest;
    }
    public void addUserDetails(AdduserModel adduserModel)
    {

        ApiInterfaces apiInterfaces= ApiClient.getClient().create(ApiInterfaces.class);
        Call<AddUserResponceModel> call=apiInterfaces.addUserAccount(adduserModel);
        call.enqueue(new Callback<AddUserResponceModel>() {
            @Override
            public void onResponse(Call<AddUserResponceModel> call, Response<AddUserResponceModel> response) {
                if(response.message().equals("OK"))
                {
                    if (!response.body().getError())
                    {String message=response.body().getMessage();
                        onHttpUserRequest.onHttpAddUserDetailsSuccess(message);

                    }
                    else
                    {String message=response.body().getMessage();
                        onHttpUserRequest.onHttpAddUserDetailsFailed(message);

                    }


                }
                else
                {
                    onHttpUserRequest.onHttpAddUserDetailsFailed("Server Error !");
                }
            }
            @Override
            public void onFailure(Call<AddUserResponceModel> call, Throwable t) {
                onHttpUserRequest.onHttpAddUserDetailsFailed("Server Error !");
            }
        });
    }
}
