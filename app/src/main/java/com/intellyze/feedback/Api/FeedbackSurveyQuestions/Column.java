
package com.intellyze.feedback.Api.FeedbackSurveyQuestions;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Column {

    @SerializedName("col_Title")
    private String mColTitle;
    @SerializedName("Qmultiple_grid_col_Id")
    private String mQmultipleGridColId;
    @SerializedName("Question_Id")
    private String mQuestionId;

    public String getColTitle() {
        return mColTitle;
    }

    public void setColTitle(String colTitle) {
        mColTitle = colTitle;
    }

    public String getQmultipleGridColId() {
        return mQmultipleGridColId;
    }

    public void setQmultipleGridColId(String QmultipleGridColId) {
        mQmultipleGridColId = QmultipleGridColId;
    }

    public String getQuestionId() {
        return mQuestionId;
    }

    public void setQuestionId(String QuestionId) {
        mQuestionId = QuestionId;
    }

}
