package com.intellyze.feedback.Api.AnalysisModel;


import com.intellyze.feedback.Api.ApiClient;
import com.intellyze.feedback.Api.ApiInterfaces;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by INTELLYZE-202 on 01-01-2018.
 */

public class HttpRequestForListAnalysis {
    OnHttpRequestAnalysis OnHttpRequestAnalysis;

    public HttpRequestForListAnalysis(OnHttpRequestAnalysis OnHttpRequestAnalysis) {
        this.OnHttpRequestAnalysis = OnHttpRequestAnalysis;
    }

    public void getList(AnalysisDataModel addMultipleSurveyRequestModel) {

        ApiInterfaces apiInterfaces = ApiClient.getClient().create(ApiInterfaces.class);
        Call<AnalysisResponceModel> call = apiInterfaces.getAnalysis(addMultipleSurveyRequestModel);
        call.enqueue(new Callback<AnalysisResponceModel>() {
            @Override
            public void onResponse(Call<AnalysisResponceModel> call, Response<AnalysisResponceModel> response) {
                if (response.message().equals("OK")) {
                    if (!response.body().getError()) {
                        OnHttpRequestAnalysis.onHttpAnalysisSuccess(response.body().getMessage(),response.body().getSurveyData());
                    } else {
                        OnHttpRequestAnalysis.onHttpAnalysisFailed(response.body().getMessage());
                    }
                } else {
                    OnHttpRequestAnalysis.onHttpAnalysisFailed("Server Error !");
                }
            }

            @Override
            public void onFailure(Call<AnalysisResponceModel> call, Throwable t) {
                OnHttpRequestAnalysis.onHttpAnalysisFailed("Server Error !");
            }
        });
    }
}
