
package com.intellyze.feedback.Api.AddMultpileSurveys;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Linearquestion {

    @SerializedName("endText")
    private String mEndText;
    @SerializedName("endValue")
    private Long mEndValue;
    @SerializedName("Qlinear_Id")
    private Long mQlinearId;
    @SerializedName("startText")
    private String mStartText;
    @SerializedName("startValue")
    private Long mStartValue;

    public String getEndText() {
        return mEndText;
    }

    public void setEndText(String endText) {
        mEndText = endText;
    }

    public Long getEndValue() {
        return mEndValue;
    }

    public void setEndValue(Long endValue) {
        mEndValue = endValue;
    }

    public Long getQlinearId() {
        return mQlinearId;
    }

    public void setQlinearId(Long QlinearId) {
        mQlinearId = QlinearId;
    }

    public String getStartText() {
        return mStartText;
    }

    public void setStartText(String startText) {
        mStartText = startText;
    }

    public Long getStartValue() {
        return mStartValue;
    }

    public void setStartValue(Long startValue) {
        mStartValue = startValue;
    }

}
