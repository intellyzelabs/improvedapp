
package com.intellyze.feedback.Api.Login;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Result {

    @SerializedName("business_address")
    private String mBusinessAddress;
    @SerializedName("business_Category")
    private String mBusinessCategory;
    @SerializedName("business_Customer_CareNo")
    private String mBusinessCustomerCareNo;
    @SerializedName("business_Email")
    private String mBusinessEmail;
    @SerializedName("business_GST")
    private String mBusinessGST;
    @SerializedName("Business_Id")
    private String mBusinessId;
    @SerializedName("business_LandLine")
    private String mBusinessLandLine;
    @SerializedName("business_Logo")
    private String mBusinessLogo;
    @SerializedName("business_MobileNo")
    private String mBusinessMobileNo;
    @SerializedName("business_name")
    private String mBusinessName;
    @SerializedName("business_OauthKey")
    private String mBusinessOauthKey;
    @SerializedName("business_Website")
    private String mBusinessWebsite;
    @SerializedName("frid")
    private String mFrid;
    @SerializedName("isAuthorized")
    private String mIsAuthorized;
    @SerializedName("profilepic")
    private String mProfilepic;
    @SerializedName("u_password")
    private String mUPassword;
    @SerializedName("u_role")
    private String mURole;
    @SerializedName("userId")
    private String mUserId;
    @SerializedName("username")
    private String mUsername;

    public String getBusinessAddress() {
        return mBusinessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        mBusinessAddress = businessAddress;
    }

    public String getBusinessCategory() {
        return mBusinessCategory;
    }

    public void setBusinessCategory(String businessCategory) {
        mBusinessCategory = businessCategory;
    }

    public String getBusinessCustomerCareNo() {
        return mBusinessCustomerCareNo;
    }

    public void setBusinessCustomerCareNo(String businessCustomerCareNo) {
        mBusinessCustomerCareNo = businessCustomerCareNo;
    }

    public String getBusinessEmail() {
        return mBusinessEmail;
    }

    public void setBusinessEmail(String businessEmail) {
        mBusinessEmail = businessEmail;
    }

    public String getBusinessGST() {
        return mBusinessGST;
    }

    public void setBusinessGST(String businessGST) {
        mBusinessGST = businessGST;
    }

    public String getBusinessId() {
        return mBusinessId;
    }

    public void setBusinessId(String BusinessId) {
        mBusinessId = BusinessId;
    }

    public String getBusinessLandLine() {
        return mBusinessLandLine;
    }

    public void setBusinessLandLine(String businessLandLine) {
        mBusinessLandLine = businessLandLine;
    }

    public String getBusinessLogo() {
        return mBusinessLogo;
    }

    public void setBusinessLogo(String businessLogo) {
        mBusinessLogo = businessLogo;
    }

    public String getBusinessMobileNo() {
        return mBusinessMobileNo;
    }

    public void setBusinessMobileNo(String businessMobileNo) {
        mBusinessMobileNo = businessMobileNo;
    }

    public String getBusinessName() {
        return mBusinessName;
    }

    public void setBusinessName(String businessName) {
        mBusinessName = businessName;
    }

    public String getBusinessOauthKey() {
        return mBusinessOauthKey;
    }

    public void setBusinessOauthKey(String businessOauthKey) {
        mBusinessOauthKey = businessOauthKey;
    }

    public String getBusinessWebsite() {
        return mBusinessWebsite;
    }

    public void setBusinessWebsite(String businessWebsite) {
        mBusinessWebsite = businessWebsite;
    }

    public String getFrid() {
        return mFrid;
    }

    public void setFrid(String frid) {
        mFrid = frid;
    }

    public String getIsAuthorized() {
        return mIsAuthorized;
    }

    public void setIsAuthorized(String isAuthorized) {
        mIsAuthorized = isAuthorized;
    }

    public String getProfilepic() {
        return mProfilepic;
    }

    public void setProfilepic(String profilepic) {
        mProfilepic = profilepic;
    }

    public String getUPassword() {
        return mUPassword;
    }

    public void setUPassword(String uPassword) {
        mUPassword = uPassword;
    }

    public String getURole() {
        return mURole;
    }

    public void setURole(String uRole) {
        mURole = uRole;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

}
