
package com.intellyze.feedback.Api.UserModel;


import com.google.gson.annotations.SerializedName;


public class Result {

    @SerializedName("Business_Id")
    private String mBusinessId;
    @SerializedName("frid")
    private String mFrid;
    @SerializedName("profilepic")
    private String mProfilepic;
    @SerializedName("u_role")
    private String mURole;
    @SerializedName("userId")
    private String mUserId;
    @SerializedName("username")
    private String mUsername;

    public String getBusinessId() {
        return mBusinessId;
    }

    public void setBusinessId(String BusinessId) {
        mBusinessId = BusinessId;
    }

    public String getFrid() {
        return mFrid;
    }

    public void setFrid(String frid) {
        mFrid = frid;
    }

    public String getProfilepic() {
        return mProfilepic;
    }

    public void setProfilepic(String profilepic) {
        mProfilepic = profilepic;
    }

    public String getURole() {
        return mURole;
    }

    public void setURole(String uRole) {
        mURole = uRole;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

}
