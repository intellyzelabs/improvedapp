package com.intellyze.feedback.Api.UserModel;

import android.util.Log;

import com.intellyze.feedback.Api.ApiClient;
import com.intellyze.feedback.Api.ApiInterfaces;
import com.intellyze.feedback.Api.FeedbackSurveyQuestions.FeedbackQuestionRequestModel;

import com.intellyze.feedback.Api.FeedbackSurveyQuestions.OnHttpRequestSurveyQuestion;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by intellyelabs on 23/01/18.
 */

public class HttpRequestForUserListing {
    OnHttpUserRequest onHttpUserRequest;
    public HttpRequestForUserListing(OnHttpUserRequest onHttpUserRequest) {
        this.onHttpUserRequest = onHttpUserRequest;
    }
    public void getList(ListUserItem feedbackQuestionRequestModel)
    {

        ApiInterfaces apiInterfaces= ApiClient.getClient().create(ApiInterfaces.class);
        Call<UserDetailsModel> call=apiInterfaces.ListUsers(feedbackQuestionRequestModel);
        call.enqueue(new Callback<UserDetailsModel>() {
            @Override
            public void onResponse(Call<UserDetailsModel> call, Response<UserDetailsModel> response) {
                if(response.message().equals("OK"))
                {

                    if(!response.body().getError()) {
                        onHttpUserRequest.onHttpUserDetailsSuccess(response.body().getMessage(), response.body().getResult());

                    }
                    else
                    {
                        onHttpUserRequest.onHttpUserDetailsFailed(response.body().getMessage());



                    }

                }
                else
                {
                    onHttpUserRequest.onHttpUserDetailsFailed("Server Error !");
                }
            }
            @Override
            public void onFailure(Call<UserDetailsModel> call, Throwable t) {
                onHttpUserRequest.onHttpUserDetailsFailed("Server Error !");
            }
        });
    }
}
