
package com.intellyze.feedback.Api.ListSurveyAnswerAdmin;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ResultDatum {

    @SerializedName("age")
    private String mAge;
    @SerializedName("answer_date")
    private String mAnswerDate;
    @SerializedName("Answer_Id")
    private String mAnswerId;
    @SerializedName("answer_time")
    private String mAnswerTime;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("Full_name")
    private String mFullName;
    @SerializedName("gender")
    private String mGender;
    @SerializedName("mobile_no")
    private String mMobileNo;
    @SerializedName("profession")
    private String mProfession;
    @SerializedName("survey_Id")
    private String mSurveyId;
    @SerializedName("survey_Title")
    private String mSurveyTitle;

    public String getAge() {
        return mAge;
    }

    public void setAge(String age) {
        mAge = age;
    }

    public String getAnswerDate() {
        return mAnswerDate;
    }

    public void setAnswerDate(String answerDate) {
        mAnswerDate = answerDate;
    }

    public String getAnswerId() {
        return mAnswerId;
    }

    public void setAnswerId(String AnswerId) {
        mAnswerId = AnswerId;
    }

    public String getAnswerTime() {
        return mAnswerTime;
    }

    public void setAnswerTime(String answerTime) {
        mAnswerTime = answerTime;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String FullName) {
        mFullName = FullName;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mobileNo) {
        mMobileNo = mobileNo;
    }

    public String getProfession() {
        return mProfession;
    }

    public void setProfession(String profession) {
        mProfession = profession;
    }

    public String getSurveyId() {
        return mSurveyId;
    }

    public void setSurveyId(String surveyId) {
        mSurveyId = surveyId;
    }

    public String getSurveyTitle() {
        return mSurveyTitle;
    }

    public void setSurveyTitle(String surveyTitle) {
        mSurveyTitle = surveyTitle;
    }

}
