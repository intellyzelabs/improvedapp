
package com.intellyze.feedback.Api.AnalysisModel;


import com.google.gson.annotations.SerializedName;

public class AnswersList {

    @SerializedName("Answr_value")
    private String mAnswrValue;

    public String getAnswrValue() {
        return mAnswrValue;
    }

    public void setAnswrValue(String AnswrValue) {
        mAnswrValue = AnswrValue;
    }

}
