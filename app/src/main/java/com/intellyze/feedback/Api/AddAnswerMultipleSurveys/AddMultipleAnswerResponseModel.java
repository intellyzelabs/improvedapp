
package com.intellyze.feedback.Api.AddAnswerMultipleSurveys;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AddMultipleAnswerResponseModel {

    @SerializedName("error")
    private Boolean mError;
    @SerializedName("Message")
    private String mMessage;
    @SerializedName("resultData")
    private List<ResultDatum> mResultData;

    public Boolean getError() {
        return mError;
    }

    public void setError(Boolean error) {
        mError = error;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String Message) {
        mMessage = Message;
    }

    public List<ResultDatum> getResultData() {
        return mResultData;
    }

    public void setResultData(List<ResultDatum> resultData) {
        mResultData = resultData;
    }

}
