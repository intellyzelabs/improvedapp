
package com.intellyze.feedback.Api.AddMultpileSurveys;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Row {

    @SerializedName("Qmultiple_grid_row_Id")
    private Long mQmultipleGridRowId;
    @SerializedName("row_name")
    private String mRowName;

    public Long getQmultipleGridRowId() {
        return mQmultipleGridRowId;
    }

    public void setQmultipleGridRowId(Long QmultipleGridRowId) {
        mQmultipleGridRowId = QmultipleGridRowId;
    }

    public String getRowName() {
        return mRowName;
    }

    public void setRowName(String rowName) {
        mRowName = rowName;
    }

}
