
package com.intellyze.feedback.Api.AddAnswerMultipleSurveys;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AddMultplieAnswerSurveyRequestModel {

    @SerializedName("Answers")
    private List<Answer> mAnswers;

    public List<Answer> getAnswers() {
        return mAnswers;
    }

    public void setAnswers(List<Answer> Answers) {
        mAnswers = Answers;
    }

}
