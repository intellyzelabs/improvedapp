package com.intellyze.feedback.Api.UpdateSurveyStatus;

/**
 * Created by INTELLYZE-202 on 05-01-2018.
 */

public interface OnHttpResponseForServerStatusUpdate {
    public void onHttpServerStatusUpdateSuccess(String Message);
    public void onHttpServerStatusUpdateFailed(String message);
}
