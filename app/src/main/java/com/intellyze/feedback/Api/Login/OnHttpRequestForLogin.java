package com.intellyze.feedback.Api.Login;

import java.util.List;

/**
 * Created by INTELLYZE-202 on 18-12-2017.
 */

public interface OnHttpRequestForLogin {
    public void onHttpLoginSuccess(String Message, List<Result> result);
    public void onHttpLoginFailed(String message);
}
