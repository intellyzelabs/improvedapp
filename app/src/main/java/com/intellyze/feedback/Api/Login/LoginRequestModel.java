
package com.intellyze.feedback.Api.Login;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class LoginRequestModel {

    @SerializedName("password")
    private String mPassword;
    @SerializedName("UserName")
    private String mUserName;

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String UserName) {
        mUserName = UserName;
    }

}
