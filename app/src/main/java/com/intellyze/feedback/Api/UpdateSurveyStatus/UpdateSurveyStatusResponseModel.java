
package com.intellyze.feedback.Api.UpdateSurveyStatus;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class UpdateSurveyStatusResponseModel {

    @SerializedName("error")
    private Boolean mError;
    @SerializedName("Message")
    private String mMessage;
    @SerializedName("resultData")
    private ResultData mResultData;

    public Boolean getError() {
        return mError;
    }

    public void setError(Boolean error) {
        mError = error;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String Message) {
        mMessage = Message;
    }

    public ResultData getResultData() {
        return mResultData;
    }

    public void setResultData(ResultData resultData) {
        mResultData = resultData;
    }

}
