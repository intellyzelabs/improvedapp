
package com.intellyze.feedback.Api.AnalysisModel;

import com.google.gson.annotations.SerializedName;


public class ColumnsResult {

    @SerializedName("Answer_Count")
    private String mAnswerCount;
    @SerializedName("Column_Name")
    private String mColumnName;

    public String getAnswerCount() {
        return mAnswerCount;
    }

    public void setAnswerCount(String AnswerCount) {
        mAnswerCount = AnswerCount;
    }

    public String getColumnName() {
        return mColumnName;
    }

    public void setColumnName(String ColumnName) {
        mColumnName = ColumnName;
    }

}
