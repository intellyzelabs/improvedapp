
package com.intellyze.feedback.Api.AddMultpileSurveys;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SurveyQuestion {

    @SerializedName("isOther")
    private Long mIsOther;
    @SerializedName("Question_Id")
    private Long mQuestionId;
    @SerializedName("QuestionImage")
    private String mQuestionImage;
    @SerializedName("QuestionMore")
    private List<com.intellyze.feedback.Api.AddMultpileSurveys.QuestionMore> mQuestionMore;
    @SerializedName("QuestionName")
    private String mQuestionName;
    @SerializedName("QuestionNumber")
    private Long mQuestionNumber;
    @SerializedName("QuestionRequired")
    private Long mQuestionRequired;
    @SerializedName("QuestionType")
    private String mQuestionType;

    public Long getIsOther() {
        return mIsOther;
    }

    public void setIsOther(Long isOther) {
        mIsOther = isOther;
    }

    public Long getQuestionId() {
        return mQuestionId;
    }

    public void setQuestionId(Long QuestionId) {
        mQuestionId = QuestionId;
    }

    public String getQuestionImage() {
        return mQuestionImage;
    }

    public void setQuestionImage(String QuestionImage) {
        mQuestionImage = QuestionImage;
    }

    public List<com.intellyze.feedback.Api.AddMultpileSurveys.QuestionMore> getQuestionMore() {
        return mQuestionMore;
    }

    public void setQuestionMore(List<com.intellyze.feedback.Api.AddMultpileSurveys.QuestionMore> QuestionMore) {
        mQuestionMore = QuestionMore;
    }

    public String getQuestionName() {
        return mQuestionName;
    }

    public void setQuestionName(String QuestionName) {
        mQuestionName = QuestionName;
    }

    public Long getQuestionNumber() {
        return mQuestionNumber;
    }

    public void setQuestionNumber(Long QuestionNumber) {
        mQuestionNumber = QuestionNumber;
    }

    public Long getQuestionRequired() {
        return mQuestionRequired;
    }

    public void setQuestionRequired(Long QuestionRequired) {
        mQuestionRequired = QuestionRequired;
    }

    public String getQuestionType() {
        return mQuestionType;
    }

    public void setQuestionType(String QuestionType) {
        mQuestionType = QuestionType;
    }

}
