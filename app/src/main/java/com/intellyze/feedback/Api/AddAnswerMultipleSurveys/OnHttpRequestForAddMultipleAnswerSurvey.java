package com.intellyze.feedback.Api.AddAnswerMultipleSurveys;

/**
 * Created by INTELLYZE-202 on 05-01-2018.
 */

public interface OnHttpRequestForAddMultipleAnswerSurvey {
    public void onHttpAddMultipleAnswerSurveySuccess(String Message);
    public void onHttpAddMultipleAnswerSurveyFailed(String message);
}
