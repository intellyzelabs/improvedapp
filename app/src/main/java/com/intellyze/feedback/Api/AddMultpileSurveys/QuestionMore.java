
package com.intellyze.feedback.Api.AddMultpileSurveys;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class QuestionMore {

    @SerializedName("linearquestion")
    private List<Linearquestion> mLinearquestion;
    @SerializedName("matrixquestion")
    private List<Matrixquestion> mMatrixquestion;
    @SerializedName("multiplechoicesingle")
    private List<Multiplechoicesingle> mMultiplechoicesingle;

    public List<Linearquestion> getLinearquestion() {
        return mLinearquestion;
    }

    public void setLinearquestion(List<Linearquestion> linearquestion) {
        mLinearquestion = linearquestion;
    }

    public List<Matrixquestion> getMatrixquestion() {
        return mMatrixquestion;
    }

    public void setMatrixquestion(List<Matrixquestion> matrixquestion) {
        mMatrixquestion = matrixquestion;
    }

    public List<Multiplechoicesingle> getMultiplechoicesingle() {
        return mMultiplechoicesingle;
    }

    public void setMultiplechoicesingle(List<Multiplechoicesingle> multiplechoicesingle) {
        mMultiplechoicesingle = multiplechoicesingle;
    }

}
