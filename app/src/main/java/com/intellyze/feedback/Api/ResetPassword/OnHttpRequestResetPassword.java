package com.intellyze.feedback.Api.ResetPassword;

/**
 * Created by INTELLYZE-202 on 01-01-2018.
 */

public interface OnHttpRequestResetPassword {
    public void onHttpResetPassSuccess(String Message);
    public void onHttpResetPassFailed(String message);
}
