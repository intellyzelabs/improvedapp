
package com.intellyze.feedback.Api.AnswerDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LinearQuestionInfo {

    @SerializedName("Qlinear_Id")
    @Expose
    private String qlinearId;
    @SerializedName("Question_Id")
    @Expose
    private String questionId;
    @SerializedName("start_text")
    @Expose
    private String startText;
    @SerializedName("start_value")
    @Expose
    private String startValue;
    @SerializedName("end_text")
    @Expose
    private String endText;
    @SerializedName("end_value")
    @Expose
    private String endValue;

    public String getQlinearId() {
        return qlinearId;
    }

    public void setQlinearId(String qlinearId) {
        this.qlinearId = qlinearId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getStartText() {
        return startText;
    }

    public void setStartText(String startText) {
        this.startText = startText;
    }

    public String getStartValue() {
        return startValue;
    }

    public void setStartValue(String startValue) {
        this.startValue = startValue;
    }

    public String getEndText() {
        return endText;
    }

    public void setEndText(String endText) {
        this.endText = endText;
    }

    public String getEndValue() {
        return endValue;
    }

    public void setEndValue(String endValue) {
        this.endValue = endValue;
    }

}
