package com.intellyze.feedback.Api.AddMultpileSurveys;

import com.intellyze.feedback.Api.ApiClient;
import com.intellyze.feedback.Api.ApiInterfaces;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by INTELLYZE-202 on 01-01-2018.
 */

public class HttpRequestForAddMultipleSurvey {
    OnHttpRequestAddMultipleSurvey onHttpRequestAddMultipleSurvey;

    public HttpRequestForAddMultipleSurvey(OnHttpRequestAddMultipleSurvey onHttpRequestAddMultipleSurvey) {
        this.onHttpRequestAddMultipleSurvey = onHttpRequestAddMultipleSurvey;
    }

    public void AddMultipleSurveyList(AddMultipleSurveyRequestModel addMultipleSurveyRequestModel) {

        ApiInterfaces apiInterfaces = ApiClient.getClient().create(ApiInterfaces.class);
        Call<AddMultipleSurveyResponseModel> call = apiInterfaces.Add_MultipleSurvey(addMultipleSurveyRequestModel);
        call.enqueue(new Callback<AddMultipleSurveyResponseModel>() {
            @Override
            public void onResponse(Call<AddMultipleSurveyResponseModel> call, Response<AddMultipleSurveyResponseModel> response) {
                if (response.message().equals("OK")) {
                    if (!response.body().getError()) {
                        onHttpRequestAddMultipleSurvey.onHttpAddMultipleSurveySuccess(response.body().getMessage());
                    } else {
                        onHttpRequestAddMultipleSurvey.onHttpAddMultipleSurveyFailed(response.body().getMessage());
                    }
                } else {
                    onHttpRequestAddMultipleSurvey.onHttpAddMultipleSurveyFailed("Server Error !");
                }
            }

            @Override
            public void onFailure(Call<AddMultipleSurveyResponseModel> call, Throwable t) {
                onHttpRequestAddMultipleSurvey.onHttpAddMultipleSurveyFailed("Server Error !");
            }
        });
    }
}
