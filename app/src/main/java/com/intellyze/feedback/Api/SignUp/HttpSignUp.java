package com.intellyze.feedback.Api.SignUp;

import com.intellyze.feedback.Api.ApiClient;
import com.intellyze.feedback.Api.ApiInterfaces;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by INTELLYZE-202 on 18-12-2017.
 */

public class HttpSignUp {
    OnHttpRespoceForSignUp onHttpRespoceForSignUp;
    public HttpSignUp(OnHttpRespoceForSignUp onHttpRespoceForSignUp) {
        this.onHttpRespoceForSignUp = onHttpRespoceForSignUp;
    }
    public void signUpp(SignUpRequestModel signUpRequestModel)
    {
        ApiInterfaces apiInterfaces= ApiClient.getClient().create(ApiInterfaces.class);
        Call<SignUpResponseModel> call=apiInterfaces.signUp(signUpRequestModel);
        call.enqueue(new Callback<SignUpResponseModel>() {
            @Override
            public void onResponse(Call<SignUpResponseModel> call, Response<SignUpResponseModel> response) {
                if(response.message().equals("OK"))
                {
                    if(!response.body().getError())
                    {
                        onHttpRespoceForSignUp.onHttpSignUpSuccess(response.body().getMessage());
                    }else {
                        onHttpRespoceForSignUp.onHttpSignUpFailed(response.body().getMessage());
                    }
                }
                else
                {
                    onHttpRespoceForSignUp.onHttpSignUpFailed("Server Error !");
                }
            }
            @Override
            public void onFailure(Call<SignUpResponseModel> call, Throwable t) {
                onHttpRespoceForSignUp.onHttpSignUpFailed("Server Error !");
            }
        });
    }
}
