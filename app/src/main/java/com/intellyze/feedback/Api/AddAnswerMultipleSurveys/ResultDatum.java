
package com.intellyze.feedback.Api.AddAnswerMultipleSurveys;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ResultDatum {

    @SerializedName("ErrorCode")
    private String mErrorCode;
    @SerializedName("Result")
    private String mResult;

    public String getErrorCode() {
        return mErrorCode;
    }

    public void setErrorCode(String ErrorCode) {
        mErrorCode = ErrorCode;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String Result) {
        mResult = Result;
    }

}
