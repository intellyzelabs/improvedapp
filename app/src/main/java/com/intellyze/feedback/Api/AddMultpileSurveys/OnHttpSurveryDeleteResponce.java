package com.intellyze.feedback.Api.AddMultpileSurveys;


/**
 * Created by intellyelabs on 23/01/18.
 */

public interface OnHttpSurveryDeleteResponce {

        public void onHttpSurveryDeleteSuccess(String Message, DeleteSurveryParameters surveyid);
        public void onHttpSurveryDeleteFailed(String message);

}
