package com.intellyze.feedback.Api.UserModel;


import java.util.List;

/**
 * Created by intellyelabs on 23/01/18.
 */

public interface OnHttpUserDeleteResponce {

        public void onHttpUserDeleteSuccess(String Message);
        public void onHttpUserDeleteFailed(String message);

}
