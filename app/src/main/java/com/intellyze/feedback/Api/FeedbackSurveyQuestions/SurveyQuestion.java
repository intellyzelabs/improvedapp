
package com.intellyze.feedback.Api.FeedbackSurveyQuestions;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SurveyQuestion {

    @SerializedName("isOther")
    private String isOther;
    @SerializedName("isRequired")
    private String mIsRequired;
    @SerializedName("Question_Id")
    private String mQuestionId;
    @SerializedName("Question_Image")
    private String mQuestionImage;
    @SerializedName("QuestionMore")
    private com.intellyze.feedback.Api.FeedbackSurveyQuestions.QuestionMore mQuestionMore;
    @SerializedName("QuestionNumber")
    private String mQuestionNumber;
    @SerializedName("Question_Title")
    private String mQuestionTitle;
    @SerializedName("Question_Type")
    private String mQuestionType;
    @SerializedName("survey_Id")
    private String mSurveyId;

    public String getIsOther() {
        return isOther;
    }

    public void setIsOther(String isOther) {
        this.isOther = isOther;
    }

    public String getIsRequired() {
        return mIsRequired;
    }

    public void setIsRequired(String isRequired) {
        mIsRequired = isRequired;
    }

    public String getQuestionId() {
        return mQuestionId;
    }

    public void setQuestionId(String QuestionId) {
        mQuestionId = QuestionId;
    }

    public String getQuestionImage() {
        return mQuestionImage;
    }

    public void setQuestionImage(String QuestionImage) {
        mQuestionImage = QuestionImage;
    }

    public com.intellyze.feedback.Api.FeedbackSurveyQuestions.QuestionMore getQuestionMore() {
        return mQuestionMore;
    }

    public void setQuestionMore(com.intellyze.feedback.Api.FeedbackSurveyQuestions.QuestionMore QuestionMore) {
        mQuestionMore = QuestionMore;
    }

    public String getQuestionNumber() {
        return mQuestionNumber;
    }

    public void setQuestionNumber(String QuestionNumber) {
        mQuestionNumber = QuestionNumber;
    }

    public String getQuestionTitle() {
        return mQuestionTitle;
    }

    public void setQuestionTitle(String QuestionTitle) {
        mQuestionTitle = QuestionTitle;
    }

    public String getQuestionType() {
        return mQuestionType;
    }

    public void setQuestionType(String QuestionType) {
        mQuestionType = QuestionType;
    }

    public String getSurveyId() {
        return mSurveyId;
    }

    public void setSurveyId(String surveyId) {
        mSurveyId = surveyId;
    }

}
