
package com.intellyze.feedback.Api.AddAnswerMultipleSurveys;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AnswersList {

    @SerializedName("Answr_value")
    private String mAnswrValue;
    @SerializedName("Answr_value_id")
    private String mAnswrValueId;
    @SerializedName("Answr_value_row_id")
    private String mAnswrValueRowId;
    @SerializedName("Question_Id")
    private String mQuestionId;

    public String getAnswrValue() {
        return mAnswrValue;
    }

    public void setAnswrValue(String AnswrValue) {
        mAnswrValue = AnswrValue;
    }

    public String getAnswrValueId() {
        return mAnswrValueId;
    }

    public void setAnswrValueId(String AnswrValueId) {
        mAnswrValueId = AnswrValueId;
    }

    public String getAnswrValueRowId() {
        return mAnswrValueRowId;
    }

    public void setAnswrValueRowId(String AnswrValueRowId) {
        mAnswrValueRowId = AnswrValueRowId;
    }

    public String getQuestionId() {
        return mQuestionId;
    }

    public void setQuestionId(String QuestionId) {
        mQuestionId = QuestionId;
    }

}
