
package com.intellyze.feedback.Api.SignUp;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SignUpResponseModel {

    @SerializedName("error")
    private Boolean mError;
    @SerializedName("inputData")
    private InputData mInputData;
    @SerializedName("Message")
    private String mMessage;
    @SerializedName("resultData")
    private List<ResultDatum> mResultData;

    public Boolean getError() {
        return mError;
    }

    public void setError(Boolean error) {
        mError = error;
    }

    public InputData getInputData() {
        return mInputData;
    }

    public void setInputData(InputData inputData) {
        mInputData = inputData;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String Message) {
        mMessage = Message;
    }

    public List<ResultDatum> getResultData() {
        return mResultData;
    }

    public void setResultData(List<ResultDatum> resultData) {
        mResultData = resultData;
    }

}
