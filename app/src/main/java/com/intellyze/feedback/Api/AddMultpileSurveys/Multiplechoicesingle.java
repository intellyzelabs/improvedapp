
package com.intellyze.feedback.Api.AddMultpileSurveys;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Multiplechoicesingle {

    @SerializedName("questions_options_Id")
    private Long mQuestionsOptionsId;
    @SerializedName("textChoice")
    private String mTextChoice;

    public Long getQuestionsOptionsId() {
        return mQuestionsOptionsId;
    }

    public void setQuestionsOptionsId(Long questionsOptionsId) {
        mQuestionsOptionsId = questionsOptionsId;
    }

    public String getTextChoice() {
        return mTextChoice;
    }

    public void setTextChoice(String textChoice) {
        mTextChoice = textChoice;
    }

}
