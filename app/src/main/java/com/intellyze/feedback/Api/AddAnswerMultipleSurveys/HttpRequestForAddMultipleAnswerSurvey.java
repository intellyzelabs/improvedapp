package com.intellyze.feedback.Api.AddAnswerMultipleSurveys;

import com.intellyze.feedback.Api.AddMultpileSurveys.AddMultipleSurveyRequestModel;
import com.intellyze.feedback.Api.AddMultpileSurveys.AddMultipleSurveyResponseModel;
import com.intellyze.feedback.Api.AddMultpileSurveys.OnHttpRequestAddMultipleSurvey;
import com.intellyze.feedback.Api.ApiClient;
import com.intellyze.feedback.Api.ApiInterfaces;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by INTELLYZE-202 on 05-01-2018.
 */

public class HttpRequestForAddMultipleAnswerSurvey {
    OnHttpRequestForAddMultipleAnswerSurvey onHttpRequestForAddMultipleAnswerSurvey;

    public HttpRequestForAddMultipleAnswerSurvey(OnHttpRequestForAddMultipleAnswerSurvey onHttpRequestForAddMultipleAnswerSurvey) {
        this.onHttpRequestForAddMultipleAnswerSurvey = onHttpRequestForAddMultipleAnswerSurvey;
    }

    public void AddMultipleAnswerSurveyList(AddMultplieAnswerSurveyRequestModel addMultipleSurveyRequestModel) {

        ApiInterfaces apiInterfaces = ApiClient.getClient().create(ApiInterfaces.class);
        Call<AddMultipleAnswerResponseModel> call = apiInterfaces.Add_MultipleAnswerSurvey(addMultipleSurveyRequestModel);
        call.enqueue(new Callback<AddMultipleAnswerResponseModel>() {
            @Override
            public void onResponse(Call<AddMultipleAnswerResponseModel> call, Response<AddMultipleAnswerResponseModel> response) {
                if (response.message().equals("OK")) {
                    if (!response.body().getError()) {
                        onHttpRequestForAddMultipleAnswerSurvey.onHttpAddMultipleAnswerSurveySuccess(response.body().getMessage());
                    } else {
                        onHttpRequestForAddMultipleAnswerSurvey.onHttpAddMultipleAnswerSurveyFailed(response.body().getMessage());
                    }
                } else {
                    onHttpRequestForAddMultipleAnswerSurvey.onHttpAddMultipleAnswerSurveyFailed("Server Error !");
                }
            }

            @Override
            public void onFailure(Call<AddMultipleAnswerResponseModel> call, Throwable t) {
                onHttpRequestForAddMultipleAnswerSurvey.onHttpAddMultipleAnswerSurveyFailed("Server Error !");
            }
        });
    }
}
