package com.intellyze.feedback.Api.ListSurveyAnswerAdmin;

import android.util.Log;

import com.intellyze.feedback.Api.AddAnswerMultipleSurveys.AddMultplieAnswerSurveyRequestModel;
import com.intellyze.feedback.Api.AddAnswerMultipleSurveys.OnHttpRequestForAddMultipleAnswerSurvey;
import com.intellyze.feedback.Api.ApiClient;
import com.intellyze.feedback.Api.ApiInterfaces;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by INTELLYZE-202 on 05-01-2018.
 */

public class HttpRequestForLiostAnswerSurvey {
    OnHttpRequestForListSurveyAnswerSurvey onHttpRequestForAddMultipleAnswerSurvey;

    public HttpRequestForLiostAnswerSurvey(OnHttpRequestForListSurveyAnswerSurvey onHttpRequestForAddMultipleAnswerSurvey) {
        this.onHttpRequestForAddMultipleAnswerSurvey = onHttpRequestForAddMultipleAnswerSurvey;
    }

    public void getAnswerSurveyList(SurveyAnswerAdminListBean addMultipleSurveyRequestModel) {

        ApiInterfaces apiInterfaces = ApiClient.getClient().create(ApiInterfaces.class);
        Call<SurveyAnswerListModelAdmin> call = apiInterfaces.List_Survey_AnswersByUser(addMultipleSurveyRequestModel);
        call.enqueue(new Callback<SurveyAnswerListModelAdmin>() {
            @Override
            public void onResponse(Call<SurveyAnswerListModelAdmin> call, Response<SurveyAnswerListModelAdmin> response) {
                if (response.message().equals("OK")) {
                    if (!response.body().getError()) {
                        onHttpRequestForAddMultipleAnswerSurvey.onHttpAddForListSurveyAnswerSurveySuccess(response.body().getResultData(),response.body().getMessage());
                    } else {
                        onHttpRequestForAddMultipleAnswerSurvey.onHttpAddForListSurveyAnswerSurveyFailed(response.body().getMessage());
                    }
                } else {
                    onHttpRequestForAddMultipleAnswerSurvey.onHttpAddForListSurveyAnswerSurveyFailed("Server Error !");
                }
            }

            @Override
            public void onFailure(Call<SurveyAnswerListModelAdmin> call, Throwable t) {
                onHttpRequestForAddMultipleAnswerSurvey.onHttpAddForListSurveyAnswerSurveyFailed("Server Error !");
            }
        });
    }
}
