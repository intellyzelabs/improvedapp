
package com.intellyze.feedback.Api.AnalysisModel;


import com.google.gson.annotations.SerializedName;


public class AnalysisDataModel {

    @SerializedName("survey_Id")
    private String mSurveyId;

    public String getSurveyId() {
        return mSurveyId;
    }

    public void setSurveyId(String surveyId) {
        mSurveyId = surveyId;
    }

}
