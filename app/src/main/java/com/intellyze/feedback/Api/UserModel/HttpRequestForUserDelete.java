package com.intellyze.feedback.Api.UserModel;

import android.util.Log;

import com.intellyze.feedback.Api.ApiClient;
import com.intellyze.feedback.Api.ApiInterfaces;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by intellyelabs on 23/01/18.
 */

public class HttpRequestForUserDelete {
    OnHttpUserDeleteResponce onHttpUserRequest;
    public HttpRequestForUserDelete(OnHttpUserDeleteResponce onHttpUserRequest) {
        this.onHttpUserRequest = onHttpUserRequest;
    }
    public void getList(UserDeleteModel feedbackQuestionRequestModel)
    {

        ApiInterfaces apiInterfaces= ApiClient.getClient().create(ApiInterfaces.class);
        Call<UserDeleteResponce> call=apiInterfaces.deleteUserAccount(feedbackQuestionRequestModel);
        call.enqueue(new Callback<UserDeleteResponce>() {
            @Override
            public void onResponse(Call<UserDeleteResponce> call, Response<UserDeleteResponce> response) {
                if(response.message().equals("OK"))
                {

                    if(!response.body().getError()) {
                        onHttpUserRequest.onHttpUserDeleteSuccess(response.body().getMessage());

                    }
                    else
                    {
                        onHttpUserRequest.onHttpUserDeleteFailed(response.body().getMessage());



                    }

                }
                else
                {
                    onHttpUserRequest.onHttpUserDeleteFailed("Server Error !");
                }
            }
            @Override
            public void onFailure(Call<UserDeleteResponce> call, Throwable t) {
                onHttpUserRequest.onHttpUserDeleteFailed("Server Error !");
            }
        });
    }
}
