
package com.intellyze.feedback.Api.AnalysisModel;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class QuestionMore {

    @SerializedName("Answer")
    private List<com.intellyze.feedback.Api.AnalysisModel.Answer> mAnswer;
    @SerializedName("AnswerGrid")
    private List<com.intellyze.feedback.Api.AnalysisModel.AnswerGrid> mAnswerGrid;
    @SerializedName("Answers_List")
    private List<com.intellyze.feedback.Api.AnalysisModel.AnswersList> mAnswersList;
    @SerializedName("isOther")
    private String mIsOther;
    @SerializedName("isRequired")
    private String mIsRequired;
    @SerializedName("Question_Id")
    private String mQuestionId;
    @SerializedName("Question_Image")
    private String mQuestionImage;
    @SerializedName("QuestionNumber")
    private String mQuestionNumber;
    @SerializedName("Question_Title")
    private String mQuestionTitle;
    @SerializedName("Question_Type")
    private String mQuestionType;
    @SerializedName("survey_Id")
    private String mSurveyId;

    public List<com.intellyze.feedback.Api.AnalysisModel.Answer> getAnswer() {
        return mAnswer;
    }

    public void setAnswer(List<com.intellyze.feedback.Api.AnalysisModel.Answer> Answer) {
        mAnswer = Answer;
    }

    public List<com.intellyze.feedback.Api.AnalysisModel.AnswerGrid> getAnswerGrid() {
        return mAnswerGrid;
    }

    public void setAnswerGrid(List<com.intellyze.feedback.Api.AnalysisModel.AnswerGrid> AnswerGrid) {
        mAnswerGrid = AnswerGrid;
    }

    public List<com.intellyze.feedback.Api.AnalysisModel.AnswersList> getAnswersList() {
        return mAnswersList;
    }

    public void setAnswersList(List<com.intellyze.feedback.Api.AnalysisModel.AnswersList> AnswersList) {
        mAnswersList = AnswersList;
    }

    public String getIsOther() {
        return mIsOther;
    }

    public void setIsOther(String isOther) {
        mIsOther = isOther;
    }

    public String getIsRequired() {
        return mIsRequired;
    }

    public void setIsRequired(String isRequired) {
        mIsRequired = isRequired;
    }

    public String getQuestionId() {
        return mQuestionId;
    }

    public void setQuestionId(String QuestionId) {
        mQuestionId = QuestionId;
    }

    public String getQuestionImage() {
        return mQuestionImage;
    }

    public void setQuestionImage(String QuestionImage) {
        mQuestionImage = QuestionImage;
    }

    public String getQuestionNumber() {
        return mQuestionNumber;
    }

    public void setQuestionNumber(String QuestionNumber) {
        mQuestionNumber = QuestionNumber;
    }

    public String getQuestionTitle() {
        return mQuestionTitle;
    }

    public void setQuestionTitle(String QuestionTitle) {
        mQuestionTitle = QuestionTitle;
    }

    public String getQuestionType() {
        return mQuestionType;
    }

    public void setQuestionType(String QuestionType) {
        mQuestionType = QuestionType;
    }

    public String getSurveyId() {
        return mSurveyId;
    }

    public void setSurveyId(String surveyId) {
        mSurveyId = surveyId;
    }

}
