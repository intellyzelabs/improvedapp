
package com.intellyze.feedback.Api.AnalysisModel;


import com.google.gson.annotations.SerializedName;


public class Answer {

    @SerializedName("Option_Count")
    private String mOptionCount;
    @SerializedName("Option_Name")
    private String mOptionName;

    public String getOptionCount() {
        return mOptionCount;
    }

    public void setOptionCount(String OptionCount) {
        mOptionCount = OptionCount;
    }

    public String getOptionName() {
        return mOptionName;
    }

    public void setOptionName(String OptionName) {
        mOptionName = OptionName;
    }

}
