
package com.intellyze.feedback.Api.UserModel;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class UserDetailsModel {

    @SerializedName("error")
    private Boolean mError;
    @SerializedName("Message")
    private String mMessage;
    @SerializedName("result")
    private List<Result> mResult;

    public Boolean getError() {
        return mError;
    }

    public void setError(Boolean error) {
        mError = error;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String Message) {
        mMessage = Message;
    }

    public List<Result> getResult() {
        return mResult;
    }

    public void setResult(List<Result> result) {
        mResult = result;
    }

}
