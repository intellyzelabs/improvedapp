package com.intellyze.feedback.Api.AddMultpileSurveys;

/**
 * Created by INTELLYZE-202 on 01-01-2018.
 */

public interface OnHttpRequestAddMultipleSurvey {
    public void onHttpAddMultipleSurveySuccess(String Message);
    public void onHttpAddMultipleSurveyFailed(String message);
}
