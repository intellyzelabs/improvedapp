
package com.intellyze.feedback.Api.AddMultpileSurveys;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class DeleteSurveryParameters {

    @SerializedName("Business_Id")
    private String mBusinessId;
    @SerializedName("survey_Id")
    private String mSurveyId;

    public String getBusinessId() {
        return mBusinessId;
    }

    public void setBusinessId(String BusinessId) {
        mBusinessId = BusinessId;
    }

    public String getSurveyId() {
        return mSurveyId;
    }

    public void setSurveyId(String surveyId) {
        mSurveyId = surveyId;
    }

}
