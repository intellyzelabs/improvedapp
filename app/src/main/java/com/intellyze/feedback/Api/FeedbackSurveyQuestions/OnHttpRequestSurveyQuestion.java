package com.intellyze.feedback.Api.FeedbackSurveyQuestions;

import com.intellyze.feedback.Api.Login.Result;

import java.util.List;

/**
 * Created by INTELLYZE-202 on 26-12-2017.
 */

public interface OnHttpRequestSurveyQuestion {
    public void onHttpSurveyQuestionSuccess(String Message, List<SurveyDatum> result);
    public void onHttpSurveyQuestionFailed(String message);
}
