package com.intellyze.feedback.Api;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by INTELLYZE-202 on 18-12-2017.
 */

public class ApiClient {
    public static final String BASE_URL="http://intellyze.hol.es/improved/newapi/public/";
    public static Retrofit retrofit=null;
    public static Retrofit getClient()
    {
        if (retrofit==null)
        {
            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(100, TimeUnit.SECONDS)
                    .readTimeout(100,TimeUnit.SECONDS).build();
            retrofit=new Retrofit.Builder().baseUrl(BASE_URL).client(client).addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }

}