
package com.intellyze.feedback.Api.UserModel;

import com.google.gson.annotations.SerializedName;


public class ListUserItem {

    @SerializedName("BusinessId")
    private Long mBusinessId;

    public Long getBusinessId() {
        return mBusinessId;
    }

    public void setBusinessId(Long BusinessId) {
        mBusinessId = BusinessId;
    }

}
