
package com.intellyze.feedback.Api.AnalysisModel;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class SurveyData {

    @SerializedName("QuestionMore")
    private List<com.intellyze.feedback.Api.AnalysisModel.QuestionMore> mQuestionMore;

    public List<com.intellyze.feedback.Api.AnalysisModel.QuestionMore> getQuestionMore() {
        return mQuestionMore;
    }

    public void setQuestionMore(List<com.intellyze.feedback.Api.AnalysisModel.QuestionMore> QuestionMore) {
        mQuestionMore = QuestionMore;
    }

}
