
package com.intellyze.feedback.Api.FeedbackSurveyQuestions;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class FeedbackQuestionRequestModel {

    @SerializedName("Question_Id")
    private String mQuestionId;

    public String getBusiness_Id() {
        return Business_Id;
    }

    public void setBusiness_Id(String business_Id) {
        Business_Id = business_Id;
    }

    @SerializedName("Business_Id")

    private String Business_Id;
    @SerializedName("survey_Id")
    private String mSurveyId;

    public String getQuestionId() {
        return mQuestionId;
    }

    public void setQuestionId(String QuestionId) {
        mQuestionId = QuestionId;
    }

    public String getSurveyId() {
        return mSurveyId;
    }

    public void setSurveyId(String surveyId) {
        mSurveyId = surveyId;
    }



}
