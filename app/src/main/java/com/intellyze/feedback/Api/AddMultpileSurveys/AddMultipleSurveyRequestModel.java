
package com.intellyze.feedback.Api.AddMultpileSurveys;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AddMultipleSurveyRequestModel {

    @SerializedName("Surveys")
    private List<Survey> mSurveys;

    public List<Survey> getSurveys() {
        return mSurveys;
    }

    public void setSurveys(List<Survey> Surveys) {
        mSurveys = Surveys;
    }

}
