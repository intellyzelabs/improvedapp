package com.intellyze.feedback.Api.UpdateSurveyStatus;
import android.util.Log;
import com.intellyze.feedback.Api.ApiClient;
import com.intellyze.feedback.Api.ApiInterfaces;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
/**
 * Created by INTELLYZE-202 on 05-01-2018.
 */
public class HttpResponseForServerStatusUpdate
{

    OnHttpResponseForServerStatusUpdate onHttpResponseForServerStatusUpdate;
    public HttpResponseForServerStatusUpdate(OnHttpResponseForServerStatusUpdate onHttpResponseForServerStatusUpdate)
    {
        this.onHttpResponseForServerStatusUpdate = onHttpResponseForServerStatusUpdate;
    }
    public void UpdateStaus(int surveyId, String surveyStatus)
    {
        ApiInterfaces apiClient = ApiClient.getClient().create(ApiInterfaces.class);
        Call<UpdateSurveyStatusResponseModel> call = apiClient.getStatusResponse(surveyId, surveyStatus);
        call.enqueue(new Callback<UpdateSurveyStatusResponseModel>() {
            @Override
            public void onResponse(Call<UpdateSurveyStatusResponseModel> call, Response<UpdateSurveyStatusResponseModel> response)
            {
                String responce_string = response.toString();
                Log.d("HttpRequestForLogin", responce_string);
                if (response.message().equals("OK"))
                {
                    if (!response.body().getError()) {
                        onHttpResponseForServerStatusUpdate.onHttpServerStatusUpdateSuccess(response.body().getMessage());
                    } else
                    {
                        onHttpResponseForServerStatusUpdate.onHttpServerStatusUpdateFailed(response.body().getMessage());
                    }

                }
                else
                {
                    onHttpResponseForServerStatusUpdate.onHttpServerStatusUpdateFailed("Server Error !");
                }
            }
            @Override
            public void onFailure(Call<UpdateSurveyStatusResponseModel> call, Throwable t)
            {
                onHttpResponseForServerStatusUpdate.onHttpServerStatusUpdateFailed("Server Error !");
            }
        });
    }

}