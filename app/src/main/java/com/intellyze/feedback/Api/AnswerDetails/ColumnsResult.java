
package com.intellyze.feedback.Api.AnswerDetails;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ColumnsResult {

    @SerializedName("Column_Name")
    private String mColumnName;
    @SerializedName("isAnswer")
    private String mIsAnswer;

    public String getColumnName() {
        return mColumnName;
    }

    public void setColumnName(String ColumnName) {
        mColumnName = ColumnName;
    }

    public String getIsAnswer() {
        return mIsAnswer;
    }

    public void setIsAnswer(String isAnswer) {
        mIsAnswer = isAnswer;
    }

}
