package com.intellyze.feedback.Api.AddUserModel;


/**
 * Created by intellyelabs on 23/01/18.
 */

public interface OnHttpAddUserRequest {

        public void onHttpAddUserDetailsSuccess(String Message);
        public void onHttpAddUserDetailsFailed(String message);

}
