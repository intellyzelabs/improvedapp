
package com.intellyze.feedback.Api.AddMultpileSurveys;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Column {

    @SerializedName("column_name")
    private String mColumnName;
    @SerializedName("Qmultiple_grid_col_Id")
    private Long mQmultipleGridColId;

    public String getColumnName() {
        return mColumnName;
    }

    public void setColumnName(String columnName) {
        mColumnName = columnName;
    }

    public Long getQmultipleGridColId() {
        return mQmultipleGridColId;
    }

    public void setQmultipleGridColId(Long QmultipleGridColId) {
        mQmultipleGridColId = QmultipleGridColId;
    }

}
