
package com.intellyze.feedback.Api.AnswerDetails;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AnswerGrid {

    @SerializedName("ColumnsResult")
    private List<com.intellyze.feedback.Api.AnswerDetails.ColumnsResult> mColumnsResult;
    @SerializedName("RowTitle")
    private String mRowTitle;

    public List<com.intellyze.feedback.Api.AnswerDetails.ColumnsResult> getColumnsResult() {
        return mColumnsResult;
    }

    public void setColumnsResult(List<com.intellyze.feedback.Api.AnswerDetails.ColumnsResult> ColumnsResult) {
        mColumnsResult = ColumnsResult;
    }

    public String getRowTitle() {
        return mRowTitle;
    }

    public void setRowTitle(String RowTitle) {
        mRowTitle = RowTitle;
    }

}
