
package com.intellyze.feedback.Api.AddUserModel;


import com.google.gson.annotations.SerializedName;


public class AdduserModel {

    @SerializedName("Business_Id")
    private Long mBusinessId;
    @SerializedName("frid")
    private String mFrid;
    @SerializedName("profilepic")
    private String mProfilepic;
    @SerializedName("upassword")
    private String mUpassword;
    @SerializedName("urole")
    private String mUrole;
    @SerializedName("userId")
    private Long mUserId;
    @SerializedName("username")
    private String mUsername;

    public Long getBusinessId() {
        return mBusinessId;
    }

    public void setBusinessId(Long BusinessId) {
        mBusinessId = BusinessId;
    }

    public String getFrid() {
        return mFrid;
    }

    public void setFrid(String frid) {
        mFrid = frid;
    }

    public String getProfilepic() {
        return mProfilepic;
    }

    public void setProfilepic(String profilepic) {
        mProfilepic = profilepic;
    }

    public String getUpassword() {
        return mUpassword;
    }

    public void setUpassword(String upassword) {
        mUpassword = upassword;
    }

    public String getUrole() {
        return mUrole;
    }

    public void setUrole(String urole) {
        mUrole = urole;
    }

    public Long getUserId() {
        return mUserId;
    }

    public void setUserId(Long userId) {
        mUserId = userId;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }

}
