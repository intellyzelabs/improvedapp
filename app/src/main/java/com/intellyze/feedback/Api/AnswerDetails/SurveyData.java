
package com.intellyze.feedback.Api.AnswerDetails;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SurveyData {

    @SerializedName("Details")
    private List<Detail> mDetails;
    @SerializedName("QuestionMore")
    private List<com.intellyze.feedback.Api.AnswerDetails.QuestionMore> mQuestionMore;

    public List<Detail> getDetails() {
        return mDetails;
    }

    public void setDetails(List<Detail> Details) {
        mDetails = Details;
    }

    public List<com.intellyze.feedback.Api.AnswerDetails.QuestionMore> getQuestionMore() {
        return mQuestionMore;
    }

    public void setQuestionMore(List<com.intellyze.feedback.Api.AnswerDetails.QuestionMore> QuestionMore) {
        mQuestionMore = QuestionMore;
    }

}
