package com.intellyze.feedback.Api.Login;

import com.intellyze.feedback.Api.ApiClient;
import com.intellyze.feedback.Api.ApiInterfaces;
import com.intellyze.feedback.Api.SignUp.SignUpRequestModel;
import com.intellyze.feedback.Api.SignUp.SignUpResponseModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by INTELLYZE-202 on 18-12-2017.
 */

public class HttpLogin {
    OnHttpRequestForLogin onHttpRequestForLogin;
    public HttpLogin(OnHttpRequestForLogin onHttpRequestForLogin) {
        this.onHttpRequestForLogin = onHttpRequestForLogin;
    }
    public void Loginn(LoginRequestModel loginRequestModel)
    {
        ApiInterfaces apiInterfaces= ApiClient.getClient().create(ApiInterfaces.class);
        Call<LoginResponseModel> call=apiInterfaces.Login(loginRequestModel);
        call.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {
                if(response.message().equals("OK"))
                {
                    if(!response.body().getError())
                    {
                        onHttpRequestForLogin.onHttpLoginSuccess(response.body().getMessage(),response.body().getResult());
                    }else {
                        onHttpRequestForLogin.onHttpLoginFailed(response.body().getMessage());
                    }
                }
                else
                {
                    onHttpRequestForLogin.onHttpLoginFailed("Server Error !");
                }
            }
            @Override
            public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                onHttpRequestForLogin.onHttpLoginFailed("Server Error !");
            }
        });
    }

}
