
package com.intellyze.feedback.Api.AnswerDetails;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class AnswerDetailModelParameter {

    @SerializedName("Answer_Id")
    private String mAnswerId;
    @SerializedName("survey_Id")
    private String mSurveyId;

    public String getAnswerId() {
        return mAnswerId;
    }

    public void setAnswerId(String AnswerId) {
        mAnswerId = AnswerId;
    }

    public String getSurveyId() {
        return mSurveyId;
    }

    public void setSurveyId(String surveyId) {
        mSurveyId = surveyId;
    }

}
