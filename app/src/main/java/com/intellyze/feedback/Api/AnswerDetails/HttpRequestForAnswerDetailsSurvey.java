package com.intellyze.feedback.Api.AnswerDetails;

import android.util.Log;

import com.intellyze.feedback.Api.ApiClient;
import com.intellyze.feedback.Api.ApiInterfaces;
import com.intellyze.feedback.Api.ListSurveyAnswerAdmin.OnHttpRequestForListSurveyAnswerSurvey;
import com.intellyze.feedback.Api.ListSurveyAnswerAdmin.SurveyAnswerAdminListBean;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by INTELLYZE-202 on 05-01-2018.
 */

public class HttpRequestForAnswerDetailsSurvey {
    OnHttpRequestForSurveyAnswerDetaisSurvey onHttpRequestForAddMultipleAnswerSurvey;

    public HttpRequestForAnswerDetailsSurvey(OnHttpRequestForSurveyAnswerDetaisSurvey onHttpRequestForAddMultipleAnswerSurvey) {
        this.onHttpRequestForAddMultipleAnswerSurvey = onHttpRequestForAddMultipleAnswerSurvey;
    }

    public void getAnswerDetails(AnswerDetailModelParameter addMultipleSurveyRequestModel) {

        ApiInterfaces apiInterfaces = ApiClient.getClient().create(ApiInterfaces.class);
        Call<SurveyAnswerModel> call = apiInterfaces.Get_Answers(addMultipleSurveyRequestModel);
        call.enqueue(new Callback<SurveyAnswerModel>() {
            @Override
            public void onResponse(Call<SurveyAnswerModel> call, Response<SurveyAnswerModel> response) {
                if (response.message().equals("OK")) {
                    if (!response.body().getError()) {
                        onHttpRequestForAddMultipleAnswerSurvey.onHttpAddForSurveyAnswerDetaisSurveySuccess(response.body().getSurveyData(),response.body().getMessage());
                    } else {
                        onHttpRequestForAddMultipleAnswerSurvey.onHttpAddFoListSurveyAnswerDetaisSurveyFailed(response.body().getMessage());
                    }
                } else {
                    onHttpRequestForAddMultipleAnswerSurvey.onHttpAddFoListSurveyAnswerDetaisSurveyFailed("Server Error !");
                }
            }

            @Override
            public void onFailure(Call<SurveyAnswerModel> call, Throwable t) {
                onHttpRequestForAddMultipleAnswerSurvey.onHttpAddFoListSurveyAnswerDetaisSurveyFailed("Server Error !");
            }
        });
    }
}
