
package com.intellyze.feedback.Api.AddAnswerMultipleSurveys;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Answer {

    @SerializedName("age")
    private String mAge;
    @SerializedName("answer_date")
    private String mAnswerDate;
    @SerializedName("answer_time")
    private String mAnswerTime;
    @SerializedName("AnswersList")
    private List<com.intellyze.feedback.Api.AddAnswerMultipleSurveys.AnswersList> mAnswersList;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("Full_name")
    private String mFullName;
    @SerializedName("gender")
    private String mGender;
    @SerializedName("mobile_no")
    private String mMobileNo;
    @SerializedName("profession")
    private String mProfession;
    @SerializedName("Survey_Id")
    private Long mSurveyId;

    public String getAge() {
        return mAge;
    }

    public void setAge(String age) {
        mAge = age;
    }

    public String getAnswerDate() {
        return mAnswerDate;
    }

    public void setAnswerDate(String answerDate) {
        mAnswerDate = answerDate;
    }

    public String getAnswerTime() {
        return mAnswerTime;
    }

    public void setAnswerTime(String answerTime) {
        mAnswerTime = answerTime;
    }

    public List<com.intellyze.feedback.Api.AddAnswerMultipleSurveys.AnswersList> getAnswersList() {
        return mAnswersList;
    }

    public void setAnswersList(List<com.intellyze.feedback.Api.AddAnswerMultipleSurveys.AnswersList> AnswersList) {
        mAnswersList = AnswersList;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String FullName) {
        mFullName = FullName;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mobileNo) {
        mMobileNo = mobileNo;
    }

    public String getProfession() {
        return mProfession;
    }

    public void setProfession(String profession) {
        mProfession = profession;
    }

    public Long getSurveyId() {
        return mSurveyId;
    }

    public void setSurveyId(Long SurveyId) {
        mSurveyId = SurveyId;
    }

}
