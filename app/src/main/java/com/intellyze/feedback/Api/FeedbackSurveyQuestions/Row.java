
package com.intellyze.feedback.Api.FeedbackSurveyQuestions;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Row {

    @SerializedName("Qmultiple_grid_row_Id")
    private String mQmultipleGridRowId;
    @SerializedName("Question_Id")
    private String mQuestionId;
    @SerializedName("row_Title")
    private String mRowTitle;

    public String getQmultipleGridRowId() {
        return mQmultipleGridRowId;
    }

    public void setQmultipleGridRowId(String QmultipleGridRowId) {
        mQmultipleGridRowId = QmultipleGridRowId;
    }

    public String getQuestionId() {
        return mQuestionId;
    }

    public void setQuestionId(String QuestionId) {
        mQuestionId = QuestionId;
    }

    public String getRowTitle() {
        return mRowTitle;
    }

    public void setRowTitle(String rowTitle) {
        mRowTitle = rowTitle;
    }

}
