
package com.intellyze.feedback.Api.SignUp;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class InputData {

    @SerializedName("business_address")
    private String mBusinessAddress;
    @SerializedName("business_Category")
    private String mBusinessCategory;
    @SerializedName("business_Customer_CareNo")
    private String mBusinessCustomerCareNo;
    @SerializedName("business_Email")
    private String mBusinessEmail;
    @SerializedName("business_LandLine")
    private String mBusinessLandLine;
    @SerializedName("business_LogoURL")
    private String mBusinessLogoURL;
    @SerializedName("business_MobileNo")
    private String mBusinessMobileNo;
    @SerializedName("business_name")
    private String mBusinessName;
    @SerializedName("business_OauthKey")
    private String mBusinessOauthKey;
    @SerializedName("business_Website")
    private String mBusinessWebsite;
    @SerializedName("u_password")
    private String mUPassword;

    public String getBusinessAddress() {
        return mBusinessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        mBusinessAddress = businessAddress;
    }

    public String getBusinessCategory() {
        return mBusinessCategory;
    }

    public void setBusinessCategory(String businessCategory) {
        mBusinessCategory = businessCategory;
    }

    public String getBusinessCustomerCareNo() {
        return mBusinessCustomerCareNo;
    }

    public void setBusinessCustomerCareNo(String businessCustomerCareNo) {
        mBusinessCustomerCareNo = businessCustomerCareNo;
    }

    public String getBusinessEmail() {
        return mBusinessEmail;
    }

    public void setBusinessEmail(String businessEmail) {
        mBusinessEmail = businessEmail;
    }

    public String getBusinessLandLine() {
        return mBusinessLandLine;
    }

    public void setBusinessLandLine(String businessLandLine) {
        mBusinessLandLine = businessLandLine;
    }

    public String getBusinessLogoURL() {
        return mBusinessLogoURL;
    }

    public void setBusinessLogoURL(String businessLogoURL) {
        mBusinessLogoURL = businessLogoURL;
    }

    public String getBusinessMobileNo() {
        return mBusinessMobileNo;
    }

    public void setBusinessMobileNo(String businessMobileNo) {
        mBusinessMobileNo = businessMobileNo;
    }

    public String getBusinessName() {
        return mBusinessName;
    }

    public void setBusinessName(String businessName) {
        mBusinessName = businessName;
    }

    public String getBusinessOauthKey() {
        return mBusinessOauthKey;
    }

    public void setBusinessOauthKey(String businessOauthKey) {
        mBusinessOauthKey = businessOauthKey;
    }

    public String getBusinessWebsite() {
        return mBusinessWebsite;
    }

    public void setBusinessWebsite(String businessWebsite) {
        mBusinessWebsite = businessWebsite;
    }

    public String getUPassword() {
        return mUPassword;
    }

    public void setUPassword(String uPassword) {
        mUPassword = uPassword;
    }

}
