package com.intellyze.feedback.Realm;

import io.realm.RealmObject;

/**
 * Created by INTELLYZE-202 on 08-12-2017.
 */

public class rowDetails extends RealmObject {
    private int Qmultiple_grid_row_Id;
    private String row_name;

    public String getRow_name() {
        return row_name;
    }

    public void setRow_name(String row_name) {
        this.row_name = row_name;
    }

    public int getQmultiple_grid_row_Id() {

        return Qmultiple_grid_row_Id;
    }

    public void setQmultiple_grid_row_Id(int qmultiple_grid_row_Id) {
        Qmultiple_grid_row_Id = qmultiple_grid_row_Id;
    }
}
