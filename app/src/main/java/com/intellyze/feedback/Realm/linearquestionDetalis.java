package com.intellyze.feedback.Realm;

import io.realm.RealmObject;

/**
 * Created by INTELLYZE-202 on 08-12-2017.
 */

public class linearquestionDetalis extends RealmObject {
    private int Qlinear_Id;
    private String startText;
    private String endText;
    private int startValue;
    private int endValue;

    public int getQlinear_Id() {
        return Qlinear_Id;
    }

    public void setQlinear_Id(int qlinear_Id) {
        Qlinear_Id = qlinear_Id;
    }

    public String getStartText() {
        return startText;
    }

    public void setStartText(String startText) {
        this.startText = startText;
    }

    public String getEndText() {
        return endText;
    }

    public void setEndText(String endText) {
        this.endText = endText;
    }

    public int getStartValue() {
        return startValue;
    }

    public void setStartValue(int startValue) {
        this.startValue = startValue;
    }

    public int getEndValue() {
        return endValue;
    }

    public void setEndValue(int endValue) {
        this.endValue = endValue;
    }
}
