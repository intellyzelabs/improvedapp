
package com.intellyze.feedback.Realm.FeedbackQuestionModel;

import io.realm.RealmList;
import io.realm.RealmObject;

public class MultipleChoiceGrid extends RealmObject {

    private RealmList<Column> mColumn;
    private RealmList<Row> mRow;

    public RealmList<Column> getColumn() {
        return mColumn;
    }

    public void setColumn(RealmList<Column> column) {
        mColumn = column;
    }

    public RealmList<Row> getRow() {
        return mRow;
    }

    public void setRow(RealmList<Row> row) {
        mRow = row;
    }

}
