
package com.intellyze.feedback.Realm.FeedbackQuestionModel;


import io.realm.RealmList;
import io.realm.RealmObject;

public class FeedQuestUserSide extends RealmObject {

    private String mSStatus;
    private String mSurveyDate;
    private String mSurveyDescr;
    private String mSurveyId;
    private String mSurveyImage;
    private RealmList<SurveyQuestion> mSurveyQuestions;
    private String mSurveyTime;
    private String mSurveyTitle;
    private String mSurveyCount;

    public String getmSurveyCount() {
        return mSurveyCount;
    }

    public void setmSurveyCount(String mSurveyCount) {
        this.mSurveyCount = mSurveyCount;
    }

    public String getmSurveyImage() {
        return mSurveyImage;
    }

    public void setmSurveyImage(String mSurveyImage) {
        this.mSurveyImage = mSurveyImage;
    }

    public String getSStatus() {
        return mSStatus;
    }

    public void setSStatus(String sStatus) {
        mSStatus = sStatus;
    }

    public String getSurveyDate() {
        return mSurveyDate;
    }

    public void setSurveyDate(String surveyDate) {
        mSurveyDate = surveyDate;
    }

    public String getSurveyDescr() {
        return mSurveyDescr;
    }

    public void setSurveyDescr(String surveyDescr) {
        mSurveyDescr = surveyDescr;
    }

    public String getSurveyId() {
        return mSurveyId;
    }

    public void setSurveyId(String surveyId) {
        mSurveyId = surveyId;
    }

    public RealmList<SurveyQuestion> getSurveyQuestions() {
        return mSurveyQuestions;
    }

    public void setSurveyQuestions(RealmList<SurveyQuestion> SurveyQuestions) {
        mSurveyQuestions = SurveyQuestions;
    }

    public String getSurveyTime() {
        return mSurveyTime;
    }

    public void setSurveyTime(String surveyTime) {
        mSurveyTime = surveyTime;
    }

    public String getSurveyTitle() {
        return mSurveyTitle;
    }

    public void setSurveyTitle(String surveyTitle) {
        mSurveyTitle = surveyTitle;
    }

}
