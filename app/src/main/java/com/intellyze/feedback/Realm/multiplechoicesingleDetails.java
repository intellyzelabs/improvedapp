package com.intellyze.feedback.Realm;

import io.realm.RealmObject;

/**
 * Created by INTELLYZE-202 on 08-12-2017.
 */

public class multiplechoicesingleDetails extends RealmObject {
    private int questions_options_Id;
    private String textChoice;
    private String edited;

    public String getEdited() {
        return edited;
    }

    public void setEdited(String edited) {
        this.edited = edited;
    }

    public int getQuestions_options_Id() {
        return questions_options_Id;
    }

    public void setQuestions_options_Id(int questions_options_Id) {
        this.questions_options_Id = questions_options_Id;
    }

    public String getTextChoice() {
        return textChoice;
    }

    public void setTextChoice(String textChoice) {
        this.textChoice = textChoice;
    }
}
