
package com.intellyze.feedback.Realm.FeedbackQuestionModel;

import io.realm.RealmObject;

public class Linear extends RealmObject{

    private String mEndText;
    private String mEndValue;
    private String mQlinearId;
    private String mQuestionId;
    private String mStartText;
    private String mStartValue;

    public String getEndText() {
        return mEndText;
    }

    public void setEndText(String endText) {
        mEndText = endText;
    }

    public String getEndValue() {
        return mEndValue;
    }

    public void setEndValue(String endValue) {
        mEndValue = endValue;
    }

    public String getQlinearId() {
        return mQlinearId;
    }

    public void setQlinearId(String QlinearId) {
        mQlinearId = QlinearId;
    }

    public String getQuestionId() {
        return mQuestionId;
    }

    public void setQuestionId(String QuestionId) {
        mQuestionId = QuestionId;
    }

    public String getStartText() {
        return mStartText;
    }

    public void setStartText(String startText) {
        mStartText = startText;
    }

    public String getStartValue() {
        return mStartValue;
    }

    public void setStartValue(String startValue) {
        mStartValue = startValue;
    }

}
