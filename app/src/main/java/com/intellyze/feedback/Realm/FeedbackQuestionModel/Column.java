
package com.intellyze.feedback.Realm.FeedbackQuestionModel;

import io.realm.RealmObject;

public class Column extends RealmObject{

    private String mColTitle;
    private String mQmultipleGridColId;
    private String mQuestionId;

    public String getColTitle() {
        return mColTitle;
    }

    public void setColTitle(String colTitle) {
        mColTitle = colTitle;
    }

    public String getQmultipleGridColId() {
        return mQmultipleGridColId;
    }

    public void setQmultipleGridColId(String QmultipleGridColId) {
        mQmultipleGridColId = QmultipleGridColId;
    }

    public String getQuestionId() {
        return mQuestionId;
    }

    public void setQuestionId(String QuestionId) {
        mQuestionId = QuestionId;
    }

}
