package com.intellyze.feedback.Realm;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by INTELLYZE-202 on 08-12-2017.
 */

public class QuestionMoreDetails extends RealmObject {
    private String choiceType;
    private RealmList<multiplechoicesingleDetails> multiplechoicesingleDetails;
    private RealmList<linearquestionDetalis> linearquestionDetalis;
    private RealmList<matrixquestionDetails> matrixquestionDetails;

    public String getChoiceType() {
        return choiceType;
    }

    public void setChoiceType(String choiceType) {
        this.choiceType = choiceType;
    }

    public RealmList<com.intellyze.feedback.Realm.multiplechoicesingleDetails> getMultiplechoicesingleDetails() {
        return multiplechoicesingleDetails;
    }

    public void setMultiplechoicesingleDetails(RealmList<com.intellyze.feedback.Realm.multiplechoicesingleDetails> multiplechoicesingleDetails) {
        this.multiplechoicesingleDetails = multiplechoicesingleDetails;
    }

    public RealmList<com.intellyze.feedback.Realm.linearquestionDetalis> getLinearquestionDetalis() {
        return linearquestionDetalis;
    }

    public void setLinearquestionDetalis(RealmList<com.intellyze.feedback.Realm.linearquestionDetalis> linearquestionDetalis) {
        this.linearquestionDetalis = linearquestionDetalis;
    }

    public RealmList<com.intellyze.feedback.Realm.matrixquestionDetails> getMatrixquestionDetails() {
        return matrixquestionDetails;
    }

    public void setMatrixquestionDetails(RealmList<com.intellyze.feedback.Realm.matrixquestionDetails> matrixquestionDetails) {
        this.matrixquestionDetails = matrixquestionDetails;
    }
}
