
package com.intellyze.feedback.Realm.FeedbackQuestionModel;

import io.realm.RealmObject;

public class SurveyQuestion extends RealmObject{
    private String mIsRequired;
    private String mQuestionId;
    private String mQuestionImage;
    private int isOther;
    private com.intellyze.feedback.Realm.FeedbackQuestionModel.QuestionMore mQuestionMore;
    private String mQuestionNumber;
    private String mQuestionTitle;
    private String mQuestionType;
    private String mSurveyId;

    public int getIsOther() {
        return isOther;
    }

    public void setIsOther(int isOther) {
        this.isOther = isOther;
    }

    public String getIsRequired() {
        return mIsRequired;
    }

    public void setIsRequired(String isRequired) {
        mIsRequired = isRequired;
    }

    public String getQuestionId() {
        return mQuestionId;
    }

    public void setQuestionId(String QuestionId) {
        mQuestionId = QuestionId;
    }

    public String getQuestionImage() {
        return mQuestionImage;
    }

    public void setQuestionImage(String QuestionImage) {
        mQuestionImage = QuestionImage;
    }

    public com.intellyze.feedback.Realm.FeedbackQuestionModel.QuestionMore getQuestionMore() {
        return mQuestionMore;
    }

    public void setQuestionMore(com.intellyze.feedback.Realm.FeedbackQuestionModel.QuestionMore QuestionMore) {
        mQuestionMore = QuestionMore;
    }

    public String getQuestionNumber() {
        return mQuestionNumber;
    }

    public void setQuestionNumber(String QuestionNumber) {
        mQuestionNumber = QuestionNumber;
    }

    public String getQuestionTitle() {
        return mQuestionTitle;
    }

    public void setQuestionTitle(String QuestionTitle) {
        mQuestionTitle = QuestionTitle;
    }

    public String getQuestionType() {
        return mQuestionType;
    }

    public void setQuestionType(String QuestionType) {
        mQuestionType = QuestionType;
    }

    public String getSurveyId() {
        return mSurveyId;
    }

    public void setSurveyId(String surveyId) {
        mSurveyId = surveyId;
    }

}
