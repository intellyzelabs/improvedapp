package com.intellyze.feedback.Realm;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by INTELLYZE-202 on 08-12-2017.
 */

public class SurveyQuestionsDetails extends RealmObject {
    private int Question_Id;
    private int QuestionNumber;
    private int isOther;
    private String QuestionType;
    private String QuestionName;
    private int QuestionRequired;
    private String QuestionImage;
    private String added_flag;
    private String edit_flag;

    public String getEdit_flag() {
        return edit_flag;
    }

    public void setEdit_flag(String edit_flag) {
        this.edit_flag = edit_flag;
    }

    public String getAdded_flag() {
        return added_flag;
    }

    public void setAdded_flag(String added_flag) {
        this.added_flag = added_flag;
    }

    private RealmList<QuestionMoreDetails> questionMoreDetails;

    public int getIsOther() {
        return isOther;
    }

    public void setIsOther(int isOther) {
        this.isOther = isOther;
    }

    public int getQuestion_Id() {
        return Question_Id;
    }

    public void setQuestion_Id(int question_Id) {
        Question_Id = question_Id;
    }

    public int getQuestionNumber() {
        return QuestionNumber;
    }

    public void setQuestionNumber(int questionNumber) {
        QuestionNumber = questionNumber;
    }

    public String getQuestionType() {
        return QuestionType;
    }

    public void setQuestionType(String questionType) {
        QuestionType = questionType;
    }

    public String getQuestionName() {
        return QuestionName;
    }

    public void setQuestionName(String questionName) {
        QuestionName = questionName;
    }

    public int getQuestionRequired() {
        return QuestionRequired;
    }

    public void setQuestionRequired(int questionRequired) {
        QuestionRequired = questionRequired;
    }

    public String getQuestionImage() {
        return QuestionImage;
    }

    public void setQuestionImage(String questionImage) {
        QuestionImage = questionImage;
    }

    public RealmList<QuestionMoreDetails> getQuestionMoreDetails() {
        return questionMoreDetails;
    }

    public void setQuestionMoreDetails(RealmList<QuestionMoreDetails> questionMoreDetails) {
        this.questionMoreDetails = questionMoreDetails;
    }
}
