package com.intellyze.feedback.Realm;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by INTELLYZE-202 on 08-12-2017.
 */

public class SurveyDetails extends RealmObject {

    private String SurveyId;
    private String SurveyName;
    private int SurveyComplete;
    private int SurveySync;
    private int count;
    private String LastEntryDate;
    private String LastEntryTime;
    private String SurveyDate;
    private String SurveyTime;
    private String SurveyStatus;

    public String getEditflag() {
        return editflag;
    }

    public void setEditflag(String editflag) {
        this.editflag = editflag;
    }

    private String SurveyDescription;
    private String SurveyImage;
    private String editflag;
    private RealmList<SurveyQuestionsDetails> surveyQuestionsDetails;
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getLastEntryDate() {
        return LastEntryDate;
    }

    public void setLastEntryDate(String lastEntryDate) {
        LastEntryDate = lastEntryDate;
    }

    public String getLastEntryTime() {
        return LastEntryTime;
    }

    public void setLastEntryTime(String lastEntryTime) {
        LastEntryTime = lastEntryTime;
    }

    public int getSurveySync() {
        return SurveySync;
    }

    public void setSurveySync(int surveySync) {
        SurveySync = surveySync;
    }

    public int getSurveyComplete() {
        return SurveyComplete;
    }

    public void setSurveyComplete(int surveyComplete) {
        SurveyComplete = surveyComplete;
    }

    public String getSurveyDescription() {
        return SurveyDescription;
    }

    public void setSurveyDescription(String surveyDescription) {
        SurveyDescription = surveyDescription;
    }

    public String getSurveyImage() {
        return SurveyImage;
    }

    public void setSurveyImage(String surveyImage) {
        SurveyImage = surveyImage;
    }



    public String getSourveyId() {
        return SurveyId;
    }

    public void setSurveyId(String surveyId) {
        SurveyId = surveyId;
    }

    public String getSurveyName() {
        return SurveyName;
    }

    public void setSurveyName(String surveyName) {
        SurveyName = surveyName;
    }

    public String getSurveyDate() {
        return SurveyDate;
    }

    public void setSurveyDate(String surveyDate) {
        SurveyDate = surveyDate;
    }

    public String getSurveyTime() {
        return SurveyTime;
    }

    public void setSurveyTime(String surveyTime) {
        SurveyTime = surveyTime;
    }

    public String getSurveyStatus() {
        return SurveyStatus;
    }

    public void setSurveyStatus(String surveyStatus) {
        SurveyStatus = surveyStatus;
    }

    public RealmList<SurveyQuestionsDetails> getSurveyQuestionsDetails() {
        return surveyQuestionsDetails;
    }

    public void setSurveyQuestionsDetails(RealmList<SurveyQuestionsDetails> surveyQuestionsDetails) {
        this.surveyQuestionsDetails = surveyQuestionsDetails;
    }
}
