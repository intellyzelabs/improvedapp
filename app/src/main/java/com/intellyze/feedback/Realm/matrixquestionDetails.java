package com.intellyze.feedback.Realm;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by INTELLYZE-202 on 08-12-2017.
 */

public class matrixquestionDetails extends RealmObject {
    private RealmList<rowDetails> rowDetails;
    private RealmList<columnDetails> columnDetails;

    public RealmList<com.intellyze.feedback.Realm.rowDetails> getRowDetails() {
        return rowDetails;
    }

    public void setRowDetails(RealmList<com.intellyze.feedback.Realm.rowDetails> rowDetails) {
        this.rowDetails = rowDetails;
    }

    public RealmList<com.intellyze.feedback.Realm.columnDetails> getColumnDetails() {
        return columnDetails;
    }

    public void setColumnDetails(RealmList<com.intellyze.feedback.Realm.columnDetails> columnDetails) {
        this.columnDetails = columnDetails;
    }
}
