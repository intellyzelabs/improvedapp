
package com.intellyze.feedback.Realm.FeedbackQuestionModel;

import io.realm.RealmObject;

public class Row extends RealmObject{

    private String mQmultipleGridRowId;
    private String mQuestionId;
    private String mRowTitle;

    public String getQmultipleGridRowId() {
        return mQmultipleGridRowId;
    }

    public void setQmultipleGridRowId(String QmultipleGridRowId) {
        mQmultipleGridRowId = QmultipleGridRowId;
    }

    public String getQuestionId() {
        return mQuestionId;
    }

    public void setQuestionId(String QuestionId) {
        mQuestionId = QuestionId;
    }

    public String getRowTitle() {
        return mRowTitle;
    }

    public void setRowTitle(String rowTitle) {
        mRowTitle = rowTitle;
    }

}
