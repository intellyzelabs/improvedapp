
package com.intellyze.feedback.Realm.FeedbackQuestionModel;

import io.realm.RealmList;
import io.realm.RealmObject;

public class QuestionMore extends RealmObject{

    private RealmList<Linear> mLinear;
    private RealmList<com.intellyze.feedback.Realm.FeedbackQuestionModel.MultipleChoice> mMultipleChoice;
    private RealmList<com.intellyze.feedback.Realm.FeedbackQuestionModel.MultipleChoiceGrid> mMultipleChoiceGrid;

    public RealmList<com.intellyze.feedback.Realm.FeedbackQuestionModel.Linear> getLinear() {
        return mLinear;
    }

    public void setLinear(RealmList<com.intellyze.feedback.Realm.FeedbackQuestionModel.Linear> Linear) {
        mLinear = Linear;
    }

    public RealmList<com.intellyze.feedback.Realm.FeedbackQuestionModel.MultipleChoice> getMultipleChoice() {
        return mMultipleChoice;
    }

    public void setMultipleChoice(RealmList<com.intellyze.feedback.Realm.FeedbackQuestionModel.MultipleChoice> MultipleChoice) {
        mMultipleChoice = MultipleChoice;
    }

    public RealmList<MultipleChoiceGrid> getmMultipleChoiceGrid() {
        return mMultipleChoiceGrid;
    }

    public void setmMultipleChoiceGrid(RealmList<MultipleChoiceGrid> mMultipleChoiceGrid) {
        this.mMultipleChoiceGrid = mMultipleChoiceGrid;
    }
}
