package com.intellyze.feedback.Realm;

import io.realm.RealmObject;

/**
 * Created by INTELLYZE-202 on 08-12-2017.
 */

public class columnDetails extends RealmObject {
    private int Qmultiple_grid_col_Id;
    private String column_name;

    public int getQmultiple_grid_col_Id() {
        return Qmultiple_grid_col_Id;
    }

    public void setQmultiple_grid_col_Id(int qmultiple_grid_col_Id) {
        Qmultiple_grid_col_Id = qmultiple_grid_col_Id;
    }

    public String getColumn_name() {
        return column_name;
    }

    public void setColumn_name(String column_name) {
        this.column_name = column_name;
    }
}
