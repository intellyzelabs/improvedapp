
package com.intellyze.feedback.Realm;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class AddAnswerMaster extends RealmObject {
    @PrimaryKey
    private int mSurveyUserId;
    private int mSurveyId;
    private int mSurveySync;
    private String mAge;
    private String mAnswerDate;
    private String mAnswerTime;
    private RealmList<AnswersList> mAnswersList;
    private String mEmail;
    private String mFullName;
    private String mGender;
    private String mMobileNo;
    private String mProfession;

    public int getmSurveySync() {
        return mSurveySync;
    }

    public void setmSurveySync(int mSurveySync) {
        this.mSurveySync = mSurveySync;
    }

    public int getmSurveyId() {
        return mSurveyId;
    }

    public void setmSurveyId(int mSurveyId) {
        this.mSurveyId = mSurveyId;
    }

    public int getmSurveyUserId() {
        return mSurveyUserId;
    }

    public void setmSurveyUserId(int mSurveyUserId) {
        this.mSurveyUserId = mSurveyUserId;
    }

    public String getAge() {
        return mAge;
    }

    public void setAge(String age) {
        mAge = age;
    }

    public String getAnswerDate() {
        return mAnswerDate;
    }

    public void setAnswerDate(String answerDate) {
        mAnswerDate = answerDate;
    }

    public String getAnswerTime() {
        return mAnswerTime;
    }

    public void setAnswerTime(String answerTime) {
        mAnswerTime = answerTime;
    }

    public RealmList<AnswersList> getAnswersList() {
        return mAnswersList;
    }

    public void setAnswersList(RealmList<AnswersList> AnswersList) {
        mAnswersList = AnswersList;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getFullName() {
        return mFullName;
    }

    public void setFullName(String FullName) {
        mFullName = FullName;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mobileNo) {
        mMobileNo = mobileNo;
    }

    public String getProfession() {
        return mProfession;
    }

    public void setProfession(String profession) {
        mProfession = profession;
    }


}
