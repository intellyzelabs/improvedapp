
package com.intellyze.feedback.Realm.FeedbackQuestionModel;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

@SuppressWarnings("unused")
public class MultipleChoice extends RealmObject {

    @SerializedName("option_Title")
    private String mOptionTitle;
    @SerializedName("Question_Id")
    private String mQuestionId;
    @SerializedName("questions_options_Id")
    private String mQuestionsOptionsId;

    public String getOptionTitle() {
        return mOptionTitle;
    }

    public void setOptionTitle(String optionTitle) {
        mOptionTitle = optionTitle;
    }

    public String getQuestionId() {
        return mQuestionId;
    }

    public void setQuestionId(String QuestionId) {
        mQuestionId = QuestionId;
    }

    public String getQuestionsOptionsId() {
        return mQuestionsOptionsId;
    }

    public void setQuestionsOptionsId(String questionsOptionsId) {
        mQuestionsOptionsId = questionsOptionsId;
    }

}
