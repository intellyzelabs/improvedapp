
package com.intellyze.feedback.Realm;

import io.realm.RealmObject;

public class AnswersList extends RealmObject{

    private String mAnswrValue;
    private int mAnswrValueId;
    private int mAnswrValueRowId;
    private int mQuestionId;

    public String getAnswrValue() {
        return mAnswrValue;
    }

    public void setAnswrValue(String AnswrValue) {
        mAnswrValue = AnswrValue;
    }

    public int getAnswrValueId() {
        return mAnswrValueId;
    }

    public void setAnswrValueId(int AnswrValueId) {
        mAnswrValueId = AnswrValueId;
    }

    public int getAnswrValueRowId() {
        return mAnswrValueRowId;
    }

    public void setAnswrValueRowId(int AnswrValueRowId) {
        mAnswrValueRowId = AnswrValueRowId;
    }

    public int getQuestionId() {
        return mQuestionId;
    }

    public void setQuestionId(int QuestionId) {
        mQuestionId = QuestionId;
    }

}
